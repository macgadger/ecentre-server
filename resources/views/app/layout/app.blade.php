
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title') | {{ env('APP_NAME', 'Edu CRM') }}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="robots" content="index, follow">
    
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{ asset(config('app.prefix').'components/bootstrap/dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset(config('app.prefix').'components/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset(config('app.prefix').'components/Ionicons/css/ionicons.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset(config('app.prefix').'dist/css/AdminLTE.min.css') }}">
    <link rel="stylesheet" href="{{ asset(config('app.prefix').'dist/css/skins/skin-yellow.min.css') }}">
    <link rel="stylesheet" href="{{ asset(config('app.prefix').'dist/css/styles.css') }}">

    <style>
        li{
            font-size: 16px;
            font-weight: bold;
        }
    </style>

    <!-- Custom styles -->
    @stack('styles')
	
	  <!-- Pace style -->
    <link rel="stylesheet" href="{{ asset(config('app.prefix').'plugins/pace/pace.min.css') }}">
  
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-yellow sidebar-mini fixed">
    
    <!-- Loader -->
    <div id="loader" style="display: none">
        <img src="{{ asset(config('app.prefix').'dist/img/loading.gif') }}" class="ajax-loader"/>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="openModal" role="dialog">
        <div class="modal-dialog modal-lg">
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title" id="mod-title"></h4>
            </div>
            <div class="modal-body">
            </div>
          </div>
        </div>
    </div>

    <div class="wrapper">  
    <!-- Header -->
    @include('app.inc.header')

    <!-- Sidebar -->
    @include('app.inc.sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      
    <!-- Main content -->
    <section class="content">    
        @yield('content')
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

     <!-- Footer -->
    @include('app.inc.footer')
</div><!-- ./wrapper -->

<!-- Core JavaScript -->	 
<!-- jQuery 3 -->
<script src="{{ asset(config('app.prefix').'components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset(config('app.prefix').'components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset(config('app.prefix').'components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
<script src="{{ asset(config('app.prefix').'components/fastclick/lib/fastclick.js') }}"></script>
<script src="{{ asset(config('app.prefix').'components/PACE/pace.min.js') }}"></script>	
<script src="{{ asset(config('app.prefix').'dist/js/adminlte.min.js') }}"></script>
<script src="{{ asset(config('app.prefix').'plugins/validator.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

<!-- Custom scripts -->
@stack('scripts')
<script src="{{ asset(config('app.prefix').'dist/js/app.js') }}"></script>

<script>
  $(document).ready(function () {
    init("@yield('mod_title','Blank Page')");
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
  });
	
  $(document).ajaxStart(function () {
    Pace.restart()
  })
</script>
</body>
</html>
