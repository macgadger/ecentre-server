<?php
  $uriP = explode('/',\Request::path());
?>
   <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{ asset(config('app.prefix').'dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
            @if(!Auth::check())
              <p>Student</p>
            @else
              <p>{{ ucwords(Auth::user()->getProfile->first_name.' '.Auth::user()->getProfile->last_name) }}</p>
            @endif
          <a href="javascript:void(0)"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu">

        <li class="header">Management</li>
        @if(!Auth::check())
          <li class="<?php echo ($uriP[1] == 'dashboard' && isset($uriP[2]) == '')? 'active' : '' ?>"><a href="/student/dashboard"><i class="fa fa-tachometer"></i> <span>Dashboard</span></a></li>

        @else
          <li class="<?php echo ($uriP[1] == 'dashboard' && isset($uriP[2]) == '')? 'active' : '' ?>"><a href="/account/app/dashboard"><i class="fa fa-tachometer"></i> <span>Dashboard</span></a></li>

          @include('app.inc.sidebar'.strtolower('.'.Auth::user()->getRole->name))
        @endif
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>
