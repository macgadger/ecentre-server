<header class="main-header">
  @if(Auth::check())
    <a href="/" class="logo">      
      <span class="logo-mini"><b>E</b>C</span>      
      <span class="logo-lg"><b>{{ env('APP_NAME') }}</b></span>
    </a>
    @else
    <a href="/student/signin" class="logo">      
      <span class="logo-mini"><b>E</b>C</span>      
      <span class="logo-lg"><b>{{ env('APP_NAME') }}</b></span>
    </a>
  @endif
    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">      
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
         @if(Auth::check())
          <!-- Notifications Menu -->
          <li class="dropdown notifications-menu">
            <!-- Menu toggle button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-danger" id="nCount">0</span>
            </a>
            <ul class="dropdown-menu">
              <li>
                <!-- Inner Menu: contains the notifications -->
                <ul class="menu">
                  
                  <li><!-- start notification -->
                    <a href="#">
                      <i class="fa fa-info text-aqua"></i> No None
                    </a>
                  </li>                                    
                  <!-- end notification -->
                </ul>
              </li>
              <li class="footer"><a href="#">View all</a></li>
            </ul>
          </li>
          <!-- Tasks Menu -->
          <!-- User Account Menu -->
          <li class="dropdown user user-menu">
            <!-- Menu Toggle Button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <!-- The user image in the navbar-->
              <img src="{{ asset(config('app.prefix').'dist/img/user2-160x160.jpg') }}" class="user-image" alt="User Image">
              <!-- hidden-xs hides the username on small devices so only the image appears. -->
                <span class="hidden-xs">{{ ucwords(Auth::user()->getProfile->first_name) }}</span>
             
            </a>
            <ul class="dropdown-menu">
              <!-- The user image in the menu -->
              <li class="user-header">
                <img src="{{ asset(config('app.prefix').'dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">                
              </li>
              
              
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="/account/profile" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="/account/signout" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          @else
            <!-- Tasks Menu -->
            <!-- User Account Menu -->
            <li class="dropdown user user-menu">
              <!-- Menu Toggle Button -->
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <!-- The user image in the navbar-->
                <img src="{{ asset(config('app.prefix').'dist/img/user2-160x160.jpg') }}" class="user-image" alt="User Image">
                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                  <span class="hidden-xs">Student</span>
                
              </a>
              <ul class="dropdown-menu">
                <!-- The user image in the menu -->
                <li class="user-header">
                  <img src="{{ asset(config('app.prefix').'dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">                
                </li>
                
                
                <!-- Menu Footer-->
                <li class="user-footer">
                  
                  <div class="pull-right">
                    <a href="/student/signout" class="btn btn-default btn-flat">Sign out</a>
                  </div>
                </li>
              </ul>
            </li>
          @endif
        </ul>
      </div>
    </nav>
  </header>