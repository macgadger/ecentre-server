<!-- Main content -->
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-danger collapsed-box">
                    <div class="box-header with-border">
                        <h2 class="box-title"><b>Quick Search</b></h2>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                                title="Collapse">
                                <i class="fa fa-plus"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <form name="searchForm" id="searchForm" method="POST" action="./search" data-toggle="validator">
                            @csrf
                            <div class="row">
                                    <div class="col-md-6">
                                            <div class="form-group has-feedback">
                                                <label class="control-label">Search Query</label>
                                                <input type="text" class="form-control" name="query" autocomplete="query" placeholder="Enter Search Query (eg. name, phone, reciept id)">
                                                <div class="help-block with-errors"></div>
                                            </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group has-feedback">
                                            <label class="control-label">Date from</label>
                                            <input type="text" class="form-control datepicker" name="date_from" autocomplete="date_from">
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group has-feedback">
                                            <label class="control-label">Date to</label>
                                            <input type="text" class="form-control datepicker" name="date_to" autocomplete="date_to">
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group has-feedback">
                                            <label class="control-label">Type</label>
                                            <select class="form-control" name="search_type" required>
                                                <option value="" selected>--Select Type--</option>
                                                <option value="admissions">Admissions</option>
                                                <option value="students">Students</option>
                                                <option value="inquiry">Inquiry</option>
                                                <option value="employee">Employees</option>
                                                <option value="fee">Fees</option>
                                            </select>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                </div>
                            
                                <div class="row">
                                    <div class="col-md-3">&nbsp;</div>
                                    <div class="col-md-6">
                                        <button type="submit" class="btn btn-block btn-danger">Search</button>
                                    </div>
                                    <div class="col-md-3">&nbsp;</div>
                                </div>
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    
        @push('styles')
            <!-- bootstrap datepicker -->
            <link rel="stylesheet" href="{{ asset('components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
        @endpush

        @push('scripts')
        <!-- bootstrap datepicker -->
        <script src="{{ asset('components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
        <script>
            $(function () {
                //Date picker
                $('.datepicker').datepicker({
                    autoclose: true,
                    format: "dd-mm-yyyy",
                    todayHighlight: true,
                    todayBtn: true,
                    clearBtn: true,
                });
            });
        </script>
        @endpush