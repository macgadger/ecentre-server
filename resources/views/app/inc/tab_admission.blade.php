<table class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>Cousellor Name</th>
            <th>Total</th>
            <th>Joined</th>
            <th>Pending</th>
            <th>Overall</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($counselor_adm as $key => $item)
        <tr>
            <td>{{ $item->profile->full_name }}</td>
            <td>{{ $item->adm_total }}</td>
            <td>{{ $item->adm_joined }}</td>
            <td>{{ $item->adm_pending }}</td>
            <td>{{ ($item->adm_joined != 0 || $item->adm_total != 0) ? round($item->adm_joined*100/$item->adm_total) : 0 }}%</td>
        </tr>
        @endforeach
    </tbody>
</table>