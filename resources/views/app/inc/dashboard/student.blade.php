@if(session('status'))
    <div class="alert alert-success" id="msg-alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close" style="color: white">
            <span aria-hidden="true">&times;</span>
        </button>
        <p style="font-size: 18px"><strong>{{ session('status') }}</strong></p>
    </div>
@endif
<div class="col-md-4">
    <!-- Profile Image -->
    <div class="box box-primary">
      <div class="box-body box-profile">
        <img class="profile-user-img img-responsive img-circle" src="{{asset(config('app.prefix').'dist/img/default-user-icon.jpg')}}" alt="User profile picture">

        <h3 class="profile-username text-center">{{ $admission->full_name }}</h3>

        <ul class="list-group list-group-unbordered">
          <li class="list-group-item">
            <b> Join Date</b> <a class="pull-right">{{ date('d M, Y', strtotime($admission->course_joindate))}}</a>
          </li>
          <li class="list-group-item">
            <b> End Date</b> <a class="pull-right">{{ date('d M, Y', strtotime($admission->course_enddate))}}</a>
          </li>
          <li class="list-group-item">
            <b>Batch Assigned</b> <a class="pull-right">{{ ucwords($admission->batch->title) }}</a>
          </li>
          <li class="list-group-item">
            <b>Batch Timimg</b> <a class="pull-right">{{ date('h:i A', strtotime($admission->batch->start_time)) }} - {{ date('h:i A', strtotime($admission->batch->end_time)) }}</a>
          </li>
          <li class="list-group-item">
            <b>Trainer Name</b> <a class="pull-right">{{ $admission->batch->trainer->full_name }}</a>
          </li>
        </ul>
        <div style="text-align: right">
          @if($certificate)
            <a href="javascript:void(0)" class="btn btn-success"><b>Already Applied</b></a>
          @else
            <a href="javascript:void(0)" onclick="applyCertificate('{{ route('apply.certificate') }}')" class="btn btn-primary @if($admission->status != 'COMPLETED') disabled @endif"><b>Apply Certificate</b></a>
          @endif
          <a href="{{ route('review.trainer') }}" class="btn btn-danger"><b>Review</b></a>
        </div>
      </div>
      <!-- /.box-body -->
    </div>
</div>
<div class="col-md-8">
    <div class="box box-primary">
        <table class="table table-hover table-striped">
        <thead>
            <tr>
                <th>Attendance</th>
                <th>Date</th>
                <th>Syllabus</th>
            </tr>
        </thead>
          <tbody>
            @foreach($attendence['dairy'] as $attn)
            <tr>
                <td>
                  @if($attn['attendance'] == 'present')
                      <span class="label label-success">PRESENT</span>
                  @elseif($attn['attendance'] == 'leave' || $attn['attendance'] == 'holiday')
                      <span class="label label-warning">ON {{ strtoupper($attn['attendance']) }}</span>
                  @else
                      <span class="label label-danger">ABSENT</span>
                  @endif
                </td>
                <td>{{ date('d M, Y @ h:i A', strtotime($attn['date']))}}</td>
                <td>{{ $attn['subject'] }}</td>
            </tr>
            @endforeach

          </tbody>
        </table>
        <!-- /.table -->
    </div>
</div>
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset(config('app.prefix').'components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">

<style>
  th { font-size: 16px; }
  td { font-size: 18px; }
  .big{
    font-size: 12px;
    font-weight: bold;
  }
</style>
<!-- DataTables -->
<script src="{{ asset(config('app.prefix').'components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset(config('app.prefix').'components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>

<script>
$(function () {
    $('#data-table').DataTable({
        "aaSorting": [],
    });
});
</script>
