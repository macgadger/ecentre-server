<div class="col-md-4">
        <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-bell"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">Batches</span>
                <span class="info-box-number">{{ $batches }}</span>
            </div>
        </div>
</div>

<div class="col-md-4">
    <div class="info-box">
        <span class="info-box-icon bg-red"><i class="fa fa-users"></i></span>
        <div class="info-box-content">
            <span class="info-box-text">Total Students</span>
            <span class="info-box-number">
            @isset($batch['admissions'])
                {{ $batch['admissions']->count() }}
            @else
                0
            @endisset
            </span>
        </div>
    </div>
</div>

<div class="col-md-4">
    <div class="info-box">
        <span class="info-box-icon bg-green"><i class="fa fa-star"></i></span>
        <div class="info-box-content">
            <span class="info-box-text">Your Rating</span>
            <span class="info-box-number">{{ $overall_rating}} out of 5</span>
        </div>
    </div>
</div>

<div class="col-md-12">
        <div class="info-box" style="padding: 10px;">
        <h3>Pending Attendance</h3>
        <table id="data-table" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Batch</th>
                        <th>Batch Timings</th>
                        <th>Total Students</th>
                        <th>Attendance Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($attendance[]->dairy as $item)
                    <tr>
                        <td>{{ ucwords($item->batch->title) }}</td>
                        <td>{{ date('h:i A',strtotime($item->batch->start_time)) }} - {{ date('h:i A',strtotime($item->batch->end_time)) }}</td>
                        <td>{{ collect(json_decode($item->log))->count() }}</td>
                        <td></td>
                        <td></td>
                    </tr>
                    @endforeach
                </tbody>

            </table>
        </div>
</div>
