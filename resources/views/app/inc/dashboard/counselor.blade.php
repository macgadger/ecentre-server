<div class="col-md-3">
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3>{{ (!empty($counselor_adm[0]->adm_total)) ? $counselor_adm[0]->adm_total : 0  }}</h3>
                <p>Total Admission(s)</p>
            </div>
            <div class="icon"><i class="fa fa-calendar-check-o"></i></div>
            <a href="javascript:void(0)" class="small-box-footer">
                More info <i class="fa fa-arrow-circle-right"></i>
            </a>
            {{-- <a href="{{ route('admission.filter',['type' => 'month']) }}" class="small-box-footer">
                More info <i class="fa fa-arrow-circle-right"></i>
            </a> --}}
        </div>
</div>

{{-- <div class="col-md-3">
    <div class="small-box bg-green">
        <div class="inner">
            <h3>{{ (!empty($adm_joined)) ? $adm_joined : 0  }}</h3>
            <p>Admission(s) Joined</p>
        </div>
        <div class="icon"><i class="fa fa-calendar-check-o"></i></div>
        <a href="{{ route('admission.filter',['type' => 'joined']) }}" class="small-box-footer">
            More info <i class="fa fa-arrow-circle-right"></i>
        </a>
    </div>
</div> --}}

<div class="col-md-3">
    <div class="small-box bg-green">
        <div class="inner">
            <h3>{{ (!empty($adm_pending)) ? $adm_pending : 0 }}</h3>
            <p>Admission(s) Pending</p>
        </div>
        <div class="icon"><i class="fa fa-calendar-minus-o"></i></div>
        <a href="{{ route('admission.filter',['type' => 'pending']) }}" class="small-box-footer">
            More info <i class="fa fa-arrow-circle-right"></i>
        </a>
    </div>
</div>

<div class="col-md-3">
    <div class="small-box bg-yellow">
        <div class="inner">
            <h3>{{ (!empty($adm_expired)) ? $adm_expired : 0 }}</h3>
            <p>Admission(s) Expired</p>
        </div>
        <div class="icon"><i class="fa fa-calendar-times-o"></i></div>
        <a href="{{ route('admission.filter',['type' => 'expired']) }}" class="small-box-footer">
            More info <i class="fa fa-arrow-circle-right"></i>
        </a>
    </div>
</div>

<div class="col-md-3">
    <div class="small-box bg-red">
        <div class="inner">
            <h3>{{ (!empty($adm_drop)) ? $adm_drop : 0 }}</h3>
            <p>Admission(s) Drop Out</p>
        </div>
        <div class="icon"><i class="fa fa-calendar-times-o"></i></div>
        <a href="{{ route('admission.filter',['type' => 'dropout']) }}" class="small-box-footer">
            More info <i class="fa fa-arrow-circle-right"></i>
        </a>
    </div>
</div>

<div class="col-md-12">
    <div class="small-box bg-blue">
        <div class="inner">
            <h3><i class="fa fa-rupee"></i>{{ (!empty($counselor_fee[0]->fee_pending)) ? $counselor_fee[0]->fee_pending : 0 }}</h3>
            <p>Fee Pending</p>
        </div>
        <div class="icon"><i class="fa fa-clock-o"></i></div>
        <a href="{{ route('counsellor.fees')}}" class="small-box-footer">
            More info <i class="fa fa-arrow-circle-right"></i>
        </a>
    </div>
</div>
