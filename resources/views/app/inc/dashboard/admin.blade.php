@component('app.components.filter', ['title' => 'Dashboard Filter', 'type' => 'dashboard' , 'open' => true , 'search' => false])@endcomponent

<div class="col-md-4">
        <a href="{{ route('admission.filter',['type' => 'pending']) }}" style="color: black">
            <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="fa fa-user"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Admissions Pending</span>
                    <span class="info-box-number">{{ $adm_pending }}</span>
                </div>
            </div>
        </a>
</div>
<!-- /.col -->

    <div class="col-md-4">
        <a href="{{ route('admission.filter',['type' => 'expired']) }}" style="color: black">
            <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa-users"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Admissions Expired</span>
                    <span class="info-box-number">{{ $adm_expired }}</span>
                </div>
            </div>
        </a>
    </div>
    <!-- /.col -->

    <div class="col-md-4">
        <a href="{{ route('admission.filter',['type' => 'joined']) }}" style="color: black">
            <div class="info-box">
                <span class="info-box-icon bg-green"><i class="fa fa-user-plus"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Admissions Joined</span>
                    <span class="info-box-number">{{  $students }}</span>
                </div>
            </div>
        </a>
    </div>
    <!-- /.col -->

</div>
<!-- /.row -->

<div class="row">

    <div class="col-md-4">
        <a href="{{ route('fee.filter',['type' => 'collection']) }}" style="color: black">
            <div class="info-box">
                <span class="info-box-icon bg-green"><i class="fa fa-rupee"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Today Fee Collection</span>
                    <span class="info-box-number"><i class="fa fa-rupee"></i> {{ $fee_collect }}</span>
                </div>
            </div>
        </a>
    </div>
    <!-- /.col -->

    <div class="col-md-4">
        <a href="{{ route('fee.filter',['type' => 'today']) }}" style="color: black">
            <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa-money"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Fee Due Today</span>
                    <span class="info-box-number"><i class="fa fa-rupee"></i> {{ $fee_due_sum }} of {{ $fee_dues }}
                        Student(s)</span>
                </div>
            </div>
        </a>
    </div>
    <!-- /.col -->

    <div class="col-md-4">
        <a href="{{ route('fee.filter',['type' => 'overdue']) }}" style="color: black">
            <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="fa fa-credit-card"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Fee Over Due</span>
                    <span class="info-box-number"><i class="fa fa-rupee"></i> {{ $fee_expired_sum }} of
                        {{ $fee_expired }} Student(s)</span>
                </div>
            </div>
        </a>
    </div>
    <!-- /.col -->

</div>
</section>

<section class="content">
<div class="row">
    <div class="col-xs-12">
        <div class="box box-danger">
            <div class="box-header with-border">
                <h2 class="box-title"><b>Cousellors Reporting</b></h2>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <!-- Custom Tabs -->
                <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab_admission" data-toggle="tab"><i class="fa fa-university"></i> Admissions</a></li>
                            <li><a href="#tab_fees" data-toggle="tab"><i class="fa fa-rupee"></i> Fees</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_admission">
                                @include('app.inc.tab_admission')
                            </div>
                            <div class="tab-pane" id="tab_fees">
                                @include('app.inc.tab_fees')
                            </div>
                        </div>
                </div>

            </div>
        </div>
    </div>
</div>

@push('styles')
<style>
	.info-box-text{
		padding-bottom: 5px;
		font-size: 20px;
		border-bottom: 2px dotted black;
	}

	.info-box-number{
		padding-top: 5px;
	}
</style>
@endpush
