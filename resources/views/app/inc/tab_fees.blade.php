<table class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>Name</th>
            <th>Total</th>
            <th>Collected</th>
            <th>Pending</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($counselor_fee as $key => $item)
        <tr>
            <td>{{ $item->profile->full_name }}</td>
            <td><i class="fa fa-rupee"></i>{{ $item->fee_total}}</td>
            <td><i class="fa fa-rupee"></i>{{ $item->payments->sum('amount')  }}</td>
            <td><i class="fa fa-rupee"></i>{{ $item->fee_total-$item->payments->sum('amount') }}</td>
        </tr>
        @endforeach
    </tbody>
</table>