<li class="<?php if(isset($uriP[1])){ echo ($uriP[2] == 'my-batch')? 'active' : ''; }?>"><a href="{{ route('batch.trainer') }}""><i class="fa fa-bell"></i> <span>My Batches</span></a></li>

<li class="treeview <?php if(isset($uriP[1])){ echo ($uriP[2] == 'attendance')? 'treeview active' : ''; }?>">
        <a href="#">
          <i class="fa fa-calendar-check-o"></i>
          <span>Attendance</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
            <li class="@if(url()->current() == route('attendance.today',['filter' => 'today']))  active @endif"><a href="/account/app/attendance/my/today"><i class="fa fa-circle-o"></i> Today</a></li>
            <li class="@if(url()->current() == route('attendance.index'))  active @endif"><a href="/account/app/attendance"><i class="fa fa-circle-o"></i> All</a></li>
        </ul>
    </li>
