<li class="treeview <?php if(isset($uriP[1])){ echo ($uriP[2] == 'admission')? 'treeview active' : ''; }?>">
    <a href="#">
      <i class="fa fa-university"></i>
      <span>Admissions</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li class="@if(url()->current() == route('admission.index').'/filter/today')  active @endif"><a href="{{ route('admission.index') }}/filter/today"><i class="fa fa-circle-o"></i> All Admissions</a></li>
      <li class="@if(url()->current() == route('admission.create')) active @endif"><a href="/account/app/admission/create"><i class="fa fa-circle-o"></i> New Admission</a></li>
    </ul>
</li>

<li class="treeview <?php if(isset($uriP[1])){ echo ($uriP[2] == 'fee')? 'treeview active' : ''; }?>">
    <a href="#">
      <i class="fa fa-rupee"></i>
      <span>Fees</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li><a href="/account/app/fee"><i class="fa fa-circle-o"></i> All Fees</a></li>
      <li><a href="/account/app/fee/search"><i class="fa fa-circle-o"></i> Pay Fee</a></li>
    </ul>
</li>

<li class="treeview <?php if(isset($uriP[1])){ echo ($uriP[2] == 'batch' || $uriP[2] == 'batch-category' || $uriP[2] == 'batch-assigned')? 'treeview active' : ''; }?>">
    <a href="#">
      <i class="fa fa-bell-o"></i>
      <span>Batches</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
        <li class="treeview <?php if(isset($uriP[1])){ echo ($uriP[2] == 'batch-category')? 'treeview active' : ''; }?>">
            <a href="#"><i class="fa fa-circle-o"></i> Batch Categories
                <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li class="@if(url()->current() == route('batch-category.index'))  active @endif"><a href="{{ route('batch-category.index') }}"><i class="fa fa-circle-o"></i> All Categorie(s)</a></li>
                <li class="@if(url()->current() == route('batch-category.create'))  active @endif"><a href="{{ route('batch-category.create') }}"><i class="fa fa-circle-o"></i> New Category</a></li>
            </ul>
        </li>

        <li class="treeview <?php if(isset($uriP[1])){ echo ($uriP[2] == 'batch')? 'treeview active' : ''; }?>">
            <a href="#"><i class="fa fa-circle-o"></i> Batches
                <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li class="@if(url()->current() == route('batch.index'))  active @endif"><a href="{{ route('batch.index') }}"><i class="fa fa-circle-o"></i> All Batches</a></li>
                <li class="@if(url()->current() == route('batch.create')) active @endif"><a href="{{ route('batch.create') }}"><i class="fa fa-circle-o"></i> New Batch</a></li>
            </ul>
        </li>
        <li class="@if(url()->current() == route('batch.assigned',['filter' => 'all'] ))  active @endif"><a href="{{ route('batch.assigned',['filter' => 'all']) }}"><i class="fa fa-circle-o"></i> Assigned Batches</a></li>
    </ul>
</li>

<li class="treeview <?php if(isset($uriP[1])){ echo ($uriP[2] == 'attendance')? 'treeview active' : ''; }?>">
    <a href="#">
      <i class="fa fa-calendar-check-o"></i>
      <span>Attendance</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li class="@if(url()->current() == route('attendance.admin',['filter' => 'all']))  active @endif"><a href="/account/app/attendance/admin/all"><i class="fa fa-circle-o"></i> View Attendance</a></li>
      <li class="@if(url()->current() == route('attendance.make'))  active @endif"><a href="{{ route('attendance.make') }}"><i class="fa fa-circle-o"></i> Make Attendance</a></li>
    </ul>
</li>
<li class="<?php if(isset($uriP[1])){ echo ($uriP[2] == 'reviews')? 'active' : ''; }?>"><a href="/account/app/reviews"><i class="fa fa-star"></i> <span>Reviews</span></a></li>
<li class="<?php if(isset($uriP[1])){ echo ($uriP[2] == 'reports')? 'active' : ''; }?>"><a href="/account/app/reports"><i class="fa fa-area-chart"></i> <span>Reports</span></a></li>

<li class="header">Advanced</li>

  <li class="treeview <?php if(isset($uriP[1])){ echo ($uriP[2] == 'role')? 'treeview active' : ''; }?>">
    <a href="#">
      <i class="fa fa-lock"></i>
      <span>Roles</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li><a href="/account/app/role"><i class="fa fa-circle-o"></i> All Roles</a></li>
      <li><a href="/account/app/role/create"><i class="fa fa-circle-o"></i> Add Role</a></li>
    </ul>
  </li>

  <li class="treeview <?php if(isset($uriP[1])){ echo ($uriP[2] == 'employee')? 'treeview active' : ''; }?>">
    <a href="#">
      <i class="fa fa-group"></i>
      <span>Employees</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li><a href="/account/app/employee"><i class="fa fa-circle-o"></i> All Employees</a></li>
      <li><a href="/account/app/employee/create"><i class="fa fa-circle-o"></i> Add Employee</a></li>
    </ul>
</li>

     <li class="treeview <?php if(isset($uriP[1])){ echo ($uriP[2] == 'course')? 'treeview active' : ''; }?>">
      <a href="#">
        <i class="fa fa-list"></i>
        <span>Courses</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        <li><a href="/account/app/course"><i class="fa fa-circle-o"></i> All Courses</a></li>
        <li><a href="/account/app/course/create"><i class="fa fa-circle-o"></i> Add Course</a></li>
      </ul>
  </li>

  <li class="treeview <?php if(isset($uriP[1])){ echo ($uriP[2] == 'certificate')? 'treeview active' : ''; }?>">
    <a href="#">
      <i class="fa fa-list-alt"></i>
      <span>Certificates</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li class="@if(url()->current() == route('certificate.index')) active @endif"><a href="{{ route('certificate.index')   }}"><i class="fa fa-circle-o"></i> Issue </a></li>
      <li class="@if(url()->current() == route('certificate.create')) active @endif"><a href="{{ route('certificate.create')  }}"><i class="fa fa-circle-o"></i> Print</a></li>
      <li class="@if(url()->current() == route('certificate.request')) active @endif"><a href="{{ route('certificate.request') }}"><i class="fa fa-circle-o"></i>New Requests</a></li>
    </ul>
  </li>
