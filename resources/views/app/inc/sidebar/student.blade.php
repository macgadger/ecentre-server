<li class="treeview <?php if(isset($uriP[0])){ echo ($uriP[1] == 'admission')? 'treeview active' : ''; }?>">
        <a href="#">
          <i class="fa fa-university"></i>
          <span>Admissions</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="@if(url()->current() == route('admission.index'))  active @endif"><a href="/account/app/admission"><i class="fa fa-circle-o"></i> All Admissions</a></li>
        </ul>
      </li>
    