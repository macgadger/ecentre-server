<div class="row">
    <div class="col-xs-12">
        <div class="box box-danger @if(!$open) collapsed-box @endif">
            <div class="box-header with-border">
                <h2 class="box-title"><b>{{ (!empty($title)) ? $title : 'Filter' }}</b></h2>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                        @if(!$open)
                            <i class="fa fa-plus"></i>
                        @else
                            <i class="fa fa-minus"></i>
                        @endif
                    </button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <form name="searchForm" id="searchForm" method="POST" action="{{ route('search.filter') }}" data-toggle="validator">
                @csrf
                <div class="row">
                    @if($search)
                    <div class="col-md-5">
                        <div class="form-group has-feedback">
                            <label class="control-label">Search Query</label>
                            <input type="text" class="form-control" name="query" autocomplete="off"
                                placeholder="Enter Search Query">
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group has-feedback">
                            <label class="control-label">Date from</label>
                            <input type="text" class="form-control datepicker" name="date_from" autocomplete="off">
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group has-feedback">
                            <label class="control-label">Date to</label>
                            <input type="text" class="form-control datepicker" name="date_to" autocomplete="off">
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>

                    <div class="col-md-1">
                        <div class="form-group">
                            <label class="control-label">&nbsp;</label>
                            <button type="submit" class="btn btn-block btn-danger"><i class="fa fa-search"></i></button>
                        </div>
                    </div>

                    @endif

                    @if($type == 'dashboard')
                    <div class="col-md-10">&nbsp;</div>
                    <div class="col-md-2">
                        <div class="form-group" style="padding-right: 20px">
                            <label class="control-label">Sort Data By: </label>
                            <select class="form-control" onChange="top.location.href=this.options[this.selectedIndex].value;" value="GO">
                                <option value="{{ route('account.dashboard') }}" @if(url()->current() == route('account.dashboard')) selected @endif> Today </option>
                                <option value="{{ route('account.dashboard', ['filter' => 'weekly']) }}"  @if(url()->current() == route('account.dashboard', ['filter' => 'weekly']))  selected @endif> Weekly </option>
                                <option value="{{ route('account.dashboard', ['filter' => 'monthly']) }}" @if(url()->current() == route('account.dashboard', ['filter' => 'monthly'])) selected @endif> Monthly </option>
                                <option value="{{ route('account.dashboard', ['filter' => 'all']) }}"     @if(url()->current() == route('account.dashboard', ['filter' => 'all']))     selected @endif> All </option>
                            </select>
                        </div>
                    </div>
                    @endif

                    @if($type == 'admission')
                    <div class="col-md-10">&nbsp;</div>
                    <div class="col-md-2">
                        <div class="form-group" style="padding-right: 20px">
                            <label class="control-label">Sort Admissions By: </label>
                            <select class="form-control" onChange="top.location.href=this.options[this.selectedIndex].value;" value="GO">
                                <option value="{{ route('admission.index') }}" @if(url()->current() == route('admission.index')) selected @endif> --All-- </option>
								<option value="{{ route('admission.index') }}/filter/today"   @if(url()->current() == route('admission.index').'/filter/today')  selected @endif> Today </option>
                                <option value="{{ route('admission.index') }}/filter/joined"  @if(url()->current() == route('admission.index').'/filter/joined') selected @endif> Joined </option>
                                <option value="{{ route('admission.index') }}/filter/pending" @if(url()->current() == route('admission.index').'/filter/pending') selected @endif> Pending </option>
                                <option value="{{ route('admission.index') }}/filter/dropout" @if(url()->current() == route('admission.index').'/filter/dropout') selected @endif> Drop Out </option>
                                <option value="{{ route('admission.index') }}/filter/attnblock" @if(url()->current() == route('admission.index').'/filter/attnblock') selected @endif> Attendance Block </option>
                                <option value="{{ route('admission.index') }}/filter/completed" @if(url()->current() == route('admission.index').'/filter/completed') selected @endif> Completed </option>
                                <option value="{{ route('admission.index') }}/filter/expired" @if(url()->current() == route('admission.index').'/filter/expired') selected @endif> Expired </option>
                            </select>
                        </div>
                    </div>
                    @endif

                    @if($type == 'fees')
                    <div class="col-md-10">&nbsp;</div>
                    <div class="col-md-2">
                        <div class="form-group" style="padding-right: 20px">
                            <label class="control-label">Sort Fees By: </label>
                            <select class="form-control" onChange="top.location.href=this.options[this.selectedIndex].value;" value="GO">
                                <option value="{{ route('fee.index') }}" @if(url()->current() == route('fee.index')) selected @endif> --All-- </option>
                                <option value="{{ route('fee.index') }}/filter/today"   @if(url()->current() == route('fee.index').'/filter/today') selected @endif>Due Today</option>
                                <option value="{{ route('fee.index') }}/filter/due"     @if(url()->current() == route('fee.index').'/filter/due') selected @endif>Upcoming Due</option>
                                <option value="{{ route('fee.index') }}/filter/overdue" @if(url()->current() == route('fee.index').'/filter/overdue') selected @endif>Over Due</option>
                                <option value="{{ route('fee.index') }}/filter/collection"   @if(url()->current() == route('fee.index').'/filter/collection') selected @endif>Today Fee Collection</option>
                            </select>
                        </div>
                    </div>
                    @endif

                    @if($type == 'performance')
                    <div class="col-md-10">&nbsp;</div>
                    <div class="col-md-2">
                        <div class="form-group" style="padding-right: 20px">
                            <label class="control-label">Sort Performance By: </label>
                            <select class="form-control" onChange="top.location.href=this.options[this.selectedIndex].value;" value="GO">
                                <option value="{{ route('performance.index') }}" @if(url()->current() == route('performance.index')) selected @endif>Trainer</option>
                                <option value="{{ route('performance.index') }}/filter/a2it"   @if(url()->current() == route('performance.index').'/filter/a2it') selected @endif>A2IT</option>
                            </select>
                        </div>
                    </div>
                    @endif

                </div>
                </form>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>

@push('styles')
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{ asset(config('app.prefix').'components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
@endpush

@push('scripts')
<!-- bootstrap datepicker -->
<script src="{{ asset(config('app.prefix').'components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script>
    $(function () {
        //Date picker
        $('.datepicker').datepicker({
            autoclose: true,
            format: "dd-mm-yyyy",
            todayHighlight: true,
            todayBtn: true,
            clearBtn: true,
        });
    });
</script>
@endpush
