@extends('app.layout.app')
@section('title','Edit Course')
@section('content')
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h2 class="box-title"><b>Edit Course</b></h2>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse"> <i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">

                    @if($errors->count())
                    @foreach ($errors->all() as $message)
                    <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        {{ $message }}
                    </div>
                    @endforeach
                    @endif

                    <form name="courseForm" id="courseForm" method="POST" action="{{ route('course.update',['id' => $course->id ]) }}" data-toggle="validator">
                        @csrf
                        @method('PUT')

                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group has-feedback">
                                    <label class="control-label">Course Name</label>
                                    <input type="text" class="form-control" name="name" placeholder="Enter Course Name"
                                        value="{{ $course->name }}" autocomplete="false" required="required" style="text-transform: uppercase;">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group has-feedback">
                                    <label class="control-label">Status</label>
                                    <select name="status" class="form-control">
                                        <option value="1" @if($course->status == 1) selected @endif>Active</option>
                                        <option value="0" @if($course->status == 0) selected @endif>Suspend</option>
                                    </select>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>

                        </div>
                        <hr>

                        <div class="row">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-lg btn-block btn-danger">Submit</button>
                            </div>
                        </div>
                    </form>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
@endsection

@push('styles')

@endpush

@push('scripts')

@endpush