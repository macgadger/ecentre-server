<!-- Main content -->
<section class="content">
        <!-- Custom Tabs -->
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_personel" data-toggle="tab">Details</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_personel">
                    <table class="table table-bordered table-hover table-condensed">
                        <tbody>
                          <tr class="techSpecRow">
                            <td class="techSpecTD1">Course Name:</td>
                            <td class="techSpecTD2">{{ strtoupper($course->name) }}</td>
                          </tr>
                          <tr class="techSpecRow">
                            <td class="techSpecTD1">Status:</td>
                            <td class="techSpecTD2">
                                @if($course->status == 1)
                                    <span class="label label-success">ACTIVE</span>
                                @elseif($course->status == 2)
                                    <span class="label label-danger">DELETED</span>
                                @elseif($course->status == 0)
                                    <span class="label label-warning">IN-ACTIVE</span>
                                @endif
                            </td>
                          </tr>
                          <tr class="techSpecRow">
                            <td class="techSpecTD1">Created On:</td>
                            <td class="techSpecTD2">{{ date('d-m-Y',strtotime($course->created_at)) }}</td>
                          </tr>
                        </tbody>
                      </table>
                </div>
    
               
            </div>
            <!-- /.tab-content -->
        </div>
        <!-- nav-tabs-custom -->
    </section>