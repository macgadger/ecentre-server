@extends('app.layout.app')
@section('title','Attendance')
@section('mod_title','Attendance View')
@section('content')
<!-- Main content -->
<section class="content">

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h2 class="box-title"><b>Attendance Manager</b></h2>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">

                    <div class="row" style="padding: 10px;">
                        <div class="col-md-6">
                            <div class="form-group" style="padding-right: 20px">
                                    <label class="control-label">Attendance by Batch: </label>
                                    <select class="form-control" onChange="top.location.href=this.options[this.selectedIndex].value;" value="GO">
                                        <option value=""> --Select Batch-- </option>
                                        @foreach ($batch as $item)
                                            <option value="{{ route('attendance.today', ['filter' => $item->id ]) }}" @if(url()->current() == route('attendance.today', ['filter' => $item->id ]))  selected @endif>{{ strtoupper($item->title) }}</option>
                                        @endforeach
                                    </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            @isset($students)
                                <h1 class="text-red">Total: {{ $students->count() }} Student(s)</h1>
                            @endisset
                        </div>
                    </div>

                    @if(session('status'))
                        <div class="alert alert-success" id="msg-alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close" style="color: white">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <p style="font-size: 18px"><strong>{{ session('status') }}</strong></p>
                        </div>
                    @endif

                    @if(session('error'))
                        <div class="alert alert-danger" id="msg-alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close" style="color: white">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <p style="font-size: 18px"><strong>{{ session('error') }}</strong></p>
                        </div>
                    @endif

                    @isset($students)
<<<<<<< HEAD
                    <form name="attendanceForm" method="POST" action="{{ route('attendance.store') }}" data-toggle="validator">
=======
                    <form name="attendanceForm" method="POST" action="{{ route('attendance.store') }}"  data-toggle="validator">
>>>>>>> c102559a3f488d89320688055df80fe34fa0fd99
                    @csrf
                    <input name="batch_id" type="hidden" value="{{ $filter }}">

                    <div class="table-responsive mailbox-messages">
                            <table id="data-table" class="table table-hover table-striped">
                            <thead>
                                <tr>
                                    <th>Attendance</th>
                                    <th>AID</th>
                                    <th>Student Name</th>
                                    <th>Course</th>
                                    <th>Course Duration</th>
                                    <th>Course End Date</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                              <tbody>
                              @foreach ($students as $key => $student)
                              @if($student->status != 'COMPLETED')
                              <tr>
                                <td>
                                    @if(strtotime(date('d-m-Y')) > strtotime($student->course_enddate))
                                        <select name="attendance_ids[{{  $student->id }}]" class="form-control">
                                            <option value="absent">Absent</option>
                                        </select>
                                    @else
                                        <select name="attendance_ids[{{  $student->id }}]" class="form-control">
                                            <option value="present">Present</option>
                                            <option value="absent">Absent</option>
                                            <option value="holiday">Holiday</option>
                                            <option value="leave">Leave</option>
                                        </select>
                                    @endif
                                    {{-- <input type="hidden"   name="attendance_ids[{{  $student->id }}]" value="0">
                                    <input type="checkbox" name="attendance_ids[{{  $student->id }}]" value="1" checked> --}}
                                </td>
                                <td><b>{{ $student->aid }}</b></td>
                                <td class="mailbox-name"><a href="javascript:void(0)">{{ ucwords($student->full_name) }}</a></td>
                                <td class="mailbox-subject">{{ strtoupper($student->getCourse->name) }}</td>
                                <td class="mailbox-date">{{ $student->course_durartion }}</td>
                                <td class="mailbox-date">{{ date('d M,y',strtotime($student->course_enddate)) }}</td>
                                <td class="mailbox-date">
                                    @if(strtotime(date('d-m-Y')) > strtotime($student->course_enddate))
                                        <span class="label label-danger">EXPIRED</span>
                                    @else
                                        <span class="label label-success">ACTIVE</span>
                                    @endif
                                </td>
                              </tr>
                              @endif
                              @endforeach
                              </tbody>
                            </table>
                            <!-- /.table -->
                          </div>

                    <div class="box-footer">
                            <div class="mailbox-controls">
                                <div class="form-group has-feedback">
                                    <label class="control-label">Remarks</label>
                                    <textarea class="form-control" name="remarks" placeholder="What subjects you Teach today to Students" rows="5" required>{{ old('remarks') }}</textarea>
                                    <div class="help-block with-errors"></div>
                                </div>
                            <button type="submit" class="btn btn-block btn-danger"><i class="fa fa-check"></i> Submit Attendance</button>
                            </div>
                    </div>

                </form>
                @endisset

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
@endsection

@push('styles')
   <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset(config('app.prefix').'components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{ asset(config('app.prefix').'plugins/iCheck/flat/red.css') }}">

  <style>
      th { font-size: 16px; }
      td { font-size: 18px; }
      .big{
        font-size: 12px;
        font-weight: bold;
      }
  </style>
@endpush

@push('scripts')
<!-- DataTables -->
<script src="{{ asset(config('app.prefix').'components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset(config('app.prefix').'components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<!-- iCheck -->
<script src="{{ asset(config('app.prefix').'plugins/iCheck/icheck.min.js') }}"></script>
<script>
<<<<<<< HEAD

=======
>>>>>>> c102559a3f488d89320688055df80fe34fa0fd99
    $(function () {
        $('#data-table').DataTable({
            "aaSorting": [],
            "bPaginate": false,
        });

        //Enable iCheck plugin for checkboxes
        //iCheck for checkbox and radio inputs
<<<<<<< HEAD
        /*$('.mailbox-messages input[type="checkbox"]').iCheck({
            checkboxClass: 'icheckbox_flat-red',
            radioClass: 'iradio_flat-red'
        });

        //Enable check and uncheck all functionality
        $(".checkbox-toggle").click(function ()
        {
            var clicks = $(this).data('clicks');
            if (clicks) {
                //Uncheck all checkboxes
                $(".mailbox-messages input[type='checkbox']").iCheck("uncheck");
                $(".fa", this).removeClass("fa-check-square-o").addClass('fa-square-o');
            } else {
                //Check all checkboxes
                $(".mailbox-messages input[type='checkbox']").iCheck("check");
                $(".fa", this).removeClass("fa-square-o").addClass('fa-check-square-o');
            }
            $(this).data("clicks", !clicks);
        });*/
=======
        // $('.mailbox-messages input[type="checkbox"]').iCheck({
        //     checkboxClass: 'icheckbox_flat-red',
        //     radioClass: 'iradio_flat-red'
        // });

        //Enable check and uncheck all functionality
        // $(".checkbox-toggle").click(function ()
        // {
        //     var clicks = $(this).data('clicks');
        //     if (clicks) {
        //         //Uncheck all checkboxes
        //         $(".mailbox-messages input[type='checkbox']").iCheck("uncheck");
        //         $(".fa", this).removeClass("fa-check-square-o").addClass('fa-square-o');
        //     } else {
        //         //Check all checkboxes
        //         $(".mailbox-messages input[type='checkbox']").iCheck("check");
        //         $(".fa", this).removeClass("fa-square-o").addClass('fa-check-square-o');
        //     }
        //     $(this).data("clicks", !clicks);
        // });
>>>>>>> c102559a3f488d89320688055df80fe34fa0fd99
    });
</script>
@endpush
