@extends('app.layout.app')
@section('title','Attendance')
@section('mod_title','Attendance View')
@section('content')
<!-- Main content -->
<section class="content">

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h2 class="box-title"><b>Attendance Manager</b></h2>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">

                     {{-- <div class="row" style="padding: 10px;">
                        <div class="col-md-6">
                            <div class="form-group" style="padding-right: 20px">
                                <label class="control-label">Attendance by Date: </label>
                                <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control datepicker" name="attendance_date" value="{{ old('attendance_date') }}" autocomplete="off" required>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>

                        </div>
                        <div class="col-md-6" style="padding-top: 20px">
                                <button type="submit" class="btn btn-block btn-danger">Submit</button>
                        </div>
                    </div> --}}


                    @if(session('status'))
                    <div class="alert alert-success" id="msg-alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close" style="color: white">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <p style="font-size: 18px"><strong>{{ session('status') }}</strong></p>
                    </div>
                    @endif

                    <div class="table-responsive mailbox-messages">
                            <table id="data-table" class="table table-hover table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Batch</th>
                                    <th>Attendance Date</th>
                                </tr>
                            </thead>
                              <tbody>
                              @foreach ($attendance as $key => $attn)
                              <tr>
                                <td>{{ $key+1 }}</td>
                                    <td class="mailbox-name"><a href="javascript:void(0)"  class="openBtn" data-href="{{ route('attendance.show',['id' => $attn->id]) }}">{{ strtoupper($attn->batch->title) }}</a></td>
                                <td>{{ date('d M Y',strtotime($attn->created_at)) }}</td>
                              </tr>
                              @endforeach
                              </tbody>
                            </table>
                            <!-- /.table -->
                          </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
@endsection

@push('styles')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset(config('app.prefix').'components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <!-- iCheck -->
    {{-- <link rel="stylesheet" href="{{ asset(config('app.prefix').'plugins/iCheck/flat/red.css') }}">
    <link rel="stylesheet" href="{{ asset(config('app.prefix').'components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}"> --}}

  <style>
      th { font-size: 16px; }
      td { font-size: 18px; }
      .big{
        font-size: 12px;
        font-weight: bold;
      }
  </style>
@endpush

@push('scripts')
<!-- DataTables -->
<script src="{{ asset(config('app.prefix').'components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset(config('app.prefix').'components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<!-- iCheck -->
{{-- <script src="{{ asset(config('app.prefix').'plugins/iCheck/icheck.min.js') }}"></script>
<script src="{{ asset(config('app.prefix').'components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script> --}}

<script>
    $(function () {
        $('#data-table').DataTable({
            "aaSorting": [],
        });

        //Date picker
        // $('.datepicker').datepicker({
        //     autoclose: true,
        //     format: "dd-mm-yyyy",
        //     todayHighlight: true,
        //     todayBtn: true,
        //     clearBtn: true,
        // });
    });
</script>
@endpush
