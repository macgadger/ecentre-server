@extends('app.layout.app')
@section('title','Attendance')
@section('mod_title','Attendance View')
@section('content')
<!-- Main content -->
<section class="content">

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h2 class="box-title"><b>Attendance Manager</b></h2>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">

                    <div class="row" style="padding: 10px;">
                        <div class="col-md-6">
                            <div class="form-group" style="padding-right: 20px">
                                    <label class="control-label">Attendance by Batch: </label>
                                    <select class="form-control" onChange="top.location.href=this.options[this.selectedIndex].value;" value="GO">
                                        <option value=""> --Select Batch-- </option>
                                        @foreach ($batch as $item)
                                            <option value="{{ route('attendance.admin', ['filter' => $item->id ]) }}" @if(url()->current() == route('attendance.admin', ['filter' => $item->id ]))  selected @endif>{{ strtoupper($item->title) }}</option>
                                        @endforeach
                                    </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            &nbsp;
                        </div>
                    </div>

                    @if(session('status'))
                        <div class="alert alert-success" id="msg-alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close" style="color: white">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <p style="font-size: 18px"><strong>{{ session('status') }}</strong></p>
                        </div>
                    @endif

                    @isset($students)
                    <div class="table-responsive mailbox-messages">
                        <table id="data-table" class="table table-hover table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Batch</th>
                                <th>Trainer</th>
                                <th>Attendance Date</th>
                            </tr>
                        </thead>
                          <tbody>
                          @foreach ($students as $key => $attn)
                          <tr>
                            <td>{{ $key+1 }}</td>
                            <td class="mailbox-name"><a href="javascript:void(0)"  class="openBtn" data-href="{{ route('attendance.show',['id' => $attn->id]) }}">{{ strtoupper($attn->batch->title) }}</a></td>
                            <td>{{ $attn->trainer->full_name }}</td>
                            <td>{{ date('d M Y',strtotime($attn->created_at)) }}</td>
                          </tr>
                          @endforeach
                          </tbody>
                        </table>
                        <!-- /.table -->
                      </div>
                    @endisset
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
@endsection

@push('styles')
   <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset(config('app.prefix').'components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">

  <style>
      th { font-size: 16px; }
      td { font-size: 18px; }
      .big{
        font-size: 12px;
        font-weight: bold;
      }
  </style>
@endpush

@push('scripts')
<!-- DataTables -->
<script src="{{ asset(config('app.prefix').'components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset(config('app.prefix').'components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script>
    $(function () {
        $('#data-table').DataTable({
            "aaSorting": [],
        });
    });
</script>
@endpush
