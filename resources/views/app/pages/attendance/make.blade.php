@extends('app.layout.app')
@section('title','Attendance')
@section('mod_title','Attendance View')
@section('content')
<!-- Main content -->
<section class="content">

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h2 class="box-title"><b>Make Attendance</b></h2>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row" style="padding: 10px;">
                        <div class="col-md-6">
                        <div class="form-group has-feedback" style="padding-right: 20px">
                                <label class="control-label">Batch: </label>
                                <select class="form-control" onChange="top.location.href=this.options[this.selectedIndex].value;" value="GO">
                                    <option value=""> --Select Batch-- </option>
                                    @foreach ($batch as $item)
                                        <option value="{{ route('attendance.make', ['filter' => $item->id ]) }}" @if(url()->current() == route('attendance.make', ['filter' => $item->id ]))  selected @endif>{{ strtoupper($item->title) }}</option>
                                    @endforeach
                                </select>
                                <div class="help-block with-errors"></div>
                        </div>
                        </div>
                    </div>

                    @if(session('status'))
                        <div class="alert alert-success" id="msg-alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close" style="color: white">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <p style="font-size: 18px"><strong>{{ session('status') }}</strong></p>
                        </div>
                    @endif

                    @if(session('error'))
                        <div class="alert alert-danger" id="msg-alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close" style="color: white">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <p style="font-size: 18px"><strong>{{ session('error') }}</strong></p>
                        </div>
                    @endif

                    @isset($students)
                    <form name="attendanceForm" method="POST" action="{{ route('attendance.admin_create') }}"  data-toggle="validator">
                    @csrf
                    <input name="batch_id" type="hidden" value="{{ $filter }}">
                    <hr>
                    <div class="row" style="padding: 10px;">
                            <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Attendance for Trainer: </label>
                                        <select class="form-control" name="trainer" required>
                                            <option value=""> --Select Trainer-- </option>
                                            @foreach ($trainer as $item)
                                                <option @if(old('trainer') == $item->profile->id) selected @endif value="{{ $item->profile->id }}">{{ strtoupper($item->profile->full_name) }}</option>
                                            @endforeach
                                        </select>
                                        <div class="help-block with-errors"></div>
                                    </div>
                            </div>

                            <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Attendance Date: </label>
                                        <div class="input-group date">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" name="attendance_date" class="form-control pull-right datepicker" value="{{ old('attendance_date') }}"  style="border-radius: 0px" autocomplete="off" required>
                                        </div>
                                        <div class="help-block with-errors"></div>
                                    </div>
                            </div>
                    </div>

                    <div class="table-responsive mailbox-messages">
                            <table id="data-table" class="table table-hover table-striped">
                            <thead>
                                <tr>
                                    <th>Attendance</th>
                                    <th>AID</th>
                                    <th>Student Name</th>
                                    <th>Course</th>
                                    <th>Course Duration</th>
                                    <th>Course End Date</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                              <tbody>
                              @foreach ($students as $key => $student)
                              <tr>
                                <td>
                                    @if(strtotime(date('d-m-Y')) > strtotime($student->course_enddate))
                                        <select name="attendance_ids[{{  $student->id }}]" class="form-control">
                                            <option value="absent">Absent</option>
                                        </select>
                                    @else
                                        <select name="attendance_ids[{{  $student->id }}]" class="form-control">
                                            <option value="present">Present</option>
                                            <option value="absent">Absent</option>
                                            <option value="holiday">Holiday</option>
                                            <option value="leave">Leave</option>
                                        </select>
                                    @endif
                                </td>
                                <td><b>{{ $student->aid }}</b></td>
                                <td class="mailbox-name"><a href="javascript:void(0)">{{ ucwords($student->full_name) }}</a></td>
                                <td class="mailbox-subject">{{ strtoupper($student->getCourse->name) }}</td>
                                <td class="mailbox-date">{{ $student->course_durartion }}</td>
                                <td class="mailbox-date">{{ date('d M,y',strtotime($student->course_enddate)) }}</td>
                                <td class="mailbox-date">
                                    @if(strtotime(date('d-m-Y')) > strtotime($student->course_enddate))
                                        <span class="label label-danger">EXPIRED</span>
                                    @else
                                        <span class="label label-success">ACTIVE</span>
                                    @endif
                                </td>
                              </tr>
                              @endforeach
                              </tbody>
                            </table>
                            <!-- /.table -->
                          </div>

                    <div class="box-footer">
                            <div class="mailbox-controls">
                                <div class="form-group has-feedback">
                                    <label class="control-label">Remarks</label>
                                    <textarea class="form-control" name="remarks" placeholder="Trainer Remarks" rows="5" required>ATTENDANCE BY A2IT ADMIN FOR THE TRAINER</textarea>
                                    <div class="help-block with-errors"></div>
                                </div>
                            <button type="submit" class="btn btn-block btn-danger"><i class="fa fa-check"></i> Submit Attendance</button>
                            </div>
                    </div>

                </form>
                @endisset

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
@endsection

@push('styles')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset(config('app.prefix').'components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{ asset(config('app.prefix').'plugins/iCheck/flat/red.css') }}">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="{{ asset(config('app.prefix').'components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">

  <style>
      th { font-size: 16px; }
      td { font-size: 18px; }
      .big{
        font-size: 12px;
        font-weight: bold;
      }
  </style>
@endpush

@push('scripts')
<!-- DataTables -->
<script src="{{ asset(config('app.prefix').'components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset(config('app.prefix').'components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<!-- iCheck -->
<script src="{{ asset(config('app.prefix').'plugins/iCheck/icheck.min.js') }}"></script>

<!-- bootstrap datepicker -->
<script src="{{ asset(config('app.prefix').'components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>

<script>
    $(function () {
        $('#data-table').DataTable({
            "aaSorting": [],
            "bPaginate": false,
            "searching": false,
            "info": false
        });

        //Date picker
        $('.datepicker').datepicker({
                autoclose: true,
                format: "dd-mm-yyyy",
                todayHighlight: true,
                todayBtn: true,
                clearBtn: true,
        });

    });
</script>
@endpush
