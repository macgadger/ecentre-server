<section class="content">
    <div class="row">

        <div class="well well-sm">
        <div class="table-responsive">
            <table id="data-table" class="table table-hover table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th>AID</th>
                    <th>Student Name</th>
                    <th>Course</th>
                    <th>Attendance Date</th>
                </tr>
            </thead>
              <tbody>
              @foreach ($attn as $item)
                <tr>
                    <td>
                        @if($item['present'] == 'present')
                            <span class="label label-success">PRESENT</span>
                        @elseif($item['present'] == 'leave' || $item['present'] == 'holiday')
                            <span class="label label-warning">ON {{ strtoupper($item['present']) }}</span>
                        @else
                            <span class="label label-danger">ABSENT</span>
                        @endif
                    </td>
                    <td><b>{{ $item['aid'] }}</b></td>
                    <td class="mailbox-name"><a href="javascript:void(0)">{{ $item['name'] }}</a></td>
                    <td class="mailbox-subject">{{ $item['course'] }}</td>
                    <td class="mailbox-subject">{{ $item['date'] }}</td>
                </tr>
              @endforeach
              </tbody>
            </table>
            <!-- /.table -->
          </div>
        </div>


        <div class="well well-sm">
            <h4>Trainer Remarks:</h4>
            <p>{{ $remark }}</p>
        </div>

    </div>
</section>
