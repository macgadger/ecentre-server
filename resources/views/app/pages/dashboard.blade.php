@extends('app.layout.app')
@section('title','Dashboard')
@section('content')
<!-- Main content -->
<section class="content">
    <div class="row">
        @if(!Auth::check())
            @include('app.inc.dashboard.student')
        @else
            @include('app.inc.dashboard'.strtolower('.'.Auth::user()->getRole->name))
        @endif
    </div>
</section>
@endsection

@push('styles')
   <!-- DataTables -->
   <link rel="stylesheet" href="{{ asset(config('app.prefix').'components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
   <style>
       th { font-size: 16px; }
       td { font-size: 18px; }
       .big{
         font-size: 12px;
         font-weight: bold;
       }
   </style>
@endpush

@push('scripts')
<!-- DataTables -->
<script src="{{ asset(config('app.prefix').'components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset(config('app.prefix').'components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>

<script>
    $(function () {
        $('#data-table').DataTable({
            "aaSorting": [],
        });
    })
</script>
@endpush
