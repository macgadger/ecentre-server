@extends('app.layout.app')
@section('title','New Admission')
@section('content')
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h2 class="box-title"><b>New Admission</b></h2>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse"> <i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">

                    @if($errors->count())
                    @foreach ($errors->all() as $message)
                    <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        {{ $message }}
                    </div>
                    @endforeach
                    @endif

                <form name="admissionForm" id="admissionForm" method="POST" action="{{ route('admission.store') }}"
                        data-toggle="validator">
                        @csrf
                        <input type="hidden" name="aid" value="{{ $aid }}" required="required" readonly>
                        <fieldset>
                            <legend style="font-weight:bold; font-size: 22px; color: red;">Personel Information<span
                                    class="pull-right">Admission ID (A2IT-{{ $aid }})</span></legend>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">First Name</label>
                                        <input type="text" class="form-control" name="first_name"
                                            placeholder="Enter First Name" value="{{ old('first_name') }}"
                                            required="required">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Last Name</label>
                                        <input type="text" class="form-control" name="last_name"
                                            placeholder="Enter Last Name" value="{{ old('last_name') }}"
                                            autocomplete="last_name">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Gender</label>
                                        <select class="form-control" name="gender" required>
                                            <option value="" selected>--Select Gender--</option>
                                            <option value="male">Male</option>
                                            <option value="female">Female</option>
                                        </select>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Father's Name</label>
                                        <input type="text" class="form-control" name="father_name"
                                            placeholder="Enter Father's Name" value="{{ old('father_name') }}"
                                            autocomplete="off" required="required">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Date of Birth</label>
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control datepicker" name="dob" value="{{ old('dob') }}" autocomplete="dob" required>
                                        </div>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Email</label>
                                        <input type="text" class="form-control" name="email" placeholder="Enter Email"
                                            value="{{ old('email') }}" autocomplete="email" required="required"
                                            style="text-transform: lowercase;">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Mobile</label>
                                        <input type="text" class="form-control" name="mobile"
                                            placeholder="Enter Mobile Number" value="{{ old('mobile') }}"
                                            autocomplete="mobile" required="required">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Alt. Mobile</label>
                                        <input type="text" class="form-control" name="alt_mobile"
                                            autocomplete="alt_mobile" placeholder="Enter Alt. Mobile Number"
                                            value="{{ old('alt_mobile') }}">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Address</label>
                                        <textarea class="form-control" name="address" placeholder="Enter Address"
                                            required="required">{{ old('address') }}</textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Local/PG Address</label>
                                        <textarea class="form-control" name="pg_address"
                                            placeholder="Enter Local or PG Address">{{ old('pg_address') }}</textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">City</label>
                                        <input type="text" class="form-control" name="city" placeholder="Enter City"
                                            value="{{ old('city') }}" autocomplete="city" required="required">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">State</label>
                                        <select class="form-control select2" name="state" placeholder="Enter State"
                                            required="required" data-placeholder="Select a State">
                                            <option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands
                                            </option>
                                            <option value="Andhra Pradesh">Andhra Pradesh</option>
                                            <option value="Arunachal Pradesh">Arunachal Pradesh</option>
                                            <option value="Assam">Assam</option>
                                            <option value="Bihar">Bihar</option>
                                            <option value="Chandigarh">Chandigarh</option>
                                            <option value="Chhattisgarh">Chhattisgarh</option>
                                            <option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
                                            <option value="Daman and Diu">Daman and Diu</option>
                                            <option value="Delhi">Delhi</option>
                                            <option value="Goa">Goa</option>
                                            <option value="Gujarat">Gujarat</option>
                                            <option value="Haryana">Haryana</option>
                                            <option value="Himachal Pradesh">Himachal Pradesh</option>
                                            <option value="Jammu and Kashmir">Jammu and Kashmir</option>
                                            <option value="Jharkhand">Jharkhand</option>
                                            <option value="Karnataka">Karnataka</option>
                                            <option value="Kerala">Kerala</option>
                                            <option value="Lakshadweep">Lakshadweep</option>
                                            <option value="Madhya Pradesh">Madhya Pradesh</option>
                                            <option value="Maharashtra">Maharashtra</option>
                                            <option value="Manipur">Manipur</option>
                                            <option value="Meghalaya">Meghalaya</option>
                                            <option value="Mizoram">Mizoram</option>
                                            <option value="Nagaland">Nagaland</option>
                                            <option value="Orissa">Orissa</option>
                                            <option value="Pondicherry">Pondicherry</option>
                                            <option value="Punjab" selected="selected">Punjab</option>
                                            <option value="Rajasthan">Rajasthan</option>
                                            <option value="Sikkim">Sikkim</option>
                                            <option value="Tamil Nadu">Tamil Nadu</option>
                                            <option value="Tripura">Tripura</option>
                                            <option value="Uttaranchal">Uttaranchal</option>
                                            <option value="Uttar Pradesh">Uttar Pradesh</option>
                                            <option value="West Bengal">West Bengal</option>
                                        </select>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Country</label>
                                        <input type="text" class="form-control" name="country"
                                            placeholder="Enter Country" autocomplete="country" required="required"
                                            value="India">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>

                        </fieldset>

                        <fieldset>
                            <legend style="font-weight:bold; font-size: 22px; color: red;">Educational Information
                            </legend>

                            <div class="row">

                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">College Name</label>
                                        <input type="text" class="form-control" name="college_name"
                                            placeholder="Enter College Name" value="{{ old('college_name') }}"
                                            autocomplete="college_name" required="required">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Degree/Diploma/Master</label>
                                        <select class="form-control" name="college_deg_type" required>
                                            <option value="" selected>--Select One--</option>
                                            <option value="Degree">Degree</option>
                                            <option value="Diploma">Diploma</option>
                                            <option value="Master">Master</option>
                                        </select>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Stream</label>
                                        <input type="text" class="form-control" name="college_stream"
                                            placeholder="Enter Stream" value="{{ old('college_stream') }}"
                                            autocomplete="college_stream" required="required">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Semester</label>
                                        <input type="text" class="form-control" name="college_semester"
                                            placeholder="Enter Semester" value="{{ old('college_semester') }}"
                                            autocomplete="college_semester" required="required">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>

                            </div>

                        </fieldset>

                        <fieldset>
                            <legend style="font-weight:bold; font-size: 22px; color: red;">Registration Details</legend>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Main Course Name</label>
                                        <select class="form-control select2" name="course_name"
                                            data-placeholder="-- Select main Course --" required="required">
                                            <option value="">-- Select main Course --</option>
                                            @foreach($courses as $item)
                                            <option value="{{ $item->id }}">{{ strtoupper($item->name) }}</option>
                                            @endforeach
                                        </select>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Other Courses</label>
										<input type="text" class="form-control" name="other_course"
                                            placeholder="Other Courses" value="{{ old('other_course') }}"
                                            autocomplete="other_course">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Course Duration</label>
                                        <input type="text" class="form-control" name="course_durartion"
                                            placeholder="Enter Course Duration" value="{{ old('course_durartion') }}"
                                            autocomplete="course_duration" required="required">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Joining Date</label>
                                        <input type="text" class="form-control datepicker" name="course_joindate"
                                            placeholder="Course Joining Date" autocomplete="course_joindate"
                                            value="{{ old('course_joindate') }}" required="required">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">End Date</label>
                                        <input type="text" class="form-control datepicker" name="course_enddate"
                                            placeholder="Course End Date" autocomplete="course_enddate"
                                            value="{{ old('course_enddate') }}" required="required">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Preffered Time</label>
                                        <input type="text" class="form-control timepicker" name="course_time"
                                            placeholder="Enter Preffered Time" value="{{ old('course_time') }}"
                                            autocomplete="off" required="required">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Referred by</label>
                                        <select class="form-control" name="refer_by" autocomplete="refer_by"
                                            required="required">
                                            <option value="" selected>-- Select --</option>
                                            <option value="google">Google</option>
                                            <option value="just-dial">JustDial</option>
                                            <option value="online">Online</option>
                                            <option value="social">Social Platform</option>
                                            <option value="advertisement">Advertisement</option>
                                            <option value="friend">Friend</option>
                                            <option value="family">Family</option>
                                            <option value="email">Email</option>
                                            <option value="other">Other</option>
                                        </select>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Referrer Name/Type</label>
                                        <input type="text" class="form-control" name="refer_name"
                                            placeholder="Enter Referrer Name" value="{{ old('refer_name') }}"
                                            autocomplete="refer_name" required="required">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Admission By</label>
                                        <select class="form-control" name="admission_by" autocomplete="admission_by"
                                            required="required">
                                            <option value="" selected> -- Choose One -- </option>
                                            @foreach($staff as $item)
                                                <option value="{{ $item->getProfile->user_id }}">{{ $item->getProfile->first_name.' '.$item->getProfile->last_name }}</option>
                                            @endforeach
                                        </select>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>

                        </fieldset>

                        <fieldset>
                            <legend style="font-weight:bold; font-size: 22px; color: red;">Fee Details</legend>

                            <div class="row">

                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Reciept/Transaction No.</label>
                                        <input type="text" class="form-control" name="payment_reciept"
                                                    placeholder="Enter Reciept/Transaction Number" autocomplete="payment_reciept"
                                                    value="{{ old('payment_reciept') }}" required="required">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Course Fees</label>
                                        <input type="text" class="form-control" name="course_fee"
                                            placeholder="Enter Course Fees" value="{{ old('course_fee') }}"
                                            autocomplete="course_fee" required="required" onkeyup="calc_total()">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Advance Amount</label>
                                        <input type="text" class="form-control" name="course_advance"
                                            placeholder="Advance Amount" autocomplete="course_advance"
                                            value="{{ old('course_advance') }}" required="required"
                                            onkeyup="calc_total()">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Next Payment Date</label>
                                        <input type="text" class="form-control datepicker" name="nextfee_date"
                                            placeholder="Enter Next Payment Date" autocomplete="nextfee_date"
                                            value="{{ old('nextfee_date') }}" required="required">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Payment Mode</label>
                                        <select class="form-control" name="payment_mode" required>
                                            <option value="">Select Mode</option>
                                            <option value="cash">Cash</option>
                                            <option value="online">Online Bank Transfer</option>
                                            <option value="qrcode">QR Code</option>
                                            <option value="paytm">Paytm</option>
                                            <option value="gpay">Google Pay</option>
                                        </select>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>

                            </div>
                            <hr>

                            <div class="row">
                                <div class="col-md-6">
                                        <div class="form-group has-feedback">
                                            <label class="control-label">Payment Remarks</label>
                                            <textarea name="remarks" class="form-control" rows="7">{{ old('remarks') }}</textarea>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                </div>

								<div class="col-md-2">
                                   <fieldset>
                                    <legend style="font-weight:bold; font-size: 22px; color: red;">Pay using QR Code</legend>
                                    <img src="{{ asset(config('app.prefix').'uploads/qrcode/qr.png') }}" class="img-responsive" width="150px" height="150px" style="display:block; margin:auto;" />
                                   </fieldset>
                                </div>

                                <div class="col-md-4">
                                    <input type="hidden" id="fee_due" name="fee_due" autocomplete="fee_due" value="{{ old('fee_due') }}" required="required" readonly />
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr class="active" style="font-size: 18px;">
                                                <th>Course Details</th>
                                                <th>Total Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody style="font-size: 16px;">
                                            <tr>
                                                <td>Course Fee</td>
                                                <td><i class="fa fa-rupee"></i> <span id="cfee">&nbsp;</span></td>
                                            </tr>
                                            <tr>
                                                <td>Amount Recieved</td>
                                                <td><i class="fa fa-rupee"></i> <span id="cadv">&nbsp;</span></td>
                                            </tr>
                                            <tr>
                                                <td><b>Grand Total</b></td>
                                                <td><i class="fa fa-rupee"></i> <span id="ctotal">&nbsp;</span></td>
                                            </tr>
                                            <tr>
                                                <td><b style="color: red">Amount Due</b></td>
                                                <td><i class="fa fa-rupee"></i> <span id="cdue">&nbsp;</span></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <hr>
                                    <button type="submit" class="btn btn-lg btn-block btn-danger">Submit</button>
                                </div>

                            </div>
                        </fieldset>

                    </form>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
@endsection

@push('styles')
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset(config('app.prefix').'components/select2/dist/css/select2.min.css') }}">

<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{ asset(config('app.prefix').'components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset(config('app.prefix').'plugins/timepicker/bootstrap-timepicker.min.css') }}">
<style>
    .form-control{
        text-transform: uppercase;
    }
</style>
@endpush

@push('scripts')
<!-- Select2 -->
<script src="{{ asset(config('app.prefix').'components/select2/dist/js/select2.full.min.js') }}"></script>

<!-- bootstrap datepicker -->
<script src="{{ asset(config('app.prefix').'components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset(config('app.prefix').'plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
<script>
    $(function () {
        admission_init();
    });
</script>
@endpush
