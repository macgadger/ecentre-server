<!-- Main content -->
<section class="content">
    <form name="admissionForm" method="POST" action="{{ route('admission.status',['id' => $data->id ]) }}"
        data-toggle="validator">
        @csrf
        <div class="form-group has-feedback">
            <label for="status">Status </label>
            <select name="status" class="form-control" required>
                <option value="PENDING" @if($data->status == 'PENDING') selected @endif>PENDING</option>
                <option value="JOINED"  @if($data->status == 'JOINED')  selected @endif>JOINED</option>
                <option value="LEAVE"   @if($data->status == 'LEAVE')   selected @endif>LEAVE</option>
                <option value="DROPOUT" @if($data->status == 'DROPOUT') selected @endif>DROPOUT</option>
                @if(Auth::user()->role == 1)
                    <option value="ATTNBLOCK"  @if($data->status == 'ATTNBLOCK')  selected @endif>ATTANDENCE BLOCK</option>
                    <option value="COMPLETED"   @if($data->status == 'COMPLETED')   selected @endif>COMPLETED</option>
                @endif
            </select>
            <div class="help-block with-errors"></div>
        </div>

        <div class="form-group has-feedback">
            <label for="remarks">Remarks </label>
            <textarea name="remarks" class="form-control" rows="5">{{ ucwords($data->remarks) }}</textarea>
            <div class="help-block with-errors"></div>
        </div>
        <hr>
        <div class="form-group">
            <button type="submit" class="btn btn-lg btn-block btn-danger">Update</button>
        </div>

    </form>
</section>
