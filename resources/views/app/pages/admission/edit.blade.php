@extends('app.layout.app')
@section('title','Edit Admission')
@section('content')
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h2 class="box-title"><b>Edit Admission</b></h2>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse"> <i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">

                    @if($errors->count())
                    @foreach ($errors->all() as $message)
                    <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        {{ $message }}
                    </div>
                    @endforeach
                    @endif

                    <form name="admissionForm" id="admissionForm" method="POST" action="{{ route('admission.update',['id' => $admission->id ]) }}" data-toggle="validator">
                        @csrf
                        @method('PUT')
                        <input type="hidden" name="aid" value="{{ $admission->aid }}" required="required" readonly>
                        <fieldset>
                            <legend style="font-weight:bold; font-size: 22px; color: red;">Personel Information<span
                                    class="pull-right">Admission ID (A2IT-{{ $admission->aid }})</span></legend>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">First Name</label>
                                        <input type="text" class="form-control" name="first_name"
                                            placeholder="Enter First Name" value="{{ $admission->first_name }}"
                                            required="required">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Last Name</label>
                                        <input type="text" class="form-control" name="last_name"
                                            placeholder="Enter Last Name" value="{{ $admission->last_name }}"
                                            autocomplete="last_name">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Gender</label>
                                        <select class="form-control" name="gender" required>
                                            <option value="">--Select Gender--</option>
                                            <option value="male" @if($admission->gender == 'male') selected @endif>Male
                                            </option>
                                            <option value="female" @if($admission->gender == 'female') selected
                                                @endif>Female</option>
                                        </select>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Father's Name</label>
                                        <input type="text" class="form-control" name="father_name"
                                            placeholder="Enter Father's Name" value="{{ $admission->father_name }}"
                                            autocomplete="off" required="required">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Date of Birth</label>
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control datepicker" name="dob"
                                                value="{{ date('d-m-Y',strtotime($admission->dob)) }}"
                                                autocomplete="dob" required>
                                        </div>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Email</label>
                                        <input type="text" class="form-control" name="email" placeholder="Enter Email"
                                            value="{{ $admission->email }}" autocomplete="email" required="required"
                                            style="text-transform: lowercase;">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Mobile</label>
                                        <input type="text" class="form-control" name="mobile"
                                            placeholder="Enter Mobile Number" value="{{ $admission->mobile }}"
                                            autocomplete="mobile" required="required">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Alt. Mobile</label>
                                        <input type="text" class="form-control" name="alt_mobile"
                                            autocomplete="alt_mobile" placeholder="Enter Alt. Mobile Number"
                                            value="{{ $admission->alt_mobile }}">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Address</label>
                                        <textarea class="form-control" name="address" placeholder="Enter Address"
                                            required="required">{{ $admission->address }}</textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Local/PG Address</label>
                                        <textarea class="form-control" name="pg_address"
                                            placeholder="Enter Local or PG Address">{{ $admission->pg_address }}</textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">City</label>
                                        <input type="text" class="form-control" name="city" placeholder="Enter City"
                                            value="{{ $admission->city }}" autocomplete="city" required="required">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">State</label>
                                        <select class="form-control select2" name="state" placeholder="Enter State"
                                            required="required" data-placeholder="Select a State">
                                            <option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands
                                            </option>
                                            <option value="Andhra Pradesh">Andhra Pradesh</option>
                                            <option value="Arunachal Pradesh">Arunachal Pradesh</option>
                                            <option value="Assam">Assam</option>
                                            <option value="Bihar">Bihar</option>
                                            <option value="Chandigarh" @if($admission->state == 'Chandigarh') selected
                                                @endif>Chandigarh</option>
                                            <option value="Chhattisgarh">Chhattisgarh</option>
                                            <option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
                                            <option value="Daman and Diu">Daman and Diu</option>
                                            <option value="Delhi">Delhi</option>
                                            <option value="Goa">Goa</option>
                                            <option value="Gujarat">Gujarat</option>
                                            <option value="Haryana" @if($admission->state == 'Haryana') selected
                                                @endif>Haryana</option>
                                            <option value="Himachal Pradesh" @if($admission->state == 'Himachal
                                                Pradesh') selected @endif>Himachal Pradesh</option>
                                            <option value="Jammu and Kashmir">Jammu and Kashmir</option>
                                            <option value="Jharkhand">Jharkhand</option>
                                            <option value="Karnataka">Karnataka</option>
                                            <option value="Kerala">Kerala</option>
                                            <option value="Lakshadweep">Lakshadweep</option>
                                            <option value="Madhya Pradesh">Madhya Pradesh</option>
                                            <option value="Maharashtra">Maharashtra</option>
                                            <option value="Manipur">Manipur</option>
                                            <option value="Meghalaya">Meghalaya</option>
                                            <option value="Mizoram">Mizoram</option>
                                            <option value="Nagaland">Nagaland</option>
                                            <option value="Orissa">Orissa</option>
                                            <option value="Pondicherry">Pondicherry</option>
                                            <option value="Punjab" @if($admission->state == 'Punjab') selected
                                                @endif>Punjab</option>
                                            <option value="Rajasthan">Rajasthan</option>
                                            <option value="Sikkim">Sikkim</option>
                                            <option value="Tamil Nadu">Tamil Nadu</option>
                                            <option value="Tripura">Tripura</option>
                                            <option value="Uttaranchal">Uttaranchal</option>
                                            <option value="Uttar Pradesh">Uttar Pradesh</option>
                                            <option value="West Bengal">West Bengal</option>
                                        </select>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Country</label>
                                        <input type="text" class="form-control" name="country"
                                            placeholder="Enter Country" autocomplete="country" required="required"
                                            value="{{ $admission->country }}">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>

                        </fieldset>

                        <fieldset>
                            <legend style="font-weight:bold; font-size: 22px; color: red;">Educational Information
                            </legend>

                            <div class="row">

                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">College Name</label>
                                        <input type="text" class="form-control" name="college_name"
                                            placeholder="Enter College Name" value="{{ $admission->college_name }}"
                                            autocomplete="college_name" required="required">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Degree/Diploma/Master</label>
                                        <select class="form-control" name="college_deg_type" required>
                                            <option value="" selected>--Select One--</option>
                                            <option value="Degree" @if($admission->college_deg_type == 'Degree')
                                                selected @endif>Degree</option>
                                            <option value="Diploma" @if($admission->college_deg_type == 'Diploma')
                                                selected @endif>Diploma</option>
                                            <option value="Master" @if($admission->college_deg_type == 'Master')
                                                selected @endif>Master</option>
                                        </select>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Stream</label>
                                        <input type="text" class="form-control" name="college_stream"
                                            placeholder="Enter Stream" value="{{ $admission->college_stream }}"
                                            autocomplete="college_stream" required="required">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Semester</label>
                                        <input type="text" class="form-control" name="college_semester"
                                            placeholder="Enter Semester" value="{{ $admission->college_semester }}"
                                            autocomplete="college_semester" required="required">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>

                            </div>

                        </fieldset>

                        <fieldset>
                            <legend style="font-weight:bold; font-size: 22px; color: red;">Registration Details</legend>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Main Course Name</label>
                                        <select class="form-control select2" name="course_name"
                                            data-placeholder="-- Select main Course --" required="required">
                                            <option value="">-- Select main Course --</option>
                                            @foreach($courses as $item)
                                            <option value="{{ $item->id }}" @if($admission->course_name == $item->id)
                                                selected @endif>{{ strtoupper($item->name) }}</option>
                                            @endforeach
                                        </select>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Other Courses</label>
                                        <select class="form-control select2" name="other_course[]" multiple="multiple"
                                            data-placeholder="-- Select other Courses --">
                                            <optgroup label="-- Other Courses --">
                                                @foreach($courses as $item)
                                                <option value="{{ $item->name }}" @if($admission->other_course ==
                                                    $item->name) selected @endif>{{ strtoupper($item->name) }}</option>
                                                @endforeach
                                            </optgroup>
                                        </select>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Course Duration</label>
                                        <input type="text" class="form-control" name="course_durartion"
                                            placeholder="Enter Course Duration"
                                            value="{{ $admission->course_durartion }}" autocomplete="course_duration"
                                            required="required">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Joining Date</label>
                                        <input type="text" class="form-control datepicker" name="course_joindate"
                                            placeholder="Course Joining Date" autocomplete="course_joindate"
                                            value="{{ date('d-m-Y',strtotime($admission->course_joindate)) }}"
                                            required="required">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">End Date</label>
                                        <input type="text" class="form-control datepicker" name="course_enddate"
                                            placeholder="Course End Date" autocomplete="course_enddate"
                                            value="{{ date('d-m-Y',strtotime($admission->course_enddate)) }}"
                                            required="required">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Preffered Time</label>
                                        <input type="text" class="form-control timepicker" name="course_time"
                                            placeholder="Enter Preffered Time" value="{{ $admission->course_time }}"
                                            autocomplete="course_time" required="required">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Referred by</label>
                                        <select class="form-control" name="refer_by" autocomplete="refer_by"
                                            required="required">
                                            <option value="" selected>-- Select --</option>
                                            <option value="google" @if($admission->refer_by == 'google') selected
                                                @endif>Google</option>
                                            <option value="just-dial" @if($admission->refer_by == 'just-dial') selected
                                                @endif>JustDial</option>
                                            <option value="online" @if($admission->refer_by == 'online') selected
                                                @endif>Online</option>
                                            <option value="social" @if($admission->refer_by == 'social') selected
                                                @endif>Social Platform</option>
                                            <option value="advertisement" @if($admission->refer_by == 'advertisement')
                                                selected @endif>Advertisement</option>
                                            <option value="friend" @if($admission->refer_by == 'friend') selected
                                                @endif>Friend</option>
                                            <option value="family" @if($admission->refer_by == 'family') selected
                                                @endif>Family</option>
                                            <option value="email" @if($admission->refer_by == 'email') selected
                                                @endif>Email</option>
                                            <option value="other" @if($admission->refer_by == 'other') selected
                                                @endif>Other</option>
                                        </select>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Referrer Name/Type</label>
                                        <input type="text" class="form-control" name="refer_name"
                                            placeholder="Enter Referrer Name" value="{{ $admission->refer_name }}"
                                            autocomplete="refer_name" required="required">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Admission By</label>
                                        <select class="form-control" name="admission_by" autocomplete="admission_by"
                                            required="required">
                                            <option value="" selected> -- Choose One -- </option>
                                            @foreach($staff as $item)
                                            <option value="{{ $item->getProfile->user_id }}" @if($admission->
                                                admission_by == $item->getProfile->user_id) selected
                                                @endif>{{ $item->getProfile->first_name.' '.$item->getProfile->last_name }}
                                            </option>
                                            @endforeach
                                        </select>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>

                        </fieldset>

                        <fieldset>
                            <legend style="font-weight:bold; font-size: 22px; color: red;">Fee Details</legend>

                            <div class="row">

                                <div class="col-md-8">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Course Fees</label>
                                        <input type="text" class="form-control" name="course_fee"
                                            placeholder="Enter Course Fees" value="{{ $admission->fees->course_amount }}"
                                            autocomplete="course_fee" required="required" onkeyup="calc_total()">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Payment Mode</label>
                                        <select class="form-control" name="payment_mode" required>
                                            <option value="">Select Mode</option>
                                            <option value="cash" @if($admission->payment_mode == 'cash') selected
                                                @endif>Cash</option>
                                            <option value="online" @if($admission->payment_mode == 'online') selected
                                                @endif>Online Bank Transfer</option>
                                            <option value="qrcode" @if($admission->payment_mode == 'qrcode') selected
                                                @endif>QR Code</option>
                                            <option value="paytm" @if($admission->payment_mode == 'paytm') selected
                                                @endif>Paytm</option>
                                            <option value="gpay" @if($admission->payment_mode == 'gpay') selected
                                                @endif>Google Pay</option>
                                        </select>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>

                            </div>
                            <hr>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Payment Remarks</label>
                                        <textarea name="remarks" class="form-control"
                                            rows="5">{{ $admission->remarks }}</textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <hr>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-lg btn-block btn-danger">Submit</button>
                                    </div>
                                </div>

                            </div>
                        </fieldset>

                    </form>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
@endsection

@push('styles')
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset(config('app.prefix').'components/select2/dist/css/select2.min.css') }}">

<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{ asset(config('app.prefix').'components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset(config('app.prefix').'plugins/timepicker/bootstrap-timepicker.min.css') }}">
<style>
    .form-control{
        text-transform: uppercase;
    }
</style>
@endpush

@push('scripts')
<!-- Select2 -->
<script src="{{ asset(config('app.prefix').'components/select2/dist/js/select2.full.min.js') }}"></script>

<!-- bootstrap datepicker -->
<script src="{{ asset(config('app.prefix').'components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset(config('app.prefix').'plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
<script>
    $(function () {
        admission_init();
    });
</script>
@endpush