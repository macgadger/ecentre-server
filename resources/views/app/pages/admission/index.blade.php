@extends('app.layout.app')
@section('title','Admissions')
@section('mod_title','Admission View')
@section('content')
<!-- Main content -->
<section class="content">
    @if(Auth::user()->role == "1")
        @component('app.components.filter', ['type' => 'admission' , 'open' => true , 'search' => false])@endcomponent
    @endif
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h2 class="box-title"><b>Admissions Manager</b></h2>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">

                    @if(Auth::user()->getRole->name == 'ADMIN')
                        <div class="row" style="padding: 10px;">
                            <div class="col-md-12">
                                <a href="{{ route('admission.create') }}" class="btn btn-md btn-danger"><i class="fa fa-plus"></i> New Admission</a>
                                <hr>
                            </div>
                        </div>
                    @endif

                    @if(session('status'))
                    <div class="alert alert-success" id="msg-alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close" style="color: white">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <p style="font-size: 18px"><strong>{{ session('status') }}</strong></p>
                    </div>
                    @endif

                    <table id="data-table" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Registration Date</th>
								<th>ID</th>
                                <th>Name</th>
                                <th>Father Name</th>
                                <th>Course</th>
                                <th>Joining Date</th>
                                <th>End Date</th>
                                <th>Join Status</th>
                                <th>Fee Status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($admissions as $key => $adms)
                            <tr>
                                <td>{{ date('d M, Y',strtotime($adms->created_at)) }}</td>
								<td>A2IT-{{ $adms->aid }}</td>
                                <td>{{ ucwords($adms->first_name.' '.$adms->last_name) }}</td>
                                <td>{{ ucwords($adms->father_name) }}</td>
                                <td>{{ strtoupper($adms->getCourse->name) }}</td>
                                <td>{{ date('d M, Y',strtotime($adms->course_joindate)) }}</td>
                                <td>{{ date('d M, Y',strtotime($adms->course_enddate)) }}</td>
                                <td>
                                    @if(Auth::user()->role == "1")
                                        @if($adms->status == 'JOINED')
                                            <a class="openBtn" data-href="{{ route('admission.status',['id' => $adms->id ]) }}" href="javascript:void(0);"><span class="label label-success">JOINED</span></a>
                                        @elseif($adms->status == 'TRASH')
                                            <a class="openBtn" data-href="{{ route('admission.status',['id' => $adms->id ]) }}" href="javascript:void(0);"><span class="label label-danger">DELETED</span></a>
                                        @elseif($adms->status == 'PENDING' && $adms->course_joindate >= $today)
                                            <a class="openBtn" data-href="{{ route('admission.status',['id' => $adms->id ]) }}" href="javascript:void(0);"><span class="label label-warning">PENDING</span></a>
                                        @elseif($adms->status == 'PENDING' && $adms->course_joindate < $today)
                                            <a class="openBtn" data-href="{{ route('admission.status',['id' => $adms->id ]) }}" href="javascript:void(0);"><span class="label label-danger">EXPIRED</span></a>
                                        @elseif($adms->status == 'DROPOUT' && $adms->course_joindate < $today)
                                            <a class="openBtn" data-href="{{ route('admission.status',['id' => $adms->id ]) }}" href="javascript:void(0);"><span class="label label-primary">DROP OUT</span></a>
                                        @elseif($adms->status == 'COMPLETED' && $adms->course_joindate < $today)
                                            <a class="openBtn" data-href="{{ route('admission.status',['id' => $adms->id ]) }}" href="javascript:void(0);"><span class="label label-success">COMPLETED</span></a>
                                        @elseif($adms->status == 'ATTNBLOCK' && $adms->course_joindate < $today)
                                            <a class="openBtn" data-href="{{ route('admission.status',['id' => $adms->id ]) }}" href="javascript:void(0);"><span class="label label-info">ATTENDANCE BLOCK</span></a>
                                        @endif
                                    @else
                                        @if($adms->status == 'PENDING' && $adms->course_joindate >= $today)
                                            <a href="javascript:void(0);"><span class="label label-warning">PENDING</span></a>
                                        @elseif($adms->status == 'PENDING' && $adms->course_joindate < $today)
                                            <a href="javascript:void(0);"><span class="label label-danger">EXPIRED</span></a>
                                        @elseif($adms->status == 'DROPOUT' && $adms->course_joindate < $today)
                                            <a href="javascript:void(0);"><span class="label label-primary">DROP OUT</span></a>
                                        @endif
                                    @endif
                                </td>
                                <td>
                                    @if($adms->payment->sum('amount') < $adms->fees->course_amount)
                                        <span class="label label-warning">DUE</span>
                                    @elseif($adms->payment->sum('amount') >= $adms->fees->course_amount )
                                        <span class="label label-success">PAID</span>
                                    @endif
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <a class="btn btn-danger btn-flat openBtn big" data-backdrop="static" data-href="{{ route('admission.show',['id' => $adms->id ]) }}" href="javascript:void(0);"><i class="fa fa-eye"></i> View</a>
                                          @if(Auth::user()->getRole->name == 'ADMIN')
                                          <a class="btn btn-danger btn-flat dropdown-toggle big" data-toggle="dropdown">
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                          </a>
                                          <ul class="dropdown-menu big" role="menu">
                                            <li><a href="{{ route('admission.edit',['id' => $adms->id ]) }}"><i class="fa fa-pencil"></i> Edit</a></li>
                                            <li>
                                                <a href="javascript:void(0);" onclick="deleteRecord('{{ route('admission.destroy',['id' => $adms->id ]) }}')">
                                                    <i class="fa fa-trash"></i> Delete
                                                </a>
                                            </li>
                                          </ul>
                                          @endif
                                    </div>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Registration Date</th>
								<th>ID</th>
                                <th>Name</th>
                                <th>Father Name</th>
                                <th>Course</th>
                                <th>Joining Date</th>
                                <th>End Date</th>
                                <th>Join Status</th>
                                <th>Fee Status</th>
                                <th>&nbsp;</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
@endsection

@push('styles')
   <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset(config('app.prefix').'components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">

  <style>
      th { font-size: 16px; }
      td { font-size: 18px; }
      .big{
        font-size: 12px;
        font-weight: bold;
      }
  </style>
@endpush

@push('scripts')
<!-- DataTables -->
<script src="{{ asset(config('app.prefix').'components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset(config('app.prefix').'components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>

<script>
    $(function () {
        $('#data-table').DataTable({
            "aaSorting": [],
        });
    });
</script>
@endpush
