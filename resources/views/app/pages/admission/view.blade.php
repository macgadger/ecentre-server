<!-- Main content -->
<section class="content">
    <!-- Custom Tabs -->
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_personel" data-toggle="tab">Personel Details</a></li>
            <li><a href="#tab_registration" data-toggle="tab">Registration Details</a></li>
            <li><a href="#tab_fee" data-toggle="tab">Fee Details</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_personel">
                <table class="table table-bordered table-hover table-condensed">
                    <tbody>
                      <tr class="techSpecRow">
                        <td class="techSpecTD1">Name:</td>
                        <td class="techSpecTD2">{{ ucwords($admission->first_name.' '.$admission->last_name) }}</td>
                      </tr>
                      <tr class="techSpecRow">
                        <td class="techSpecTD1">Mobile:</td>
                        <td class="techSpecTD2">{{ ucwords($admission->mobile) }}</td>
                      </tr>
                      <tr class="techSpecRow">
                        <td class="techSpecTD1">Email:</td>
                        <td class="techSpecTD2">{{ $admission->email }}</td>
                      </tr>
                      @if(!empty($admission->alt_mobile))
                      <tr class="techSpecRow">
                        <td class="techSpecTD1">Alt Mobile:</td>
                        <td class="techSpecTD2">{{ ucwords($admission->alt_mobile) }}</td>
                      </tr>
                      @endif
                      <tr class="techSpecRow">
                        <td class="techSpecTD1">Gender:</td>
                        <td class="techSpecTD2">{{ ucwords($admission->gender) }}</td>
                      </tr>
                      <tr class="techSpecRow">
                        <td class="techSpecTD1">Date of Birth:</td>
                        <td class="techSpecTD2">{{ date('d-m-Y',strtotime($admission->dob)) }}</td>
                      </tr>
                      <tr class="techSpecRow">
                        <td class="techSpecTD1">Address:</td>
                        <td class="techSpecTD2">{{ ucwords($admission->address) }}</td>
                      </tr>
                      @if(!empty($admission->pg_address))
                      <tr class="techSpecRow">
                        <td class="techSpecTD1">Alt Address:</td>
                        <td class="techSpecTD2">{{ ucwords($admission->pg_address) }}</td>
                      </tr>
                      @endif
                      <tr class="techSpecRow">
                        <td class="techSpecTD1">City / State / Country:</td>
                        <td class="techSpecTD2">{{ ucwords($admission->city.' / '.$admission->state.' / '.$admission->country) }}</td>
                      </tr>

                      <tr class="techSpecRow">
                        <th colspan="2" style="color: red">Education</th>
                      </tr>
                      <tr class="techSpecRow">
                        <td class="techSpecTD1">College Name:</td>
                        <td class="techSpecTD2">{{ ucwords($admission->college_name) }}</td>
                      </tr>
                      <tr class="techSpecRow">
                        <td class="techSpecTD1">Degree Type:</td>
                        <td class="techSpecTD2">{{ ucwords($admission->college_deg_type) }}</td>
                      </tr>
                      <tr class="techSpecRow">
                        <td class="techSpecTD1">Stream/Semester:</td>
                        <td class="techSpecTD2">{{ ucwords($admission->college_stream.'/'.$admission->college_semester) }}</td>
                      </tr>
                    </tbody>
                  </table>
            </div>

            <div class="tab-pane" id="tab_registration">
                <table class="table table-bordered table-hover table-condensed">
                    <tbody>
                      <tr class="techSpecRow">
                        <td class="techSpecTD1">Registration Date:</td>
                        <td class="techSpecTD2">{{ date('d-m-Y @ H:i A',strtotime($admission->created_at)) }}</td>
                      </tr>
                      <tr class="techSpecRow">
                        <td class="techSpecTD1">Main Course Name:</td>
                        <td class="techSpecTD2">{{ strtoupper($admission->getCourse->name) }}</td>
                      </tr>
                      @if(!empty($admission->other_course))
                      <tr class="techSpecRow">
                        <td class="techSpecTD1">Other Courses:</td>
                        <td class="techSpecTD2">{{ strtoupper($admission->other_course) }}</td>
                      </tr>
                      @endif
                      <tr class="techSpecRow">
                        <td class="techSpecTD1">Course Duration:</td>
                        <td class="techSpecTD2">{{ ucwords($admission->course_durartion) }}</td>
                      </tr>
                      <tr class="techSpecRow">
                        <td class="techSpecTD1">Course Join Date:</td>
                        <td class="techSpecTD2">{{ date('d-m-Y',strtotime($admission->course_joindate)) }}</td>
                      </tr>
                      <tr class="techSpecRow">
                        <td class="techSpecTD1">Course End Date:</td>
                        <td class="techSpecTD2">{{ date('d-m-Y',strtotime($admission->course_enddate)) }}</td>
                      </tr>
                      <tr class="techSpecRow">
                        <td class="techSpecTD1">Course Time:</td>
                        <td class="techSpecTD2">{{ ucwords($admission->course_time) }}</td>
                      </tr>
                      <tr class="techSpecRow">
                        <td class="techSpecTD1">Referred By:</td>
                        <td class="techSpecTD2">{{ ucwords($admission->refer_by) }}</td>
                      </tr>
                      <tr class="techSpecRow">
                        <td class="techSpecTD1">Referred Name:</td>
                        <td class="techSpecTD2">{{ ucwords($admission->refer_name) }}</td>
                      </tr>
                      <tr class="techSpecRow">
                        <td class="techSpecTD1">Admission By:</td>
                        <td class="techSpecTD2">{{ ucwords($admission->profile->full_name) }}</td>
                      </tr>
                    </tbody>
                </table>
            </div>
            <div class="tab-pane" id="tab_fee">
                <table class="table table-bordered table-hover table-condensed">
                    <tbody>
                      <tr class="techSpecRow">
                        <td class="techSpecTD1">Course Fee:</td>
                        <td class="techSpecTD2"><i class="fa fa-rupee"></i> {{ $admission->course_fee }}</td>
                      </tr>
                      <tr class="techSpecRow">
                        <td class="techSpecTD1">Advance Deposit:</td>
                        <td class="techSpecTD2"><i class="fa fa-rupee"></i> {{ $admission->course_advance }}</td>
                      </tr>
                      <tr class="techSpecRow">
                        <td class="techSpecTD1">Payment Mode:</td>
                        <td class="techSpecTD2">{{ strtoupper($admission->payment_mode) }}</td>
                      </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.tab-content -->
    </div>
    <!-- nav-tabs-custom -->
</section>

<div class="modal-footer">
  <button type="button" class="btn btn-danger btn-lg" data-dismiss="modal">Close</button>
</div>
