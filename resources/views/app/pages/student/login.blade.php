
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Login | {{ env('APP_NAME', 'Edu CRM') }}</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="robots" content="index, follow">
    
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{ asset(config('app.prefix').'components/bootstrap/dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset(config('app.prefix').'components/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset(config('app.prefix').'components/Ionicons/css/ionicons.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset(config('app.prefix').'dist/css/AdminLTE.min.css') }}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{ asset(config('app.prefix').'plugins/iCheck/square/blue.css') }}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="login-box" style="">
  <div class="login-logo">
    <a href="/"><b>{{ env('APP_NAME', 'Edu CRM') }}</b></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>

    <form class="validate-form" method="post" action="{{ route('login.student')}}" data-toggle="validator" >
      @csrf
	  
		 @if(session('error'))
             <div class="alert alert-danger">
                 <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                 </button>
                 {{ session('error') }}
             </div>
      @endif
		
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Phone Number" name="phone" required>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        <div class="help-block with-errors"></div>
      </div>

      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Admission AID" name="aid" required>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        <div class="help-block with-errors"></div>
      </div>

      <div class="row">
        <div class="col-xs-12">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
      </div>
    </form>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="{{ asset(config('app.prefix').'components/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset(config('app.prefix').'components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset(config('app.prefix').'plugins/validator.js') }}"></script>

</body>
</html>
