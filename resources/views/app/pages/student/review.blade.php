@extends('app.layout.app')
@section('title','Review Trainer')
@section('content')
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h2><b style="color: red">Review Trainer</b></h2>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse"> <i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">

                    @if($errors->count())
                    @foreach ($errors->all() as $message)
                    <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        {{ $message }}
                    </div>
                    @endforeach
                    @endif
                    @if($review)
                        <h4 style="color:blue">You have already submitted the review for this month. To submit new review please come back after one month.<br>Your last review was on ({{ ($review->created_at)->format('d M, Y')}})</h4>
                        <a href="/student/dashboard" class="btn btn-primary">Go Back</a>
                    @else
                    <form name="reviewForm" id="reviewForm" method="POST" action="{{ route('review.trainer') }}" data-toggle="validator">
                        @csrf
            
                        <div class="row">
                            <div class="col-md-12">
                                <fieldset class="starability-basic">
                                    <legend style="font-size: 20px; font-weight: bold">What to do think about A2IT?</legend>
                                    <input type="radio" id="no-rate" class="input-no-rate" name="a2it_rating" value="0" checked aria-label="No rating." />
                                    
                                    <input type="radio" id="a2it_rate1" name="a2it_rating" value="1" />
                                    <label for="a2it_rate1" title="Unsatisfactory">1 stars</label>
                            
                                    <input type="radio" id="a2it_rate2" name="a2it_rating" value="2" />
                                    <label for="a2it_rate2" title="Fair">2 stars</label>
                            
                                    <input type="radio" id="a2it_rate3" name="a2it_rating" value="3" />
                                    <label for="a2it_rate3" title="Satisfactory">3 stars</label>
                            
                                    <input type="radio" id="a2it_rate4" name="a2it_rating" value="4" />
                                    <label for="a2it_rate4" title="Very Good">4 stars</label>
                            
                                    <input type="radio" id="a2it_rate5" name="a2it_rating" value="5" />
                                    <label for="a2it_rate5" title="Excellent">5 star</label>
                                </fieldset>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <legend style="font-size: 20px; font-weight: bold">Trainer Name</legend>
                                    <input type="text" readonly class="form-control" value="{{ $admission->batch->trainer->full_name }}"/>
                                    </select>
                                </div>
                            </div>
                        </div>
    
                        <div class="row">
                            <div class="col-md-12">
                                <fieldset class="starability-basic">
                                    <legend style="font-size: 20px; font-weight: bold; padding-top: 10px">Subject matter knowledge/Command on subject?</legend>
                                    <input type="radio" id="no-rate" class="input-no-rate" name="tr_sub_rate" value="0" checked aria-label="No rating." />
                                    
                                    <input type="radio" id="tr_sub_rate1" name="tr_sub_rate" value="1" />
                                    <label for="tr_sub_rate1" title="Unsatisfactory">1 stars</label>
                            
                                    <input type="radio" id="tr_sub_rate2" name="tr_sub_rate" value="2" />
                                    <label for="tr_sub_rate2" title="Fair">2 stars</label>
                            
                                    <input type="radio" id="tr_sub_rate3" name="tr_sub_rate" value="3" />
                                    <label for="tr_sub_rate3" title="Satisfactory">3 stars</label>
                            
                                    <input type="radio" id="tr_sub_rate4" name="tr_sub_rate" value="4" />
                                    <label for="tr_sub_rate4" title="Very Good">4 stars</label>
                            
                                    <input type="radio" id="tr_sub_rate5" name="tr_sub_rate" value="5" />
                                    <label for="tr_sub_rate5" title="Excellent">5 star</label>
                                </fieldset>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-12">
                                <fieldset class="starability-basic">
                                    <legend style="font-size: 20px; font-weight: bold; padding-top: 10px">Presented subject matter clearly and systematically?</legend>
                                    <input type="radio" id="no-rate" class="input-no-rate" name="tr_pres_rate" value="0" checked aria-label="No rating." />
                                    
                                    <input type="radio" id="tr_pres_rate1" name="tr_pres_rate" value="1" />
                                    <label for="tr_pres_rate1" title="Unsatisfactory">1 stars</label>
                            
                                    <input type="radio" id="tr_pres_rate2" name="tr_pres_rate" value="2" />
                                    <label for="tr_pres_rate2" title="Fair">2 stars</label>
                            
                                    <input type="radio" id="tr_pres_rate3" name="tr_pres_rate" value="3" />
                                    <label for="tr_pres_rate3" title="Satisfactory">3 stars</label>
                            
                                    <input type="radio" id="tr_pres_rate4" name="tr_pres_rate" value="4" />
                                    <label for="tr_pres_rate4" title="Very Good">4 stars</label>
                            
                                    <input type="radio" id="tr_pres_rate5" name="tr_pres_rate" value="5" />
                                    <label for="tr_pres_rate5" title="Excellent">5 star</label>
                                </fieldset>
                            </div>
                        </div>
    
                        <div class="row">
                            <div class="col-md-12">
                                <fieldset class="starability-basic">
                                    <legend style="font-size: 20px; font-weight: bold; padding-top: 10px">Time management?</legend>
                                    <input type="radio" id="no-rate" class="input-no-rate" name="tr_time_rate" value="0" checked aria-label="No rating." />
                                    
                                    <input type="radio" id="tr_time_rate1" name="tr_time_rate" value="1" />
                                    <label for="tr_time_rate1" title="Unsatisfactory">1 stars</label>
                            
                                    <input type="radio" id="tr_time_rate2" name="tr_time_rate" value="2" />
                                    <label for="tr_time_rate2" title="Fair">2 stars</label>
                            
                                    <input type="radio" id="tr_time_rate3" name="tr_time_rate" value="3" />
                                    <label for="tr_time_rate3" title="Satisfactory">3 stars</label>
                            
                                    <input type="radio" id="tr_time_rate4" name="tr_time_rate" value="4" />
                                    <label for="tr_time_rate4" title="Very Good">4 stars</label>
                            
                                    <input type="radio" id="tr_time_rate5" name="tr_time_rate" value="5" />
                                    <label for="tr_time_rate5" title="Excellent">5 star</label>
                                </fieldset>
                            </div>
                        </div>
    
                        <div class="row">
                            <div class="col-md-12">
                                <fieldset class="starability-basic">
                                    <legend style="font-size: 20px; font-weight: bold; padding-top: 10px">Encouraged students to ask questions/interaction with individual student?</legend>
                                    <input type="radio" id="no-rate" class="input-no-rate" name="tr_enc_rate" value="0" checked aria-label="No rating." />
                                    
                                    <input type="radio" id="tr_enc_rate1" name="tr_enc_rate" value="1" />
                                    <label for="tr_enc_rate1" title="Unsatisfactory">1 stars</label>
                            
                                    <input type="radio" id="tr_enc_rate2" name="tr_enc_rate" value="2" />
                                    <label for="tr_enc_rate2" title="Fair">2 stars</label>
                            
                                    <input type="radio" id="tr_enc_rate3" name="tr_enc_rate" value="3" />
                                    <label for="tr_enc_rate3" title="Satisfactory">3 stars</label>
                            
                                    <input type="radio" id="tr_enc_rate4" name="tr_enc_rate" value="4" />
                                    <label for="tr_enc_rate4" title="Very Good">4 stars</label>
                            
                                    <input type="radio" id="tr_enc_rate5" name="tr_enc_rate" value="5" />
                                    <label for="tr_enc_rate5" title="Excellent">5 star</label>
                                </fieldset>
                            </div>
                        </div>
                        <hr>
    
                        <!-- Project Detail-->
    
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <legend style="font-size: 20px; font-weight: bold">Project</legend>
                                   <input type="text" name="pro_name" class="form-control" required/>
                                </div>
                            </div>
                        </div>
    
                        <div class="row">
                            <div class="col-md-12">
                                <fieldset class="starability-basic">
                                    <legend style="font-size: 20px; font-weight: bold; padding-top: 10px">Satisfaction level about project undertaken?</legend>
                                    <input type="radio" id="no-rate" class="input-no-rate" name="pro_sat_rate" value="0" checked aria-label="No rating." />
                                    
                                    <input type="radio" id="pro_sat_rate1" name="pro_sat_rate" value="1" />
                                    <label for="pro_sat_rate1" title="Unsatisfactory">1 stars</label>
                            
                                    <input type="radio" id="pro_sat_rate2" name="pro_sat_rate" value="2" />
                                    <label for="pro_sat_rate2" title="Fair">2 stars</label>
                            
                                    <input type="radio" id="pro_sat_rate3" name="pro_sat_rate" value="3" />
                                    <label for="pro_sat_rate3" title="Satisfactory">3 stars</label>
                            
                                    <input type="radio" id="pro_sat_rate4" name="pro_sat_rate" value="4" />
                                    <label for="pro_sat_rate4" title="Very Good">4 stars</label>
                            
                                    <input type="radio" id="pro_sat_rate5" name="pro_sat_rate" value="5" />
                                    <label for="pro_sat_rate5" title="Excellent">5 star</label>
                                </fieldset>
                            </div>
                        </div>
    
                        <hr>
    
                        <!-- Academic Counselor -->
    
                        <div class="row">
                            <div class="col-md-12">
                                <legend style="font-size: 20px; font-weight: bold; padding-top: 10px">Was knowledgeable and helped me to consider options & examine my alternatives?</legend>
                                <label>
                                    <input type="radio" name="was_kno" value="yes" required class="flat-red" >
                                    Yes
                                </label>
                                <label>
                                    <input type="radio" name="was_kno" value="no" required class="flat-red" >
                                    No
                                </label>
                                    
                            </div>
                        </div>
    
                        <div class="row">
                            <div class="col-md-12">
                                <legend style="font-size: 20px; font-weight: bold; padding-top: 10px">Was all commitments fulfilled that made by counselor during counseling?</legend>
                                <label>
                                    <input type="radio" name="was_all" value="yes" required class="flat-red" >
                                    Yes
                                </label>
                                <label>
                                    <input type="radio" name="was_all" value="no" required class="flat-red" >
                                    No
                                </label>
                                
                            </div>
                        </div>
                        
                        <hr>
    
                        <!-- Coordination & infrastructure -->
    
                        <div class="row">
                            <div class="col-md-12">
                                <fieldset class="starability-basic">
                                    <legend style="font-size: 20px; font-weight: bold; padding-top: 10px">Cooperation provided by other staff?</legend>
                                    <input type="radio" id="no-rate" class="input-no-rate" name="cor_staf_rate" value="0" checked aria-label="No rating." />
                                    
                                    <input type="radio" id="cor_staf_rate1" name="cor_staf_rate" value="1" />
                                    <label for="cor_staf_rate1" title="Unsatisfactory">1 stars</label>
                            
                                    <input type="radio" id="cor_staf_rate2" name="cor_staf_rate" value="2" />
                                    <label for="cor_staf_rate2" title="Fair">2 stars</label>
                            
                                    <input type="radio" id="cor_staf_rate3" name="cor_staf_rate" value="3" />
                                    <label for="cor_staf_rate3" title="Satisfactory">3 stars</label>
                            
                                    <input type="radio" id="cor_staf_rate4" name="cor_staf_rate" value="4" />
                                    <label for="cor_staf_rate4" title="Very Good">4 stars</label>
                            
                                    <input type="radio" id="cor_staf_rate5" name="cor_staf_rate" value="5" />
                                    <label for="cor_staf_rate5" title="Excellent">5 star</label>
                                </fieldset>
                            </div>
                        </div>
    
                        <div class="row">
                            <div class="col-md-12">
                                <fieldset class="starability-basic">
                                    <legend style="font-size: 20px; font-weight: bold; padding-top: 10px">Labs are fully equipped with latest devices?</legend>
                                    <input type="radio" id="no-rate" class="input-no-rate" name="cor_lab_rate" value="0" checked aria-label="No rating." />
                                    
                                    <input type="radio" id="cor_lab_rate1" name="cor_lab_rate" value="1" />
                                    <label for="cor_lab_rate1" title="Unsatisfactory">1 stars</label>
                            
                                    <input type="radio" id="cor_lab_rate2" name="cor_lab_rate" value="2" />
                                    <label for="cor_lab_rate2" title="Fair">2 stars</label>
                            
                                    <input type="radio" id="cor_lab_rate3" name="cor_lab_rate" value="3" />
                                    <label for="cor_lab_rate3" title="Satisfactory">3 stars</label>
                            
                                    <input type="radio" id="cor_lab_rate4" name="cor_lab_rate" value="4" />
                                    <label for="cor_lab_rate4" title="Very Good">4 stars</label>
                            
                                    <input type="radio" id="cor_lab_rate5" name="cor_lab_rate" value="5" />
                                    <label for="cor_lab_rate5" title="Excellent">5 star</label>
                                </fieldset>
                            </div>
                        </div>
    
                        <div class="row">
                            <div class="col-md-12">
                                <legend style="font-size: 20px; font-weight: bold; padding-top: 10px">Overall, I am satisfied with experience I had in this organization</legend>
                                <label>
                                    <input type="radio" name="ovr_sat" value="yes" required class="flat-red" >
                                    Yes
                                </label>
                                <label>
                                    <input type="radio" name="ovr_sat" value="no" required class="flat-red" >
                                    No
                                </label>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary btn-block" style="margin-top: 20px">Submit</button>
                    </form>
                    @endif
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>

@endsection

@push('styles')
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset(config('app.prefix').'components/select2/dist/css/select2.min.css') }}">

<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{ asset(config('app.prefix').'components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset(config('app.prefix').'plugins/timepicker/bootstrap-timepicker.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset(config('app.prefix').'starability-minified/starability-all.min.css') }}"/>
<link rel="stylesheet" href="{{asset(config('app.prefix').'plugins/iCheck/all.css')}}">
@endpush

@push('scripts')
<!-- Select2 -->
<script src="{{ asset(config('app.prefix').'components/select2/dist/js/select2.full.min.js') }}"></script>

<!-- bootstrap datepicker -->
<script src="{{ asset(config('app.prefix').'components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset(config('app.prefix').'plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
<script src="{{asset(config('app.prefix').'plugins/iCheck/icheck.min.js')}}"></script>
<script>
//Flat red color scheme for iCheck
$('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

</script>
@endpush