@extends('app.layout.app')
@section('title','Pay Fee')
@section('content')
<!-- Main content -->
<section class="content">

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h2 class="box-title"><b>Search</b></h2>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse"> <i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">

                        <form name="feesearchForm" id="feesearchForm" method="POST" action="../fee/search" data-toggle="validator">
                            @csrf
                            <fieldset>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group has-feedback">
                                        <div class="input-group">
                                            <input type="text" name="query"  id="query" class="form-control" placeholder="Search for Student Name/ID" required>
                                            <span class="input-group-btn">
                                                <button class="btn btn-danger" type="submit">Search</button>
                                            </span>
                                        </div>
                                        <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">&nbsp;</div>
                                </div>
                            </fieldset>
                        </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row" id="result-fail" style="display: none">
        <div class="col-xs-12">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h2 class="box-title"><b>Search Results</b></h2>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"> <i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <h3>No Results found</h3>
                </div>
            </div>
        </div>
    </div>

    <div class="row" id="result-row" style="display: none">
            <div class="col-xs-12">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h2 class="box-title"><b>Search Results</b></h2>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"> <i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                            <table id="data-table" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Phone</th>
                                            <th>Course</th>
                                            <th>Joining Date</th>
                                            <th>Fee Due</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody id="table-data">
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Phone</th>
                                            <th>Course</th>
                                            <th>Joining Date</th>
                                            <th>Fee Due</th>
                                            <th>&nbsp;</th>
                                        </tr>
                                    </tfoot>
                                </table>
                           
                    </div>
                </div>
            </div>
        </div>

</section>
@endsection

@push('styles')
   <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset(config('app.prefix').'components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
  <style>
      th { font-size: 16px; }
      td { font-size: 20px; }
      .big{
        font-size: 16px;
        font-weight: bold;
      }
  </style>
@endpush

@push('scripts')
<!-- DataTables -->
<script src="{{ asset(config('app.prefix').'components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset(config('app.prefix').'components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>

<script>
    $(function () {
        $('#data-table').DataTable({
            "aaSorting": [],
        });

        $('#feesearchForm').on('submit',function(event)
        {
            event.preventDefault();
            if($('#query').val() === ''){
                alert('Enter Search Query');
            }
            else{
            $.ajax({
                url:"../fee/search",
                method: 'POST',
                data: $('#feesearchForm').serialize(),
                success: function(data)
                {
                    if(data.length == 0){
                        $('#result-row').hide();
                        $('#result-fail').show();
                    }else{
                        $('#result-fail').hide();
                        $('#result-row').show();
                        $('#table-data').html(data);
                    }
                }
            });
        }
        });
    })
</script>
@endpush