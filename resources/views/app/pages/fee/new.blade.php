@extends('app.layout.app')
@section('title','Pay Fee')
@section('content')
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h2 class="box-title"><b>Pay Fee</b></h2>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse"> <i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">

                    @if($errors->count())
                    @foreach ($errors->all() as $message)
                    <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        {{ $message }}
                    </div>
                    @endforeach
                    @endif

                    <form name="feeForm" id="feeForm" method="POST" action="/account/app/fee" data-toggle="validator">
                        @csrf

                        <input type="hidden" name="sid"   value="{{  $admission->id }}" readonly required="required">
                        <input type="hidden" name="aid"   value="{{  $admission->admission_uid }}" readonly required="required">
                        <input type="hidden" name="total" value="{{  $admission->fees->course_fee }}" readonly required="required">
                        <input type="hidden" name="due"   value="{{  $admission->fees->due_amount }}" readonly required="required">
                        <input type="hidden" name="cousellor_id"   value="{{  $admission->admission_by }}" readonly required="required">
                        
                        <div class="row">
                            <div class="col-md-4">
                                <fieldset>
                                    <legend style="font-weight:bold; font-size: 22px; color: red;">Payment Info</legend>

                                    <div class="form-group has-feedback">
                                        <label class="control-label">Amount</label>
                                        <input type="text" class="form-control" name="amount"
                                            placeholder="Enter amount to Pay" value="{{ old('amount') }}"
                                            autocomplete="off" required="required">
                                        <div class="help-block with-errors"></div>
                                    </div>

                                    <div class="form-group has-feedback">
                                        <label class="control-label">Next Payment Date</label>
                                        <input type="text" class="form-control datepicker" name="date_due"
                                            placeholder="Enter Next Payment Date" value="{{ old('date_due') }}"
                                            autocomplete="off" required="required">
                                        <div class="help-block with-errors"></div>
                                    </div>

                                    <div class="form-group has-feedback">
                                        <label class="control-label">Reciept No./Transaction ID</label>
                                        <input type="text" class="form-control" name="payment_reciept"
                                            placeholder="Enter Reciept/Transaction Number"
                                            autocomplete="off" value="{{ old('payment_reciept') }}"
                                            required="required">
                                        <div class="help-block with-errors"></div>
                                    </div>

                                    <div class="form-group has-feedback">
                                            <label class="control-label">Remarks</label>
                                            <textarea class="form-control" name="remarks" autocomplete="off">{{ old('remarks') }}</textarea>
                                            <div class="help-block with-errors"></div>
                                    </div>
    
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Payment Mode</label>
                                        <select class="form-control" name="payment_mode" autocomplete="off" required>
                                            <option value="">Select Mode</option>
                                            <option value="cash"   {{ ( old("payment_mode") == "cash"   ? 'selected' :"") }}>Cash</option>
                                            <option value="online" {{ ( old("payment_mode") == "online" ? 'selected' :"") }}>Online Bank Transfer</option>
                                            <option value="qrcode" {{ ( old("payment_mode") == "qrcode" ? 'selected' :"") }}>QR Code</option>
                                            <option value="paytm"  {{ ( old("payment_mode") == "paytm"  ? 'selected' :"") }}>Paytm</option>
                                            <option value="gpay"   {{ ( old("payment_mode") == "gpay"   ? 'selected' :"") }}>GooglePay</option>
                                        </select>
                                        <div class="help-block with-errors"></div>
                                    </div>

                                    
                                </fieldset>
                            </div>

                            <div class="col-md-4">
                                <fieldset>

                                    <legend style="font-weight:bold; font-size: 22px; color: red;">Student Info</legend>
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr class="active" style="font-size: 18px;">
                                                <th>Student Name: </th>
                                                <th>{{ ucwords($admission->first_name.' '.$admission->last_name) }}</th>
                                            </tr>
                                            <tr class="active" style="font-size: 18px;">
                                                <th>ID: </th>
                                                <th>A2IT-{{ $admission->aid }}</th>
                                            </tr>
                                        </thead>
                                        <tbody style="font-size: 16px;">
                                            <tr>
                                                <td>Course Name</td>
                                                <td>{{ strtoupper($admission->getCourse->name) }}</td>
                                            </tr>
                                            @if(!empty($admission->other_course))
                                            <tr>
                                                <td>Other Course(s)</td>
                                                <td>{{ strtoupper($admission->other_course) }}</td>
                                            </tr>
                                            @endif
                                            <tr>
                                                <td>Course Fee</td>
                                                <td><i class="fa fa-rupee"></i> {{ $admission->course_fee }}/-</td>
                                            </tr>
                                            <tr style="border-top: red dotted 2px; border-left: red dotted 2px; border-right: red dotted 2px">
                                                <td>Fee DUE</td>
                                                <td style="color: red;"><i class="fa fa-rupee"></i> {{ $admission->fees->due_amount }}/-
                                                </td>
                                            </tr>
                                            <tr style="border-bottom: red dotted 2px; border-left: red dotted 2px; border-right: red dotted 2px">
                                                <td>DUE Date</td>
                                                <td style="color: red;">{{ date('d M, Y',strtotime($admission->fees->due_date)) }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </fieldset>
                                {{-- <fieldset>
                                    <legend style="font-weight:bold; font-size: 22px; color: red;">Pay using QR Code</legend>
                                    <img src="{{ asset(config('app.prefix').'uploads/qrcode/qr.png') }}" class="img-responsive" width="150px" height="150px" style="display:block; margin:auto;" />
                                </fieldset> --}}
                                <hr>
                                <button type="submit" class="btn btn-lg btn-block btn-danger">Update and Pay</button>
                            </div>

                            <div class="col-md-4 text-center">
                                <fieldset>
                                    <legend style="font-weight:bold; font-size: 22px; color: red;">Payment History</legend>
                                    <table class="table table-bordered table-strip table-hover table-condensed">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Payment</th>
                                                <th>Recieved On</th>
                                                <th>Mode</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($admission->payment as $key => $payment)
                                                    <tr>
                                                       <td align="left">{{ $key+1 }}</td>
                                                       <td align="left"><i class="fa fa-rupee"> {{ $payment->amount }}</td>
                                                       <td align="left">{{ date('d M, Y',strtotime($payment->created_at)) }}</td>
                                                       <td align="left">{{ strtoupper($payment->payment_mode) }}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                    </table>
                                </fieldset>
                            </div>

                        </div>

                    </form>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

</section>
@endsection

@push('styles')
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset(config('app.prefix').'components/select2/dist/css/select2.min.css') }}">

<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="{{ asset(config('app.prefix').'plugins/iCheck/all.css') }}">

<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{ asset(config('app.prefix').'components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<style>
    .form-control{
        text-transform: uppercase;
    }
</style>
@endpush

@push('scripts')
<!-- Select2 -->
<script src="{{ asset(config('app.prefix').'components/select2/dist/js/select2.full.min.js') }}"></script>

<!-- iCheck 1.0.1 -->
<script src="{{ asset(config('app.prefix').'plugins/iCheck/icheck.min.js') }}"></script>

<!-- bootstrap datepicker -->
<script src="{{ asset(config('app.prefix').'components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script>
    $(function () {
        //Date picker
        $('.datepicker').datepicker({
            autoclose: true,
            format: "dd-mm-yyyy",
            todayHighlight: true,
            todayBtn: true,
            clearBtn: true,
        });

        //iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-red',
            radioClass   : 'iradio_flat-red'
        });
    });
</script>
@endpush