@extends('app.layout.app')
@section('title','Fees')
@section('mod_title','Fees View')
@section('content')
<!-- Main content -->
<section class="content">
  

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h2 class="box-title"><b>Fees Manager</b></h2>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">

                    @if(session('status'))
                    <div class="alert alert-success" id="msg-alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close" style="color: white">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <p style="font-size: 18px"><strong>{{ session('status') }}</strong></p>
                    </div>
                    @endif

                    <table id="data-table" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th></th>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Course</th>
                                <th>Course Fees</th>
                                <th>Last Payment</th>
                                <th>Paid On</th>
                                <th>Amount Due</th>
                                <th>Due Date</th>
								<th>Counselor</th>
                                <th>Fee Status</th>
                               
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($admission as $key => $adms)
                            @if ($adms->fees != NULL)   
                            <tr data-child-value="{{ $adms->payment }}">
                                <td class="details-control"></td>
                                <td>A2IT-{{ $adms->aid }}</td>
                                <td>{{ $adms->full_name }}</td>
                                <td>{{ strtoupper($adms->getCourse->name) }}</td>
                                <td><i class="fa fa-rupee"></i>{{ $adms->fees->course_amount }}</td>
                                <td><i class="fa fa-rupee"></i>{{ $adms->fees->last_payment }}</td>
                                <td>{{ date('d M, Y',strtotime($adms->fees->created_at)) }}</td>
                                <td><i class="fa fa-rupee"></i>{{ $adms->fees->due_amount }}</td>
                                <td>{{ date('d M, Y',strtotime($adms->fees->due_date)) }}</td>
								<td>{{ $adms->profile->first_name.' '.$adms->profile->last_name }}</td>	
                                <td>
                                    @if($adms->payment->sum('amount') < $adms->fees->course_amount)
                                        <span class="label label-warning">DUE</span>
                                    @elseif($adms->payment->sum('amount') >= $adms->fees->course_amount )
                                        <span class="label label-success">PAID</span>
                                    @endif
                                </td>
                                
                            </tr>
                            @endif
                            @endforeach
                            
                        </tbody>
                        
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
@endsection

@push('styles')
   <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset(config('app.prefix').'components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
  <style>
      th { font-size: 16px; }
      td { font-size: 18px; }
      .big{
        font-size: 12px;
        font-weight: bold;
      }
       td.details-control {
        background: url('http://www.datatables.net/examples/resources/details_open.png') no-repeat center center;
        cursor: pointer;
        }
        tr.shown td.details-control {
            background: url('http://www.datatables.net/examples/resources/details_close.png') no-repeat center center;
        }
  </style>
@endpush

@push('scripts')
<!-- DataTables -->
<script src="{{ asset(config('app.prefix').'components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset(config('app.prefix').'components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>

<script src="{{ asset(config('app.prefix').'components/moment/min/moment.min.js') }}"></script>

<script>
    function format(value) {
        var data = "<table class='table table-condensed table-bordered' style='border: dashed 2px red'>";
        data+= "<thead><tr>";
        data+= "<th>Reciept #</th>";
        data+= "<th>Payment Mode</th>";
        data+= "<th>Payment Type</th>";
        data+= "<th>Payment</th>";
        data+= "<th>Recieved On</th>";
        data+= "</tr></thead>";
        data+= "<tbody>";
        var total = 0;
        value.forEach(function(element) {
            data+= "<tr>";
                data+= "<td>"+element.reciept_no.toUpperCase()+"</td>";
                data+= "<td>"+element.payment_mode.toUpperCase()+"</td>";
                data+= "<td>"+element.type.toUpperCase()+"</td>";
                data+= "<td><i class='fa fa-rupee'></i>"+element.amount+"</td>";
                data+= "<td>"+moment(element.created_at).format('D MMM, YYYY')+"</td>";
            data+= "</tr>";
            total = total + element.amount;
        });
        data+= "</tbody><tfoot style='border-top: dashed 2px red'>";
        data+= "<tr>";
            data+= "<td>&nbsp;</td>";
            data+= "<td>&nbsp;</td>";
            data+= "<td align='right' style='font-size: 20px; color: green'>Total: </td>";
            data+= "<td style='font-size: 20px; color: green'><i class='fa fa-rupee'></i>"+total+"/-</td>";
            data+= "<td>&nbsp;</td>";
        data+= "</tr></tfoot>";
        return data;
    }
    
    $(function () {
        var table = $('#data-table').DataTable({
            "aaSorting": [],
        });

        // Add event listener for opening and closing details
        $('#data-table').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            } else {
                // Open this row
                row.child(format(tr.data('child-value'))).show();
                tr.addClass('shown');
            }
        });
    })
</script>
@endpush