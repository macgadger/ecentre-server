@extends('app.layout.app')
@section('title','Fees Collection')
@section('mod_title','Fees View')
@section('content')
<!-- Main content -->
<section class="content">
    @component('app.components.filter', ['type' => 'fees' , 'open' => true , 'search' => false])@endcomponent
    
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h2 class="box-title"><b>Today Fee Collection</b></h2>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">

                    @if(session('status'))
                    <div class="alert alert-success" id="msg-alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close" style="color: white">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <p style="font-size: 18px"><strong>{{ session('status') }}</strong></p>
                    </div>
                    @endif

                    <table id="data-table" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Reciept #</th>
                                <th>Payment Mode</th>
                                <th>Payment Type</th>
                                <th>Payment</th>
                                <th>Recieved On</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($admission as $key => $adms)
                            <tr>
                                <td>A2IT-{{ $adms->admission->aid }}</td>
                                <td>{{ $adms->admission->full_name }}</td>
                                <td>{{ strtoupper($adms->reciept_no) }}</td>
                                <td>{{ strtoupper($adms->payment_mode) }}</td>
                                <td>{{ strtoupper($adms->type) }}</td>
                                <td><i class="fa fa-rupee"></i>{{ $adms->amount }}</td>
                                <td>{{ date('d M, Y',strtotime($adms->created_at)) }}</td>
                                <td>
                                    <a class='btn btn-danger btn-flat big' href='{{ route('fee.pay',['id' => $adms->id ]) }}'><i class='fa fa-pencil'></i> Edit</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>&nbsp;</th>
                                <th>&nbsp;</th>
                                <th>&nbsp;</th>
                                <th>&nbsp;</th>
                                <th style='font-size: 20px; color: green; border: dashed 2px red'>Total: </th>                                                                
                                <th style='font-size: 20px; color: green; border: dashed 2px red'><i class="fa fa-rupee"></i> {{ $total }}</th>                       
                                <th>&nbsp;</th>
                            </tr>
                        </tfoot>
                        
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
@endsection

@push('styles')
   <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset(config('app.prefix').'components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
  <style>
      th { font-size: 16px; }
      td { font-size: 18px; }
      .big{
        font-size: 12px;
        font-weight: bold;
      }
  </style>
@endpush

@push('scripts')
<!-- DataTables -->
<script src="{{ asset(config('app.prefix').'components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset(config('app.prefix').'components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>

<script>
    $(function () {
        var table = $('#data-table').DataTable({
            "aaSorting": [],
        });
    })
</script>
@endpush