@extends('app.layout.app')
@section('title','Inquiry')
@section('mod_title','Inquiry View')
@section('content')
<!-- Main content -->
<section class="content">

    @include('app.inc.searchbar')

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h2 class="box-title"><b>Inquiry Manager</b></h2>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">

                    <div class="row" style="padding: 10px;">
                        <div class="col-md-12">
                            <a href="./inquiry/create" class="btn btn-md btn-danger"><i class="fa fa-plus"></i> New Inquiry</a>
                            <hr>
                        </div>
                    </div>

                    @if(session('status'))
                    <div class="alert alert-success" id="msg-alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close" style="color: white">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <p style="font-size: 18px"><strong>{{ session('status') }}</strong></p>
                    </div>
                    @endif

                    <table id="data-table" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Inquiry Date</th>
                                <th>Name</th>
                                <th>Phone</th>
                                <th>Course</th>
                                <th>Expected Joining Date</th>                                                                
                                <th>Lead Status</th>
                                @if(Auth::user()->getRole->can_view == 1)                                
                                    <th>Actions</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($inquiry as $key => $inqs)   
                            <tr>
                                <td>{{ date('d M, Y',strtotime($inqs->created_at)) }}</td>
                                <td>{{ ucwords($inqs->first_name.' '.$inqs->last_name) }}</td>
                                <td>{{ strtoupper($inqs->mobile) }}</td>
                                <td>{{ strtoupper($inqs->course_name) }}</td>                                
                                <td>{{ date('d M, Y',strtotime($inqs->course_join_date)) }}</td>                                                                
                                <td>
                                    @if($inqs->status == 'OPEN')
                                        <span class="label label-warning">{{ strtoupper($inqs->status) }}</span>
                                    @elseif($inqs->status == 'CLOSE')
                                        <span class="label label-danger">{{ strtoupper($inqs->status) }}</span>
                                    @elseif($inqs->status == 'CONVERT')
                                        <span class="label label-success">{{ strtoupper($inqs->status) }}</span>
                                    @endif
                                </td>
                                
                                @if(Auth::user()->getRole->can_view == 1)
                                <td>
                                    <div class="btn-group">
                                        <a class="btn btn-danger btn-flat openBtn big" data-href="./admission/{{ $inqs->id }}" href="javascript:void(0);"><i class="fa fa-eye"></i> View</a>
                                          <a class="btn btn-danger btn-flat dropdown-toggle big" data-toggle="dropdown">
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                          </a>
                                          <ul class="dropdown-menu big" role="menu">
                                            @if(Auth::user()->getRole->can_update == 1)
                                                <li><a href="./"><i class="fa fa-pencil"></i> Edit</a></li>
                                            @endif
                                            @if(Auth::user()->getRole->can_remove == 1)
                                                <li><a href="#"><i class="fa fa-trash"></i> Delete</a></li>
                                            @endif
                                          </ul>
                                    </div>
                                </td>
                                @endif

                            </tr>
                            @endforeach
                            
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Inquiry Date</th>
                                <th>Name</th>
                                <th>Phone</th>
                                <th>Course</th>
                                <th>Expected Joining Date</th>                                                                
                                <th>Lead Status</th>
                                @if(Auth::user()->getRole->can_view == 1)                                
                                <th>&nbsp;</th>
                                @endif
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
@endsection

@push('styles')
   <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset(config('app.prefix').'components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
  <style>
      th { font-size: 16px; }
      td { font-size: 18px; }
      .big{
        font-size: 12px;
        font-weight: bold;
      }
  </style>
@endpush

@push('scripts')
<!-- DataTables -->
<script src="{{ asset(config('app.prefix').'components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset(config('app.prefix').'components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>

<script>
    $(function () {
        $('#data-table').DataTable({
            "aaSorting": [],
        });
    })
</script>
@endpush