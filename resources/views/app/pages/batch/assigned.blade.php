@extends('app.layout.app')
@section('title','Assigned Batches')
@section('mod_title','Assign Batch to Admission')
@section('content')
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h2 class="box-title"><b>Assigned Batch Manager</b></h2>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">

                    <div class="row" style="padding: 10px;">
                        <div class="col-md-4">
                            <a href="javascript:void(0)" data-backdrop="static" data-href="{{ route('batch.assign') }}" class="btn btn-md btn-danger openBtn"><i class="fa fa-plus"></i> Assign Batch..</a>
                        </div>
                        <div class="col-md-4">
                            <h1 class="text-red">Total: {{ $batch->count() }} Student(s)</h1>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group" style="padding-right: 20px">
                            <label class="control-label">Sort By Batch: </label>
                                <select class="form-control" onChange="top.location.href=this.options[this.selectedIndex].value;" value="GO">
                                    <option value="{{ route('batch.assigned', ['filter' => 'all' ]) }}"  @if(url()->current() == route('batch.assigned', ['filter' => 'all' ]))  selected @endif> All </option>
                                    @foreach ($batches as $item)
                                        <option value="{{ route('batch.assigned', ['filter' => $item->id ]) }}"  @if(url()->current() == route('batch.assigned', ['filter' => $item->id ]))  selected @endif>{{ strtoupper($item->title) }}</option>
                                    @endforeach
                                </select>
                            </div>

                        </div>
                    </div>

                    @if(session('status'))
                        <div class="alert alert-success" id="msg-alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close" style="color: white">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <p style="font-size: 18px"><strong>{{ session('status') }}</strong></p>
                        </div>
                    @endif

                    <table id="data-table" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Course</th>
                                <th>Batch Name</th>
                                <th>Batch Timings</th>
                                <th>Trainer</th>
                                <th>Status</th>
                                @if(Auth::user()->getRole->can_view == 1)
                                    <th>Actions</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($batch as $key => $batch)
                            <tr>
                                <td>{{ $batch->aid }}</td>
                                <td>{{ ucwords($batch->full_name) }}</td>
                                <td>{{ strtoupper($batch->getCourse->name) }}</td>
                                <td>{{ strtoupper($batch->batch->title) }}</td>
                                <td>{{ date('h:i A',strtotime($batch->batch->start_time)).' - '.date('h:i A',strtotime($batch->batch->end_time)) }}</td>
                                <td>{{ ucwords($batch->batch->trainer['full_name']) }}</td>
                                <td>
                                    @if($batch->batch->status == '1')
                                        <span class="label label-success">ACTIVE</span>
                                    @elseif($batch->batch->status == '0')
                                        <span class="label label-danger">SUSPENDED</span>
                                    @endif
                                </td>
                                @if(Auth::user()->getRole->can_view == 1)
                                    <td>
                                        <div class="btn-group">
                                            @if(Auth::user()->getRole->can_update == 1)
                                                <a class="btn btn-primary btn-flat openBtn"  href="javascript:void(0)" data-backdrop="static" data-href="{{ route('batch.edit_batch',['id' => $batch->id ]) }}"><i class="fa fa-pencil"></i></a>
                                            @endif
                                            @if(Auth::user()->getRole->can_remove == 1)
                                                <a class="btn btn-danger btn-flat "href="javascript:void(0);" onclick="deleteBatch('{{ route('batch.unassign',['id' => $batch->id ]) }}')">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            @endif
                                        </div>
                                    </td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>

                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
@endsection

@push('styles')
   <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset(config('app.prefix').'components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
  <style>
      th { font-size: 16px; }
      td { font-size: 18px; }
      .big{
        font-size: 12px;
        font-weight: bold;
      }
  </style>
@endpush

@push('scripts')
<!-- DataTables -->
<script src="{{ asset(config('app.prefix').'components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset(config('app.prefix').'components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>

<script>
    $(function () {
        $('#data-table').DataTable({
            "aaSorting": [],
        });
    });


function deleteBatch(uri){
    //var result = confirm("Want to delete?");
    Swal.fire({
            title: 'Are you sure you want to Remove from Batch?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Unassign Batch',
            }).then((result) => {
            if (result.value)
            {
                $.ajax({
                    url:   uri,
                    type: 'GET',
                    success: function(result) {
                        location.reload();
                    }
                });
            }
        });
}
</script>
@endpush
