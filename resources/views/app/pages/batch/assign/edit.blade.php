<!-- Main content -->
<section class="content">
    <div class="row">
            <form name="batchForm" id="batchForm" method="POST" action="{{ route('batch.edit_batch', ['id' => $admission->id]) }}" data-toggle="validator">
                    @csrf
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group has-feedback">
                                <label class="control-label">Student Name</label>
                                <input class="form-control" name="student" value="{{ $admission->aid }} - {{ $admission->full_name}}" readonly/>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group has-feedback">
                                <label class="control-label">Select Batch</label>
                                <select class="form-control" name="batch" required="required">
                                    <option value=""> --Select One-- </option>
                                    @foreach ($batches as $batch)
                                        <option value="{{ $batch->id }}" @if($admission->batch_id == $batch->id) selected @endif>{{ $batch->title.' | Timings ('.date('h:i A',strtotime($batch->start_time)).' to '.date('h:i A',strtotime($batch->end_time)).')' }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-lg btn-block btn-danger">Submit</button>
                        </div>
                    </div>
                </form>
    </div>
    </section>

    <script src="{{ asset(config('app.prefix').'plugins/validator.js') }}"></script>
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset(config('app.prefix').'components/select2/dist/css/select2.min.css') }}">
    <script src="{{ asset(config('app.prefix').'components/select2/dist/js/select2.full.min.js') }}"></script>
    <script>
            $(function () {
              //Initialize Select2 Elements
              $('.select2').select2()
            });
    </script>
