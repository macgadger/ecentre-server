<!-- Main content -->
<section class="content">
<div class="row">
        <form name="batchForm" id="batchForm" method="POST" action="{{ route('batch.assign') }}" data-toggle="validator">
                @csrf
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group has-feedback">
                            <label class="control-label">Select Admission(Student)</label>
                            <select class="form-control select2" name="students[]" required="required" data-placeholder="--Select Admissions--" placeholder="--Select Admissions--" multiple="multiple" style="width: 100%;">
                                @foreach ($admission as $item)
                                    <option value="{{ $item->id }}">{{ $item->aid }} - {{ $item->full_name}}</option>
                                @endforeach
                            </select>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group has-feedback">
                            <label class="control-label">Select Batch</label>
                            <select class="form-control" name="batch" required="required">
                                <option value=""> --Select One-- </option>
                                @foreach ($batches as $batch)
                                    <option value="{{ $batch->id }}">{{ $batch->title.' | Timings ('.date('h:i A',strtotime($batch->start_time)).' to '.date('h:i A',strtotime($batch->end_time)).')' }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                </div>

                <hr>

                <div class="row">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-lg btn-block btn-danger">Submit</button>
                    </div>
                </div>
            </form>
</div>
</section>

<script src="{{ asset(config('app.prefix').'plugins/validator.js') }}"></script>
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset(config('app.prefix').'components/select2/dist/css/select2.min.css') }}">
<script src="{{ asset(config('app.prefix').'components/select2/dist/js/select2.full.min.js') }}"></script>
<script>
        $(function () {
          //Initialize Select2 Elements
          $('.select2').select2()
        });
</script>
