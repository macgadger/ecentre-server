@extends('app.layout.app')
@section('title','My Batch')
@section('mod_title','Assign Batch to Admission')
@section('content')
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h2 class="box-title"><b>My Batch</b></h2>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">

                    <div class="row" style="padding: 10px;">
                        <div class="col-md-4">
                            &nbsp;
                        </div>
                        <div class="col-md-4">
                            @isset($batch)
                                <h1 class="text-red">Total: {{ $batch->count() }} Student(s)</h1>
                            @endisset
                        </div>
                        <div class="col-md-4">
                            <div class="form-group" style="padding-right: 20px">
                            <label class="control-label">Sort By Batch: </label>
                                <select class="form-control" onChange="top.location.href=this.options[this.selectedIndex].value;" value="GO">
                                    <option value="{{ route('batch.trainer', ['filter' => 'all' ]) }}"  @if(url()->current() == route('batch.trainer', ['filter' => 'all' ]))  selected @endif> All </option>
                                    @foreach ($batches as $item)
                                        <option value="{{ route('batch.trainer', ['filter' => $item->id ]) }}"  @if(url()->current() == route('batch.trainer', ['filter' => $item->id ]))  selected @endif>{{ strtoupper($item->title) }}</option>
                                    @endforeach
                                </select>
                            </div>

                        </div>
                    </div>
                    @isset($batch)
                    <table id="data-table" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Course</th>
                                <th>Batch Name</th>
                                <th>Batch Timings</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($batch as $key => $batch)
                            <tr>
                                <td>{{ $batch->aid }}</td>
                                <td>{{ ucwords($batch->full_name) }}</td>
                                <td>{{ strtoupper($batch->getCourse->name) }}</td>
                                <td>{{ strtoupper($batch->batch['title']) }}</td>
                                <td>{{ date('h:i A',strtotime($batch->batch['start_time'])).' - '.date('h:i A',strtotime($batch->batch['end_time'])) }}</td>
                            </tr>
                            @endforeach
                        </tbody>

                    </table>
                    @endisset
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
@endsection

@push('styles')
   <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset(config('app.prefix').'components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
  <style>
      th { font-size: 16px; }
      td { font-size: 18px; }
      .big{
        font-size: 12px;
        font-weight: bold;
      }
  </style>
@endpush

@push('scripts')
<!-- DataTables -->
<script src="{{ asset(config('app.prefix').'components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset(config('app.prefix').'components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>

<script>
    $(function () {
        $('#data-table').DataTable({
            "aaSorting": [],
        });
    });
</script>
@endpush
