@extends('app.layout.app')
@section('title','Batches')
@section('mod_title','Batch View')
@section('content')
<!-- Main content -->
<section class="content">

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h2 class="box-title"><b>Batch Manager</b></h2>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">

                    <div class="row" style="padding: 10px;">
                        <div class="col-md-12">
                        <a href="{{ route('batch.create') }}" class="btn btn-md btn-danger"><i class="fa fa-plus"></i> New Batch</a>
                            <hr>
                        </div>
                    </div>

                    @if(session('status'))
                    <div class="alert alert-success" id="msg-alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close" style="color: white">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <p style="font-size: 18px"><strong>{{ session('status') }}</strong></p>
                    </div>
                    @endif

                    <table id="data-table" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Batch Category</th>
                                <th>Batch Name</th>
                                <th>Start Time</th>
                                <th>End Time</th>
                                <th>Trainer Assigned</th>
                                <th>Status</th>
                                @if(Auth::user()->getRole->can_view == 1)
                                    <th>Actions</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($batch as $key => $batch)
                            <tr>
                                <td>{{ strtoupper($batch->category->name) }}</td>
                                <td>{{ strtoupper($batch->title) }}</td>
                                <td>{{ date('h:i A',strtotime($batch->start_time)) }}</td>
                                <td>{{ date('h:i A',strtotime($batch->end_time))   }}</td>
                                <td>{{ $batch->trainer->full_name }}</td>
                                <td>
                                    @if($batch->status == '1')
                                        <span class="label label-success">ACTIVE</span>
                                    @elseif($batch->status == '0')
                                        <span class="label label-danger">SUSPENDED</span>
                                    @endif
                                </td>
                                @if(Auth::user()->getRole->can_view == 1)
                                <td>
                                    <div class="btn-group">
                                        @if(Auth::user()->getRole->can_update == 1)
                                            <a class="btn btn-primary btn-flat" href="{{ route('batch.edit',['id' => $batch->id ]) }}"><i class="fa fa-pencil"></i> Edit</a>
                                        @endif
                                        @if(Auth::user()->getRole->can_remove == 1)
                                            <a class="btn btn-danger btn-flat "href="javascript:void(0);" onclick="deleteRecord('{{ route('batch.destroy',['id' => $batch->id ]) }}')">
                                                <i class="fa fa-trash"></i> Delete
                                            </a>
                                        @endif
                                    </div>
                                    </td>
                                @endif

                            </tr>
                            @endforeach

                        </tbody>

                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
@endsection

@push('styles')
   <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset(config('app.prefix').'components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
  <style>
      th { font-size: 16px; }
      td { font-size: 18px; }
      .big{
        font-size: 12px;
        font-weight: bold;
      }
  </style>
@endpush

@push('scripts')
<!-- DataTables -->
<script src="{{ asset(config('app.prefix').'components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset(config('app.prefix').'components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>

<script>
    $(function () {
        $('#data-table').DataTable({
            "aaSorting": [],
        });
    })
</script>
@endpush
