@extends('app.layout.app')
@section('title','Add Batch')
@section('content')
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h2 class="box-title"><b>Add Batch</b></h2>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse"> <i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">

                    @if($errors->count())
                        @foreach ($errors->all() as $message)
                        <div class="alert alert-danger" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ $message }}
                        </div>
                        @endforeach
                    @endif

                    <form name="courseForm" id="courseForm" method="POST" action="{{ route('batch.store') }}" data-toggle="validator">
                        @csrf

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group has-feedback">
                                    <label class="control-label">Batch Category</label>
                                    <select class="form-control" name="category" required="required">
                                        <option value=""> --Select Batch Category-- </option>
                                        @foreach ($category as $item)
                                            <option value="{{ $item->id }}">{{ strtoupper($item->name) }}</option>
                                        @endforeach
                                    </select>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Batch Name</label>
                                        <input type="text" class="form-control" name="name" placeholder="Enter Category Name"
                                            value="{{ old('name') }}" required="required" style="text-transform: uppercase;" autocomplete="off">
                                        <div class="help-block with-errors"></div>
                                    </div>
                            </div>

                        </div>

                        <div class="row">

                            <div class="col-md-6">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Batch Start Time</label>
                                        <input type="text" class="form-control timepicker" name="start" placeholder="Enter Category Name"
                                            value="{{ old('start') }}" required="required" autocomplete="off">
                                        <div class="help-block with-errors"></div>
                                    </div>
                            </div>

                            <div class="col-md-6">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Batch End Time</label>
                                        <input type="text" class="form-control timepicker" name="end" placeholder="Enter Category Name"
                                            value="{{ old('end') }}" required="required" autocomplete="off">
                                        <div class="help-block with-errors"></div>
                                    </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Trainer</label>
                                        <select name="trainer" class="form-control" required>
                                            <option value="" selected> --Choose One-- </option>
                                            @foreach ($trainers as $trainer)
                                                <option value="{{ $trainer->profile->id }}">{{ $trainer->profile->full_name }}</option>
                                            @endforeach
                                        </select>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>

                            <div class="col-md-6">
                                <div class="form-group has-feedback">
                                    <label class="control-label">Status</label>
                                    <select name="status" class="form-control">
                                        <option value="1">Active</option>
                                        <option value="0">Suspend</option>
                                    </select>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                        <hr>

                        <div class="row">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-lg btn-block btn-danger">Submit</button>
                            </div>
                        </div>
                    </form>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
@endsection

@push('styles')
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset(config('app.prefix').'components/select2/dist/css/select2.min.css') }}">

<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{ asset(config('app.prefix').'components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset(config('app.prefix').'plugins/timepicker/bootstrap-timepicker.min.css') }}">
<style>
    .form-control{
        text-transform: uppercase;
    }
</style>
@endpush

@push('scripts')
<!-- Select2 -->
<script src="{{ asset(config('app.prefix').'components/select2/dist/js/select2.full.min.js') }}"></script>

<!-- bootstrap datepicker -->
<script src="{{ asset(config('app.prefix').'components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset(config('app.prefix').'plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
<script>
    //Timepicker
    $('.timepicker').timepicker({
        showInputs: true
    });
</script>
@endpush
