@extends('app.layout.app')
@section('title','Roles Manager')
@section('mod_title','View Role')
@section('content')
<!-- Main content -->
<section class="content">

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h2 class="box-title"><b>Roles Manager</b></h2>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"> <i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">

                    <div class="row" style="padding: 10px;">
                        <div class="col-md-12">
                        <a href="./role/create" class="btn btn-md btn-danger"><i class="fa fa-plus"></i> Add Role</a><hr>
                        </div>
                    </div>

                    @if(session('status'))
                        <div class="alert alert-success" id="msg-alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close" style="color: white">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <p style="font-size: 18px"><strong>{{ session('status') }}</strong></p>
                        </div>
                    @endif
                    
                    <table id="data-table" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Can View</th>
                                <th>Can Edit</th>
                                <th>Can Delete</th>
                                <th>Can Handle Accounts</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($roles as $key => $role)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ strtoupper($role->name) }}</td>
                                <td>
                                    @if($role->can_view == 1)
                                        <span class="label label-success">Yes</span>
                                    @else
                                        <span class="label label-danger">No</span>
                                    @endif
                                </td>
                                <td>
                                    @if($role->can_update == 1)
                                        <span class="label label-success">Yes</span>
                                    @else
                                        <span class="label label-danger">No</span>
                                    @endif
                                </td>
                                <td>
                                    @if($role->can_remove == 1)
                                        <span class="label label-success">Yes</span>
                                    @else
                                        <span class="label label-danger">No</span>
                                    @endif
                                </td>
                                <td>
                                    @if($role->can_account == 1)
                                        <span class="label label-success">Yes</span>
                                    @else
                                        <span class="label label-danger">No</span>
                                    @endif
                                </td>
                                <td>
                                    <div class="btn-group">
                                    <a class="btn btn-danger btn-flat openBtn" data-href="./role/{{ $role->id }}" href="javascript:void(0);"><i class="fa fa-eye"></i> View</a>
                                        <a class="btn btn-danger btn-flat dropdown-toggle" data-toggle="dropdown">
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="./"><i class="fa fa-pencil"></i> Edit</a></li>
                                            <li><a href="#"><i class="fa fa-trash"></i> Delete</a></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                        <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Can View</th>
                                <th>Can Edit</th>
                                <th>Can Delete</th>
                                <th>Can Handle Accounts</th>
                                <th>Actions</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
@endsection

@push('styles')
   <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset(config('app.prefix').'components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
  <style>
      th { font-size: 16px; }
      td { font-size: 18px; }
      .big{
        font-size: 12px;
        font-weight: bold;
      }
  </style>
@endpush

@push('scripts')
<!-- DataTables -->
<script src="{{ asset(config('app.prefix').'components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset(config('app.prefix').'components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>

<script>
    $(function () {
        $('#data-table').DataTable({
            "aaSorting": [],
        });
    })
</script>
@endpush