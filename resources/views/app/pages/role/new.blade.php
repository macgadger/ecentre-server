@extends('app.layout.app')
@section('title','Add Role')
@section('content')
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h2 class="box-title"><b>Add Role</b></h2>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse"> <i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">

                    @if($errors->count())
                    @foreach ($errors->all() as $message)
                    <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        {{ $message }}
                    </div>
                    @endforeach
                    @endif

                    <form name="employeeForm" id="employeeForm" method="POST" action="../role" data-toggle="validator">
                        @csrf


                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group has-feedback">
                                    <label class="control-label">Role Name</label>
                                    <input type="text" class="form-control" name="name" placeholder="Enter Role Name"
                                        value="{{ old('name') }}" required="required" style="text-transform: uppercase;">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>


                        <hr/>
                        <h4>Credentials</h4>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="text-center">Can View</th>
                                            <th class="text-center">Can Edit</th>
                                            <th class="text-center">Can Handle Accounts</th>
                                            <th class="text-center">Can Delete</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="text-center">
                                                <div class="form-group">
                                                    <label>
                                                        <input type="checkbox" class="minimal-red" name="can_view"
                                                            value="1">
                                                    </label>
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                <div class="form-group">
                                                    <label>
                                                        <input type="checkbox" class="minimal-red" name="can_update"
                                                            value="1">
                                                    </label>
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                    <div class="form-group">
                                                        <label>
                                                            <input type="checkbox" class="minimal-red" name="can_account"
                                                                value="1">
                                                        </label>
                                                    </div>
                                            </td>
                                            <td class="text-center">
                                                <div class="form-group">
                                                    <label>
                                                        <input type="checkbox" class="minimal-red" name="can_remove"
                                                            value="1">
                                                    </label>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-lg btn-block btn-danger">Submit</button>
                            </div>
                        </div>
                    </form>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
@endsection

@push('styles')
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="{{ asset(config('app.prefix').'plugins/iCheck/all.css') }}">

  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="{{ asset(config('app.prefix').'components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
@endpush

@push('scripts')
    <!-- bootstrap datepicker -->
    <script src="{{ asset(config('app.prefix').'components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>

    <!-- iCheck 1.0.1 -->
    <script src="{{ asset(config('app.prefix').'plugins/iCheck/icheck.min.js') }}"></script>

    <script>
        $(function () {
            //Red color scheme for iCheck
            $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                checkboxClass: 'icheckbox_minimal-red',
                radioClass   : 'iradio_minimal-red'
            })
        })
    </script>
@endpush