@extends('app.layout.app')
@section('title','Performance')
@section('content')
<!-- Main content -->
<section class="content">
        @component('app.components.filter', ['type' => 'performance' , 'open' => true , 'search' => false])@endcomponent

        <div class="row">
                <div class="col-xs-12">
                    <div class="box box-danger">
                        <div class="box-header with-border">
                            <h2 class="box-title"><b>Performance</b></h2>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"> <i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                                <table id="data-table" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Trainer Name</th>
                                                <th></th>
                                                <th>Father Name</th>
                                                <th>Course</th>
                                                <th>Certificate Issued On</th>
                                                <th>Issue Date</th>
                                                <th>Issued By</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($performace as $item)
                                            <tr>
                                                
                                            </tr>
                                        @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>ID</th>
                                                <th>Name</th>
                                                <th>Father Name</th>
                                                <th>Course</th>
                                                <th>Certificate Issued</th>
                                                <th>Issue Date</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                               
                        </div>
                    </div>
                </div>
            </div>
    
    </section>
    @endsection
    
    @push('styles')
       <!-- DataTables -->
      <link rel="stylesheet" href="{{ asset(config('app.prefix').'components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
      <style>
          th { font-size: 16px; }
          td { font-size: 20px; }
          .big{
            font-size: 16px;
            font-weight: bold;
          }
      </style>
    @endpush
    
    @push('scripts')
    <!-- DataTables -->
    <script src="{{ asset(config('app.prefix').'components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset(config('app.prefix').'components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    
    <script>
            $(function () {
                $('#data-table').DataTable({
                    "aaSorting": [],
                });
            });
    </script>
@endpush