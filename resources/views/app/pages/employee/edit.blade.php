@extends('app.layout.app')
@section('title','Edit Employee')
@section('content')
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h2 class="box-title"><b>Edit Employee</b></h2>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse"> <i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">

                    @if($errors->count())
                        @foreach ($errors->all() as $message)
                            <div class="alert alert-danger" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                {{ $message }}
                            </div>
                        @endforeach
                    @endif

                    <form name="staffForm" id="staffForm" method="POST" action="{{ route('employee.update',['id' => $employee->id ]) }}" data-toggle="validator">
                    @csrf
                    @method('PUT')
                    
                    <fieldset>
                    <legend>Personel Information</legend>

                    <div class="row">
                            <div class="col-md-6">
                                <div class="form-group has-feedback">
                                    <label class="control-label">Joining Date</label>
                                    <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                        <input type="text" class="form-control" name="join_date" id="datepicker" value="{{ date('d-m-Y',strtotime($employee->join_date)) }}"  required>
                                        </div>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Set Profile Status</label>
                                        <select class="form-control" name="status" required>
                                            <option value="" selected>--Select Status--</option>
                                            <option value="active"  @if($employee->status == 'active') selected @endif>Active</option>
                                            <option value="suspend" @if($employee->status == 'suspend') selected @endif>Supend</option>
                                        </select>
                                        <div class="help-block with-errors"></div>
                                    </div>
                            </div>
                    </div>

                    <hr>
                        
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group has-feedback">
                                <label class="control-label">First Name</label>
                            <input type="text" class="form-control" name="first_name" placeholder="Enter First Name" value="{{ $employee->first_name }}" required="required">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group has-feedback">
                                <label class="control-label">Last Name</label>
                                <input type="text" class="form-control" name="last_name" placeholder="Enter Last Name" value="{{ $employee->last_name }}" required="required">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-md-4">
                                <div class="form-group has-feedback">
                                    <label class="control-label">Phone</label>
                                    <input type="text" class="form-control" name="mobile" placeholder="Enter Phone Number" value="{{ $employee->mobile }}" required="required">
                                    <div class="help-block with-errors"></div>
                                </div>
                        </div>
                    </div>

                    <div class="row">
                            <div class="col-md-12">
                                <div class="form-group has-feedback">
                                    <label class="control-label">Address</label>
                                    <textarea class="form-control" name="address" placeholder="Enter Address Name" required="required">{{ $employee->address }}</textarea>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                    </div>

                    <div class="row">
                            <div class="col-md-4">
                                <div class="form-group has-feedback">
                                    <label class="control-label">City</label>
                                    <input type="text" class="form-control" name="city" placeholder="Enter City" value="{{ $employee->city }}" required="required">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group has-feedback">
                                    <label class="control-label">State</label>
                                    <input type="text" class="form-control" name="state" placeholder="Enter State" value="{{ $employee->state }}" required="required">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Country</label>
                                        <input type="text" class="form-control" name="country" placeholder="Enter Country" value="{{ $employee->country }}" required="required" value="India">
                                        <div class="help-block with-errors"></div>
                                    </div>
                            </div>
                        </div>
                    </fieldset>

                    <fieldset>
                        <legend>Login information</legend>
                        <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">Email/Username</label>
                                        <input type="email" class="form-control" name="user_name" placeholder="Enter Email" value="{{ $employee->user->email }}" readonly>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                
                                <div class="col-md-6">
                                    <div class="form-group has-feedback">
                                        <label class="control-label">User Role</label>
                                        <select class="form-control" name="role" required>
                                            <option value="" selected>--Select Role--</option>
                                            <option value="1" @if($employee->user->role == 1) selected @endif>Admin</option>
                                            <option value="2" @if($employee->user->role == 2) selected @endif>Center Manager</option>
                                            <option value="3" @if($employee->user->role == 3) selected @endif>Counsellor</option>
                                            <option value="4" @if($employee->user->role == 4) selected @endif>Accounts</option>
                                        </select>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                        </div>
                    </fieldset>

                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-lg btn-block btn-danger">Submit</button>
                        </div>
                    </div>
                    </form>

                    <hr>

                    <fieldset>
                            <form name="passwordForm" id="passwordForm" method="POST" action="{{ route('employee.password',['id' => $employee->id ]) }}" data-toggle="validator">
                            @csrf
                            <legend>Change Password</legend>
                            <div class="row">
                                    <div class="col-md-5">
                                        <div class="form-group has-feedback">
                                            <label class="control-label">Password</label>
                                            <input type="password" class="form-control" name="password" placeholder="Set Password" required="required">
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group has-feedback">
                                            <label class="control-label">Repeat Password</label>
                                            <input type="password" class="form-control" name="password_confirmation" placeholder="Repeat Password" required="required">
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group has-feedback">
                                            <label class="control-label">&nbsp;</label>
                                            <button type="submit" class="btn btn-sm btn-block btn-warning">Update</button>
                                        </div>
                                    </div>
                            </div>
                            </form>
                            </fieldset>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>

@endsection

@push('styles')
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="{{ asset(config('app.prefix').'components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
@endpush

@push('scripts')
    <!-- bootstrap datepicker -->
    <script src="{{ asset(config('app.prefix').'components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script>
        $(function () {
            //Date picker
            $('#datepicker').datepicker({
                autoclose: true,
                format: "dd-mm-yyyy",
                todayBtn: true,
                clearBtn: true,
            })
        })
    </script>
@endpush