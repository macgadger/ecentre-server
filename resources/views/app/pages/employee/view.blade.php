<!-- Main content -->
<section class="content">
        <!-- Custom Tabs -->
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_personel" data-toggle="tab">Personel Details</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_personel">
                    <table class="table table-bordered table-hover table-condensed">
                        <tbody>
                          <tr class="techSpecRow" style="border: 1px dotted red">
                            <td class="techSpecTD1">Username/Email:</td>
                            <td class="techSpecTD2">{{ $employee->user->email }}</td>
                          </tr>
                          <tr class="techSpecRow">
                            <td class="techSpecTD1">Name:</td>
                            <td class="techSpecTD2">{{ ucwords($employee->first_name.' '.$employee->last_name) }}</td>
                          </tr>
                          <tr class="techSpecRow">
                            <td class="techSpecTD1">Mobile:</td>
                            <td class="techSpecTD2">{{ $employee->mobile }}</td>
                          </tr>
                          <tr class="techSpecRow">
                            <td class="techSpecTD1">Address:</td>
                            <td class="techSpecTD2">{{ ucwords($employee->address) }}</td>
                          </tr>
                          <tr class="techSpecRow">
                            <td class="techSpecTD1">City / State:</td>
                            <td class="techSpecTD2">{{ ucwords($employee->city.' / '.$employee->state) }}</td>
                          </tr>
                          <tr class="techSpecRow">
                            <td class="techSpecTD1">Country:</td>
                            <td class="techSpecTD2">{{ ucwords($employee->country) }}</td>
                          </tr>
                          <tr class="techSpecRow">
                            <td class="techSpecTD1">Joined On:</td>
                            <td class="techSpecTD2">{{ date('d-m-Y',strtotime($employee->join_date)) }}</td>
                          </tr>
                        </tbody>
                      </table>
                </div>
    
               
            </div>
            <!-- /.tab-content -->
        </div>
        <!-- nav-tabs-custom -->
    </section>