@extends('app.layout.app')
@section('title','Reviews')
@section('mod_title','Reviews')
@section('content')
<!-- Main content -->
<section class="content">
    
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h2 class="box-title"><b>Review Manager</b></h2>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    @if(session('status'))
                    <div class="alert alert-success" id="msg-alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close" style="color: white">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <p style="font-size: 18px"><strong>{{ session('status') }}</strong></p>
                    </div>
                    @endif

                    <table id="data-table" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Student ID</th>
                                <th>Student Name</th>
                                <th>Course</th>
                                <th>Batch</th>
                                <th>Trainer Name</th>
                                <th>Overall Rating</th>                                                                                                 
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($reviews as $key => $review)   
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>A2IT-{{ $review->student_id }}</td> 
                                <td>{{ $review->admissions->full_name }}</td>
                                <td>{{ $review->admissions->getcourse->name }}</td> 
                                <td>{{ ucwords($review->admissions->batch->title) }}</td>   
                                <td>{{ $review->admissions->batch->trainer->full_name }}</td>
                                <td>{{ $review->overall_rating }}</td>                              
                                <td>
                                    <div class="btn-group">
                                        <a class="btn btn-danger btn-flat openBtn big" data-href="{{ route('reviews.show',['id' => $review->id ]) }}" href="javascript:void(0);"><i class="fa fa-eye"></i> View</a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                            
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
@endsection

@push('styles')
   <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset(config('app.prefix').'components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
  <style>
      th { font-size: 16px; }
      td { font-size: 18px; }
      .big{
        font-size: 12px;
        font-weight: bold;
      }
  </style>
@endpush

@push('scripts')
<!-- DataTables -->
<script src="{{ asset(config('app.prefix').'components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset(config('app.prefix').'components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>

<script>
    $(function () {
        $('#data-table').DataTable({
            "aaSorting": [],
        });
    })
</script>
@endpush