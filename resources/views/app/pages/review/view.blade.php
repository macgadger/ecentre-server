<!-- Main content -->
<section class="content">
        <!-- Custom Tabs -->
        <div class="nav-tabs-custom">
            {{-- <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_personel" data-toggle="tab">Details</a></li>
            </ul> --}}
            <div class="tab-content">
                <div class="tab-pane active" id="tab_personel">
                    <table class="table table-bordered table-hover table-condensed">
                            <tbody>
                                <tr class="techSpecRow" style="border: 1px dotted red">
                                <td class="techSpecTD1">What to do think about A2IT:</td>
                                <td class="techSpecTD2 textbold">{{ $diary->a2it_rating }}</td>
                                </tr>

                                <tr class="techSpecRow success">
                                    <td class="techSpecTD1" style="color:red">Trainer</td>
                                    <td class="techSpecTD1"></td>
                                </tr>
                                <tr class="techSpecRow success">
                                <td class="techSpecTD1">Subject matter knowledge/Command on subject:</td>
                                <td class="techSpecTD2 textbold"> {{ $diary->tr_sub_rate }}</td>
                                </tr>
                                <tr class="techSpecRow success">
                                    <td class="techSpecTD1">Presented subject matter clearly and systematically:</td>
                                    <td class="techSpecTD2 textbold">{{ $diary->tr_pres_rate }}</td>
                                </tr>
                                <tr class="techSpecRow success">
                                <td class="techSpecTD1">Time management:</td>
                                <td class="techSpecTD2 textbold"> {{ $diary->tr_time_rate }}</td>
                                </tr>
                                <tr class="techSpecRow success">
                                <td class="techSpecTD1">Encouraged students to ask questions/interaction with individual student:</td>
                                <td class="techSpecTD2 textbold">{{ $diary->tr_enc_rate }}</td>
                                </tr>

                                <tr class="techSpecRow">
                                <td class="techSpecTD1">Satisfaction level about project undertaken:</td>
                                <td class="techSpecTD2 textbold">{{ $diary->pro_sat_rate }}</td>
                                </tr>
                                <tr class="techSpecRow">
                                <td class="techSpecTD1">Was knowledgeable and helped me to consider options & examine my alternatives:</td>
                                <td class="techSpecTD2 textbold">{{ ucwords($diary->was_kno) }}</td>
                                </tr>
                                <tr class="techSpecRow">
                                <td class="techSpecTD1">Was all commitments fulfilled that made by counselor during counseling:</td>
                                <td class="techSpecTD2 textbold"> {{ ucwords($diary->was_all) }}</td>
                                </tr>
                                <tr class="techSpecRow">
                                <td class="techSpecTD1">Cooperation provided by other staff:</td>
                                <td class="techSpecTD2 textbold"> {{ $diary->cor_staf_rate }}</td>
                                </tr>
                                <tr class="techSpecRow">
                                <td class="techSpecTD1">Labs are fully equipped with latest devices:</td>
                                <td class="techSpecTD2 textbold"> {{ $diary->cor_lab_rate }}</td>
                                </tr>
                                <tr class="techSpecRow">
                                <td class="techSpecTD1">Overall, I am satisfied with experience I had in this organization:</td>
                                <td class="techSpecTD2 textbold">  {{ ucwords($diary->ovr_sat) }}</td>
                                </tr>
                            </tbody>
                        </table>                    
                </div>
    
               
            </div>
            <!-- /.tab-content -->
        </div>
        <!-- nav-tabs-custom -->
    </section>
    <style>
        .textbold{
            font-weight: bold;
        }
    </style>