@extends('app.layout.app')
@section('title','Issued Certificates')
@section('content')
<!-- Main content -->
<section class="content">

        <div class="row">
                <div class="col-xs-12">
                    <div class="box box-danger">
                        <div class="box-header with-border">
                            <h2 class="box-title"><b>Issued Certificates</b></h2>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"> <i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                                <table id="data-table" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Name</th>
                                               
                                                <th>Course</th>
                                                <th>Issued On</th>
                                                
                                                <th>Issued By</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($cert as $item)
                                            <tr>
                                                <td>A2IT-{{ $item->aid }}</td>
                                                <td>{{ ucwords($item->admissions->full_name) }}</td>
                                                
                                                <td>{{ ucwords($item->admissions->getCourse->name) }}</td>
                                                <td> {{ empty($item->issued_on) ? "" :date('d-m-Y',strtotime($item->issued_on)) }}</td>
                                                
                                                <td>{{ $item->profile->full_name }}</span></td>
                                                <td>
                                                    @if($item->status =="1")
                                                        <span class="label label-danger">Not Issued</span>
                                                    @else
                                                        <span class="label label-success">Issued</span>
                                                    @endif
                                                </td>
                                                <td>
                                                    <a href="{{ route('issue.certificate',['id' => $item->id ])}}"><span class="label label-success">Issue</span></a>
                                                    @if($item->status == 0)
                                                        <a href="{{ route('certificate.issue',['id' => $item->aid ])}}"><span class="label label-warning">Print</span></a>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                        
                                    </table>
                               
                        </div>
                    </div>
                </div>
            </div>
    
    </section>
    @endsection
    
    @push('styles')
       <!-- DataTables -->
      <link rel="stylesheet" href="{{ asset(config('app.prefix').'components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
      <style>
          th { font-size: 16px; }
          td { font-size: 20px; }
          .big{
            font-size: 16px;
            font-weight: bold;
          }
      </style>
    @endpush
    
    @push('scripts')
    <!-- DataTables -->
    <script src="{{ asset(config('app.prefix').'components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset(config('app.prefix').'components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    
    <script>
            $(function () {
                $('#data-table').DataTable({
                    "aaSorting": [],
                });
            });
    </script>
@endpush