@extends('app.layout.app')
@section('title','Issue Certificate')
@section('content')
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h2 class="box-title"><b>Issue Certificate</b></h2>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse"> <i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <h3>Certificate Preview</h3>
                            <img src="{{ asset(config('app.prefix').'uploads/certificate/preview.png') }}" class="img-responsive img-thumbnail" />
                        </div>
                        <div class="col-md-12 text-center" style="padding-top: 10px">
                            <hr>
                            <a href="{{ asset(config('app.prefix').'uploads/certificate/preview.png') }}" target="_blank" class="btn btn-danger btn-lg">Print Certificate</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
@endsection
