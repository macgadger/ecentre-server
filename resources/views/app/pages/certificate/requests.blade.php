@extends('app.layout.app')
@section('title','New Certificates Request')
@section('content')
<!-- Main content -->
<section class="content">

        <div class="row">
                <div class="col-xs-12">
                    <div class="box box-danger">
                        <div class="box-header with-border">
                            <h2 class="box-title"><b>New Certificates Request</b></h2>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"> <i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                                <table id="data-table" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Trainer Name</th>
                                                <th>Name</th>
                                                <th>Mobile</th>
                                                <th>Email</th>
                                                <th>Course</th>
                                                <th>Course From</th>
                                                <th>Course To</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($cert as $item)
                                            <tr>
                                                <td>{{ ucwords($item->admission->batch->trainer->full_name) }}</td>
                                                <td>{{ ucwords($item->admission->full_name) }}</td>
                                                <td>{{ $item->admission->mobile }}</td>
                                                <td>{{ strtolower($item->admission->email) }}</td>
                                                <td>{{ ucwords($item->admission->getCourse->name) }}</td>
                                                <td>{{ date('d M, Y',strtotime($item->admission->course_joindate)) }}</td>
                                                <td>{{ date('d M, Y',strtotime($item->admission->course_enddate)) }}</td>
                                                <td><a href="{{ route('certificate.authorise',['id' => $item->id ]) }}"><span class="label label-success">Approve</span></a></td>    
                                            </tr>
                                        @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Trainer Name</th>
                                                <th>Name</th>
                                                <th>Mobile</th>
                                                <th>Email</th>
                                                <th>Course</th>
                                                <th>Course From</th>
                                                <th>Course To</th>
                                                <th>Actions</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                               
                        </div>
                    </div>
                </div>
            </div>
    
    </section>
    @endsection
    
    @push('styles')
       <!-- DataTables -->
      <link rel="stylesheet" href="{{ asset(config('app.prefix').'components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
      <style>
          th { font-size: 16px; }
          td { font-size: 20px; }
          .big{
            font-size: 16px;
            font-weight: bold;
          }
      </style>
    @endpush
    
    @push('scripts')
    <!-- DataTables -->
    <script src="{{ asset(config('app.prefix').'components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset(config('app.prefix').'components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    
    <script>
            $(function () {
                $('#data-table').DataTable({
                    "aaSorting": [],
                });
            });
    </script>
@endpush