<?php
$today  = Carbon\Carbon::now()->format('Y-m-d');
?>
@extends('app.layout.app')
@section('title','Reports Manager')
@section('content')
<!-- Main content -->
<section class="content">
        
        <div class="row">
                <div class="col-xs-12">
                    <div class="box box-danger">
                        <div class="box-header with-border">
                            <h2 class="box-title"><b>Reports Manager</b></h2>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"> <i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">

                            <form name="reportsForm" id="reportsForm" method="POST" action="{{ route('reports.filter') }}" data-toggle="validator">
                                @csrf
                                <div class="row">
                                
                                    <div class="col-md-2">
                                        <div class="form-group has-feedback">
                                            <label class="control-label">Date From</label>
                                            <div class="input-group date">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" name="date_from" class="form-control pull-right datepicker" value="{{ old('date_from') }}" style="border-radius: 0px" autocomplete="off" required>
                                            </div>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                
                                    <div class="col-md-2">
                                        <div class="form-group has-feedback">
                                            <label class="control-label">Date To</label>
                                            <div class="input-group date">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" name="date_to" class="form-control pull-right datepicker" value="{{ old('date_to') }}"  style="border-radius: 0px" autocomplete="off" required>
                                            </div>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                            <div class="form-group has-feedback">
                                                <label class="control-label">By Counselor</label>
                                                <select class="form-control" name="couselor">
                                                    <option value="all"> --All-- </option>
                                                    @foreach ($staff as $item)
                                                        <option value="{{ $item->profile->user_id }}" @if(old('couselor') == $item->profile->user_id) selected @endif>{{ $item->profile->full_name }}</option>
                                                    @endforeach
                                                </select>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                    </div>
                                    
                                    <div class="col-md-2">
                                            <div class="form-group has-feedback">
                                                <label class="control-label">Report Type</label>
                                                <select class="form-control" required name="search_type">
                                                    <option value=""> --Select Type-- </option>
                                                    <option value="admission" @if(old('search_type') == 'admission') selected @endif>Admissions</option>
                                                    <option value="fee"  @if(old('search_type') == 'fee') selected @endif>Fees</option> 
                                                </select>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                    </div>
                
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label class="control-label">&nbsp;</label>
                                            <button type="submit" class="btn btn-block btn-danger"><i class="fa fa-search"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <hr>
                           
                            @if(Session::get('type')=='admission')
                            <div id="admissions-table">
                                <table id="data-table-admissions" class="table table-bordered table-striped">
                                    <thead>
                                       <tr>
                                            <th>Registration Date</th>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Father Name</th>
                                            <th>Course</th>
                                            <th>Joining Date</th>
                                            <th>End Date</th>
                                            <th>Join Status</th>
                                            <th>Fee Status</th>
                                            <th>Counselor</th>
                                        </tr> 
                                    </thead>
                                    <tbody>
                                            @foreach (Session::get('data') as $key => $adms)   
                                            <tr>
                                                <td>{{ date('d M, Y',strtotime($adms->created_at)) }}</td>
                                                <td>A2IT-{{ $adms->aid }}</td>
                                                <td>{{ ucwords($adms->first_name.' '.$adms->last_name) }}</td>
                                                <td>{{ ucwords($adms->father_name) }}</td>
                                                <td>{{ strtoupper($adms->getCourse->name) }}</td>
                                                <td>{{ date('d M, Y',strtotime($adms->course_joindate)) }}</td>
                                                <td>{{ date('d M, Y',strtotime($adms->course_enddate)) }}</td>
                                                <td>
                                                    @if($adms->status == 'JOINED')
                                                        <a href="javascript:void(0);"><span class="label label-success">JOINED</span></a>
                                                    @elseif($adms->status == 'TRASH')
                                                        <a href="javascript:void(0);"><span class="label label-danger">DELETED</span></a>       
                                                    @elseif($adms->status == 'PENDING' && $adms->course_joindate >= $today)
                                                        <a href="javascript:void(0);"><span class="label label-warning">PENDING</span></a>
                                                    @elseif($adms->status == 'PENDING' && $adms->course_joindate < $today)
                                                        <a href="javascript:void(0);"><span class="label label-danger">EXPIRED</span></a>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($adms->payment->sum('amount') < $adms->fees->course_amount)
                                                        <span class="label label-warning">DUE</span>
                                                    @elseif($adms->payment->sum('amount') >= $adms->fees->course_amount )
                                                        <span class="label label-success">PAID</span>
                                                    @endif
                                                </td>
                                                <td>
                                                    {{ $adms->profile->full_name }}
                                                </td>
                                            </tr>
                                            @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Registration Date</th>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Father Name</th>
                                            <th>Course</th>
                                            <th>Joining Date</th>
                                            <th>End Date</th>
                                            <th>Join Status</th>
                                            <th>Fee Status</th>
                                            <th>&nbsp;</th>
                                        </tr>
                                    </tfoot>
                                </table>   
                            </div>
                            @endif

                            @if(Session::get('type')=='fee')
                            <div id="fees-table">
                                <table id="data-table-fees" class="table table-bordered table-striped">
                                    <thead>
                                       <tr>
                                            <th></th>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Course</th>
                                            <th>Course Fees</th>
                                            <th>Last Payment</th>
                                            <th>Paid On</th>
                                            <th>Amount Due</th>
                                            <th>Due Date</th>
                                            <th>Counselor</th>
                                            <th>Fee Status</th>
                                        </tr> 
                                    </thead>
                                    <tbody>
                                        @foreach(Session::get('data') as $key => $adms)   
                                        @if ($adms->fees != NULL)   
                                        <tr data-child-value="{{ $adms->payment }}">
                                            <td class="details-control"></td>
                                            <td>A2IT-{{ $adms->aid }}</td>
                                            <td>{{ $adms->full_name }}</td>
                                            <td>{{ strtoupper($adms->getCourse->name) }}</td>
                                            <td><i class="fa fa-rupee"></i>{{ $adms->fees->course_amount }}</td>
                                            <td><i class="fa fa-rupee"></i>{{ $adms->fees->last_payment }}</td>
                                            <td>{{ date('d M, Y',strtotime($adms->fees->created_at)) }}</td>
                                            <td><i class="fa fa-rupee"></i>{{ $adms->fees->due_amount }}</td>
                                            <td>{{ date('d M, Y',strtotime($adms->fees->due_date)) }}</td>
                                            <td>{{ $adms->profile->first_name.' '.$adms->profile->last_name }}</td>	
                                            <td>
                                                @if($adms->payment->sum('amount') < $adms->fees->course_amount)
                                                    <span class="label label-warning">DUE</span>
                                                @elseif($adms->payment->sum('amount') >= $adms->fees->course_amount )
                                                    <span class="label label-success">PAID</span>
                                                @endif
                                            </td>
                                        </tr>
                                        @endif
                                        @endforeach
                                    </tbody>
                                    
                                </table>   
                            </div>
                            @endif

                        </div>
                    </div>
                </div>
            </div>
    
    </section>
    @endsection
    
    @push('styles')
       <!-- DataTables -->
      <link rel="stylesheet" href="{{ asset(config('app.prefix').'components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
      <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.4.2/css/buttons.dataTables.min.css">
      
      <!-- bootstrap datepicker -->
      <link rel="stylesheet" href="{{ asset(config('app.prefix').'components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
      <style>
          th { font-size: 16px; }
          td { font-size: 20px; }
          .big{
            font-size: 16px;
            font-weight: bold;
          }
          td.details-control {
            background: url('http://www.datatables.net/examples/resources/details_open.png') no-repeat center center;
            cursor: pointer;
            }
            tr.shown td.details-control {
                background: url('http://www.datatables.net/examples/resources/details_close.png') no-repeat center center;
            }
      </style>
    @endpush
    
    @push('scripts')
    <!-- DataTables -->
    <script src="{{ asset(config('app.prefix').'components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset(config('app.prefix').'components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    
    <script src="https://cdn.datatables.net/select/1.2.1/js/dataTables.select.min.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
    <script src="//cdn.datatables.net/buttons/1.4.2/js/buttons.flash.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script src="//cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>
    <script src="//cdn.datatables.net/buttons/1.4.2/js/buttons.print.min.js"></script>
    
    <script src="{{ asset(config('app.prefix').'components/moment/min/moment.min.js') }}"></script>
    <!-- bootstrap datepicker -->
    <script src="{{ asset(config('app.prefix').'components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>

    <script>
        function format(value) 
        {
            var data = "<table class='table table-condensed table-bordered' style='border: dashed 2px red'>";
            data+= "<thead><tr>";
            data+= "<th>Reciept #</th>";
            data+= "<th>Payment Mode</th>";
            data+= "<th>Payment Type</th>";
            data+= "<th>Payment</th>";
            data+= "<th>Recieved On</th>";
            data+= "</tr></thead>";
            data+= "<tbody>";
            var total = 0;
            value.forEach(function(element) {
                data+= "<tr>";
                    data+= "<td>"+element.reciept_no.toUpperCase()+"</td>";
                    data+= "<td>"+element.payment_mode.toUpperCase()+"</td>";
                    data+= "<td>"+element.type.toUpperCase()+"</td>";
                    data+= "<td><i class='fa fa-rupee'></i>"+element.amount+"</td>";
                    data+= "<td>"+moment(element.created_at).format('D MMM, YYYY')+"</td>";
                data+= "</tr>";
                total = total + element.amount;
            });
            data+= "</tbody><tfoot style='border-top: dashed 2px red'>";
            data+= "<tr>";
                data+= "<td>&nbsp;</td>";
                data+= "<td>&nbsp;</td>";
                data+= "<td align='right' style='font-size: 20px; color: green'>Total: </td>";
                data+= "<td style='font-size: 20px; color: green'><i class='fa fa-rupee'></i>"+total+"/-</td>";
                data+= "<td>&nbsp;</td>";
            data+= "</tr></tfoot>";
            return data;
        }

        $(function () {
            $('#data-table-admissions').DataTable({
                "aaSorting": [],
                dom: 'Bfrtip',
                buttons: [
                    'excel', 'pdf', 'print'
                ],
            });

            var table = $('#data-table-fees').DataTable({
                "aaSorting": [],
                dom: 'Bfrtip',
                buttons: [
                    'excel', 'pdf', 'print'
                ],
            });

            //Date picker
            $('.datepicker').datepicker({
                autoclose: true,
                format: "dd-mm-yyyy",
                todayHighlight: true,
                todayBtn: true,
                clearBtn: true,
            });

            // Add event listener for opening and closing details
            $('#data-table-fees').on('click', 'td.details-control', function () {
                var tr = $(this).closest('tr');
                var row = table.row(tr);

                if (row.child.isShown()) {
                    // This row is already open - close it
                    row.child.hide();
                    tr.removeClass('shown');
                } else {
                    // Open this row
                    row.child(format(tr.data('child-value'))).show();
                    tr.addClass('shown');
                }
            });
        });
    </script>
@endpush