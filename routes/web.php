<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
*/
Route::get('/', function ()
{
    if(Auth::check()){
        return redirect()->intended('account/app/dashboard');
    }else{
        return redirect()->intended('account/signin');
    }
});
/*
|-------------------------------------------------------------------------


/*
|--------------------------------------------------------------------------
| Account/Auth Routes
*/
Route::group(['namespace' => 'Account', 'prefix' => 'account'], function()
{
    //Authentication Routes
    Route::get('signin', 'AccountController@signin')->name('account.login');
    Route::post('auth/login', 'AccountController@authenticate');
    Route::get('signout','AccountController@signout')->name('account.logout');;

    //Dashboar Account routes
    Route::get('/','AccountController@index')->middleware(['auth']);
    Route::get('dashboard','AccountController@index')->middleware(['auth'])->name('account.index');;
});
/*
|-------------------------------------------------------------------------

/*
|--------------------------------------------------------------------------
| Application Routes
*/
Route::group(['middleware' => ['auth'], 'namespace' => 'App', 'prefix' => 'account/app'], function()
{
    //Dashboard
    Route::get('/', 'AppController@index');
    Route::get('dashboard/{filter?}', 'AppController@index')->where('filter','[A-Za-z]+')->name('account.dashboard');
    Route::any('search'   , 'SearchController@filter')->name('search.filter');

	Route::get('admission/filter/{type}','AdmissionController@filter')->name('admission.filter');
    Route::any('admission/status/{id}'  ,'AdmissionController@status')->name('admission.status');
    Route::resource('admission'   ,'AdmissionController');

	Route::any('fee/search'       ,'FeeController@search');
    Route::get('fee/pay/{id}'     ,'FeeController@pay')->name('fee.pay');
    Route::get('fee/filter/{type}','FeeController@filter')->name('fee.filter');
    Route::get('counsellor-fee','FeeController@counsellorfeesIndex')->name('counsellor.fees');
    Route::resource('fee' ,'FeeController');

    Route::post('employee/password/{id}' ,'EmployeeController@updatePassword')->name('employee.password');
	Route::resource('employee'   ,'EmployeeController');
    Route::resource('inquiry'    ,'InquiryController');
    Route::resource('role'       ,'RoleController');
    Route::resource('course'     ,'CourseController');

    Route::any('certificate/search'    ,'CertificateController@search')->name('certificate.search');
    Route::get('certificate/request'   ,'CertificateController@requests')->name('certificate.request');
    Route::get('authorise/certificate/{id}'   ,'CertificateController@authorise')->name('certificate.authorise');
    Route::get('issue/certificate/{id}','CertificateController@issueCertificate')->name('issue.certificate');
    Route::get('certificate/issue/{id}','CertificateController@issue')->name('certificate.issue');
    Route::resource('certificate'      ,'CertificateController');

    Route::any('reports/search','ReportsController@filter')->name('reports.filter');
    Route::resource('reports' ,'ReportsController');

    Route::get('performance/filter/{type}','PerformanceController@filter')->name('performance.filter');
    Route::resource('performance' ,'PerformanceController');

    Route::any('update-batch/{id}/edit','BatchController@editBatch')->name('batch.edit_batch');
    Route::any('assign-batch','BatchController@assignBatch')->name('batch.assign');
    Route::get('batch-assigned/{filter?}' ,'BatchController@assignedBatches')->name('batch.assigned');
    Route::get('batch-unassign/{id}' ,'BatchController@unassignBatch')->name('batch.unassign');
    Route::get('my-batch/{filter?}'  ,'BatchController@trainerBatch')->name('batch.trainer');

    Route::resource('batch' ,'BatchController');
    Route::resource('batch-category' ,'BatchCategoryController');

    Route::get('attendance/admin/{filter}' ,'AttendanceController@indexAdmin')->name('attendance.admin');
    Route::get('attendance/make/{filter?}' ,'AttendanceController@indexMake')->name('attendance.make');
    Route::post('attendance/admin-store' ,'AttendanceController@storeAdmin')->name('attendance.admin_create');

    Route::get('get-attendance/{id}' ,'AttendanceController@getAttendance');
    Route::get('attendance/my/{filter}' ,'AttendanceController@indexToday')->name('attendance.today');
    Route::resource('attendance' ,'AttendanceController');
    Route::resource('reviews' ,'ReviewsController');

});


Route::group(['namespace' => 'Student', 'prefix' => 'student'], function()
{
    //Authentication Routes
    Route::get('signin', 'StudentController@signin')->name('student.login');
    Route::post('auth/login', 'StudentController@authenticate')->name('login.student');
    Route::get('signout','StudentController@signout')->name('student.logout');

    //Dashboar Account routes
    // Route::get('/','StudentController@index');
    Route::get('dashboard','StudentController@dashboard')->name('student.dashboard');
    Route::match(['get','post'],'review/trainer','StudentController@reviewTrainer')->name('review.trainer');
    Route::get('apply/certificate','StudentController@applyCertificate')->name('apply.certificate');
});

/*
|-------------------------------------------------------------------------- */
