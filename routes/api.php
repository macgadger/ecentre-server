<?php
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
*/
use App\Controllers\API\APIController;

Route::group(['namespace' => 'API'], function() 
{
    Route::get('/get-trainers' ,'APIController@getTainers'); 
    Route::post('/validate' ,'APIController@validateID'); 
    Route::post('/apply-certificate' ,'APIController@applyCertificate'); 
    Route::post('/verify-certificate','APIController@verifyCertificate');
});
