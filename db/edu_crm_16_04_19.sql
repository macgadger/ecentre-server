-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 16, 2019 at 01:11 PM
-- Server version: 5.7.25-0ubuntu0.18.04.2
-- PHP Version: 7.2.15-0ubuntu0.18.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `edu_crm`
--

-- --------------------------------------------------------

--
-- Table structure for table `app_admission`
--

CREATE TABLE `app_admission` (
  `id` int(11) NOT NULL,
  `admission_uid` varchar(255) NOT NULL,
  `aid` int(11) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `gender` varchar(10) NOT NULL,
  `dob` date NOT NULL,
  `mobile` varchar(10) NOT NULL,
  `alt_mobile` varchar(10) DEFAULT NULL,
  `address` varchar(255) NOT NULL,
  `pg_address` varchar(255) DEFAULT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `country` varchar(100) NOT NULL,
  `email` varchar(255) NOT NULL,
  `college_name` varchar(255) NOT NULL,
  `college_deg_type` varchar(100) NOT NULL,
  `college_stream` varchar(100) NOT NULL,
  `college_semester` varchar(100) NOT NULL,
  `course_name` varchar(255) NOT NULL,
  `other_course` varchar(255) DEFAULT NULL,
  `course_durartion` varchar(100) NOT NULL,
  `course_joindate` date NOT NULL,
  `course_enddate` date NOT NULL,
  `course_time` varchar(100) NOT NULL,
  `course_fee` int(11) NOT NULL,
  `course_advance` int(11) NOT NULL,
  `payment_reciept` varchar(100) NOT NULL,
  `payment_type` varchar(100) DEFAULT NULL,
  `payment_mode` varchar(100) NOT NULL,
  `batch_assigned` int(11) NOT NULL DEFAULT '0',
  `refer_by` varchar(100) NOT NULL,
  `refer_name` varchar(100) NOT NULL,
  `admission_by` varchar(100) NOT NULL,
  `remarks` text,
  `status` enum('PENDING','JOINED') NOT NULL DEFAULT 'PENDING',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app_admission`
--

INSERT INTO `app_admission` (`id`, `admission_uid`, `aid`, `first_name`, `last_name`, `gender`, `dob`, `mobile`, `alt_mobile`, `address`, `pg_address`, `city`, `state`, `country`, `email`, `college_name`, `college_deg_type`, `college_stream`, `college_semester`, `course_name`, `other_course`, `course_durartion`, `course_joindate`, `course_enddate`, `course_time`, `course_fee`, `course_advance`, `payment_reciept`, `payment_type`, `payment_mode`, `batch_assigned`, `refer_by`, `refer_name`, `admission_by`, `remarks`, `status`, `created_at`, `updated_at`) VALUES
(23, '4473b2c7-8883-48fc-88e0-d7c78b3143c0', 1001, 'Manpreet', 'Singh', 'male', '2019-04-09', '7696200218', NULL, 'SCO 701, Seventh Floor', 'Tricity Trade Towen', 'zir', 'Punjab', 'IN', 'macgadger@outlook.com', 'sdasd', 'Degree', '113', 'final', '2', 'CCNA', '6 moths', '2019-04-15', '2019-04-30', '5:45 PM', 12000, 2000, '123123', NULL, 'cash', 0, 'google', 'sam', 'counsellor', NULL, 'PENDING', '2019-04-11 06:35:39', '2019-04-15 13:05:32'),
(24, 'f4bff222-cfa9-4947-aa17-1a3b90de4b71', 1002, 'sam', NULL, 'male', '2019-04-01', '7696200258', NULL, 'vip', NULL, 'zira', 'Punjab', 'India', 'sam@sam.com', 'cdc', 'Degree', 'sci', 'final', '1', NULL, '6 months', '2019-04-30', '2019-04-30', '5:45 PM', 12000, 2000, '89798798798', NULL, 'cash', 0, 'google', 'sam', 'counsellor', NULL, 'PENDING', '2019-04-11 06:38:20', '2019-04-15 13:05:37'),
(25, '935f6671-8ff1-48e2-94e0-c40396eba5b0', 1003, 'Manpreet', NULL, 'male', '2019-04-01', '7696200222', NULL, 'SCO 701, Seventh Floor', 'Tricity Trade Towen', 'zira', 'Punjab', 'India', '12macgadger@outlook.com', 'cdc', 'Degree', 'sci', 'final', '2', NULL, '6 months', '2019-04-12', '2019-04-30', '5:45 PM', 12500, 1500, '133213123', NULL, 'cash', 0, 'google', 'sami', 'counsellor', 'pay in emi 3', 'PENDING', '2019-04-11 06:41:20', '2019-04-15 13:06:13');

-- --------------------------------------------------------

--
-- Table structure for table `app_batch`
--

CREATE TABLE `app_batch` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `admission_uid` varchar(255) NOT NULL,
  `trainer_id` int(11) NOT NULL,
  `batch_date` date NOT NULL,
  `batch_time` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `app_course`
--

CREATE TABLE `app_course` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app_course`
--

INSERT INTO `app_course` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'CCNA', 1, '2019-03-30 07:13:56', '2019-03-30 07:13:56'),
(2, 'advance php', 1, '2019-03-30 01:50:06', '2019-03-30 01:50:06'),
(3, '.NEt', 1, '2019-04-09 06:35:44', '2019-04-09 06:35:44');

-- --------------------------------------------------------

--
-- Table structure for table `app_emi`
--

CREATE TABLE `app_emi` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `admission_id` varchar(255) NOT NULL,
  `amount` int(11) NOT NULL,
  `due_date` date NOT NULL,
  `status` enum('PAID','UNPAID') NOT NULL DEFAULT 'UNPAID',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `app_fee`
--

CREATE TABLE `app_fee` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `admission_id` varchar(255) NOT NULL,
  `amount_due` int(11) NOT NULL,
  `date_due` date NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` enum('DUE','PAID') NOT NULL DEFAULT 'DUE',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app_fee`
--

INSERT INTO `app_fee` (`id`, `student_id`, `admission_id`, `amount_due`, `date_due`, `user_id`, `status`, `created_at`, `updated_at`) VALUES
(10, 23, '4473b2c7-8883-48fc-88e0-d7c78b3143c0', 6500, '2019-04-15', 1, 'DUE', '2019-04-11 06:35:39', '2019-04-15 07:41:59'),
(11, 24, 'f4bff222-cfa9-4947-aa17-1a3b90de4b71', 9500, '2019-04-10', 1, 'DUE', '2019-04-11 06:38:21', '2019-04-15 07:44:52'),
(12, 25, '935f6671-8ff1-48e2-94e0-c40396eba5b0', 11000, '2019-04-30', 1, 'DUE', '2019-04-11 06:41:21', '2019-04-11 06:41:21');

-- --------------------------------------------------------

--
-- Table structure for table `app_inquiry`
--

CREATE TABLE `app_inquiry` (
  `id` int(11) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `gender` varchar(100) NOT NULL,
  `dob` date NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile` varchar(100) NOT NULL,
  `alt_mobile` varchar(100) DEFAULT NULL,
  `address` varchar(255) NOT NULL,
  `pg_address` varchar(255) DEFAULT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `country` varchar(100) NOT NULL,
  `course_name` varchar(255) NOT NULL,
  `course_join_date` date NOT NULL,
  `course_join_time` varchar(100) NOT NULL,
  `refer_by` varchar(100) NOT NULL,
  `refer_name` varchar(100) NOT NULL,
  `enquiry_by` int(11) NOT NULL,
  `status` enum('OPEN','CLOSE','CONVERT','FAILED') NOT NULL DEFAULT 'OPEN',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app_inquiry`
--

INSERT INTO `app_inquiry` (`id`, `first_name`, `last_name`, `gender`, `dob`, `email`, `mobile`, `alt_mobile`, `address`, `pg_address`, `city`, `state`, `country`, `course_name`, `course_join_date`, `course_join_time`, `refer_by`, `refer_name`, `enquiry_by`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Manpreet', 'singh', 'male', '2019-03-22', 'macgadger@outlook.com', '7696200218', NULL, 'SCO 701, Seventh Floor', NULL, 'goraya', 'Punjab', 'india', 'php 6 months', '2019-03-31', '10:00 AM', 'friend', 'sam', 1, 'OPEN', '2019-03-23 04:37:55', '2019-03-23 04:37:55');

-- --------------------------------------------------------

--
-- Table structure for table `app_migrations`
--

CREATE TABLE `app_migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `app_migrations`
--

INSERT INTO `app_migrations` (`id`, `migration`, `batch`) VALUES
(1, '2019_02_15_071445_create_users_table', 1),
(2, '2019_02_15_073504_create_profile_table', 1),
(3, '2019_02_15_073637_create_customers_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `app_payment`
--

CREATE TABLE `app_payment` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `admission_id` varchar(255) NOT NULL,
  `reciept_no` varchar(100) NOT NULL,
  `amount` int(11) NOT NULL,
  `payment_mode` varchar(100) NOT NULL DEFAULT 'CASH',
  `last_due` int(11) NOT NULL,
  `type` enum('ADVANCE','FEE','OTHER') NOT NULL DEFAULT 'ADVANCE',
  `user_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app_payment`
--

INSERT INTO `app_payment` (`id`, `student_id`, `admission_id`, `reciept_no`, `amount`, `payment_mode`, `last_due`, `type`, `user_id`, `status`, `created_at`, `updated_at`) VALUES
(3, 23, '4473b2c7-8883-48fc-88e0-d7c78b3143c0', '132131', 2000, 'CASH', 10000, 'ADVANCE', 1, 1, '2019-04-11 06:35:39', '2019-04-15 12:33:58'),
(4, 24, 'f4bff222-cfa9-4947-aa17-1a3b90de4b71', '133132', 2000, 'CASH', 10000, 'ADVANCE', 1, 1, '2019-04-11 06:38:21', '2019-04-15 12:33:52'),
(5, 25, '935f6671-8ff1-48e2-94e0-c40396eba5b0', '133133', 1500, 'CASH', 10000, 'ADVANCE', 1, 1, '2019-04-11 06:41:21', '2019-04-15 12:33:56'),
(7, 23, '4473b2c7-8883-48fc-88e0-d7c78b3143c0', '133134', 2000, 'CASH', 8000, 'FEE', 1, 1, '2019-04-12 01:07:10', '2019-04-15 12:34:01'),
(8, 23, '4473b2c7-8883-48fc-88e0-d7c78b3143c0', '133135', 1500, 'CASH', 6500, 'FEE', 1, 1, '2019-04-15 01:19:24', '2019-04-15 12:34:04'),
(9, 24, 'f4bff222-cfa9-4947-aa17-1a3b90de4b71', '133136', 500, 'CASH', 9500, 'FEE', 1, 1, '2019-04-15 01:29:30', '2019-04-15 12:34:06');

-- --------------------------------------------------------

--
-- Table structure for table `app_profile`
--

CREATE TABLE `app_profile` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('active','suspend') COLLATE utf8mb4_unicode_ci NOT NULL,
  `join_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `app_profile`
--

INSERT INTO `app_profile` (`id`, `user_id`, `first_name`, `last_name`, `mobile`, `address`, `city`, `state`, `country`, `status`, `join_date`, `created_at`, `updated_at`) VALUES
(1, 1, 'mac', 'singh', '7696200218', 'vip road', 'zirakpur', 'punjab', 'india', 'active', '2019-03-05 18:56:16', '2019-03-04 23:30:00', '2019-03-05 11:01:33'),
(3, 4, 'Manpreet', 'Singh', '7696200218', 'SCO 701, Seventh Floor\r\nTricity Trade Towen', 'Zirakpur', 'Punjab', 'India', 'active', '2019-03-06 17:03:32', '2019-03-06 06:03:32', '2019-03-06 06:03:32');

-- --------------------------------------------------------

--
-- Table structure for table `app_roles`
--

CREATE TABLE `app_roles` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `can_view` int(11) NOT NULL DEFAULT '0',
  `can_update` int(11) NOT NULL DEFAULT '0',
  `can_remove` int(11) NOT NULL DEFAULT '0',
  `can_account` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app_roles`
--

INSERT INTO `app_roles` (`id`, `name`, `can_view`, `can_update`, `can_remove`, `can_account`, `status`, `created_at`, `updated_at`) VALUES
(1, 'ADMIN', 1, 1, 1, 1, 1, '2019-03-27 06:33:50', '2019-03-27 08:56:23'),
(2, 'MANAGER', 1, 1, 0, 0, 1, '2019-03-27 06:34:18', '2019-03-27 06:57:03'),
(3, 'COUNSELOR', 1, 0, 0, 0, 1, '2019-03-27 06:56:06', '2019-03-27 08:46:50'),
(4, 'ACCOUNTS', 1, 1, 0, 1, 1, '2019-03-27 07:50:32', '2019-03-27 07:50:32');

-- --------------------------------------------------------

--
-- Table structure for table `app_users`
--

CREATE TABLE `app_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('active','suspend') COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` int(11) NOT NULL DEFAULT '3',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `app_users`
--

INSERT INTO `app_users` (`id`, `email`, `password`, `status`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin@admin.com', '$2y$10$GtFN3a3gLnwdc2c0/wm/Su1s/C4Nsqgr60zAnEYzT2Ve/DzXlWNoe', 'active', 1, NULL, '2019-03-05 06:30:00', '2019-03-30 07:01:18'),
(4, 'macgadger@outlook.com', '$2y$10$TjStCeIIZdAWr2JWbFC6s.yhTDVGRzeROiQGIIQ29MhaE7Ro.dA.O', 'active', 3, NULL, '2019-03-06 06:03:32', '2019-03-27 11:20:05');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `app_admission`
--
ALTER TABLE `app_admission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_batch`
--
ALTER TABLE `app_batch`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `student_id` (`student_id`),
  ADD KEY `student_id_2` (`student_id`);

--
-- Indexes for table `app_course`
--
ALTER TABLE `app_course`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_emi`
--
ALTER TABLE `app_emi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_fee`
--
ALTER TABLE `app_fee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_inquiry`
--
ALTER TABLE `app_inquiry`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_migrations`
--
ALTER TABLE `app_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_payment`
--
ALTER TABLE `app_payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_profile`
--
ALTER TABLE `app_profile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_roles`
--
ALTER TABLE `app_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_users`
--
ALTER TABLE `app_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `app_admission`
--
ALTER TABLE `app_admission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `app_batch`
--
ALTER TABLE `app_batch`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `app_course`
--
ALTER TABLE `app_course`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `app_emi`
--
ALTER TABLE `app_emi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `app_fee`
--
ALTER TABLE `app_fee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `app_inquiry`
--
ALTER TABLE `app_inquiry`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `app_migrations`
--
ALTER TABLE `app_migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `app_payment`
--
ALTER TABLE `app_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `app_profile`
--
ALTER TABLE `app_profile`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `app_roles`
--
ALTER TABLE `app_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `app_users`
--
ALTER TABLE `app_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
