-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 15, 2019 at 05:04 PM
-- Server version: 5.7.25-0ubuntu0.18.04.2
-- PHP Version: 7.2.15-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `edu_crm`
--

-- --------------------------------------------------------

--
-- Table structure for table `app_batch`
--

CREATE TABLE `app_batch` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `type` varchar(100) NOT NULL,
  `centre` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `batch_code` varchar(255) NOT NULL,
  `batch_start_date` date NOT NULL,
  `batch_time` varchar(100) NOT NULL,
  `course` varchar(255) NOT NULL,
  `course_fee` int(11) NOT NULL,
  `payment_mode` enum('pay_cash','pay_emi') NOT NULL DEFAULT 'pay_cash',
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `app_emi`
--

CREATE TABLE `app_emi` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `admission_id` varchar(255) NOT NULL,
  `amount` int(11) NOT NULL,
  `due_date` date NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app_emi`
--

INSERT INTO `app_emi` (`id`, `student_id`, `admission_id`, `amount`, `due_date`, `status`, `created_at`, `updated_at`) VALUES
(1, 7, 'fc65f1d7-4193-4008-9bf6-366df3544a6c', 5000, '2019-04-01', 1, '2019-03-15 09:11:35', '2019-03-15 09:11:35'),
(2, 7, 'fc65f1d7-4193-4008-9bf6-366df3544a6c', 10000, '2019-05-01', 1, '2019-03-15 09:11:35', '2019-03-15 09:11:35');

-- --------------------------------------------------------

--
-- Table structure for table `app_fee`
--

CREATE TABLE `app_fee` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `admission_id` varchar(255) NOT NULL,
  `amount` int(11) NOT NULL,
  `type` enum('COURSE_FULL_FEE','COURSE_FEE','EMI_FEE','REGISTRATION_FEE') NOT NULL DEFAULT 'REGISTRATION_FEE',
  `remarks` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app_fee`
--

INSERT INTO `app_fee` (`id`, `student_id`, `admission_id`, `amount`, `type`, `remarks`, `status`, `created_at`, `updated_at`) VALUES
(1, 3, '1e4f63a6-58a7-4eb8-b4dd-70a6981dde39', 15000, 'COURSE_FEE', 'pay_cash', 1, '2019-03-15 02:56:37', '2019-03-15 10:11:15'),
(2, 5, 'ee726f0e-4b82-4265-9c1d-c9c1da7737e0', 5000, 'REGISTRATION_FEE', 'pay_registration', 1, '2019-03-15 03:33:51', '2019-03-15 10:11:24'),
(3, 7, 'fc65f1d7-4193-4008-9bf6-366df3544a6c', 5000, 'REGISTRATION_FEE', 'pay_emi', 1, '2019-03-15 03:41:35', '2019-03-15 10:11:28');

-- --------------------------------------------------------

--
-- Table structure for table `app_migrations`
--

CREATE TABLE `app_migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `app_migrations`
--

INSERT INTO `app_migrations` (`id`, `migration`, `batch`) VALUES
(1, '2019_02_15_071445_create_users_table', 1),
(2, '2019_02_15_073504_create_profile_table', 1),
(3, '2019_02_15_073637_create_customers_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `app_profile`
--

CREATE TABLE `app_profile` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('active','suspend') COLLATE utf8mb4_unicode_ci NOT NULL,
  `join_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `app_profile`
--

INSERT INTO `app_profile` (`id`, `user_id`, `first_name`, `last_name`, `mobile`, `address`, `city`, `state`, `country`, `status`, `join_date`, `created_at`, `updated_at`) VALUES
(1, 1, 'mac', 'singh', '7696200218', 'vip road', 'zirakpur', 'punjab', 'india', 'active', '2019-03-05 18:56:16', '2019-03-04 23:30:00', '2019-03-05 11:01:33'),
(3, 4, 'Manpreet', 'Singh', '7696200218', 'SCO 701, Seventh Floor\r\nTricity Trade Towen', 'Zirakpur', 'Punjab', 'India', 'active', '2019-03-06 17:03:32', '2019-03-06 06:03:32', '2019-03-06 06:03:32');

-- --------------------------------------------------------

--
-- Table structure for table `app_student`
--

CREATE TABLE `app_student` (
  `id` int(11) NOT NULL,
  `admission_uid` varchar(255) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `dob` date NOT NULL,
  `mobile` varchar(10) NOT NULL,
  `alt_mobile` varchar(10) DEFAULT NULL,
  `address` varchar(255) NOT NULL,
  `pg_address` varchar(255) DEFAULT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `country` varchar(100) NOT NULL,
  `email` varchar(255) NOT NULL,
  `college_name` varchar(255) NOT NULL,
  `college_deg_type` varchar(100) NOT NULL,
  `college_stream` varchar(100) NOT NULL,
  `college_semester` varchar(100) NOT NULL,
  `college_duration` varchar(100) NOT NULL,
  `course_name` varchar(255) NOT NULL,
  `course_durartion` varchar(100) NOT NULL,
  `course_joindate` date NOT NULL,
  `course_enddate` date NOT NULL,
  `course_time` varchar(100) NOT NULL,
  `course_fee` int(11) NOT NULL,
  `course_advance` int(11) NOT NULL,
  `payment_mode` varchar(100) NOT NULL,
  `status` enum('PENDING','JOINED') NOT NULL DEFAULT 'PENDING',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app_student`
--

INSERT INTO `app_student` (`id`, `admission_uid`, `first_name`, `last_name`, `gender`, `dob`, `mobile`, `alt_mobile`, `address`, `pg_address`, `city`, `state`, `country`, `email`, `college_name`, `college_deg_type`, `college_stream`, `college_semester`, `college_duration`, `course_name`, `course_durartion`, `course_joindate`, `course_enddate`, `course_time`, `course_fee`, `course_advance`, `payment_mode`, `status`, `created_at`, `updated_at`) VALUES
(3, '1e4f63a6-58a7-4eb8-b4dd-70a6981dde39', 'Manpreet', 'Singh', 'male', '2019-03-07', '7696200218', NULL, 'SCO 701, Seventh Floor\r\nTricity Trade Towen', 'SCO 701, Seventh Floor\r\nTricity Trade Towen', 'zira', 'Punjab', 'IN', 'macgadger@outlook.com', 'cdc ladran', 'Master', 'phy', 'final', '1 year', 'php advance', '6 months', '2019-03-20', '2019-03-31', '2:00 PM', 15000, 0, 'pay_cash', 'PENDING', '2019-03-15 02:56:37', '2019-03-15 02:56:37'),
(5, 'ee726f0e-4b82-4265-9c1d-c9c1da7737e0', 'Sukhi', 'singh', 'male', '2019-03-01', '981507990', NULL, 'house 234, gt road jalandhar', NULL, 'jalandhar', 'Punjab', 'India', 'suki@suki.com', 'lpu jal', 'Degree', 'phy', 'final', '1 year', 'asp .net', '6 months', '2019-03-20', '2019-09-20', '3:00 PM', 15000, 5000, 'pay_registration', 'PENDING', '2019-03-15 03:33:51', '2019-03-15 03:33:51'),
(7, 'fc65f1d7-4193-4008-9bf6-366df3544a6c', 'rohini', 'sharma', 'female', '2018-12-04', '9855012015', NULL, 'house 34, sector 35d', NULL, 'chandigarh', 'Chandigarh', 'India', 'rohini@ymail.com', 'arya tech', 'Diploma', 'science', '2nd', '1 month', 'seo expert', '6 months', '2019-04-01', '2019-10-01', '11:45 AM', 20000, 5000, 'pay_emi', 'PENDING', '2019-03-15 03:41:34', '2019-03-15 03:41:34');

-- --------------------------------------------------------

--
-- Table structure for table `app_users`
--

CREATE TABLE `app_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('active','suspend') COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` enum('admin','manager','staff','counsellor','account') COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `app_users`
--

INSERT INTO `app_users` (`id`, `email`, `password`, `status`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin@admin.com', '$2y$10$GtFN3a3gLnwdc2c0/wm/Su1s/C4Nsqgr60zAnEYzT2Ve/DzXlWNoe', 'active', 'admin', NULL, '2019-03-05 06:30:00', '2019-03-05 13:30:44'),
(4, 'macgadger@outlook.com', '$2y$10$TjStCeIIZdAWr2JWbFC6s.yhTDVGRzeROiQGIIQ29MhaE7Ro.dA.O', 'active', 'staff', NULL, '2019-03-06 06:03:32', '2019-03-06 11:38:34');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `app_batch`
--
ALTER TABLE `app_batch`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `student_id` (`student_id`),
  ADD KEY `student_id_2` (`student_id`);

--
-- Indexes for table `app_emi`
--
ALTER TABLE `app_emi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_fee`
--
ALTER TABLE `app_fee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_migrations`
--
ALTER TABLE `app_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_profile`
--
ALTER TABLE `app_profile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_student`
--
ALTER TABLE `app_student`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_users`
--
ALTER TABLE `app_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `app_batch`
--
ALTER TABLE `app_batch`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `app_emi`
--
ALTER TABLE `app_emi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `app_fee`
--
ALTER TABLE `app_fee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `app_migrations`
--
ALTER TABLE `app_migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `app_profile`
--
ALTER TABLE `app_profile`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `app_student`
--
ALTER TABLE `app_student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `app_users`
--
ALTER TABLE `app_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
