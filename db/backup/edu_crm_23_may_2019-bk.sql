-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 23, 2019 at 09:20 AM
-- Server version: 5.7.26-0ubuntu0.18.04.1
-- PHP Version: 7.2.17-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `edu_crm`
--

-- --------------------------------------------------------

--
-- Table structure for table `app_admission`
--

CREATE TABLE `app_admission` (
  `id` int(11) NOT NULL,
  `admission_uid` varchar(255) NOT NULL,
  `aid` int(11) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `gender` varchar(10) NOT NULL,
  `father_name` varchar(100) NOT NULL,
  `dob` date NOT NULL,
  `mobile` varchar(10) NOT NULL,
  `alt_mobile` varchar(10) DEFAULT NULL,
  `address` varchar(255) NOT NULL,
  `pg_address` varchar(255) DEFAULT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `country` varchar(100) NOT NULL,
  `email` varchar(255) NOT NULL,
  `college_name` varchar(255) NOT NULL,
  `college_deg_type` varchar(100) NOT NULL,
  `college_stream` varchar(100) NOT NULL,
  `college_semester` varchar(100) NOT NULL,
  `course_name` varchar(255) NOT NULL,
  `other_course` varchar(255) DEFAULT NULL,
  `course_durartion` varchar(100) NOT NULL,
  `course_joindate` date NOT NULL,
  `course_enddate` date NOT NULL,
  `course_time` varchar(100) NOT NULL,
  `course_fee` int(11) NOT NULL,
  `course_advance` int(11) NOT NULL,
  `payment_reciept` varchar(100) NOT NULL,
  `payment_type` varchar(100) DEFAULT NULL,
  `payment_mode` varchar(100) NOT NULL,
  `batch_assigned` int(11) NOT NULL DEFAULT '0',
  `refer_by` varchar(100) NOT NULL,
  `refer_name` varchar(100) NOT NULL,
  `admission_by` varchar(100) NOT NULL,
  `remarks` text,
  `status` enum('PENDING','JOINED','LEAVE','TRASH') NOT NULL DEFAULT 'PENDING',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app_admission`
--

INSERT INTO `app_admission` (`id`, `admission_uid`, `aid`, `first_name`, `last_name`, `gender`, `father_name`, `dob`, `mobile`, `alt_mobile`, `address`, `pg_address`, `city`, `state`, `country`, `email`, `college_name`, `college_deg_type`, `college_stream`, `college_semester`, `course_name`, `other_course`, `course_durartion`, `course_joindate`, `course_enddate`, `course_time`, `course_fee`, `course_advance`, `payment_reciept`, `payment_type`, `payment_mode`, `batch_assigned`, `refer_by`, `refer_name`, `admission_by`, `remarks`, `status`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`) VALUES
(34, '7e5253eb-4e4d-490c-8c79-e65f037276ca', 1002, 'HEMANT', 'MALIK', 'male', '', '2000-09-12', '9995524137', NULL, 'HISAR, Haryana', NULL, 'kharar', 'Punjab', 'India', 'FREESEWA12@GMAIL.COM', 'cu', 'Degree', 'it', '4', '1', NULL, '45 days', '2019-05-15', '2019-07-05', '4:45 PM', 8000, 2000, '4053', NULL, 'paytm', 0, 'advertisement', 'counseller', '7', 'He Will Pay In Two Installments.', 'JOINED', '2019-04-25 16:39:27', 6, '2019-05-15 16:00:00', 6, '2019-04-25 11:09:27', NULL),
(35, '10a5f432-5de5-42e4-a8c5-254a5f954828', 1003, 'GURSIMRAN', 'SINGH', 'male', '', '1999-06-08', '9988399123', NULL, 'KHANNA', NULL, 'KHANNA', 'Punjab', 'India', 'GURSIMRANGHARIAL@GMAIL.COM', 'GGI, KHANNA', 'Degree', 'CSE', '4', '2', NULL, '45 DAYS', '2019-06-03', '2019-07-18', '4:45 PM', 5000, 1000, '4110', NULL, 'paytm', 0, 'other', 'COUNSELLOR', '7', 'HE WILL PAY IN TWO INSTALMENTS', 'PENDING', '2019-04-25 16:48:51', 6, '2019-04-25 16:48:51', NULL, '2019-04-25 11:18:51', NULL),
(36, '37b63498-ad7c-46bd-9f51-18087033fc41', 1004, 'ARSHDEEP', 'SINGH', 'male', '', '1999-05-01', '9598757541', NULL, 'KHANNA', NULL, 'KHANNA', 'Punjab', 'India', 'ARSHDEEPSINGH@GMAIL.COM', 'GGI, KHANNA', 'Degree', 'CSE', '4', '2', NULL, '45 DAYS', '2019-06-03', '2019-07-18', '5:45 PM', 5000, 1000, '4111', NULL, 'paytm', 0, 'google', 'COUNSELLOR', '7', NULL, 'PENDING', '2019-04-25 17:42:55', 6, '2019-04-25 17:42:55', NULL, '2019-04-25 12:12:55', NULL),
(37, '4cb53ae0-46ae-4393-b239-044aeed070c2', 1005, 'ANKIT', NULL, 'male', '', '1999-06-01', '8286832145', NULL, 'KHANNA', NULL, 'KHANNA', 'Punjab', 'India', 'ANKITSHARMA@GMAIL.COM', 'GGI, KHANNNA', 'Degree', 'CSE', '4', '2', NULL, '45 DAYS', '2019-06-03', '2019-07-18', '5:45 PM', 5000, 1000, '4112', NULL, 'paytm', 0, 'social', 'COUNSELLOR', '7', NULL, 'PENDING', '2019-04-25 17:45:29', 6, '2019-04-25 17:45:29', NULL, '2019-04-25 12:15:29', NULL),
(38, '94569776-c9ae-4e6d-a946-5348e609013c', 1006, 'HIMANSHU', NULL, 'male', '', '2000-02-02', '8054709934', NULL, 'KHANNA', NULL, 'KHANNA', 'Punjab', 'India', 'himanshu@gmail.com', 'GGI, KHANNA', 'Degree', 'CSE', '4', '2', NULL, '45 DAYS', '2019-06-03', '2019-07-18', '6:00 PM', 5000, 1000, '4113', NULL, 'paytm', 0, 'google', 'COUNSELLOR', '7', NULL, 'PENDING', '2019-04-25 17:53:03', 6, '2019-04-25 17:53:03', NULL, '2019-04-25 12:23:03', NULL),
(39, '8b1ce920-06fc-48b3-b5e4-d9516de0799a', 1007, 'MADHAV', NULL, 'male', '', '1999-05-01', '9510101251', NULL, 'KHANNA', NULL, 'KHANNA', 'Punjab', 'India', 'madhavsharma@gmail.com', 'GGI, KHANNA', 'Degree', 'CSE', '4', '2', NULL, '45 DAYS', '2019-06-03', '2019-07-18', '6:00 PM', 5000, 1000, '4115', NULL, 'paytm', 0, 'online', 'COUNSELLOR', '7', NULL, 'PENDING', '2019-04-25 17:57:31', 6, '2019-04-25 17:57:31', NULL, '2019-04-25 12:27:31', NULL),
(40, '97c8a4c7-0f08-42a0-a0ef-7ee070c46514', 1008, 'MANISHA', 'RANI', 'female', '', '1998-08-23', '9816401539', NULL, 'DAULATPUR', NULL, 'HIMACHAL', 'Himachal Pradesh', 'India', 'manishathakur@gmail.com', 'G.C. DAULATPUR', 'Degree', 'CSE', '4', '3', NULL, '45 DAYS', '2019-05-22', '2019-07-02', '6:00 PM', 5000, 2000, '4118', NULL, 'cash', 0, 'email', 'COUNSELLOR', '7', NULL, 'JOINED', '2019-04-25 18:09:56', 6, '2019-05-22 09:50:54', 6, '2019-04-25 12:39:56', NULL),
(41, '32dd28d2-7d71-48df-9440-356c1503635d', 1009, 'RUCHI', 'DEVI', 'female', 'MOHINDER SINGH', '2000-04-01', '8580723547', NULL, 'DAULATPUR', NULL, 'HIMACHAL', 'Andaman and Nicobar Islands', 'India', 'ruchithakuruna@gmail.com', 'G.C. DAULATPUR', 'Degree', 'BBA', '4', '3', NULL, '45 DAYS', '2019-05-14', '2019-07-03', '6:15 PM', 5000, 2000, '4119', NULL, 'cash', 0, 'google', 'COUNSELLOR', '7', NULL, 'JOINED', '2019-04-25 18:15:15', 6, '2019-05-14 11:05:22', 6, '2019-04-25 12:45:15', NULL),
(42, 'bcab8c6d-1f3b-48f2-8836-fc4babfaee15', 1010, 'PRIYA', 'RANI', 'female', 'RAJ KUMAR', '1999-09-02', '7807031544', NULL, 'una', NULL, 'una', 'Himachal Pradesh', 'India', 'ranapriya89902@gmail.com', 'G.C. DAULATPUR', 'Degree', 'BBA', '4', '3', NULL, '45 DAYS', '2019-05-14', '2019-07-05', '4:15 PM', 5000, 2000, '4120', NULL, 'cash', 0, 'social', 'COUNSELLOR', '7', NULL, 'JOINED', '2019-04-29 16:13:45', 6, '2019-05-14 10:50:11', 6, '2019-04-29 10:43:45', NULL),
(43, '48dff572-1541-4e23-ad4b-98a392f761a8', 1011, 'ALKA', 'CHOUDHARY', 'female', 'CHANDERPAL SINGH', '1998-12-21', '7807309762', NULL, 'UNA', NULL, 'UNA', 'Himachal Pradesh', 'India', 'alkachaudhary3667@gmail.com', 'G.C. DAULATPUR', 'Degree', 'BBA', '4', '3', NULL, '45 DAYS', '2019-05-22', '2019-07-05', '4:15 PM', 5000, 2000, '4121', NULL, 'cash', 0, 'advertisement', 'COUNSELLOR', '7', NULL, 'JOINED', '2019-04-29 16:17:59', 6, '2019-05-22 09:51:18', 6, '2019-04-29 10:47:59', NULL),
(44, 'd6550116-e524-4d7f-a3a2-f0eb81b1ba8a', 1012, 'RABBUL ALAM', 'SARKAR', 'male', 'SOFIQUL ALAM', '1998-09-28', '7638180000', NULL, 'ASSAm', NULL, 'assam', 'Assam', 'India', 'RABBULALAMSARKAR@GMAIL.COM', 'sviet, banur', 'Diploma', 'cse', '4', '1', NULL, '45 days', '2019-05-20', '2019-07-05', '4:30 PM', 4000, 1000, '4135', NULL, 'paytm', 0, 'google', 'counsellor', '9', NULL, 'PENDING', '2019-04-29 16:25:30', 6, '2019-04-29 16:25:30', NULL, '2019-04-29 10:55:30', NULL),
(45, 'e66163a5-3368-4f9a-ba37-418454879358', 1013, 'rohit', 'sharma', 'male', 'rekha sharma', '1999-11-08', '8894671245', NULL, 'basantpur, mandi', NULL, 'mandi', 'Andaman and Nicobar Islands', 'India', 'rohitsharma9550@gmail.com', 'cu', 'Degree', 'cse', '4', '2', NULL, '45 days', '2019-05-21', '2019-06-29', '4:30 PM', 6000, 1000, '4092', NULL, 'online', 0, 'other', 'counsellor', '7', NULL, 'JOINED', '2019-04-29 16:30:13', 6, '2019-05-21 15:16:59', 6, '2019-04-29 11:00:13', NULL),
(46, '65ab8576-9097-48a9-8396-6c474c238351', 1014, 'abhinash kumar', 'mandal', 'male', 'gopal mandal', '1999-01-10', '8507762529', NULL, 'bihar', NULL, 'bihar', 'Bihar', 'India', 'kumarabhinash10199@gmail.com', 'cgctc, jhanjeri', 'Degree', 'cse', '4', '1', NULL, '45 days', '2019-05-15', '2019-07-18', '4:30 PM', 5500, 500, '4139', NULL, 'cash', 0, 'online', 'counsellor', '7', NULL, 'JOINED', '2019-04-29 16:39:43', 6, '2019-05-15 16:02:22', 6, '2019-04-29 11:09:43', NULL),
(47, 'ed8ee1e9-a26b-4263-b7b9-3781a7eb0f49', 1015, 'rana kumar', 'saini', 'male', 'pramod kumar', '2019-04-06', '7500432864', NULL, 'near labour court, saharanpur', NULL, 'saharanpur', 'Uttar Pradesh', 'India', 'ranakumarsaini@gmail.com', 'cgctc, jhanjeri', 'Degree', 'cse', '4', '1', NULL, '45 days', '2019-05-15', '2019-07-18', '4:45 PM', 5500, 500, '4140', NULL, 'cash', 0, 'google', 'counsellor', '7', NULL, 'JOINED', '2019-04-29 17:36:31', 6, '2019-05-15 16:01:47', 6, '2019-04-29 12:06:31', NULL),
(48, 'afb89688-652b-4a53-997b-941c857c2b00', 1016, 'akash', 'priyadarshi', 'male', 'akhilesh singh', '1997-06-12', '9113302937', NULL, 'jhanjeri', NULL, 'mohali', 'Punjab', 'India', 'apriyadarshi407@gmail.com', 'cgctc jhanjeri', 'Degree', 'cse', '4', '1', NULL, '45 days', '2019-05-15', '2019-07-18', '5:45 PM', 5500, 1000, '4141', NULL, 'gpay', 0, 'other', 'counsellor', '7', NULL, 'JOINED', '2019-04-29 17:45:51', 6, '2019-05-15 16:01:27', 6, '2019-04-29 12:15:51', NULL),
(49, '2c64b5da-a520-4560-90b7-d3f35c371c93', 1017, 'abhinav', 'bhardwaj', 'male', 'brajpal bhardwaj', '1998-06-06', '9915208724', NULL, 'derabassi', NULL, 'ludhiana', 'Punjab', 'India', 'bhardwaja990@gmail.com', 'cgctc, jhanjeri', 'Degree', 'cse', '4', '1', NULL, '45 days', '2019-06-03', '2019-07-18', '6:00 PM', 5500, 1000, '4142', NULL, 'cash', 0, 'google', 'counsellor', '7', NULL, 'PENDING', '2019-04-29 17:52:07', 6, '2019-04-29 17:52:07', NULL, '2019-04-29 12:22:07', NULL),
(50, '1d0dc134-1250-4015-83e2-6a9cd6a31f82', 1018, 'amlesh', 'roy', 'male', 'abhay roy', '1998-09-11', '8725879100', NULL, 'bihar', NULL, 'bihar', 'Bihar', 'India', 'amlesh31@gmail.com', 'cgctc, jhanjeri', 'Degree', 'cse', '4', '1', NULL, '45 days', '2019-05-15', '2019-07-18', '6:00 PM', 5500, 500, '4138', NULL, 'cash', 0, 'google', 'counsellor', '7', NULL, 'JOINED', '2019-04-29 17:56:49', 6, '2019-05-15 16:00:50', 6, '2019-04-29 12:26:49', NULL),
(51, '1b1d9e9e-6a68-4b53-8566-05e70cdcd2aa', 1019, 'jaspreet', 'singh', 'male', 'jaswand singh', '1998-10-20', '9649217123', NULL, 'mundi kharar', NULL, 'kharar', 'Punjab', 'India', 'jaspreetsingh6065@gmail.com', 'cu', 'Degree', 'cse', '4', '2', NULL, '45 days', '2019-05-20', '2019-07-05', '6:00 PM', 5000, 2000, '4096', NULL, 'online', 0, 'email', 'counsellor', '9', NULL, 'PENDING', '2019-04-29 18:01:13', 6, '2019-04-29 18:01:13', NULL, '2019-04-29 12:31:13', NULL),
(52, '866abc84-91c6-4100-a79a-af0675ce5e12', 1020, 'robin', 'singh', 'male', 'raghubir singh', '1999-02-24', '8571707799', NULL, 'haryana', NULL, 'haryana', 'Haryana', 'India', 'coolsingh8520@gmail.com', 'cu', 'Degree', 'cse', '4', '1', NULL, '45 days', '2019-05-14', '2019-07-18', '6:15 PM', 5000, 500, '4144', NULL, 'cash', 0, 'google', 'counsellor', '7', 'A2IT CERTIFICATE AND MICROSOFT CERTIFICATION ONLY', 'JOINED', '2019-04-29 18:05:54', 6, '2019-05-14 11:06:59', 6, '2019-04-29 12:35:54', NULL),
(53, '60a195ad-264c-4d7d-929c-247ce4ee5f5e', 1021, 'SHAGUN', 'TIWARI', 'male', 'SANJAY TIWARI', '1999-10-17', '8889377436', NULL, 'MADHYA PRADESH', NULL, 'MADHYA PRADESH', 'Andaman and Nicobar Islands', 'India', 'shagun17t@gmail.com', 'CU', 'Degree', 'CSE', '4', '2', NULL, '45 DAYS', '2019-05-20', '2019-06-30', '4:45 PM', 5000, 2000, '4095', NULL, 'online', 0, 'google', 'COUNSELLOR', '9', NULL, 'PENDING', '2019-04-30 16:43:10', 6, '2019-05-16 16:24:23', 6, '2019-04-30 11:13:10', NULL),
(54, '8b5e5db4-1823-47e6-9854-2b139561df42', 1022, 'NAVDEEP', 'SINGH', 'male', 'BALTEJ SINGH', '1994-09-23', '9855911274', NULL, 'PAYAL', NULL, 'LUDHIANA', 'Punjab', 'India', 'NAVDEEP220001@GMAIL.COM', 'DBU', 'Diploma', 'ME', '4', '5', NULL, '45 DAYS', '2019-05-20', '2019-07-05', '5:00 PM', 4000, 1000, '4166', NULL, 'paytm', 0, 'other', 'COUNSELLOR', '9', NULL, 'PENDING', '2019-04-30 16:51:31', 6, '2019-04-30 16:51:31', NULL, '2019-04-30 11:21:31', NULL),
(55, '24d89c1a-8691-44c2-8d19-2e7769ba2aad', 1023, 'NAVDEEP', 'SINGH', 'male', 'AVTAR SINGH', '2000-06-15', '8556089914', NULL, 'MORINDA', NULL, 'ROPAR', 'Punjab', 'India', 'NAVDEEPSINGH89952@GMAIL.COM', 'GOVT. SEN. SEC. SCHOOL', 'Diploma', 'ARTS', '4', '1', NULL, '2 MONTHS', '2019-05-20', '2019-07-05', '5:00 PM', 8000, 1000, '4167', NULL, 'paytm', 0, 'friend', 'MR. RAJEEV', '13', NULL, 'PENDING', '2019-04-30 17:05:53', 6, '2019-04-30 17:39:59', 6, '2019-04-30 11:35:53', NULL),
(56, '84ba54e3-efc4-43ec-b2f9-2f8b36875fa4', 1024, 'SHUBHAM', 'CHOUDHARY', 'male', 'GOPAL PRASAD CHOUDHARY', '1998-09-07', '8252297811', NULL, 'GHARUAN', NULL, 'KHARAR', 'Punjab', 'India', 'SHUBHAMAGARWAL0069@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '2', NULL, '45 DAYS', '2019-06-10', '2019-07-25', '5:15 PM', 5000, 1000, '4169', NULL, 'paytm', 0, 'friend', 'COUNSELOR', '9', NULL, 'PENDING', '2019-04-30 17:12:01', 6, '2019-05-16 16:40:23', 6, '2019-04-30 11:42:01', NULL),
(57, 'b9ba3767-519b-4a93-b1e9-7923577a3a4c', 1025, 'KULDEEP', 'ATTRI', 'male', 'KEHAR CHAND DOGRA', '1999-01-16', '8544917578', NULL, 'GHARUAN', NULL, 'KHARAR', 'Punjab', 'India', 'ATTRIKULDEEP28@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '2', NULL, '45 DAYS', '2019-05-21', '2019-06-30', '5:15 PM', 5000, 1000, '4170', NULL, 'paytm', 0, 'online', 'COUNSELOR', '9', NULL, 'PENDING', '2019-04-30 17:16:07', 6, '2019-05-17 13:34:45', 6, '2019-04-30 11:46:07', NULL),
(58, '14111cf9-9495-43f4-941c-baed158a3d5e', 1026, 'HARMAN', 'SINGH', 'male', 'GURMEET SINGH', '1998-06-30', '8556001370', NULL, 'KHANNA', NULL, 'KHANNA', 'Punjab', 'India', 'RAMGARHIA4436@GMAIL.COM', 'DBU', 'Degree', 'ME', '4', '5', NULL, '45 DAYS', '2019-06-01', '2019-07-15', '5:30 PM', 5000, 1000, '4173', NULL, 'cash', 0, 'email', 'COUNSELOR', '9', NULL, 'PENDING', '2019-04-30 17:19:26', 6, '2019-04-30 17:19:26', NULL, '2019-04-30 11:49:26', NULL),
(59, 'c85c1234-778e-4135-a592-375e96aeadc7', 1027, 'gurwinder', 'singh', 'male', 'sarabjeet singh', '1996-09-14', '9464685375', NULL, 'payal', NULL, 'ludhiana', 'Punjab', 'India', 'guri.gurwinder14@gmail.com', 'dbu', 'Degree', 'me', '4', '5', 'SOLIDWORKS', '45 days', '2019-06-01', '2019-07-15', '6:00 PM', 5000, 1000, '4174', NULL, 'cash', 0, 'other', 'counselor', '9', NULL, 'PENDING', '2019-04-30 17:55:31', 6, '2019-04-30 17:55:31', NULL, '2019-04-30 12:25:31', NULL),
(60, '5891c114-6e40-4fbc-a567-c0d9cb787aed', 1028, 'omkar', 'kumar', 'male', 'parshuram bhardwaj', '1996-10-06', '8283816517', NULL, 'khanna', NULL, 'khanna', 'Punjab', 'India', 'omkarbhardwaj1450@gmail.com', 'dbu', 'Degree', 'ae', '4', '5', 'SOLIDWORKS', '45 days', '2019-06-01', '2019-07-15', '6:00 PM', 5000, 1000, '4172', NULL, 'cash', 0, 'other', 'counselor', '9', NULL, 'PENDING', '2019-04-30 17:59:31', 6, '2019-04-30 17:59:31', NULL, '2019-04-30 12:29:31', NULL),
(61, 'bdd9203c-7b15-4cb9-a573-8d93a3eac6ea', 1029, 'ravinder', 'kaur', 'female', 'surjit singh', '1997-09-20', '9872003467', NULL, 'nawanshehr', NULL, 'sbs nagar', 'Punjab', 'India', 'er.satishkumar21@gmail.com', 'rayat inst. of management', 'Master', 'mba', '4', '3', NULL, '45 days', '2019-05-14', '2019-07-15', '6:00 PM', 3000, 3000, '4206', NULL, 'cash', 0, 'google', 'pawan', '13', 'CERTIFICATE ONLY', 'JOINED', '2019-04-30 18:07:52', 6, '2019-05-14 11:07:48', 6, '2019-04-30 12:37:52', NULL),
(62, 'bcc7a276-38fd-4217-b86c-480331a727dc', 1030, 'bikash kumar', 'das', 'male', 'tileshar das', '1998-05-30', '8427769211', NULL, 'nepal', NULL, 'janakpur', 'Punjab', 'nepal', 'adityanaryan639@gmail.com', 'sviet, banur', 'Degree', 'cse', '4', '1', NULL, '45 days', '2019-05-20', '2019-07-05', '6:15 PM', 4000, 1000, '4207', NULL, 'paytm', 0, 'google', 'counselor', '9', NULL, 'PENDING', '2019-04-30 18:16:40', 6, '2019-04-30 18:16:40', NULL, '2019-04-30 12:46:40', NULL),
(63, '27059363-46f9-4534-b226-405465efe2de', 1031, 'vishal', 'samkaria', 'male', 'vinod kumar', '1999-11-13', '8219536379', NULL, 'gharuan', NULL, 'kharar', 'Punjab', 'India', 'samkarivishal25257@gmail.com', 'cu', 'Degree', 'cse', '4', '2', NULL, '45 days', '2019-05-20', '2019-07-05', '6:30 PM', 5000, 1000, '4209', NULL, 'paytm', 0, 'google', 'counselor', '9', NULL, 'PENDING', '2019-04-30 18:24:47', 6, '2019-04-30 18:24:47', NULL, '2019-04-30 12:54:47', NULL),
(64, 'a4516a26-b3cb-41d7-ad07-0832cc273f95', 1032, 'vishu', 'bagri', 'male', 'kailash kumar', '2000-01-13', '8295110103', NULL, 'rajpura', NULL, 'patiala', 'Punjab', 'India', 'vishubagri013@gmail.com', 'cu', 'Degree', 'cse', '4', '1', NULL, '45 days', '2019-05-16', '2019-07-05', '6:30 PM', 5000, 1000, '4210', NULL, 'cash', 0, 'google', 'counselor', '9', NULL, 'JOINED', '2019-04-30 18:29:42', 6, '2019-05-16 16:49:46', 6, '2019-04-30 12:59:42', NULL),
(65, '7622a286-134d-4fc8-9ee8-0e882ae741fd', 1033, 'rohan', 'benjwal', 'male', 'chander prakash', '1999-05-22', '8894360471', NULL, 'gharuan', NULL, 'kharar', 'Punjab', 'India', 'rbenjwal22@gmail.com', 'cu', 'Degree', 'cse', '4', '1', NULL, '45 days', '2019-05-15', '2019-07-05', '6:45 PM', 5000, 1000, '4211', NULL, 'gpay', 0, 'google', 'counselor', '9', 'REGULAR CLASSES 15 DAYS CLASSES', 'JOINED', '2019-04-30 18:36:16', 6, '2019-05-15 11:06:13', 6, '2019-04-30 13:06:16', NULL),
(66, '6c81ca44-4781-4e97-9646-c0e07868bde1', 1034, 'ravinder singh', 'tanwar', 'male', 'surendra singh', '1998-01-05', '9417810109', NULL, 'panchkula', NULL, 'panchkula', 'Haryana', 'India', 'ravisinghtanwar100@gmail.com', 'cu', 'Degree', 'cse', '4', '2', NULL, '45 days', '2019-05-20', '2019-07-05', '6:45 PM', 5000, 1000, '4213', NULL, 'paytm', 0, 'google', 'counselor', '9', NULL, 'PENDING', '2019-04-30 18:41:13', 6, '2019-04-30 18:41:13', NULL, '2019-04-30 13:11:13', NULL),
(67, 'ebb005bc-168a-4ba4-bb5e-d096839f06a6', 1035, 'trishu', 'sharma', 'male', 'ajay sharma', '1998-01-01', '9805162004', NULL, 'g.c. daulatpur', NULL, 'una', 'Himachal Pradesh', 'India', 'trishusharma14@gmail.com', 'gc daulatpur', 'Degree', 'bba', '4', '3', NULL, '45 days', '2019-05-18', '2019-07-05', '6:45 PM', 5000, 2000, '4195', NULL, 'paytm', 0, 'google', 'counselor', '7', NULL, 'JOINED', '2019-04-30 18:46:43', 6, '2019-05-18 11:41:36', 6, '2019-04-30 13:16:43', NULL),
(68, '0e909d28-35c6-45ec-b276-ae21e67f454a', 1036, 'rohit', 'mehta', 'male', 'sohan lal', '1999-10-18', '8628843324', NULL, 'una', NULL, 'una', 'Himachal Pradesh', 'India', 'rohitmehta99@gmail.com', 'g.c. daulatpur', 'Degree', 'bba', '4', '3', NULL, '45 days', '2019-05-18', '2019-07-05', '2:00 PM', 5000, 2000, '4196', NULL, 'paytm', 0, 'google', 'counselor', '7', NULL, 'JOINED', '2019-05-01 13:57:38', 6, '2019-05-18 11:41:03', 6, '2019-05-01 08:27:38', NULL),
(69, '2ee5ff80-b249-4304-8d4d-2ca037414558', 1037, 'VIKAS', 'SHUKLA', 'male', 'ANIL KUMAR SHUKLA', '1999-07-28', '8289021766', NULL, 'HARYANA', NULL, 'HARYANA', 'Haryana', 'India', 'VIKASSHUKLA287@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '1', NULL, '45 DAYS', '2019-06-01', '2019-07-15', '2:15 PM', 5500, 1000, '4199', NULL, 'paytm', 0, 'google', 'COUNSELOR', '7', NULL, 'PENDING', '2019-05-01 14:05:35', 6, '2019-05-01 14:05:35', NULL, '2019-05-01 08:35:35', NULL),
(70, '5a5bf019-9d5a-4374-855e-ed630a73dc8d', 1038, 'MANPREET', 'SINGH', 'male', 'RASDEEP SINGH', '1999-10-07', '9653624951', NULL, 'TALWANDI SABO', NULL, 'BATHINDA', 'Punjab', 'India', 'RAVIDHANI532@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '1', NULL, '45 DAYS', '2019-05-20', '2019-07-05', '2:15 PM', 5000, 1000, '4218', NULL, 'paytm', 0, 'google', 'COUNSELOR', '14', NULL, 'TRASH', '2019-05-01 14:10:37', 6, '2019-05-02 13:26:40', 6, '2019-05-02 12:39:32', 6),
(71, 'b321fae4-8e84-41df-b159-aecbbab674de', 1039, 'bikramjit', 'singh', 'male', 'paramjit singh', '1999-08-19', '7347349865', NULL, 'sangrur', NULL, 'sangrur', 'Punjab', 'India', 'davidjones1999@gmail.com', 'cu', 'Degree', 'cse', '4', '1', NULL, '45 days', '2019-05-14', '2019-07-15', '12:45 PM', 5000, 2000, '4217', NULL, 'paytm', 0, 'other', 'counselor', '7', 'A2IT CERTIFICATE AND MICROSOFT CERTIFICATION ONLY', 'JOINED', '2019-05-02 12:43:31', 6, '2019-05-14 11:08:39', 6, '2019-05-02 07:13:31', NULL),
(72, '5267a7c7-3ef0-45bf-aa0f-6c4b4879bc7e', 1040, 'mohit', 'garg', 'male', 'pawan kumar garg', '1998-08-23', '9056301256', NULL, 'purana moga', NULL, 'moga', 'Punjab', 'India', 'gargm4300@gmail.com', 'cu', 'Degree', 'cse', '4th', '1', NULL, '45 days', '2019-05-14', '2019-07-05', '12:45 PM', 5000, 5000, '4219', NULL, 'paytm', 0, 'google', 'counselor', '14', 'A2IT CERTIFICATE AND MICROSOFT CERTIFICATION ONLY', 'JOINED', '2019-05-02 12:55:24', 6, '2019-05-14 11:15:16', 6, '2019-05-02 07:25:24', NULL),
(73, '13486acf-c62d-40ec-af1f-feaac0c01be2', 1041, 'sandeep', 'singh', 'male', 'ranvir  singh', '1998-05-22', '9646745678', NULL, 'purana moga', NULL, 'moga', 'Punjab', 'India', 'sandeeps7289@gmail.com', 'cu', 'Degree', 'cse', '4', '2', NULL, '45 days', '2019-05-20', '2019-07-05', '1:00 PM', 5000, 1000, '4220', NULL, 'paytm', 0, 'other', 'counselor', '14', NULL, 'PENDING', '2019-05-02 13:02:42', 6, '2019-05-02 13:25:24', 6, '2019-05-02 07:32:42', NULL),
(74, '2e605f49-9101-4c1d-934c-dea896a1d7d7', 1042, 'utkarsh', 'singh', 'male', 'satyendra kumar  singh', '1997-06-08', '9852122378', NULL, 'new area beldari  tolagaya  bihar', NULL, 'tolagaya', 'Bihar', 'India', 'utsingh12345@gmail.com', 'cu', 'Degree', 'cse', '4', '1', NULL, '45days', '2019-05-20', '2019-07-05', '1:15 PM', 4000, 2000, '4301', NULL, 'cash', 0, 'google', 'counselor', '10', NULL, 'PENDING', '2019-05-02 13:18:22', 6, '2019-05-02 13:18:22', NULL, '2019-05-02 07:48:22', NULL),
(75, '55c40eb7-e2b9-4f39-b68e-5c959aa419e5', 1043, 'aman', 'solanki', 'male', 'sripal solanki', '2000-10-19', '7027023478', NULL, '#2035 sector  -6 bahadvrgarh  haryana', NULL, 'bahadvrgarh', 'Haryana', 'India', 'haryanvijaat22@gmail.com', 'cu', 'Degree', 'cse', '4', '1', NULL, '45days', '2019-05-20', '2019-07-05', '1:30 PM', 4000, 2000, '4302', NULL, 'cash', 0, 'google', 'counselor', '10', NULL, 'PENDING', '2019-05-02 13:40:01', 6, '2019-05-02 13:40:01', NULL, '2019-05-02 08:10:01', NULL),
(76, '390b92c9-f346-441d-bbd4-9b9299e304bd', 1044, 'sanjay', 'kumar', 'male', 'shiv chander singh', '1994-02-16', '9915949856', NULL, '2761 sector 56 chandigarh', NULL, 'chandigarh', 'Chandigarh', 'India', 'kumarsanjay77888@gmail.com', 'cgc  jhanjeri', 'Degree', 'cse', '4', '1', NULL, '45days', '2019-06-01', '2019-07-15', '1:45 PM', 4500, 2000, '4303', NULL, 'cash', 0, 'google', 'counselor', '7', NULL, 'PENDING', '2019-05-02 13:54:36', 6, '2019-05-02 13:54:36', NULL, '2019-05-02 08:24:36', NULL),
(77, '1909249d-436f-4bf6-a265-0cdf0b0d10fd', 1045, 'avneet', 'singh', 'male', 'baldev  singh', '1998-10-02', '7006215643', NULL, 'singhpora  kalan  baramulla', NULL, 'baramulla', 'Punjab', 'India', 'sodhiavneetsingh@gmail.com', 'cu', 'Degree', 'cse', '45', '1', NULL, '45days', '2019-05-20', '2019-07-05', '2:00 PM', 4000, 2000, '4305', NULL, 'cash', 0, 'google', 'counselor', '10', NULL, 'PENDING', '2019-05-02 14:04:16', 6, '2019-05-02 14:04:16', NULL, '2019-05-02 08:34:16', NULL),
(78, 'b51f4b28-34c3-4a58-84bf-1ca5aa3652cb', 1046, 'ARUNPREET', 'SINGH', 'male', 'JARNAIL SINGH', '1998-06-13', '7006160479', NULL, 'JAMMU', NULL, 'JAMMU KASHMIR', 'Jammu and Kashmir', 'India', 'ARUNBALI1998@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '1', NULL, '45 DAYS', '2019-06-01', '2019-07-15', '3:45 PM', 4000, 1000, '4306', NULL, 'online', 0, 'google', 'COUNSELOR', '10', NULL, 'PENDING', '2019-05-03 15:44:20', 6, '2019-05-03 15:44:20', NULL, '2019-05-03 10:14:20', NULL),
(79, '9d0d80fe-49c2-4dd7-9e52-238f5fe496ef', 1047, 'AYUSH', NULL, 'male', 'MRITYUNJAY KUMAR', '2000-03-05', '7070576186', NULL, 'JHARKHAND', NULL, 'JHARKHAND', 'Bihar', 'India', 'AYUSH.SINGH2000DAV@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '1', NULL, '45 DAYS', '2019-05-20', '2019-07-05', '4:15 PM', 4000, 1000, '4307', NULL, 'cash', 0, 'google', 'COUNSELOR', '10', NULL, 'PENDING', '2019-05-03 16:22:32', 6, '2019-05-03 16:22:32', NULL, '2019-05-03 10:52:32', NULL),
(80, '3a73cc4b-66a7-48da-a53c-8139942d94cd', 1048, 'SAURABH', 'KUMAR', 'male', 'UMESH KUMAR YADAV', '1999-07-27', '8709444350', NULL, 'BIHAR', NULL, 'BIHAR', 'Punjab', 'India', 'SAURABHYADAVANSHI@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '1', NULL, '45 DAYS', '2019-05-16', '2019-07-05', '4:30 PM', 6000, 2000, '4308', NULL, 'paytm', 0, 'google', 'COUNSELOR', '7', NULL, 'JOINED', '2019-05-03 16:31:37', 6, '2019-05-16 16:51:34', 6, '2019-05-03 11:01:37', NULL),
(81, 'd1e378f2-8e62-4a51-bf92-bb9428be0eef', 1049, 'ATAL KUMAR', 'SHUKLA', 'male', 'R.N. SHUKLA', '2000-02-27', '8221843139', NULL, 'KHARAR', NULL, 'KHARAR', 'Punjab', 'India', '313ATALKUMARSHUKLA@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '1', NULL, '45 DAYS', '2019-05-16', '2019-07-15', '4:45 PM', 6000, 2000, '4309', NULL, 'online', 0, 'google', 'COUNSELOR', '7', NULL, 'JOINED', '2019-05-03 16:36:43', 6, '2019-05-16 16:50:24', 6, '2019-05-03 11:06:43', NULL),
(82, 'dbc720fe-60e0-4124-84b6-1b3fabbe0226', 1050, 'AMANDEEP', 'SINGH', 'male', 'AVTAR SINGH', '1999-04-20', '9592533079', NULL, 'PATIALA', NULL, 'PATIALA', 'Punjab', 'India', 'DEEPAMAN1999@GMAIL.COM', 'CGC JHANJERI', 'Degree', 'ECE', '4', '9', NULL, '45 DAYS', '2019-06-01', '2019-07-15', '4:45 PM', 4000, 2000, '4312', NULL, 'online', 0, 'social', 'COUNSELOR', '7', NULL, 'PENDING', '2019-05-03 16:43:54', 6, '2019-05-03 16:43:54', NULL, '2019-05-03 11:13:54', NULL),
(83, 'b522299e-1b88-435a-af04-53fdc4769275', 1051, 'KRITI', 'JAISWAL', 'female', 'RAKESH CHANDRA', '1998-11-10', '8076375858', NULL, 'ROHINI', NULL, 'DELHI', 'Delhi', 'India', 'KRITIJAISWAL1098@GMAILCOM', 'CU', 'Degree', 'CSE', '4', '1', NULL, '45 DAYS', '2019-06-01', '2019-07-15', '4:45 PM', 4000, 1000, '4316', NULL, 'online', 0, 'google', 'COUNSELOR', '10', NULL, 'PENDING', '2019-05-03 16:47:42', 6, '2019-05-03 16:47:42', NULL, '2019-05-03 11:17:42', NULL),
(84, '82ee089e-1c7a-44e5-9cf2-7a366524dd6b', 1052, 'kushagra', 'agrawal', 'male', 'kavindra agrawal', '1998-12-05', '9870743451', NULL, 'gharuan', NULL, 'kharar', 'Punjab', 'India', 'kushagra368@gmail.com', 'cu', 'Degree', 'cse', '4', '1', NULL, '45 days', '2019-06-01', '2019-07-15', '5:30 PM', 4000, 1000, '4317', NULL, 'cash', 0, 'google', 'counselor', '10', NULL, 'PENDING', '2019-05-03 17:33:03', 6, '2019-05-03 17:33:03', NULL, '2019-05-03 12:03:03', NULL),
(85, 'ec572308-3559-4ae7-b10c-a2876e0c5d33', 1053, 'gagan singh', 'golar', 'male', 'geetam singh golar', '2019-05-20', '9438139822', NULL, 'odisha', NULL, 'odisha', 'Orissa', 'India', 'golargagansingh@gmail.com', 'cu', 'Degree', 'cse', '4', '13', NULL, '45 days', '2019-05-16', '2019-07-05', '5:45 PM', 7000, 3500, '4323', NULL, 'paytm', 0, 'google', 'counselor', '9', NULL, 'JOINED', '2019-05-03 17:40:01', 6, '2019-05-16 16:49:05', 6, '2019-05-03 12:10:01', NULL),
(86, '211c1043-45c2-42b1-986c-f4d8cf7295e1', 1054, 'AMRITPAL', 'SINGH', 'male', 'RANJIT SINGH', '2000-01-01', '8437810952', NULL, 'KHANNA', NULL, 'KHANNA', 'Punjab', 'India', 'amritpalsingh@gmail.com', 'GGI, COLLEGE', 'Degree', 'CSE', '4', '2', 'REGULAR', '45 DAYS', '2019-06-01', '2019-07-15', '10:15 AM', 5000, 1000, '4114', NULL, 'paytm', 0, 'google', 'COUNSELOR', '7', NULL, 'PENDING', '2019-05-04 10:16:31', 6, '2019-05-04 10:16:31', NULL, '2019-05-04 04:46:31', NULL),
(87, 'ddf9d4ee-7dd7-4910-a14f-6fc962f04b56', 1055, 'MANPREET', 'SINGH', 'male', 'RASDEEP SINGH', '1999-10-17', '9653610044', NULL, 'TALWNDI SABO', NULL, 'BATHINDA', 'Punjab', 'India', 'MANPREET532@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '1', 'REGULAR', '45 DAYS', '2019-05-21', '2019-07-05', '10:30 AM', 5000, 1000, '4218', NULL, 'paytm', 0, 'google', 'COUNSELOR', '14', NULL, 'JOINED', '2019-05-04 10:24:39', 6, '2019-05-21 10:42:50', 6, '2019-05-04 04:54:39', NULL),
(88, 'f9c6c994-51c4-4bc9-8ec5-49fa1756a7bc', 1056, 'GURPREET', 'KAUR', 'female', 'BHUPINDER  SINGH', '1999-06-13', '7791937634', NULL, 'BIKANER', NULL, 'BIKANER', 'Rajasthan', 'India', 'CU.17BCS1650@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '14', 'REGULAR', '45DAYS', '2019-05-14', '2019-07-15', '10:30 AM', 5000, 1000, '4324', NULL, 'paytm', 0, 'google', 'COUNSELOR', '9', 'A2IT CERTIFICATE AND MICROSOFT CERTIFICATION ONLY', 'JOINED', '2019-05-04 10:36:01', 6, '2019-05-14 11:17:53', 6, '2019-05-04 05:06:01', NULL),
(89, '1f1a406f-2412-4ca4-85d3-052cbf6df19a', 1057, 'MEGHA', NULL, 'female', 'GHANANAND JHA', '1998-02-18', '7678651010', NULL, 'NEW DELHI', NULL, 'DELHI', 'Delhi', 'India', 'MEGHAJHA1402@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '14', 'MICROSOFT CERTIFICATION', '45 DAYS', '2019-05-20', '2019-07-05', '10:45 AM', 5000, 2000, '4223', NULL, 'online', 0, 'google', 'COUNSELOR', '10', NULL, 'PENDING', '2019-05-04 10:48:48', 6, '2019-05-04 10:48:48', NULL, '2019-05-04 05:18:48', NULL),
(90, '35ceb37a-e5ce-4dc9-a7a4-ac4370de0ea3', 1058, 'ARBAJ', 'ANSARI', 'male', 'MAKSUD ANSARI', '1999-11-26', '9650859764', NULL, 'BIHAR', NULL, 'BIHAR', 'Bihar', 'India', 'ARBAJRMACR786@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '15', 'CERTIFICATE, 1 WEEK CLASS', '45DAYS', '2019-05-20', '2019-07-05', '11:00 AM', 4000, 2000, '4224', NULL, 'cash', 0, 'google', 'COUNSELOR', '10', NULL, 'PENDING', '2019-05-04 10:54:27', 6, '2019-05-04 10:54:27', NULL, '2019-05-04 05:24:27', NULL),
(91, '8bb119c2-7400-4138-a597-d3c7cd8bd4f2', 1059, 'JATINDER', 'SINGH', 'male', 'GURJANT SINGH', '1993-04-05', '9872533600', NULL, 'PHASE 4', NULL, 'MOHALI', 'Punjab', 'India', 'MR.SARAO302@GMAIL.COM', 'CGC LANDRAN', 'Master', 'MBA', '4', '3', 'CERTIFICATE', '45 DAYS', '2019-06-03', '2019-07-18', '11:00 AM', 3000, 1000, '4225', NULL, 'cash', 0, 'friend', 'WALKIN', '13', NULL, 'PENDING', '2019-05-04 10:59:24', 6, '2019-05-04 10:59:24', NULL, '2019-05-04 05:29:24', NULL),
(92, '7fcac2a5-e1f3-4206-861b-d6d48e905181', 1060, 'SAMARJEET', 'SINGH', 'male', 'JUNG BAHADUR SINGH', '1998-08-26', '9622896629', NULL, 'SHIVALIK APARTMENT', NULL, 'KHARAR', 'Punjab', 'India', 'SAMARJEET865044@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '1', 'MICROSOFT CERTIFICATION,ONE WEEK CLASS', '45DAYS', '2019-05-20', '2019-07-05', '11:00 AM', 4000, 1000, '4325', NULL, 'online', 0, 'google', 'COUNSELOR', '10', NULL, 'PENDING', '2019-05-04 11:04:49', 6, '2019-05-04 11:04:49', NULL, '2019-05-04 05:34:49', NULL),
(93, 'a8c4d4d7-ff73-4bac-bf86-fc5b48dcc9a7', 1061, 'ABHISHEK', 'KUMAR', 'male', 'ASHOK KUMAR', '1998-08-02', '9055879478', NULL, 'NIRWANA GREEN', NULL, 'KHARAR', 'Punjab', 'India', 'ABHI.AK98@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '1', 'MICROSOFT CERTIFICATE', '45DAYS', '2019-05-20', '2019-07-05', '11:15 AM', 5000, 1000, '4326', NULL, 'online', 0, 'google', 'COUNSELOR', '10', NULL, 'PENDING', '2019-05-04 11:10:12', 6, '2019-05-04 11:10:12', NULL, '2019-05-04 05:40:12', NULL),
(94, '0c089aae-a9b7-4030-a33b-cf0381627389', 1062, 'SAHIL', 'KUMAR', 'male', 'SUBHASH KUMAR', '2000-11-10', '9805160072', NULL, 'KANGRA', NULL, 'KANGRA', 'Himachal Pradesh', 'India', 'SAHILKUMAR1011@GMAIL.COM', 'G.C. DAULATPUR', 'Degree', 'BBA', '4', '3', 'REGULAR', '45DAYS', '2019-05-20', '2019-07-05', '11:15 AM', 5000, 2000, '4228', NULL, 'paytm', 0, 'google', 'COUNSELOR', '7', NULL, 'PENDING', '2019-05-04 11:16:10', 6, '2019-05-04 11:16:10', NULL, '2019-05-04 05:46:10', NULL),
(95, '74138940-2484-4491-bddb-386142c41e09', 1063, 'AMIT', 'THAKUR', 'male', 'DEV RAJ', '1999-09-04', '9418192646', NULL, 'BILASPUR', NULL, 'BILASPUR', 'Himachal Pradesh', 'India', 'AMITTHAKUR2245@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '1', 'REGULAR', '45DAYS', '2019-05-21', '2019-07-05', '11:30 AM', 5000, 1000, '4230', NULL, 'gpay', 0, 'google', 'COUNSELOR', '9', NULL, 'JOINED', '2019-05-04 11:22:02', 6, '2019-05-21 11:00:33', 6, '2019-05-04 05:52:02', NULL),
(96, 'cfb4af32-83e9-4e3e-b114-7d88b7c76798', 1064, 'BHASKAR', 'CHAUDHARY', 'male', 'MADAN CHAUDHARY', '1999-08-05', '8630840643', NULL, 'MEERUT', NULL, 'MEERUT', 'Uttar Pradesh', 'India', 'BHASKARCHAUDHARY360@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '1', 'REGULAR', '45DAYS', '2019-05-20', '2019-07-05', '11:30 AM', 5000, 1000, '4231', NULL, 'gpay', 0, 'google', 'COUNSELOR', '9', NULL, 'PENDING', '2019-05-04 11:26:06', 6, '2019-05-04 11:26:06', NULL, '2019-05-04 05:56:06', NULL),
(97, '2bb1265f-6801-4451-9102-0b6129230573', 1065, 'AVIRAL', 'YADAV', 'male', 'GOVIND SINGH YADAV', '1998-04-14', '8979755932', NULL, 'CHANDIGARH', NULL, 'CHANDIGARH', 'Chandigarh', 'India', 'AVIRAL.YADAV7007@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '15', 'MICROSOFT CERTIFICATION', '45DAYS', '2019-05-14', '2019-07-05', '11:30 AM', 6000, 1000, '4330', NULL, 'cash', 0, 'google', 'COUNSELOR', '7', 'A2IT CERTIFICATE AND MICROSOFT CERTIFICATION ONLY', 'JOINED', '2019-05-04 11:35:19', 6, '2019-05-14 11:20:17', 6, '2019-05-04 06:05:19', NULL),
(98, '73423bd4-470a-454b-a581-3a09709a475a', 1066, 'SANDEEP KUMAR', 'GUPTA', 'male', 'BINOD  KUMAR SAW', '1999-11-26', '8340535243', NULL, 'JHARKHAND', NULL, 'JHARKHAND', 'Jharkhand', 'India', 'SIVISTINSAM2@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '15', 'CERTIFICATION', '45DAYS', '2019-05-14', '2019-07-05', '11:45 AM', 6000, 2000, '4331', NULL, 'online', 0, 'google', 'COUNSELOR', '7', 'A2IT CERTIFICATE AND MICROSOFT CERTIFICATION ONLY', 'JOINED', '2019-05-04 11:47:25', 6, '2019-05-14 11:19:03', 6, '2019-05-04 06:17:25', NULL),
(99, '143aa09b-f898-486e-a915-136719f2be48', 1067, 'SIMRAN KAUR', 'SAINI', 'female', 'SANT RAM  SAINI', '1999-07-20', '7008216453', NULL, 'CHANDIGARH', NULL, 'CHANDIGARH', 'Chandigarh', 'India', 'SIMRANKAURSAINI20@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '15', 'MICROSOFT CERTIFICATION', '45DAYS', '2019-05-14', '2019-07-05', '12:00 PM', 6000, 2000, '4332', NULL, 'online', 0, 'google', 'COUNSELOR', '7', 'A2IT CERTIFICATE AND MICROSOFT CERTIFICATION ONLY', 'JOINED', '2019-05-04 11:54:28', 6, '2019-05-14 11:19:42', 6, '2019-05-04 06:24:28', NULL),
(100, 'bd98b5c6-85b2-462b-ab8c-d6e8b7e8d011', 1068, 'VISHAL', 'KUMAR', 'male', 'MANOJ KUMAR', '1999-01-05', '9027021881', NULL, 'U.P', NULL, 'U.P', 'Uttar Pradesh', 'India', 'VISHAL05JAN1999@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '1', 'REGULAR', '45DAYS', '2019-05-20', '2019-07-05', '12:00 PM', 5500, 2000, '4333', NULL, 'gpay', 0, 'google', 'MR. RAJEEV', '13', NULL, 'PENDING', '2019-05-04 11:59:53', 6, '2019-05-04 11:59:53', NULL, '2019-05-04 06:29:53', NULL),
(101, 'a873bcba-e6a0-4091-a7e8-2cdf4c6502ee', 1069, 'RANDEEP', 'SINGH', 'male', 'HARPAL SINGH', '1996-07-15', '9653339354', NULL, 'FATEHBAD   HARAYANA', NULL, 'FATEBAD', 'Haryana', 'India', 'RANDYDHINDS6868@GMAIL.COM', 'SUS', 'Degree', 'CE', '8', '8', 'REGULAR', '2 MONTHS', '2019-05-14', '2019-06-30', '12:00 PM', 2500, 2500, '4232', NULL, 'paytm', 0, 'google', 'COUNSELOR', '9', 'A2IT CERTIFICATE ONLY', 'JOINED', '2019-05-04 12:07:49', 6, '2019-05-14 11:21:45', 6, '2019-05-04 06:37:49', NULL),
(102, '2be244df-99c6-453d-9da2-13afb7959ea5', 1070, 'KARANJOT', 'SINGH', 'male', 'HARMINDER SINGH', '1997-11-17', '9592959896', NULL, 'NABHA', NULL, 'PATIALA', 'Punjab', 'India', 'KARANGAGAT999@GMAIL.COM', 'SUS', 'Degree', 'CIVIL', '8', '8', 'CERTIFICATE', '2 MONTH', '2019-05-04', '2019-06-30', '12:15 PM', 2500, 2500, '4233', NULL, 'paytm', 0, 'google', 'COUNSELOR', '9', 'HE HAS COMPLETED HIS COURSE', 'JOINED', '2019-05-04 12:12:31', 6, '2019-05-04 17:47:22', 6, '2019-05-04 06:42:31', NULL),
(103, 'de000390-b862-45a4-aebe-40f5590fe6d0', 1071, 'VIKAS', NULL, 'male', 'OM PARKASH', '1996-08-08', '8872349851', NULL, 'BATHINDA', NULL, 'BATHINDA', 'Punjab', 'India', 'VIKASTANWER6@GMAIL.COM', 'SUS', 'Degree', 'CE', '8', '8', 'CERTIFICATE', '2 MONTH', '2019-05-04', '2019-06-30', '12:15 PM', 2500, 2500, '4234', NULL, 'paytm', 0, 'google', 'COUNSELOR', '9', 'CERTIFICATE DONE', 'JOINED', '2019-05-04 12:19:22', 6, '2019-05-04 17:48:17', 6, '2019-05-04 06:49:22', NULL),
(104, '60f61c7b-2daf-43b2-819e-4db653522800', 1072, 'TAMANNA', 'JASWAL', 'female', 'SANJEEV KUMAR', '1999-05-27', '8351095352', NULL, 'UNA', NULL, 'UNA', 'Himachal Pradesh', 'India', 'TANURANA43@GMAIL.COM', 'CU', 'Degree', 'IT', '4', '1', 'MICROSOFT CERTIFICATION', '45DAYS', '2019-05-15', '2019-07-05', '12:30 PM', 8000, 2000, '4334', NULL, 'gpay', 0, 'google', 'COUNSELOR', '7', 'regular', 'JOINED', '2019-05-04 12:28:07', 6, '2019-05-15 15:59:36', 6, '2019-05-04 06:58:07', NULL),
(105, '00a64010-ef1b-4239-b105-1913e5bd47cc', 1073, 'DANISH', 'AHMAD', 'male', 'JOHN AHMAD', '1999-04-09', '7006261041', NULL, 'CHANDIGARH', NULL, 'CHANDIGARH', 'Chandigarh', 'India', 'SAMDANYSHH@GMAIL.COM', 'CU', 'Degree', 'IT', '4', '2', 'MICROSOFT CERTIFICATION', '45DAYS', '2019-05-15', '2019-07-05', '12:30 PM', 5000, 1000, '4240', NULL, 'online', 0, 'google', 'COUNSELOR', '9', NULL, 'JOINED', '2019-05-04 12:36:59', 6, '2019-05-15 14:42:12', 6, '2019-05-04 07:06:59', NULL),
(106, '440fd186-62d3-4dd0-aa43-371672ba3ece', 1074, 'AMAN', 'KUMAR', 'male', 'SHAYAM SUNDER', '1998-01-16', '7989170998', NULL, 'GHARUAN', NULL, 'KHARAR', 'Punjab', 'India', 'AMANAK7989@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '16', 'CERTIFICATE', '45DAYS', '2019-05-14', '2019-07-05', '12:45 PM', 2500, 2500, '4241', NULL, 'gpay', 0, 'google', 'COUNSELOR', '9', 'A2IT CERTIFICATE ONLY', 'JOINED', '2019-05-04 12:41:23', 6, '2019-05-14 11:24:29', 6, '2019-05-04 07:11:23', NULL),
(107, '1c4524c3-437e-451f-a4e7-86cd599d39af', 1075, 'U', 'SRINIVAS', 'male', 'UV NARAYANA', '1999-01-01', '7995078622', NULL, 'CHANDIGARH', NULL, 'CHANDIGARH', 'Chandigarh', 'India', 'SRINIVARSROCK57@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '16', 'CERTIFICATE', '45DAYS', '2019-05-21', '2019-07-05', '12:45 PM', 2500, 2500, '4242', NULL, 'gpay', 0, 'google', 'COUNSELOR', '9', NULL, 'JOINED', '2019-05-04 12:47:03', 6, '2019-05-21 10:46:36', 6, '2019-05-04 07:17:03', NULL),
(108, '95562174-826c-46ce-904c-6a6c70089c82', 1076, 'PAVAN KALYAN', 'VELUGOLU', 'male', 'SRINIVASA RAO', '1999-08-04', '9640275254', NULL, 'GHARUAN', NULL, 'KHARAR', 'Punjab', 'India', 'PA1KALYANVEL999@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '2', 'CERTIFICATE', '45 DAYS', '2019-05-20', '2019-07-05', '1:00 PM', 2500, 2500, '4243', NULL, 'gpay', 0, 'google', 'COUNSELOR', '9', 'CERTIFICATE ONLY', 'PENDING', '2019-05-04 12:56:02', 6, '2019-05-14 15:22:31', 6, '2019-05-04 07:26:02', NULL),
(109, 'f75660d5-631d-45d6-b796-ec2aff6e2e68', 1077, 'JAYASH', 'KHEWARIYA', 'male', 'SURENDRA  KHEWARIYA', '1999-11-25', '9630399726', NULL, 'CHANDIGARH', NULL, 'CHANDIGARH', 'Chandigarh', 'India', 'JAYASHKHWAR@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '2', 'MICROSOFT CERTIFICATION', '45DAYS', '2019-06-01', '2019-07-15', '1:00 PM', 5000, 1000, '4244', NULL, 'cash', 0, 'google', 'COUNSELOR', '9', NULL, 'PENDING', '2019-05-04 13:03:32', 6, '2019-05-04 13:03:32', NULL, '2019-05-04 07:33:32', NULL),
(110, '1f11a6ac-753b-4d8d-a81c-35f064e1ed86', 1078, 'ASHISH', 'SHARMA', 'male', 'MUKESH KUMAR', '1999-07-15', '8696223705', NULL, 'GHARUAN', NULL, 'KHARAR', 'Punjab', 'India', 'ASHISH.SHARMA.ABR@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '2', 'CERTIFICATE', '45 DAYS', '2019-05-14', '2019-07-05', '1:15 PM', 2500, 2500, '4245', NULL, 'gpay', 0, 'google', 'COUNSELOR', '9', 'CERTIFICATE ONLY', 'JOINED', '2019-05-04 13:06:56', 6, '2019-05-14 14:50:01', 6, '2019-05-04 07:36:56', NULL),
(111, '080f74ef-a279-41f4-a9cd-675e7eaf85f1', 1079, 'PARUSH', 'VASHIST', 'male', 'RAMESH VASHIST', '1999-01-01', '8894490101', NULL, 'GHARUAN', NULL, 'KHARAR', 'Punjab', 'India', 'PARUSHVASHIST14@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '1', 'CERTIFICATE', '45 DAYS', '2019-06-01', '2019-07-15', '1:15 PM', 3500, 1000, '4339', NULL, 'paytm', 0, 'just-dial', 'COUNSELOR', '7', NULL, 'PENDING', '2019-05-04 13:09:49', 6, '2019-05-04 13:09:49', NULL, '2019-05-04 07:39:49', NULL),
(112, 'cb9a4acd-8fbe-4112-9061-5a7c1b339266', 1080, 'SATVIK', 'MUNJIAL', 'male', 'SANDEEP MUNJIAL', '1998-01-01', '7009422968', NULL, 'GHARUAN', NULL, 'KHARAR', 'Punjab', 'India', 'SATVIKMUNJIAL1998@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '1', 'CERTIFICATE', '45 DAYS', '2019-06-01', '2019-07-15', '1:15 PM', 3500, 1000, '4340', NULL, 'paytm', 0, 'google', 'COUNSELOR', '7', NULL, 'PENDING', '2019-05-04 13:14:08', 6, '2019-05-04 13:14:08', NULL, '2019-05-04 07:44:08', NULL),
(113, 'c9de264e-ecd4-4063-8cbb-041a98c0f96d', 1081, 'SURYA', 'SALARIA', 'male', 'J.S. SALARIA', '2000-01-01', '8295958547', NULL, 'GHARUAN', NULL, 'KHARAR', 'Punjab', 'India', 'SALARIASURYA11@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '1', 'CERTIFICATE', '45 DAYS', '2019-05-14', '2019-07-15', '1:15 PM', 3500, 3500, '4341', NULL, 'paytm', 0, 'google', 'COUNSELOR', '7', 'CERTIFICATE ONLY', 'JOINED', '2019-05-04 13:17:10', 6, '2019-05-14 14:51:00', 6, '2019-05-04 07:47:10', NULL),
(114, '03efc270-7c1b-49fc-b2b3-6c715232c4da', 1082, 'VANKUDOTHU', 'SRAVANI', 'male', 'VENKUDOTHU SEVYA', '2000-08-15', '8309346063', NULL, 'TELAGANA', NULL, 'TELAGANA', 'Sikkim', 'India', 'CU.17BCS1263@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '15', 'CERTIFICATE', '45 DAYS', '2019-05-14', '2019-07-05', '1:30 PM', 2500, 2500, '4246', NULL, 'gpay', 0, 'google', 'COUNSELOR', '9', 'A2IT CERTIFICATE ONLY', 'JOINED', '2019-05-04 13:20:39', 6, '2019-05-14 11:25:41', 6, '2019-05-04 07:50:39', NULL),
(115, '6c01d349-1865-40cb-a582-268f9a26db54', 1083, 'ASHWANI', 'KUMAR', 'male', 'KEHAR SINGH', '1999-05-20', '7060574807', NULL, 'BIJNOR', NULL, 'BIJNOR', 'Uttar Pradesh', 'India', 'ASHWANIKUMAR5142@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '12', 'CERTIFICATE', '45 DAYS', '2019-05-14', '2019-07-05', '1:30 PM', 2500, 2500, '4247', NULL, 'gpay', 0, 'google', 'COUNSELOR', '9', 'CERTIFICATE ONLY', 'JOINED', '2019-05-04 13:23:15', 6, '2019-05-14 14:51:51', 6, '2019-05-04 07:53:15', NULL),
(116, 'c5dcc609-0248-40c4-997b-16957f673057', 1084, 'TUSHAR', 'AGGARWAL', 'male', 'SANGEET KUMAR', '1996-01-17', '8059185695', NULL, 'NARNAUL', NULL, 'KHARAR', 'Punjab', 'India', 'TUSHARSINGH4510@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '12', 'CERTIFICATE', '45DAYS', '2019-05-14', '2019-07-05', '1:30 PM', 2500, 2500, '4248', NULL, 'gpay', 0, 'google', 'COUNSELOR', '9', 'CERTIFICATE ONLY', 'JOINED', '2019-05-04 13:28:09', 6, '2019-05-14 14:52:21', 6, '2019-05-04 07:58:09', NULL),
(117, '847b3e35-89bb-4a42-9c10-88cf799826d3', 1085, 'PARV', 'GUPTA', 'male', 'SANJAY GUPTA', '1999-11-16', '9671263211', NULL, 'KARNAL', NULL, 'KARNAL', 'Haryana', 'India', 'KARANGUPTANEO@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '12', 'CERTIFICATE', '45DAYS', '2019-05-14', '2019-07-05', '1:30 PM', 2500, 2500, '4249', NULL, 'gpay', 0, 'google', 'COUNSELOR', '9', 'CERTIFICATE ONLY', 'JOINED', '2019-05-04 13:31:33', 6, '2019-05-14 14:52:51', 6, '2019-05-04 08:01:33', NULL),
(118, '39ea8792-38aa-4dee-b757-3ab6c6aa0c73', 1086, 'AKHIL', 'KUMAR', 'male', 'RAJ KUMAR', '2000-06-14', '8219756915', NULL, 'KANGRA', NULL, 'KANGRA', 'Himachal Pradesh', 'India', 'AKHILGURETIA@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '12', 'CERTIFICATE', '45DAYS', '2019-05-14', '2019-07-05', '1:45 PM', 2500, 2500, '4250', NULL, 'gpay', 0, 'google', 'COUNSELOR', '9', 'CERTIFICATE ONLY', 'JOINED', '2019-05-04 13:34:17', 6, '2019-05-14 14:53:22', 6, '2019-05-04 08:04:17', NULL),
(119, '4397135b-ed60-400f-9139-b6d9654728f6', 1087, 'PRAKHAR', 'YASHASWAA', 'male', 'RAJEEV KUMAR SINHA', '2000-01-12', '9779152121', NULL, 'KHARAR', NULL, 'KHARAR', 'Punjab', 'India', 'PRAKHAR123@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '2', 'CERTIFICATE', '45DAYS', '2019-05-20', '2019-07-05', '1:45 PM', 4000, 2000, '4342', NULL, 'paytm', 0, 'google', 'COUNSELOR', '7', NULL, 'PENDING', '2019-05-04 13:38:36', 6, '2019-05-04 13:38:36', NULL, '2019-05-04 08:08:36', NULL),
(120, '84210973-f0bb-4203-94e2-cb1e83f4bddb', 1088, 'MOHAMMAD SHAHID', 'CHAUDHARY', 'male', 'MUSTAQEEM CHAUDHARY', '2000-01-04', '7982584229', NULL, 'MUZAFFAR NAGAR', NULL, 'MUZAFFAR NAGAR', 'Uttar Pradesh', 'India', 'SHAHIDCHAUDHARY0401@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '15', 'CERTIFICATE', '45 DAYS', '2019-05-20', '2019-07-05', '1:45 PM', 3000, 1000, '4343', NULL, 'cash', 0, 'google', 'COUNSELOR', '10', NULL, 'PENDING', '2019-05-04 13:44:29', 6, '2019-05-04 13:44:29', NULL, '2019-05-04 08:14:29', NULL),
(121, '7cd67f19-5435-4b33-8829-24c9cd6eac20', 1089, 'DIVANSHI', 'SETHI', 'male', 'ANIL SETHI', '1999-04-15', '8700602427', NULL, 'NEW DELHI', NULL, 'NEW DELHI', 'Delhi', 'India', 'SETHIDIVANSHI1998@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '2', 'CERTIFICATE, MICROSOFT CERTIFICATION', '45 DAYS', '2019-05-20', '2019-07-05', '4:45 PM', 5000, 1000, '4344', NULL, 'paytm', 0, 'google', 'COUNSELOR', '9', NULL, 'PENDING', '2019-05-04 16:56:01', 6, '2019-05-04 16:56:01', NULL, '2019-05-04 11:26:01', NULL),
(122, '8d6db807-d246-4e5a-800e-1901cc45daea', 1090, 'ADARSH', 'THAKUR', 'male', 'PROMILA THAKUR', '1999-07-16', '9877206148', NULL, 'GHARUAN', NULL, 'KHARAR', 'Punjab', 'India', 'THAKURADARSH377@GMAIL.COM', 'CU', 'Degree', 'IT', '4', '14', 'CERTIFICATE', '45 DAYS', '2019-05-15', '2019-07-15', '6:15 PM', 4000, 500, '4345', NULL, 'online', 0, 'google', 'COUNSELOR', '7', 'CERTIFICATE ONLY', 'JOINED', '2019-05-04 18:18:11', 6, '2019-05-15 12:50:13', 6, '2019-05-04 12:48:11', NULL),
(123, '49707202-71d7-41fb-b219-9b6b33ee876d', 1091, 'YASH', 'JETHANI', 'male', 'SANJEEV JETHANI', '1999-10-15', '7424932323', NULL, 'AJMER', NULL, 'AJMER', 'Andaman and Nicobar Islands', 'India', 'YJETHANI966@GMAIL.COM', 'CU', 'Degree', 'IT', '4', '14', 'CERTIFICATE', '45 DAYS', '2019-06-03', '2019-07-18', '6:30 PM', 4000, 500, '4346', NULL, 'online', 0, 'other', 'COUNSELOR', '7', NULL, 'PENDING', '2019-05-04 18:49:36', 6, '2019-05-15 12:46:16', 6, '2019-05-04 13:19:36', NULL),
(124, '7c4d1259-d71d-40a5-942d-10641c141f0e', 1092, 'VIJAY', 'BINDAL', 'male', 'RAJESH   KUMAR', '1999-03-31', '9050367541', NULL, 'MOHALI', NULL, 'MOHALI', 'Punjab', 'India', 'VIJAYBINDAL31@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '18', 'REGULAR', '45 DAYS', '2019-05-20', '2019-07-05', '10:00 AM', 5000, 1000, '4255', NULL, 'gpay', 0, 'google', 'COUNSELOR', '14', NULL, 'PENDING', '2019-05-06 10:06:08', 6, '2019-05-06 10:06:08', NULL, '2019-05-06 04:36:08', NULL),
(125, 'bb5b38ff-2d86-4cd1-ae2e-7cf7ed951af8', 1093, 'ROMAN', NULL, 'male', 'ASHOK KUMAR', '1999-01-21', '8708885991', NULL, 'CHANDIGARH', NULL, 'CHANDIGARH', 'Chandigarh', 'India', 'ROHANDHIMAN15@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '18', 'REGULAR', '45 DAYS', '2019-05-20', '2019-07-05', '10:15 AM', 5000, 1000, '4256', NULL, 'gpay', 0, 'google', 'COUNSELOR', '14', NULL, 'PENDING', '2019-05-06 10:13:38', 6, '2019-05-06 10:13:38', NULL, '2019-05-06 04:43:38', NULL),
(126, 'f35c05a1-49be-4650-be3b-705368f00e34', 1094, 'HEMANT', 'KUMAR', 'male', 'MANOJ   KUMAR', '1998-04-28', '8340163843', NULL, 'CHANDIGARH', NULL, 'CHANDIGARH', 'Chandigarh', 'India', 'HEMANT0198@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '18', 'REGULAR', '45 DAYS', '2019-05-20', '2019-07-05', '10:15 AM', 5000, 1000, '4257', NULL, 'gpay', 0, 'google', 'COUNSELOR', '14', NULL, 'PENDING', '2019-05-06 10:19:35', 6, '2019-05-06 10:19:35', NULL, '2019-05-06 04:49:35', NULL),
(127, '26ddf83a-a879-4596-b161-822ad61c2bba', 1095, 'ISHFAQ  AHMAD', 'BHAT', 'male', 'SARIB  MUSHTAQ', '1997-11-05', '7006286454', NULL, 'SRI NAGAR', NULL, 'SRI NAGAR', 'Jammu and Kashmir', 'India', 'BHATISHFAQ966@GMAIL.COM', 'CGC', 'Degree', 'CSE', '4', '1', 'MICROSOFT CERTIFICATION', '45 DAYS', '2019-05-14', '2019-07-15', '10:45 AM', 3000, 3000, '4347', NULL, 'online', 0, 'google', 'COUNSELOR', '9', 'MICROSOFT CERTIFICATION ONLY', 'JOINED', '2019-05-06 10:49:19', 6, '2019-05-14 14:55:27', 6, '2019-05-06 05:19:19', NULL),
(128, 'e64916b1-fe7f-471d-a39f-7b7d3697879a', 1096, 'MOHIT', 'GUPTA', 'male', 'MR RAVI GUPTA', '2006-02-18', '9529929045', NULL, 'GANGANAGAR', NULL, 'GANGANAGAR', 'Rajasthan', 'India', 'MOHIT.GUPTA30000@GMAIL.COM', 'CGC', 'Degree', 'CSE', '4', '14', 'MICROSOFT CERTIFICATION', '45 DAYS', '2019-05-14', '2019-07-15', '11:00 AM', 3000, 3000, '4348', NULL, 'online', 0, 'google', 'COUNSELOR', '7', 'MICROSOFT CERTIFICATION ONLY', 'JOINED', '2019-05-06 11:03:43', 6, '2019-05-14 14:56:10', 6, '2019-05-06 05:33:43', NULL),
(129, '9ce7abf6-3388-46d9-8eab-719a0ddf3b85', 1097, 'SATYAPRAKASH', 'YADAV', 'male', 'YOGENDRA YADAV', '1997-12-22', '7052606198', NULL, 'DEORIA', NULL, 'DEORIA', 'Uttar Pradesh', 'India', 'SSPR9143@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '2', 'CERTIFICATE', '45 DAYS', '2019-05-14', '2019-07-05', '11:15 AM', 2500, 2500, '4258', NULL, 'gpay', 0, 'google', 'COUNSELOR', '9', 'CERTIFICATE ONLY', 'JOINED', '2019-05-06 11:12:29', 6, '2019-05-14 14:56:40', 6, '2019-05-06 05:42:29', NULL),
(130, 'a6462502-65d2-44c7-8cea-c964abb2331a', 1098, 'ARUNESH', 'BHARDWAJ', 'male', 'MR BHARAT KUMAR', '1996-11-22', '9417416651', NULL, 'BATHINDA', NULL, 'BATHINDA', 'Punjab', 'India', 'ARUNESH.B96@GMAIL.COM', 'GZSCCET', 'Degree', 'CIVIL', '8', '8', 'CERTIFICATE', '45 DAYS', '2019-05-06', '2019-06-15', '11:15 AM', 3500, 3500, '4349', NULL, 'cash', 0, 'google', 'COUNSELOR', '7', 'CERTIFICATE ONLY', 'JOINED', '2019-05-06 11:27:37', 6, '2019-05-06 11:28:28', 6, '2019-05-06 05:57:37', NULL),
(131, '54b2a19a-c061-4b71-966a-97dd812d0d16', 1099, 'DAMANPREET', 'SINGH', 'male', 'GURDEV  SINGH', '1995-07-27', '9888838942', NULL, 'MOHALI', NULL, 'MOHALI', 'Punjab', 'India', 'DAMANPREET4839@GMAIL.COM', 'CGC', 'Master', 'MBA', '2', '17', 'CERTIFICATE', '45 DAYS', '2019-06-01', '2019-07-15', '11:30 AM', 3000, 1000, '4351', NULL, 'cash', 0, 'google', 'COUNSELOR', '7', NULL, 'PENDING', '2019-05-06 11:34:41', 6, '2019-05-06 11:34:41', NULL, '2019-05-06 06:04:41', NULL),
(132, '1b86bd8c-824b-412c-942b-50e806467ab3', 1100, 'SUKHWINDER', 'SINGH', 'male', 'BALWANT SINGH', '1995-06-24', '8437662380', NULL, 'KHARAR', NULL, 'MOHALI', 'Punjab', 'India', 'SINGHSUKHWINDER373@GMAIL.COM', 'CGC LANDRAN', 'Master', 'MBA', '2', '17', 'CERTIFICATE', '45 DAYS', '2019-06-01', '2019-07-15', '11:45 AM', 3000, 1000, '4352', NULL, 'cash', 0, 'google', 'COUNSELOR', '7', NULL, 'PENDING', '2019-05-06 11:38:35', 6, '2019-05-06 11:38:35', NULL, '2019-05-06 06:08:35', NULL),
(133, 'a267a9a5-6b20-45b6-837f-a03e92cbf8f0', 1101, 'HARSHDEEP', 'KAUR', 'female', 'DALBIR SINGH', '1999-12-24', '9501038611', NULL, 'PATIALA', NULL, 'PATIALA', 'Punjab', 'India', 'HARSHDEEPKAUR147@GMAIL.COM', 'CGC LANDRAN', 'Degree', 'CSE', '4', '1', 'REGULAR', '45 DAYS', '2019-06-01', '2019-07-16', '11:45 AM', 4500, 1000, '4264', NULL, 'cash', 0, 'google', 'COUNSELOR', '7', NULL, 'PENDING', '2019-05-06 11:42:29', 6, '2019-05-06 11:42:29', NULL, '2019-05-06 06:12:29', NULL),
(134, '63cf0374-736c-44a0-b3b7-9d730c316869', 1102, 'PRAHARSH', 'NAYAN', 'male', 'RAJEEV NAYAN', '1999-04-05', '8539949865', NULL, 'CHANDIGARH', NULL, 'CHANDIGARH', 'Chandigarh', 'India', 'PRAHARSHNAYAN@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '1', 'MICROSOFT CERTICATION', '45 DAYS', '2019-06-01', '2019-07-15', '11:45 AM', 5000, 2000, '4355', NULL, 'paytm', 0, 'google', 'COUNSELOR', '14', NULL, 'PENDING', '2019-05-06 11:46:42', 6, '2019-05-06 11:46:42', NULL, '2019-05-06 06:16:42', NULL),
(135, '7186d53e-2df6-4846-a729-48e2d897d540', 1103, 'ANOOP', 'KUMAR', 'male', 'RAVINDRA KUMAR', '2019-05-06', '7800010085', NULL, 'GHARUAN', NULL, 'KHARAR', 'Punjab', 'India', 'IAMANOOP@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '16', 'CERTIFICATE', '45 DAYS', '2019-05-14', '2019-07-15', '12:00 PM', 4000, 4000, '4356', NULL, 'cash', 0, 'google', 'COUNSELOR', '14', 'CERTIFICATE ONLY', 'JOINED', '2019-05-06 11:51:20', 6, '2019-05-14 14:57:42', 6, '2019-05-06 06:21:20', NULL);
INSERT INTO `app_admission` (`id`, `admission_uid`, `aid`, `first_name`, `last_name`, `gender`, `father_name`, `dob`, `mobile`, `alt_mobile`, `address`, `pg_address`, `city`, `state`, `country`, `email`, `college_name`, `college_deg_type`, `college_stream`, `college_semester`, `course_name`, `other_course`, `course_durartion`, `course_joindate`, `course_enddate`, `course_time`, `course_fee`, `course_advance`, `payment_reciept`, `payment_type`, `payment_mode`, `batch_assigned`, `refer_by`, `refer_name`, `admission_by`, `remarks`, `status`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`) VALUES
(136, '3614749b-ec22-476b-a8b7-7650cd3e71fd', 1104, 'MANISH KUMAR', 'RAJWANS', 'male', 'ANIL KUMAR RAJWANS', '2000-02-07', '7973364325', NULL, 'CHANDIGARH', NULL, 'CHANDIGARH', 'Chandigarh', 'India', 'CU.17BC2019@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '1', 'CERTIFICATE', '45 DAYS', '2019-06-01', '2019-07-15', '12:00 PM', 4000, 1000, '4357', NULL, 'paytm', 0, 'google', 'COUNSELOR', '9', NULL, 'PENDING', '2019-05-06 11:59:46', 6, '2019-05-06 11:59:46', NULL, '2019-05-06 06:29:46', NULL),
(137, '66fc4fe8-8aa2-47b7-8d20-6a84ea815f66', 1105, 'AKASHDEEP', NULL, 'male', 'GIAN CHAND', '2000-08-10', '9805038765', NULL, 'GHARUAN', NULL, 'KHARAR', 'Punjab', 'India', 'DAVIDAKASH9876@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '1', 'CERTIFICATE', '45 DAYS', '2019-05-14', '2019-07-15', '12:00 PM', 2500, 2500, '4358', NULL, 'gpay', 0, 'google', 'COUNSELOR', '9', 'CERTIFICATE ONLY', 'JOINED', '2019-05-06 12:03:38', 6, '2019-05-14 14:58:59', 6, '2019-05-06 06:33:38', NULL),
(138, 'eba8cabd-975b-48fe-9c24-1e9bda4e4cdb', 1106, 'VIVEK KU', 'TIWARI', 'male', 'R.P. TIWARI', '1999-03-20', '7999770983', NULL, 'GHARUAN', NULL, 'KHARAR', 'Punjab', 'India', 'VIVEK.TIWARI2017CU@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '1', 'MICROSOFT CERTIFICATION', '45 DAYS', '2019-05-14', '2019-07-15', '12:15 PM', 4000, 2000, '4359', NULL, 'paytm', 0, 'google', 'COUNSELOR', '14', 'MICROSOFT CERTIFICATION ONLY', 'JOINED', '2019-05-06 12:16:58', 6, '2019-05-14 14:59:36', 6, '2019-05-06 06:46:58', NULL),
(139, '7c11ed1a-9a77-4d59-b24f-e77bba43de30', 1107, 'DEV', 'TYAGI', 'male', 'BHUPENDRA KUMAR', '1998-07-25', '8745947634', NULL, 'GHARUAN', NULL, 'KHARAR', 'Punjab', 'India', 'DEV250798@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '1', 'MICROSOFT CERTIFICATION', '45 DAYS', '2019-05-14', '2019-07-15', '12:30 PM', 4000, 2000, '4360', NULL, 'cash', 0, 'google', 'COUNSELOR', '14', 'MICROSOFT CERTIFICATION ONLY', 'JOINED', '2019-05-06 12:26:16', 6, '2019-05-14 15:00:27', 6, '2019-05-06 06:56:16', NULL),
(140, '80aea40d-f150-4707-9388-1acde773cb61', 1108, 'SHAIK', 'KHALEEM', 'male', 'SHAIK  BASHA', '1999-09-06', '9182116523', NULL, 'CHANDIGARH', NULL, 'CHANDIGARH', 'Chandigarh', 'India', 'KSHAIK1999@GMAIL.COM', 'CU', 'Degree', 'ECE', '4', '10', 'CERTIFICATE', '45 DAYS', '2019-05-14', '2019-07-15', '12:30 PM', 2500, 2500, '4268', NULL, 'gpay', 0, 'google', 'COUNSELOR', '9', 'CERTIFICATE ONLY', 'JOINED', '2019-05-06 12:32:22', 6, '2019-05-14 15:00:57', 6, '2019-05-06 07:02:22', NULL),
(141, '028629f8-47a3-4b40-97ea-2132e3d37f42', 1109, 'SHUBHAM', 'KUMAR', 'male', 'ANIL KUMAR PRASD', '1998-04-10', '8340707842', NULL, 'GHARUAN', NULL, 'KHARAR', 'Punjab', 'India', 'CU.17BCS2697@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '2', 'CERTIFICATE', '45 DAYS', '2019-06-01', '2019-07-15', '12:45 PM', 4000, 1000, '4361', NULL, 'paytm', 0, 'google', 'COUNSELOR', '9', NULL, 'PENDING', '2019-05-06 12:35:48', 6, '2019-05-06 12:35:48', NULL, '2019-05-06 07:05:48', NULL),
(142, 'b3be6f9f-3ae1-409a-a79e-f258d1d5724e', 1110, 'MD  SHAHBAAZ', 'HUSSAIN', 'male', 'MD ASGAR ALI', '1998-10-15', '8226942111', NULL, 'KHAGARIA', NULL, 'BIHAR', 'Bihar', 'India', 'SHAHBAAZHUSSAIN@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '2', 'MICROSOFT CERTIFICATION', '45 DAYS', '2019-06-01', '2019-07-15', '12:45 PM', 4000, 1000, '4365', NULL, 'cash', 0, 'google', 'COUNSELOR', '10', NULL, 'PENDING', '2019-05-06 12:39:31', 6, '2019-05-06 12:39:31', NULL, '2019-05-06 07:09:31', NULL),
(143, '0acdd244-af77-4106-b569-2476c2718c47', 1111, 'SHANU', 'SEEN', 'male', 'SANTOSH KUMAR', '1999-01-01', '8409696309', NULL, 'GHARUAN', NULL, 'KHARAR', 'Punjab', 'India', 'ITSMESHANU1234@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '2', 'CERTIFICATE', '45 DAYS', '2019-05-14', '2019-07-15', '12:45 PM', 2500, 2500, '4271', NULL, 'gpay', 0, 'google', 'COUNSELOR', '9', 'CERTIFICATE ONLY', 'JOINED', '2019-05-06 12:43:04', 6, '2019-05-14 15:03:13', 6, '2019-05-06 07:13:04', NULL),
(144, '16707d80-71df-4d67-9d87-8b09616ffc21', 1112, 'MRIDUL', 'ROY', 'male', 'MRINAL KANTI ROY', '1999-01-01', '9101182611', NULL, 'GHARUAN', NULL, 'KHARAR', 'Punjab', 'India', 'MRIDUL.MRIDULROY.ROY@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '2', 'CERTIFICATE', '45 DAYS', '2019-05-14', '2019-07-15', '1:00 PM', 2500, 2500, '4272', NULL, 'gpay', 0, 'google', 'COUNSELOR', '9', 'CERTIFICATE ONLY', 'JOINED', '2019-05-06 12:58:26', 6, '2019-05-14 15:03:40', 6, '2019-05-06 07:28:26', NULL),
(145, '39972669-4efd-44e2-9942-78aa8a1a494a', 1113, 'ABHISHEK', 'ROY', 'male', 'AMARENDRA ROY', '1997-07-20', '7002610091', NULL, 'GHARUAN', NULL, 'KHARAR', 'Punjab', 'India', 'ROYSTARABHISHEK@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '2', 'CERTIFICATE', '45 DAYS', '2019-05-14', '2019-07-15', '1:00 PM', 2500, 2500, '4273', NULL, 'gpay', 0, 'google', 'COUNSELOR', '9', 'CERTIFICATE ONLY', 'JOINED', '2019-05-06 13:01:59', 6, '2019-05-14 15:04:14', 6, '2019-05-06 07:31:59', NULL),
(146, '6577ded5-e159-4830-809f-3daee96c6fef', 1114, 'ASHAB', 'ALI', 'male', 'GULAM HAIDER', '1997-08-06', '8779491004', NULL, 'SAHARANPUR', NULL, 'SAHARANPUR', 'Uttar Pradesh', 'India', '123ALIASHAB@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '13', 'CERTIFICATE', '45 DAYS', '2019-06-01', '2019-07-15', '1:15 PM', 4000, 1000, '4367', NULL, 'cash', 0, 'google', 'COUNSELOR', '10', NULL, 'PENDING', '2019-05-06 13:05:55', 6, '2019-05-06 13:05:55', NULL, '2019-05-06 07:35:55', NULL),
(147, '047e56f4-8d80-44c5-9a08-09b5c4994e54', 1115, 'AYUSH', 'MAGOTRA', 'male', 'ROMESH KUMAR', '1999-01-10', '9086743210', NULL, 'GHARUAN', NULL, 'KHARAR', 'Punjab', 'India', 'AYUSHMAGOTRA33@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '1', 'CERTIFICATE', '45 DAYS', '2019-06-01', '2019-07-15', '1:15 PM', 3000, 1500, '4369', NULL, 'paytm', 0, 'google', 'COUNSELOR', '10', NULL, 'PENDING', '2019-05-06 13:09:27', 6, '2019-05-06 13:09:27', NULL, '2019-05-06 07:39:27', NULL),
(148, 'f33823f6-4f6b-4377-9b85-c330d29478d5', 1116, 'ARNAV', 'PRASHAR', 'male', 'NAVEEN K PRASHAR', '2008-05-09', '7307708765', NULL, 'MOHALI', NULL, 'MOHALI', 'Punjab', 'India', 'ARNAVPRASHAR@GMAIL.COM', 'DOON PUBLIC  INTERNATIONL', 'Diploma', 'SCHOOL', '6', '19', 'REGULAR', '60 DAYS', '2019-05-14', '2019-06-24', '1:30 PM', 4000, 2000, '4274', NULL, 'cash', 0, 'google', 'COUNSELOR', '9', 'REGULAR', 'JOINED', '2019-05-06 13:24:44', 6, '2019-05-14 11:29:24', 6, '2019-05-06 07:54:44', NULL),
(149, 'f2a84306-f0be-42f4-8b3f-493ccbdc5fdf', 1117, 'MISHIKA', 'GUPTA', 'female', 'ANIL GUPTA', '1999-05-23', '8708170875', NULL, 'JAGADHARI HARYANA', NULL, 'JAGADHARI', 'Haryana', 'India', 'MISHIKA.G23@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '15', 'REGULAR', '45 DAYS', '2019-05-20', '2019-07-05', '1:30 PM', 4000, 1000, '4275', NULL, 'cash', 0, 'google', 'COUNSELOR', '10', NULL, 'PENDING', '2019-05-06 13:34:56', 6, '2019-05-06 13:34:56', NULL, '2019-05-06 08:04:56', NULL),
(150, 'cb566a16-f2e1-4af7-b9d8-719e3436464e', 1118, 'MOHINI', 'YADAV', 'female', 'VIMLESH YADAV', '2000-01-06', '8053888754', NULL, 'CHANDIGARH', NULL, 'CHANDIGARH', 'Chandigarh', 'India', 'MOHINIYADAV', 'CU', 'Degree', 'CSE', '4', '15', 'CERTIFICATE', '45 DAYS', '2019-05-20', '2019-07-05', '1:45 PM', 4000, 1000, '4276', NULL, 'paytm', 0, 'google', 'COUNSELOR', '10', NULL, 'PENDING', '2019-05-06 13:40:17', 6, '2019-05-06 13:40:17', NULL, '2019-05-06 08:10:17', NULL),
(151, 'afc25887-281a-457c-87eb-3fbdea748b2f', 1119, 'NIKHIL KUMAR', 'SHARMA', 'male', 'YOGENDRA KUMAR', '1998-12-31', '9760621008', NULL, 'GHARUAN', NULL, 'KHARAR', 'Punjab', 'India', 'NIKHIL.SHARMA00@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '16', 'CERTIFICATE', '45 DAYS', '2019-05-14', '2019-07-15', '1:45 PM', 2500, 2500, '4371', NULL, 'gpay', 0, 'google', 'COUNSELOR', '9', 'CERTIFICATE ONLY', 'JOINED', '2019-05-06 14:00:45', 6, '2019-05-14 15:05:09', 6, '2019-05-06 08:30:45', NULL),
(152, '9bae2b63-145d-4019-9bd9-8949cb3a5ff4', 1120, 'SHAHNWAZ', 'AHAMAD', 'male', 'MEHRAJ AHAMAD', '1998-08-10', '9878340983', NULL, 'GHARUAN', NULL, 'KHARAR', 'Punjab', 'India', 'CU.17BCS2020@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '1', 'CERTIFICATE', '45 DAYS', '2019-06-01', '2019-07-15', '2:15 PM', 4000, 2000, '4373', NULL, 'paytm', 0, 'google', 'COUNSELOR', '9', NULL, 'PENDING', '2019-05-06 14:16:43', 6, '2019-05-06 14:16:43', NULL, '2019-05-06 08:46:43', NULL),
(153, '7ecf98a2-e191-4348-b645-a1f4740f6aec', 1121, 'MUTHAR', 'MAJEED', 'male', 'ABDUL MAJEED THOKAR', '2000-09-03', '8825012353', NULL, 'JAMMU & KASHMIR', NULL, 'JAMMU & KASHMIR', 'Jammu and Kashmir', 'India', 'MUTHARMAJEED1320@GMAIL.COM', 'CGCTC JHANJERI', 'Degree', 'CSE', '4', '19', 'C++, CERTIFICATE', '45 DAYS', '2019-05-14', '2019-07-05', '3:30 PM', 2000, 2000, '4277', NULL, 'online', 0, 'google', 'COUNSELOR', '7', 'CERTIFICATE ONLY', 'JOINED', '2019-05-06 15:28:28', 6, '2019-05-14 15:06:04', 6, '2019-05-06 09:58:28', NULL),
(154, 'b2bc6e74-5919-413e-a87f-30af2ee5b75e', 1122, 'HARSH DEV', 'SINGH', 'male', 'CHARAN DEV SINGH', '1998-03-06', '9622162118', NULL, 'JAMMU', NULL, 'JAMMU', 'Andaman and Nicobar Islands', 'India', 'HARSHDEV998@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '1', 'CERTIFICATE', '45 DAYS', '2019-05-15', '2019-07-15', '3:45 PM', 3000, 1000, '4278', NULL, 'paytm', 0, 'google', 'COUNSELOR', '10', NULL, 'JOINED', '2019-05-06 15:49:56', 6, '2019-05-17 11:55:12', 6, '2019-05-06 10:19:56', NULL),
(155, '0819d294-d64c-4c84-b5c1-9a5b0fa99f3b', 1123, 'rahul', 'khajurra', 'male', 'ganesh  khajurra', '2000-01-01', '8194847854', NULL, 'gharuan', NULL, 'kharar', 'Punjab', 'India', 'rahulkhajurra34@gmail.com', 'cu', 'Degree', 'bca', '4', '1', 'certificate', '45 days', '2019-05-15', '2019-07-05', '11:15 AM', 4000, 1000, '4279', NULL, 'paytm', 0, 'google', 'counselor', '7', '10 days classes', 'JOINED', '2019-05-08 11:15:34', 6, '2019-05-15 15:48:29', 6, '2019-05-08 05:45:34', NULL),
(156, 'c136cb8a-4bca-47b4-91f9-09a288dce946', 1124, 'amit', 'bandral', 'male', 'madan  singh', '1997-03-14', '9682681256', NULL, 'kathua, jammu& kashmir', NULL, 'kathua', 'Jammu and Kashmir', 'India', 'amitbandral32@gmail.com', 'cu', 'Degree', 'ibm', '4', '1', 'certificate', '45 days', '2019-05-15', '2019-07-05', '11:15 AM', 4000, 1000, '4280', NULL, 'paytm', 0, 'google', 'counselor', '7', '10 days classes', 'JOINED', '2019-05-08 11:25:18', 6, '2019-05-15 15:49:07', 6, '2019-05-08 05:55:18', NULL),
(157, '558f004c-2f92-4474-bf38-a231c553e4b3', 1125, 'ashutosh', 'aman', 'male', 'dayanand mandal', '1999-12-21', '7652867654', NULL, 'gharuan', NULL, 'gharuan', 'Punjab', 'India', 'amanashutosh12@gmail.com', 'cu', 'Degree', 'cse', '4', '2', 'certificate', '45 days', '2019-05-20', '2019-07-05', '11:30 AM', 4000, 1000, '4281', NULL, 'paytm', 0, 'google', 'counselor', '7', NULL, 'PENDING', '2019-05-08 11:31:24', 6, '2019-05-08 11:31:24', NULL, '2019-05-08 06:01:24', NULL),
(158, '006e5745-edb6-442d-915f-326c55d9f3f6', 1126, 'priyam dev', 'ranta', 'male', 'devkumar ranta', '1999-01-07', '8146603287', NULL, 'gharuan', NULL, 'kharrar', 'Punjab', 'India', 'priyamdevranta6799@gmail.com', 'cu', 'Degree', 'cse', '4', '14', 'certificate', '45 days', '2019-05-15', '2019-07-05', '11:45 AM', 4000, 1000, '4284', NULL, 'paytm', 0, 'google', 'counselor', '9', NULL, 'JOINED', '2019-05-08 11:43:45', 6, '2019-05-15 16:20:02', 6, '2019-05-08 06:13:45', NULL),
(159, '1ac8fd2b-60a7-4af7-9014-281247a5aee4', 1127, 'nitesh pa', 'gupta', 'male', 'naresh kumar', '2000-05-04', '7889786258', NULL, 'samba , jammu&kashmir', NULL, 'samba', 'Andaman and Nicobar Islands', 'India', 'niteshgupta21@gmail.com', 'cgc jhanjeri', 'Degree', 'cse', '4', '15', 'certificate', '45 days', '2019-06-01', '2019-07-15', '11:45 AM', 3500, 2000, '4374', NULL, 'gpay', 0, 'google', 'counselor', '7', NULL, 'PENDING', '2019-05-08 11:53:39', 6, '2019-05-10 10:20:14', 6, '2019-05-08 06:23:39', NULL),
(160, 'e27740f6-da8f-41d5-b918-10a1cfadb92f', 1128, 'arjun', NULL, 'male', 'jaipal singh', '1999-09-08', '7347659453', NULL, 'kurali', NULL, 'kurali', 'Punjab', 'India', 'warjun31@gmail.com', 'cu', 'Degree', 'cse', '4', '1', 'regular', '45 days', '2019-05-21', '2019-07-05', '12:00 PM', 5000, 5000, '4375', NULL, 'cash', 0, 'google', 'counselor', '10', NULL, 'JOINED', '2019-05-08 12:00:22', 6, '2019-05-21 10:49:17', 6, '2019-05-08 06:30:22', NULL),
(161, 'd7b2ec58-ab83-4f31-af03-8b83c2963750', 1129, 'jagtar', 'singh', 'male', 'davinder singh', '1999-10-29', '7340707575', NULL, 'ropar', NULL, 'ropar', 'Punjab', 'India', 'wjaggis@gmail.com', 'cu', 'Degree', 'cse', '4', '1', 'certificate', '45 days', '2019-05-21', '2019-07-05', '12:15 PM', 5000, 1000, '4376', NULL, 'cash', 0, 'google', 'counselor', '7', NULL, 'JOINED', '2019-05-08 12:05:39', 6, '2019-05-21 10:53:35', 6, '2019-05-08 06:35:39', NULL),
(162, '9fba6d50-e975-4ddf-9e96-a39fe363041f', 1130, 'prince', NULL, 'male', 'rajinder parshad', '1999-09-26', '9023138723', NULL, 'kurali', NULL, 'kurali', 'Punjab', 'India', 'ps9827308@gmail.com', 'cu', 'Degree', 'cse', '4', '1', 'certificate', '45 days', '2019-05-21', '2019-07-05', '12:15 PM', 5000, 1000, '4377', NULL, 'cash', 0, 'google', 'counselor', '7', NULL, 'JOINED', '2019-05-08 12:10:37', 6, '2019-05-21 10:55:20', 6, '2019-05-08 06:40:37', NULL),
(163, '0810c525-6db1-4d92-b473-b12942fd3422', 1131, 'gaurab', 'mehra', 'male', 'padma devi', '1998-09-06', '7508945877', NULL, 'chandigarh', NULL, 'chnadigarh', 'Chandigarh', 'India', 'kingmehra199@gmail.com', 'cu', 'Degree', 'cse', '4', '1', 'regular', '45 days', '2019-05-21', '2019-07-05', '12:15 PM', 5000, 5000, '4378', NULL, 'cash', 0, 'google', 'counselor', '10', NULL, 'JOINED', '2019-05-08 12:17:49', 6, '2019-05-21 10:43:22', 6, '2019-05-08 06:47:49', NULL),
(164, 'd7033852-8fb0-466e-b33b-5293f4a4fafd', 1132, 'vipul', 'rohilla', 'male', 'shyam parkash rohilla', '1998-12-05', '8708175274', NULL, 'huda bhiwani haryana', NULL, 'huda bhiwani', 'Haryana', 'India', 'vipulrohilla75@gmail.com', 'cu', 'Degree', 'cse', '4', '13', 'regular', '45 days', '2019-05-15', '2019-07-05', '12:30 PM', 7000, 1000, '4379', NULL, 'cash', 0, 'google', 'counselor', '10', NULL, 'JOINED', '2019-05-08 12:27:46', 6, '2019-05-15 14:41:33', 6, '2019-05-08 06:57:46', NULL),
(165, 'a467c7a9-652f-48d0-934b-6ddec6e0c7e3', 1133, 'kumari', 'akanksha', 'female', 'anil kumar singh', '1998-10-09', '9888694892', NULL, 'chatubhujpur', NULL, 'chatubhupur', 'Andaman and Nicobar Islands', 'India', 'kumariakansha1998@gmail.com', 'cu', 'Degree', 'cse', '4', '1', 'microsoft certification', '45 days', '2019-05-15', '2019-07-05', '12:45 PM', 7000, 1000, '4380', NULL, 'cash', 0, 'google', 'COUNSELOR', '10', NULL, 'JOINED', '2019-05-08 12:43:58', 6, '2019-05-15 16:06:36', 6, '2019-05-08 07:13:58', NULL),
(166, '0d28bc33-7bb6-4bf6-8afe-ce67bd9b0e0e', 1134, 'rohan', 'sharma', 'male', 'sanjeev kumar', '1999-04-02', '8950580432', NULL, 'tipra kalka,haryana', NULL, 'tipra kalka', 'Haryana', 'India', 'sjkumar165@gmail.com', 'cu', 'Degree', 'cse', '4', '13', 'regular', '45 days', '2019-05-15', '2019-07-05', '12:45 PM', 7000, 1000, '4381', NULL, 'cash', 0, 'google', 'COUNSELOR', '10', NULL, 'JOINED', '2019-05-08 12:49:53', 6, '2019-05-15 14:42:37', 6, '2019-05-08 07:19:53', NULL),
(167, 'ff24b346-73c3-4d9a-8d2c-7d68df16ee97', 1135, 'jayant', 'kumar', 'male', 'janmejay kumar', '1998-11-07', '8292609627', NULL, 'gaya', NULL, 'gaya', 'Andaman and Nicobar Islands', 'India', 'jayantkumar338@gmail.com', 'cu', 'Degree', 'cse', '4', '1', 'microsoft certification', '45 days', '2019-05-15', '2019-07-05', '1:00 PM', 5000, 1000, '4382', NULL, 'cash', 0, 'google', 'COUNSELOR', '10', NULL, 'JOINED', '2019-05-08 13:02:08', 6, '2019-05-17 11:54:26', 6, '2019-05-08 07:32:08', NULL),
(168, '6f630443-3d1b-4eb6-a22b-a469c528e20f', 1136, 'neeraj', 'vaiday', 'male', 'naresh kumar', '1999-12-06', '7610440292', NULL, 'mandi', NULL, 'mandi', 'Himachal Pradesh', 'India', 'nrjvaidya6@gmail.com', 'cu', 'Degree', 'cse', '4', '13', 'regular', '45 days', '2019-05-15', '2019-07-05', '1:15 PM', 4000, 1000, '4383', NULL, 'cash', 0, 'google', 'COUNSELOR', '10', 'one week classes', 'JOINED', '2019-05-08 13:12:21', 6, '2019-05-15 15:50:41', 6, '2019-05-08 07:42:21', NULL),
(169, 'c427596e-93c1-46e8-ad42-198732745076', 1137, 'nikhil', 'atwal', 'male', 'balwant singh', '1999-08-31', '8725074668', NULL, 'gharuan', NULL, 'kharar', 'Punjab', 'India', 'nikhilatwal3318@gmail.com', 'cu', 'Degree', 'cse', '4', '1', 'regular', '45 days', '2019-05-20', '2019-07-05', '1:15 PM', 4000, 1000, '4384', NULL, 'cash', 0, 'google', 'COUNSELOR', '10', NULL, 'PENDING', '2019-05-08 13:27:21', 6, '2019-05-08 13:27:21', NULL, '2019-05-08 07:57:21', NULL),
(170, '6a990eb8-29e4-4486-bfd3-470d28d0d0da', 1138, 'PULKIT', 'KHULLAR', 'male', 'MANJOT KUMAR', '1999-01-09', '9855906284', NULL, 'SECTOR 40C CHNADIGARH', NULL, 'CHANDIGARH', 'Chandigarh', 'India', 'PULKITKHULLAR8@GMAIL.COM', 'CGC LANDRAN', 'Degree', 'ECE', '4', '21', 'REGULAR', '45 DAYS', '2019-06-01', '2019-07-15', '1:45 PM', 6000, 1000, '4385', NULL, 'online', 0, 'google', 'COUNSELOR', '14', NULL, 'PENDING', '2019-05-08 13:41:28', 6, '2019-05-08 13:41:28', NULL, '2019-05-08 08:11:28', NULL),
(171, '058c17c1-6e10-4c01-b433-b844c6ec5aee', 1139, 'AJAY', 'SINGH', 'male', 'DAULAT RAM', '1999-08-27', '8791486552', NULL, 'GHARUAN', NULL, 'KHARAR', 'Punjab', 'India', 'AJAYSINGH70899@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '2', 'CERTIFICATE', '45 DAYS', '2019-05-20', '2019-07-05', '1:45 PM', 3000, 1000, '4386', NULL, 'paytm', 0, 'google', 'COUNSELOR', '10', NULL, 'PENDING', '2019-05-08 13:47:40', 6, '2019-05-08 13:47:40', NULL, '2019-05-08 08:17:40', NULL),
(172, 'c7bdbc57-8ab0-4b61-b63e-42920482fbaa', 1140, 'manish kumar', 'saini', 'male', 'dwarka  prasad  saini', '1998-12-09', '7414088279', NULL, 'gharuan', NULL, 'kharar', 'Punjab', 'India', 'manishdwarka5912@gmail.com', 'cu', 'Degree', 'cse', '4', '2', 'certificate', '45 days', '2019-06-01', '2019-07-15', '10:45 AM', 3000, 1000, '4387', NULL, 'paytm', 0, 'google', 'counselor', '9', NULL, 'PENDING', '2019-05-09 10:56:06', 6, '2019-05-09 10:56:06', NULL, '2019-05-09 05:26:06', NULL),
(173, 'bf1f4250-6ff4-49eb-b51b-ca740a0dbf08', 1141, 'ajmer', 'singh', 'male', 'malkit  singh', '1996-01-02', '9915420554', NULL, 'hamirgarh', NULL, 'hamrgarh', 'Punjab', 'India', 'ajmersingh8037@gmail.com', 'baba farid', 'Degree', 'cse', '6', '1', 'regular', '45 days', '2019-06-10', '2019-07-25', '11:00 AM', 6500, 1000, '4292', NULL, 'cash', 0, 'google', 'COUNSELOR', '7', NULL, 'PENDING', '2019-05-09 11:03:27', 6, '2019-05-21 12:08:47', 6, '2019-05-09 05:33:27', NULL),
(174, 'c4a1749b-1360-4cb4-a9f0-9db699667d44', 1142, 'manpreet', NULL, 'male', 'richhpal ram', '1998-08-07', '9888362472', NULL, 'gurkirpa nagar abohar', NULL, 'abohar', 'Punjab', 'India', 'smanpreet2323@gmail.com', 'bfgi', 'Degree', 'cse', '4', '1', 'regular', '45 days', '2019-06-01', '2019-07-15', '11:15 AM', 6500, 1000, '4293', NULL, 'cash', 0, 'google', 'counselor', '7', NULL, 'PENDING', '2019-05-09 11:11:45', 6, '2019-05-09 11:11:45', NULL, '2019-05-09 05:41:45', NULL),
(175, '29aeabbe-af19-4acd-8152-54aed1a5a6b5', 1143, 'harpreet singh', 'brar', 'male', 'iqbal singh brar', '1995-12-29', '9495558864', NULL, 'muktsar', NULL, 'muktsar', 'Punjab', 'India', 'hbrar316@gmail.com', 'bfcet', 'Degree', 'cse', '6', '1', 'regular', '45 days', '2019-06-01', '2019-07-15', '11:15 AM', 6500, 1000, '4294', NULL, 'cash', 0, 'google', 'COUNSELOR', '7', NULL, 'PENDING', '2019-05-09 11:18:23', 6, '2019-05-09 11:18:23', NULL, '2019-05-09 05:48:23', NULL),
(176, '150e512f-20ac-41aa-82a8-61cc7c1ca11c', 1144, 'charnjeet  singh', 'kaler', 'male', 'jasvir singh', '1996-11-16', '7837847130', NULL, 'guru ki nagar bathinda', NULL, 'bathinda', 'Punjab', 'India', 'singh.charnjeet304@gmail.com', 'bfcet', 'Degree', 'cse', '6', '1', 'regular', '45 days', '2019-06-01', '2019-07-15', '11:30 AM', 6500, 500, '4295', NULL, 'cash', 0, 'google', 'COUNSELOR', '7', NULL, 'PENDING', '2019-05-09 11:25:03', 6, '2019-05-09 11:25:03', NULL, '2019-05-09 05:55:03', NULL),
(177, '465ec25f-7405-473b-9711-adf30b4827e0', 1145, 'anurag singh', 'patel', 'male', 'amrat singh patel', '1998-01-08', '7524878579', NULL, 'gharuan', NULL, 'kharar', 'Punjab', 'India', '91138@gmail.com', 'cu', 'Degree', 'cse', '4', '18', 'regular', '45 days', '2019-05-20', '2019-07-05', '11:30 AM', 5000, 1000, '4389', NULL, 'paytm', 0, 'google', 'COUNSELOR', '9', NULL, 'PENDING', '2019-05-09 11:32:23', 6, '2019-05-09 11:32:23', NULL, '2019-05-09 06:02:23', NULL),
(178, '6182469f-eda6-4088-8611-395efc4e780a', 1146, 'aniket', 'agarwal', 'male', 'rajiv agarwal', '1999-01-04', '8416986351', NULL, 'gharuan', NULL, 'kharar', 'Punjab', 'India', 'aniketagarwal575@gmail.com', 'cu', 'Degree', 'cse', '4', '2', 'microsoft,certificate', '45 days', '2019-06-01', '2019-07-15', '11:45 AM', 4000, 1000, '4296', NULL, 'gpay', 0, 'google', 'COUNSELOR', '10', NULL, 'PENDING', '2019-05-09 11:40:26', 6, '2019-05-09 11:40:26', NULL, '2019-05-09 06:10:26', NULL),
(179, '8dca276c-81cf-462f-a1b1-39545d84c457', 1147, 'tushar', 'shukla', 'male', 'ashok shukla', '2019-07-21', '9473660850', NULL, 'gharuan', NULL, 'kharar', 'Punjab', 'India', 'tushar10shukla@gmail.com', 'cu', 'Degree', 'cse', '4', '2', 'microsoft, certification', '45 days', '2019-06-01', '2019-07-15', '11:45 AM', 4000, 1000, '4297', NULL, 'gpay', 0, 'google', 'COUNSELOR', '10', NULL, 'PENDING', '2019-05-09 11:47:10', 6, '2019-05-09 11:47:10', NULL, '2019-05-09 06:17:10', NULL),
(180, 'f9666d28-6c52-4bb4-a2b9-dafcd0526e52', 1148, 'shivendra pratap', 'singh', 'male', 'dinesh pratap sngh', '2000-08-08', '8601269527', NULL, 'ghrauan', NULL, 'kharar', 'Punjab', 'India', 'shivendrapratap10@gmail.com', 'cu', 'Degree', 'cse', '4', '2', 'microsoft, certification', '45 days', '2019-06-01', '2019-07-15', '12:15 PM', 4000, 1000, '4298', NULL, 'gpay', 0, 'google', 'COUNSELOR', '10', NULL, 'PENDING', '2019-05-09 12:15:53', 6, '2019-05-09 12:15:53', NULL, '2019-05-09 06:45:53', NULL),
(181, 'e1d97f93-f81a-4892-b383-b44cbf1eff53', 1149, 'sujeet kumar', 'singh', 'male', 'kanhyalal singh', '1999-12-06', '7007447865', NULL, 'ghrauan', NULL, 'kharar', 'Punjab', 'India', 'btk441@gmail.com', 'cu', 'Degree', 'cse', '4', '1', 'microsoft ,certification', '45 days', '2019-05-16', '2019-07-15', '12:30 PM', 7000, 1000, '4299', NULL, 'paytm', 0, 'google', 'COUNSELOR', '10', NULL, 'JOINED', '2019-05-09 12:24:19', 6, '2019-05-16 16:50:03', 6, '2019-05-09 06:54:19', NULL),
(182, 'e59d4b59-dba0-434d-95f8-6705686776ed', 1150, 'nishi  bharadvaj', 'sharma', 'female', 'ashok kumar singh', '1998-03-03', '7007502898', NULL, 'gharuan', NULL, 'kharar', 'Punjab', 'India', 'nic.sharma43@gmail.com', 'cu', 'Degree', 'bca', '4', '11', 'regular', '45 days', '2019-05-18', '2019-07-15', '12:30 PM', 5000, 1000, '4390', NULL, 'paytm', 0, 'google', 'COUNSELOR', '9', NULL, 'JOINED', '2019-05-09 12:31:17', 6, '2019-05-18 11:38:08', 6, '2019-05-09 07:01:17', NULL),
(183, 'fe2a1c8f-4cc7-428d-8fb9-4abe8380944c', 1151, 'shrawani', 'kajal', 'female', 'lalan prasad', '1999-12-05', '9102664834', NULL, 'gharuan', NULL, 'kharar', 'Punjab', 'India', 'kajalsharwani@gmail.com', 'cu', 'Degree', 'bca', '4', '11', 'regular', '45 days', '2019-05-18', '2019-07-15', '12:45 PM', 5000, 1000, '4391', NULL, 'paytm', 0, 'google', 'COUNSELOR', '9', NULL, 'JOINED', '2019-05-09 12:38:48', 6, '2019-05-18 11:39:30', 6, '2019-05-09 07:08:48', NULL),
(184, '1d49dccb-fe0e-4109-834d-699953de8cba', 1152, 'abhijit', 'ranjan', 'male', 'abhijit ranjan', '2000-01-01', '9601525208', NULL, 'gharuan', NULL, 'kharar', 'Punjab', 'India', 'cu.17bcs1928@gmail.com', 'cu', 'Degree', 'cse', '4', '1', 'certificate', '45 days', '2019-05-20', '2019-07-05', '12:45 PM', 3000, 1000, '4400', NULL, 'cash', 0, 'google', 'COUNSELOR', '10', NULL, 'PENDING', '2019-05-09 12:46:56', 6, '2019-05-09 12:46:56', NULL, '2019-05-09 07:16:56', NULL),
(185, '4e03fed8-7a34-4e95-824b-4823b267cc29', 1153, 'ram kumar', 'yaday', 'male', 'kameshwar yadav', '1997-09-14', '7091764530', NULL, 'gharuan', NULL, 'kharar', 'Punjab', 'India', 'cu.17bcs3090@gmail.com', 'cu', 'Degree', 'cse', '4', '14', 'networking  (microsoft certification)', '45 days', '2019-06-01', '2019-07-15', '1:00 PM', 5000, 2500, '4407', NULL, 'paytm', 0, 'google', 'COUNSELOR', '14', NULL, 'PENDING', '2019-05-09 12:56:50', 6, '2019-05-09 12:56:50', NULL, '2019-05-09 07:26:50', NULL),
(186, '41865773-6845-4b69-98e4-2fff988fe799', 1154, 'ritu', 'singh', 'female', 'ram awadhesh', '1998-10-08', '6260071785', NULL, 'jabalpur', NULL, 'jabalpur', 'Andaman and Nicobar Islands', 'India', '810rits@gmail.com', 'cu', 'Degree', 'cse', '4', '1', 'microsoft ,certifiction', '45 days', '2019-05-15', '2019-06-15', '1:00 PM', 7000, 1000, '4409', NULL, 'cash', 0, 'google', 'COUNSELOR', '10', NULL, 'JOINED', '2019-05-09 13:04:01', 6, '2019-05-15 16:07:44', 6, '2019-05-09 07:34:01', NULL),
(187, '7ca2d055-6526-4719-a576-8c34375a7ed5', 1155, 'tanuja', 'bodh', 'female', 'tanzin navang', '2000-01-01', '9805087278', NULL, 'manali', NULL, 'manali', 'Himachal Pradesh', 'India', 'cu17bcs1999@gmail.com', 'cu', 'Degree', 'cse', '4', '13', 'regular', '45 days', '2019-05-15', '2019-07-05', '1:15 PM', 7000, 1000, '4410', NULL, 'cash', 0, 'google', 'COUNSELOR', '10', NULL, 'JOINED', '2019-05-09 13:10:33', 6, '2019-05-15 16:06:14', 6, '2019-05-09 07:40:33', NULL),
(188, '0c2f0e4f-5294-4ea1-8e62-f63fe7215b5f', 1156, 'arpit', 'bansal', 'male', 'sunil bansal', '1998-07-08', '9316220872', NULL, 'kapurthala', NULL, 'kapurthala', 'Punjab', 'India', 'arpits12@gmail.com', 'amity global business school', 'Degree', 'bba', '4', '17', 'regular', '45 days', '2019-05-23', '2019-07-15', '1:15 PM', 5000, 2000, '4413', NULL, 'online', 0, 'google', 'COUNSELOR', '7', 'certificate only', 'JOINED', '2019-05-09 13:20:28', 6, '2019-05-23 11:03:12', 6, '2019-05-09 07:50:28', NULL),
(189, 'dadeedcf-0989-43bc-b3f8-7bf78c5c1dd5', 1157, 'akash kumar', 'mauraya', 'male', 'suresh kumar mauraya', '1999-09-03', '7888623454', NULL, 'amritsar', NULL, 'amritsar', 'Punjab', 'India', 'akash2@gmail.com', 'cu', 'Degree', 'cse', '4', '22', 'certificate', '45 days', '2019-05-14', '2019-07-05', '1:30 PM', 4000, 4000, '4415', NULL, 'paytm', 0, 'google', 'COUNSELOR', '7', 'CERTIFICATE ONLY', 'JOINED', '2019-05-09 13:32:06', 6, '2019-05-14 15:08:56', 6, '2019-05-09 08:02:06', NULL),
(190, 'c0d66886-90b1-4e01-8df6-f853fe801ae5', 1158, 'amit', 'PANDEY', 'male', 'dinesh pandey', '1998-09-13', '8726692396', NULL, 'gharuan', NULL, 'kharar', 'Punjab', 'India', 'amit23@gmail.com', 'cu', 'Degree', 'cse', '4', '22', 'certificate', '45 days', '2019-05-14', '2019-07-05', '1:45 PM', 4000, 4000, '4414', NULL, 'paytm', 0, 'google', 'counselor', '7', 'CERTIFICATE ONLY', 'JOINED', '2019-05-09 13:43:08', 6, '2019-05-14 15:08:22', 6, '2019-05-09 08:13:08', NULL),
(191, 'd5631f1a-5fc7-4395-a0cd-5a5a50e2e921', 1159, 'kishan', 'kumar', 'male', 'mukesh', '1999-05-12', '7463864587', NULL, 'gharuan', NULL, 'kharar', 'Punjab', 'India', 'kishan34@gmail.com', 'cu', 'Degree', 'cse', '4', '22', 'certificate', '45 days', '2019-05-20', '2019-07-05', '1:45 PM', 4000, 2000, '4416', NULL, 'paytm', 0, 'google', 'COUNSELOR', '7', NULL, 'PENDING', '2019-05-09 13:48:53', 6, '2019-05-09 13:48:53', NULL, '2019-05-09 08:18:53', NULL),
(192, '5ae577bb-92fc-4509-96da-712edddd0976', 1160, 'reena', 'rani', 'female', 'desraj', '1998-11-28', '8727923674', NULL, 'nvrmahal', NULL, 'nurmahal', 'Punjab', 'India', 'reenaqw@gmail.com', 'riet phagwara', 'Degree', 'mba', '4', '3', 'regular', '45 days', '2019-06-01', '2019-07-15', '2:00 PM', 4200, 3200, '4420', NULL, 'online', 0, 'google', 'COUNSELOR', '9', NULL, 'PENDING', '2019-05-09 14:00:57', 6, '2019-05-09 14:00:57', NULL, '2019-05-09 08:30:57', NULL),
(193, 'd6466d19-a9bf-472d-8b8f-2b2e22ae7c98', 1161, 'anjana', 'kumari', 'female', 'parld kumar', '1997-02-15', '8727017851', NULL, 'phagwara', NULL, 'phagwara', 'Punjab', 'India', 'anjanagupta8727@gmail.com', 'riet pharwara', 'Degree', 'mba', '4', '3', 'regular', '45 days', '2019-06-01', '2019-07-15', '2:15 PM', 4200, 1000, '4419', NULL, 'online', 0, 'google', 'COUNSELOR', '9', NULL, 'PENDING', '2019-05-09 14:07:46', 6, '2019-05-09 14:07:46', NULL, '2019-05-09 08:37:46', NULL),
(194, '2c4e2bcb-9875-4d76-8aad-2a2060cac163', 1162, 'sandeep', 'kaur', 'female', 'parwinder singh', '1997-07-23', '9872166534', NULL, 'phagwara', NULL, 'phagwara', 'Punjab', 'India', 'sk328936@gmail.com', 'riet phagwara', 'Degree', 'mba', '4', '3', 'regular', '45 days', '2019-06-01', '2019-07-15', '10:45 AM', 4200, 1000, '4418', NULL, 'online', 0, 'google', 'counselor', '9', NULL, 'PENDING', '2019-05-10 10:39:40', 6, '2019-05-10 10:57:04', 6, '2019-05-10 05:09:40', NULL),
(195, 'a6f2be60-7bb9-4207-ab05-05cda99ad527', 1163, 'hitesh', 'chandan', 'male', 'mukesh kumar', '1995-12-11', '8230051123', NULL, 'nawashahar', NULL, 'nawashar', 'Punjab', 'India', 'chandanhitesh3@gmail.com', 'riet phagwara', 'Degree', 'mba', '4', '3', 'regular', '45 days', '2019-06-01', '2019-07-15', '10:45 AM', 4200, 1000, '4421', NULL, 'online', 0, 'google', 'counselor', '9', NULL, 'PENDING', '2019-05-10 10:56:21', 6, '2019-05-10 10:56:21', NULL, '2019-05-10 05:26:21', NULL),
(196, 'dd20c929-63b6-45fa-97a8-ca3fd87d9b0d', 1164, 'trigunanand', 'mishra', 'male', 'akhileshwar  mishra', '1998-10-14', '9794356840', NULL, 'gharuan', NULL, 'kharar', 'Punjab', 'India', 'trigunanandmishra07@gmail.com', 'cu', 'Degree', 'cse', '4', '23', 'certificate', '45 days', '2019-05-14', '2019-07-15', '11:00 AM', 2500, 2500, '4422', NULL, 'gpay', 0, 'google', 'COUNSELOR', '9', 'CERTIFICATE ONLY', 'JOINED', '2019-05-10 11:06:17', 6, '2019-05-14 15:09:36', 6, '2019-05-10 05:36:17', NULL),
(197, 'b438e00c-425a-4b2b-bd0b-a39a1d201fe3', 1165, 'moin', 'khan', 'male', 'mohd izhar alz kha', '1999-02-26', '8824851947', NULL, 'gharuan', NULL, 'kharar', 'Punjab', 'India', 'moin1999khan@gmail.com', 'cu', 'Degree', 'cse', '4', '14', 'certificate', '45 days', '2019-05-14', '2019-07-15', '11:15 AM', 2500, 2500, '4423', NULL, 'gpay', 0, 'google', 'COUNSELOR', '9', 'CERTIFICATE ONLY', 'JOINED', '2019-05-10 11:17:47', 6, '2019-05-14 15:09:59', 6, '2019-05-10 05:47:47', NULL),
(198, 'd0e157ce-cfcd-43c4-aec6-09df3c7dbdae', 1166, 'gautam', 'oraon', 'male', 'yogendra bhagat', '1999-05-22', '9814386333', NULL, 'gharuan', NULL, 'kharar', 'Punjab', 'India', 'gautamoraon1997@gmail.com', 'cu', 'Degree', 'cse', '4', '1', 'certificate', '45 days', '2019-05-14', '2019-07-05', '11:30 AM', 3000, 3000, '4425', NULL, 'gpay', 0, 'google', 'COUNSELOR', '10', 'CERTIFICATE ONLY', 'JOINED', '2019-05-10 11:24:47', 6, '2019-05-14 15:10:37', 6, '2019-05-10 05:54:47', NULL),
(199, '4344a3e4-e8bb-4b1f-99af-6ff356aac570', 1167, 'rupinder', 'singh', 'male', 'sukhpal singh', '1998-11-07', '9872638723', NULL, 'mansa', NULL, 'mansa', 'Punjab', 'India', 'itsmerupindersingh@gmail.com', 'rimt university', 'Degree', 'me', '4', '7', 'autocade brush up', '45 days', '2019-06-01', '2019-07-15', '11:45 AM', 4000, 2000, '4426', NULL, 'cash', 0, 'google', 'COUNSELOR', '7', NULL, 'PENDING', '2019-05-10 11:49:53', 6, '2019-05-10 11:49:53', NULL, '2019-05-10 06:19:53', NULL),
(200, '08fbc98d-f6f6-4439-91e3-d411ca11815c', 1168, 'rohit', 'kumar', 'male', 'rajesh kumar', '1999-01-03', '8968448082', NULL, 'ludhiana', NULL, 'ludhiana', 'Punjab', 'India', 'rohitkumar8986@gmail.com', 'rimt university', 'Degree', 'me', '4', '7', 'autocad brush up', '45 days', '2019-06-01', '2019-07-15', '12:00 PM', 4000, 2000, '4427', NULL, 'cash', 0, 'google', 'COUNSELOR', '7', NULL, 'PENDING', '2019-05-10 11:56:14', 6, '2019-05-10 11:56:14', NULL, '2019-05-10 06:26:14', NULL),
(201, '72e01610-7ffc-457f-8c1c-742d630aa158', 1169, 'rohit', 'kumar', 'male', 'rajesh kumar', '1999-01-03', '8968445623', NULL, 'ludhiana', NULL, 'ludhiana', 'Punjab', 'India', 'rohitkumar8968@gmail.com', 'rimt univrsity', 'Degree', 'me', '4', '7', 'autocad brush up', '45 days', '2019-06-01', '2019-07-15', '12:00 PM', 4000, 2000, '4427', NULL, 'cash', 0, 'google', 'COUNSELOR', '7', NULL, 'PENDING', '2019-05-10 12:04:02', 6, '2019-05-10 12:04:02', NULL, '2019-05-10 06:34:02', NULL),
(202, '1feaddb4-1d93-47ad-a24e-de36294fd4d3', 1170, 'manpreet', 'singh', 'male', 'avtar singh', '1997-04-14', '7837316230', NULL, 'doraha', NULL, 'doraha', 'Punjab', 'India', 'manpreetlohat2@gmail.com', 'rimt university', 'Degree', 'me', '4', '7', 'autocad brush up', '45 days', '2019-06-01', '2019-07-15', '12:15 PM', 4000, 2000, '4428', NULL, 'online', 0, 'google', 'COUNSELOR', '7', NULL, 'PENDING', '2019-05-10 12:08:46', 6, '2019-05-10 12:08:46', NULL, '2019-05-10 06:38:46', NULL),
(203, 'e24c37ab-5d80-4de4-9041-97a5016ca469', 1171, 'raghav', 'saggar', 'male', 'bhushan k saggar', '1999-10-28', '7009196732', NULL, 'payal', NULL, 'payal', 'Punjab', 'India', 'raghavsaggar1111@gmail.com', 'rimt university', 'Degree', 'cse', '4', '1', 'regular', '45 days', '2019-06-01', '2019-07-15', '12:15 PM', 4000, 2000, '4429', NULL, 'cash', 0, 'google', 'COUNSELOR', '7', NULL, 'PENDING', '2019-05-10 12:14:58', 6, '2019-05-10 12:14:58', NULL, '2019-05-10 06:44:58', NULL),
(204, '0ad09b4e-4da8-4b41-b9aa-97f5d9d0b89c', 1172, 'pawanpreet', 'singh', 'male', 'parminder singh', '2000-07-08', '6283431415', NULL, 'ludhiana', NULL, 'ludhiana', 'Punjab', 'India', 'pawandhammi142021@gmail.com', 'rimt university', 'Degree', 'cse', '4', '1', 'regular', '45 days', '2019-06-01', '2019-07-15', '12:15 PM', 4000, 2000, '4430', NULL, 'cash', 0, 'google', 'COUNSELOR', '7', NULL, 'PENDING', '2019-05-10 12:21:33', 6, '2019-05-10 12:21:33', NULL, '2019-05-10 06:51:33', NULL),
(205, 'c3a8eb17-dc95-4c73-a583-81035a92a090', 1173, 'hushanpreet', 'singh', 'male', 'jagmeet singh', '1998-10-07', '8847297871', NULL, 'sangrur', NULL, 'sangrur', 'Punjab', 'India', 'hushanpreetsingh7805@gmail.com', 'rimt', 'Degree', 'cse', '4', '1', 'regular', '45 days', '2019-06-01', '2019-07-15', '12:30 PM', 4000, 2000, '4431', NULL, 'cash', 0, 'google', 'COUNSELOR', '7', NULL, 'PENDING', '2019-05-10 12:28:45', 6, '2019-05-10 12:28:45', NULL, '2019-05-10 06:58:45', NULL),
(206, '5f986de6-902a-4139-925c-913d2b4d82b3', 1174, 'tejveer singh', 'waraich', 'male', 'harjinder  singh', '1997-11-24', '9779885335', NULL, 'sirhind', NULL, 'sirhind', 'Punjab', 'India', 'incrediblejatt0001@hotmail.com', 's.g.g.s.w.u', 'Degree', 'cse', '4', '1', 'regular', '45 days', '2019-06-01', '2019-07-15', '12:30 PM', 5000, 1000, '4501', NULL, 'cash', 0, 'google', 'mr rajeev', '13', NULL, 'PENDING', '2019-05-10 12:37:41', 6, '2019-05-10 12:37:41', NULL, '2019-05-10 07:07:41', NULL),
(207, '2f83a33c-4577-4964-8c0e-c8886c2ce35d', 1175, 'simranjit', NULL, 'male', 'amarjit singh', '1998-09-22', '7009083922', NULL, 'ROPAR', NULL, 'ROPAR', 'Punjab', 'India', 'singhsimranjit209@GMAIL.COM', 'S.G.G.S.W.U', 'Master', 'CSE', '6', '1', 'REGULAR', '45 DAYS', '2019-06-01', '2019-07-15', '12:45 PM', 5000, 1000, '4502', NULL, 'cash', 0, 'google', 'MR RAJEEV', '13', NULL, 'PENDING', '2019-05-10 12:45:09', 6, '2019-05-10 12:45:09', NULL, '2019-05-10 07:15:09', NULL),
(208, 'f0b6c34b-b8b4-440e-aa5f-d0a34b9823e3', 1176, 'GURWINDER', 'SINGH', 'male', 'KULWANT SINGH', '1998-08-28', '8837867681', NULL, 'SIRHIND CITY FATEHGARH SHAHIB', NULL, 'FATEHGARH SHAHIB', 'Punjab', 'India', 'GURWINDERNAGRA@GMAIL.COM', 'S.G.G.S.W.U', 'Master', 'ECE', '6', '1', 'REGULAR', '45 DAYS', '2019-06-01', '2019-07-15', '12:45 PM', 5000, 1000, '4503', NULL, 'cash', 0, 'google', 'MR RAJEEV', '13', NULL, 'PENDING', '2019-05-10 12:51:24', 6, '2019-05-10 12:51:24', NULL, '2019-05-10 07:21:24', NULL),
(209, '41680e3a-6c84-4538-82cc-f58a0824129b', 1177, 'ANKIT', 'KUMAR', 'male', 'ANIL KUMAR GUPTA', '1999-08-20', '9973990944', NULL, 'GHARUAN', NULL, 'KHARAR', 'Punjab', 'India', 'AKI2722KI@GMAIL.COM', 'CU', 'Degree', 'CSE', '45 DAYS', '16', 'MICROSOFT ,CERTIFICATION (PYTHM)', '45 DAYS', '2019-05-14', '2019-07-15', '1:00 PM', 5000, 2500, '4432', NULL, 'gpay', 0, 'google', 'COUNSELOR', '14', 'MICROSOFT CERTIFICATION ONLY', 'JOINED', '2019-05-10 12:58:45', 6, '2019-05-14 15:13:09', 6, '2019-05-10 07:28:45', NULL),
(210, 'ffdf941a-1c6a-48bb-8d25-e18cb12a9abe', 1178, 'SANJEEV', 'ASHOKA', 'male', 'PROMOD KUMAR', '2000-01-01', '9872255133', NULL, 'GHARUAN', NULL, 'KHARAR', 'Punjab', 'India', 'SANJEEVSHOKA123@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '2', 'MICROSOFT CERTIFICATE', '45 DAYS', '2019-05-21', '2019-07-15', '1:00 PM', 5000, 2500, '4433', NULL, 'gpay', 0, 'google', 'COUNSELOR', '14', 'MICROSOFT CERTIFICATION ONLY', 'JOINED', '2019-05-10 13:03:46', 6, '2019-05-21 10:50:31', 6, '2019-05-10 07:33:46', NULL),
(211, 'ae93f45e-53f8-456e-89b0-e68cb62772d6', 1179, 'KUSHAL', 'SINGH', 'male', 'VIJAY SINGH', '1997-05-24', '9413031791', NULL, 'RAJASTHAN', NULL, 'VIDHYAVIHAR PILANI', 'Rajasthan', 'India', 'KUSHALSA@GMAIL.COM', 'MIMIT', 'Degree', 'CSE', '4', '1', 'REGULAR', '45 DAYS', '2019-06-01', '2019-07-15', '1:15 PM', 4500, 2000, '4505', NULL, 'gpay', 0, 'google', 'COUNSELOR', '7', NULL, 'PENDING', '2019-05-10 13:10:42', 6, '2019-05-10 13:10:42', NULL, '2019-05-10 07:40:42', NULL),
(212, '73f101b3-9e6e-46c1-aa2e-c0a202b1b1ef', 1180, 'ARSHAN', 'ALI', 'male', 'SHAHZAD ALI', '1998-06-09', '9927454200', NULL, 'GHARUAN', NULL, 'KHARAR', 'Punjab', 'India', 'ALIARSHAN007@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '13', 'CERTIFICATE', '45 DAYS', '2019-06-01', '2019-07-15', '1:15 PM', 4500, 1000, '4510', NULL, 'paytm', 0, 'google', 'COUNSELOR', '14', NULL, 'PENDING', '2019-05-10 13:18:01', 6, '2019-05-10 13:18:01', NULL, '2019-05-10 07:48:01', NULL),
(213, '877a4b0d-90ee-4867-8126-7fa133aff399', 1181, 'UMMER', 'MUMTAZ', 'male', 'MOHD TAJ', '2000-04-01', '9682336479', NULL, 'GARUAN', NULL, 'KHARAR', 'Punjab', 'India', 'UMARQURESH064@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '1', 'MICROSOFT,CERTIFICATION', '45 DAYS', '2019-05-14', '2019-07-15', '1:30 PM', 4500, 4500, '4439', NULL, 'paytm', 0, 'google', 'COUNSELOR', '14', 'CERTIFICATE ONLY', 'JOINED', '2019-05-10 13:23:37', 6, '2019-05-14 15:15:15', 6, '2019-05-10 07:53:37', NULL),
(214, 'b1e42017-4327-4956-8f3e-635b6856c7cc', 1182, 'LOVEPREET', 'KAUR', 'female', 'ARSAL SINGH', '1998-07-25', '8872794100', NULL, 'MALOUT', NULL, 'BATHINDA', 'Punjab', 'India', 'LOVEPREET98@GMAIL.COM', 'MIMIT', 'Degree', 'CSE', '4', '1', 'REGULAR', '45 DAYS', '2019-06-01', '2019-07-15', '10:45 AM', 4500, 2000, '4442', NULL, 'gpay', 0, 'google', 'COUNSELOR', '7', NULL, 'PENDING', '2019-05-13 10:37:42', 6, '2019-05-13 10:37:42', NULL, '2019-05-13 05:07:42', NULL),
(215, '8ce66694-e818-4c3f-b82a-7c2ceb7c7178', 1183, 'SURINDER', 'SHARMA', 'male', 'KULDEEP  SHARMA', '1998-11-19', '8219195436', NULL, 'KULLU, HIMACHAL  PRADESH', NULL, 'KULLU', 'Himachal Pradesh', 'India', 'SURINDER12@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '2', 'REGULAR', '45 DAYS', '2019-05-15', '2019-07-15', '10:45 AM', 5000, 1000, '4443', NULL, 'gpay', 0, 'google', 'COUNSELOR', '7', NULL, 'JOINED', '2019-05-13 10:45:30', 6, '2019-05-15 15:41:45', 6, '2019-05-13 05:15:30', NULL),
(216, 'bfc689ef-222d-4b26-8bf9-9f27d63c54a2', 1184, 'NEETU', 'RAINA', 'female', 'KANYA LAL RAINA', '1999-09-14', '8899995628', NULL, 'JAMMU', NULL, 'JAMMU', 'Jammu and Kashmir', 'India', 'NEETURAINA28@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '13', 'REGULAR', '45 DAYS', '2019-05-15', '2019-07-05', '11:00 AM', 7000, 1000, '4515', NULL, 'gpay', 0, 'google', 'COUNSELOR', '9', NULL, 'JOINED', '2019-05-13 10:49:09', 6, '2019-05-15 16:05:37', 6, '2019-05-13 05:19:09', NULL),
(217, '752e8ced-5a95-4506-9430-a3fd7668855f', 1185, 'AJAY', 'SINGH', 'male', 'LAKHWINDER SINGH', '1998-04-28', '6284542837', NULL, 'LALRU', NULL, 'ZIRAKPUR', 'Punjab', 'India', 'AJAYSINGHPAWAR2804@GMAIL.COM', 'SSIET', 'Degree', 'EE', '4', '10', 'CERTIFICATE', '45 DAYS', '2019-06-01', '2019-07-15', '11:00 AM', 4500, 2000, '4518', NULL, 'cash', 0, 'google', 'COUNSELOR', '9', NULL, 'PENDING', '2019-05-13 10:52:33', 6, '2019-05-13 10:52:33', NULL, '2019-05-13 05:22:33', NULL),
(218, '150d87bd-2d9d-4b99-b82e-cb7a89cf7cd2', 1186, 'RITESH', 'RANJAN', 'male', 'SANJAY KUMAR JHA', '1999-03-01', '8169001928', NULL, 'GHARUAN', NULL, 'KHARAR', 'Punjab', 'India', 'RANJANRITESH446@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '16', 'CERTIFICATE', '45 DAYS', '2019-06-01', '2019-07-15', '11:00 AM', 4000, 2000, '4520', NULL, 'paytm', 0, 'google', 'COUNSELOR', '9', NULL, 'PENDING', '2019-05-13 10:55:32', 6, '2019-05-13 10:55:32', NULL, '2019-05-13 05:25:32', NULL),
(219, '3e65b3c6-509c-4281-8b40-83bd84fbf519', 1187, 'AMAN', 'SHARMA', 'male', 'SANDEEP KUMAR', '1999-02-24', '9465157575', NULL, 'GHARUAN', NULL, 'KHARAR', 'Punjab', 'India', 'SAM162674@GMAIL.COM', 'CU', 'Degree', 'CSE', '6', '2', 'REGULAR', '2 MONTH', '2019-05-20', '2019-07-20', '11:00 AM', 6000, 2000, '4521', NULL, 'paytm', 0, 'google', 'MS. harjinder kaur', '13', NULL, 'PENDING', '2019-05-13 10:59:52', 6, '2019-05-13 10:59:52', NULL, '2019-05-13 05:29:52', NULL),
(220, '647a12ec-f3f2-4724-84e0-fec8a319be2b', 1188, 'SHIVANG', NULL, 'male', 'SURESH KUMAR', '2000-06-05', '8627873160', NULL, 'GHARUAN', NULL, 'KHARAR', 'Punjab', 'India', 'SHIVANGNAGROTA@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '2', 'CERTIFICATE', '45DAYS', '2019-05-15', '2019-07-15', '11:00 AM', 3500, 3500, '4522', NULL, 'cash', 0, 'google', 'COUNSELOR', '14', 'one week classes, CERTIFICATE ONLY', 'JOINED', '2019-05-13 11:03:17', 6, '2019-05-15 15:50:09', 6, '2019-05-13 05:33:17', NULL),
(221, '751cb3d4-c2d2-4f8e-8279-42a3760700ca', 1189, 'RAMAN', 'KATOCH', 'male', 'BHADUR KATOCH', '1999-12-15', '9815253452', NULL, 'SECTOR 40', NULL, 'CHANDIGARH', 'Chandigarh', 'India', 'RAMANKATOCH19@GMAIL.COM', 'CGC LANDRAN', 'Degree', 'CSE', '4', '2', 'CERTIFICATE', '45 DAYS', '2019-05-15', '2019-07-15', '11:15 AM', 3500, 1000, '4523', NULL, 'gpay', 0, 'google', 'COUNSELOR', '14', 'one week classes', 'JOINED', '2019-05-13 11:07:33', 6, '2019-05-15 15:49:46', 6, '2019-05-13 05:37:33', NULL),
(222, '56cb56ee-12b8-4dbc-9315-39a56bf0d73e', 1190, 'DILPREET', 'SINGH', 'male', 'MANJEET SINGH', '1998-06-01', '9463621007', NULL, 'GHARUAN', NULL, 'KHARAR', 'Punjab', 'India', 'PREETBRAR8475@GMAIL.COM', 'CU', 'Degree', 'CSE', '6', '2', 'REGULAR', '2 MONTH', '2019-05-20', '2019-07-20', '11:15 AM', 6000, 2000, '4524', NULL, 'paytm', 0, 'google', 'MS HARJINDER KAUR', '13', NULL, 'JOINED', '2019-05-13 11:10:18', 6, '2019-05-20 18:54:12', 6, '2019-05-13 05:40:18', NULL),
(223, '09d1d97c-fa82-49fc-a0e1-dd9afde1ca77', 1191, 'JAPNEEL', 'SINGH', 'male', 'JITENDER SINGH', '1998-06-07', '8427480803', NULL, 'GHARUAN', NULL, 'KHARAR', 'Punjab', 'India', '16BCS2600@GMAIL.COM', 'CU', 'Degree', 'CSE', '6', '2', 'REGULAR', '2 MONTHS', '2019-05-20', '2019-07-20', '11:15 AM', 6000, 2000, '4525', NULL, 'paytm', 0, 'just-dial', 'MS. HARJINDER KAUR', '13', NULL, 'PENDING', '2019-05-13 11:15:23', 6, '2019-05-13 11:15:23', NULL, '2019-05-13 05:45:23', NULL),
(224, 'af1344fc-3ec3-4f4b-bfbf-e955e6645405', 1192, 'AMAN', 'MISHRA', 'male', 'PRITHAVI PAL MISHRA', '1970-01-01', '7347280092', NULL, 'GHARUAN', NULL, 'KHARAR', 'Punjab', 'India', 'AMANSDR2014@GMAIL.COM', 'CU', 'Degree', 'CSE', '6', '16', 'REGULAR', '2 MONTHS', '2019-05-18', '2019-07-20', '11:15 AM', 4000, 1500, '4526', NULL, 'paytm', 0, 'just-dial', 'COUNSELOR', '9', NULL, 'JOINED', '2019-05-13 11:19:40', 6, '2019-05-18 11:42:21', 6, '2019-05-13 05:49:40', NULL),
(225, 'c57473a8-ce23-476a-a81d-7b5fa5455a11', 1193, 'DEEPAK', 'YADAV', 'male', 'UDHAM SINGH YADAV', '1998-11-13', '8934030564', NULL, 'GHARUAN', NULL, 'KHARAR', 'Punjab', 'India', 'DEEPAKYADAV98@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '2', 'CERTIFICATE', '45 DAYS', '2019-05-20', '2019-07-05', '12:30 PM', 4000, 1000, '4388', NULL, 'paytm', 0, 'just-dial', 'COUNSELOR', '7', NULL, 'PENDING', '2019-05-13 12:34:43', 6, '2019-05-13 12:34:43', NULL, '2019-05-13 07:04:43', NULL),
(226, 'e794b62e-4de4-4eea-baf2-6c050d099c47', 1194, 'SUKHPREET', 'SINGH', 'male', 'HARNEK SINGH', '1993-08-12', '8146461118', NULL, 'MOGA', NULL, 'MOGA', 'Punjab', 'India', 'SUKHPREET93@GMAIL.COM', 'ARYABHATTA COLLEGE', 'Degree', 'EE', '4', '10', 'CERTIFICATE', '45 DAYS', '2019-06-01', '2019-07-15', '3:00 PM', 3000, 1500, '4533', NULL, 'paytm', 0, 'google', 'COUNSELOR', '7', NULL, 'PENDING', '2019-05-13 15:00:31', 6, '2019-05-13 15:00:31', NULL, '2019-05-13 09:30:31', NULL),
(227, 'a3c2f8c7-9601-421c-949f-c60f78b7e82b', 1195, 'SHUBHAM', 'SHARMA', 'male', 'VINOD SHARMA', '1999-09-11', '8860315605', NULL, 'GHARUAN', NULL, 'KHARAR', 'Punjab', 'India', 'SHUBHAM9019@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '2', 'MICROSOFT CERTIFICATION AND CERTIFICATE', '45 DAYS', '2019-05-20', '2019-07-05', '11:45 AM', 4000, 2000, '4446', NULL, 'cash', 0, 'google', 'COUNSELOR', '9', NULL, 'PENDING', '2019-05-14 11:47:02', 6, '2019-05-14 11:47:02', NULL, '2019-05-14 06:17:02', NULL),
(228, '0c100f74-b578-463a-9e82-53fcd3f2c66f', 1196, 'DIKSHIT', 'SHARMA', 'male', 'ALOK SHARMA', '1999-11-19', '7015135675', NULL, 'GHARUAN', NULL, 'KHARAR', 'Punjab', 'India', 'DIKSHIT1233@GMAIL.COM', 'CU', 'Degree', 'IT', '4', '1', 'MICROSOFT CERTIFICATION AND CERTIFICATE', '45 DAYS', '2019-05-20', '2019-07-05', '12:00 PM', 4000, 2000, '4447', NULL, 'cash', 0, 'google', 'COUNSELOR', '9', NULL, 'PENDING', '2019-05-14 11:51:41', 6, '2019-05-14 11:51:41', NULL, '2019-05-14 06:21:41', NULL),
(229, 'd257ac9f-5954-42ba-b356-5835d04ea0d9', 1197, 'DIVYA', 'SHARMA', 'female', 'NUTAN KUMAR', '1999-10-20', '7009646182', NULL, 'GHAUAN', NULL, 'KHARAR', 'Punjab', 'India', 'DIVYACU17BCS2996@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '14', 'ADVANCE JAVA CERTIFICATE', '45 DAYS', '2019-05-20', '2019-07-05', '12:00 PM', 3500, 1500, '4534', NULL, 'paytm', 0, 'google', 'COUNSELOR', '14', NULL, 'PENDING', '2019-05-14 11:57:43', 6, '2019-05-14 11:57:43', NULL, '2019-05-14 06:27:43', NULL),
(230, '375d7efd-b7ab-496c-815f-470096559878', 1198, 'DILKUSH', 'KUMAR', 'male', 'MOHAN SINGH', '1999-02-23', '7281944556', NULL, 'GHARUAN', NULL, 'KHARAR', 'Punjab', 'India', 'DILKUSHSIN74111@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '1', 'MICROSOFT CERTIFICATION', '45 DAYS', '2019-06-03', '2019-07-18', '5:45 PM', 6000, 1000, '4448', NULL, 'cash', 0, 'google', 'COUNSELOR', '9', NULL, 'PENDING', '2019-05-14 17:44:54', 6, '2019-05-14 17:44:54', NULL, '2019-05-14 12:14:54', NULL),
(231, 'e1375589-b232-488f-83ed-7937afe03234', 1199, 'SUMIT', NULL, 'male', 'SUBHASH', '1999-11-20', '8168896114', NULL, 'GHARUAN', NULL, 'KHARAR', 'Punjab', 'India', 'SUMITSHARMAS2099@GMAIL.COM', 'CU', 'Degree', 'IT', '4', '1', 'MICROSOFT CERTIFICATION', '45 DAYS', '2019-06-03', '2019-06-30', '11:15 AM', 4000, 2000, '4449', NULL, 'cash', 0, 'google', 'COUNSELOR', '9', NULL, 'PENDING', '2019-05-15 11:14:12', 6, '2019-05-16 14:03:26', 6, '2019-05-15 05:44:12', NULL),
(232, '855d92a5-7e93-42cd-b818-0d19b87730ab', 1200, 'PARV', 'KHULLER', 'male', 'PANKAJ KHULLER', '2000-02-16', '8628850655', NULL, 'GHARUAN', NULL, 'KHARAR', 'Punjab', 'India', 'PARVMANALI@GMAIL.COM', 'CU', 'Degree', 'CLOUD', '4', '14', 'REGULAR', '45 DAYS', '2019-05-16', '2019-06-30', '6:30 PM', 4000, 1000, '4546', NULL, 'paytm', 0, 'google', 'COUNSELOR', '9', NULL, 'JOINED', '2019-05-15 18:32:10', 6, '2019-05-16 10:19:21', 6, '2019-05-15 13:02:10', NULL),
(233, 'e7193187-a3dc-4ac7-a237-a7a0826ba21a', 1201, 'ANSHUL', 'MEHTA', 'male', 'HARPAL MEHTA', '2000-03-17', '8219280011', NULL, 'GHARUAN', NULL, 'KHARAR', 'Punjab', 'India', 'ANSHULMEHTA12@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '1', 'REGULAR', '45 DAYS', '2019-05-16', '2019-06-30', '6:45 PM', 5000, 1000, '4547', NULL, 'gpay', 0, 'google', 'COUNSELOR', '9', NULL, 'JOINED', '2019-05-15 18:35:31', 6, '2019-05-16 13:54:57', 6, '2019-05-15 13:05:31', NULL),
(234, 'faee42a6-0f0b-4a71-b77c-c1f234cda67e', 1202, 'RISHABH', 'SHARMA', 'male', 'RAVINDER SHARMA', '2000-08-06', '8219435262', NULL, 'GHARUAN', NULL, 'KHARAR', 'Punjab', 'India', 'SRISHABH3298@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '1', 'REGULAR', '45 DAYS', '2019-05-16', '2019-06-30', '6:45 PM', 5000, 1000, '4548', NULL, 'gpay', 0, 'google', 'COUNSELOR', '9', NULL, 'JOINED', '2019-05-15 18:39:21', 6, '2019-05-16 13:55:12', 6, '2019-05-15 13:09:21', NULL),
(235, 'df55f3ff-5089-48cd-baff-16913118b671', 1203, 'MUSKAN', 'GUPTA', 'female', 'VIJAY KUMAR', '1999-03-07', '9915342114', NULL, 'GHARUAN', NULL, 'KHARAR', 'Punjab', 'India', 'MG142516@GMAIL.COM', 'CU', 'Degree', 'ECE', '4', '1', 'MICROSOFT CERTIFICATION', '45 DAYS', '2019-05-20', '2019-07-05', '1:45 PM', 5000, 2000, '4550', NULL, 'cash', 0, 'google', 'COUNSELOR', '7', NULL, 'PENDING', '2019-05-16 13:46:09', 6, '2019-05-16 13:46:09', NULL, '2019-05-16 08:16:09', NULL),
(236, '5d0ba6dc-b3b0-4d8c-9528-d600e0ed598f', 1204, 'MANSI', 'CHAUHAN', 'female', 'H.K.S. CHAUHAN', '2019-04-25', '7901757888', NULL, 'GHARUAN', NULL, 'KHARAR', 'Punjab', 'India', 'MANSICHAUHAN392@GMAIL.COM', 'CU', 'Degree', 'ECE', '4', '1', 'MICROSOFT CERTIFICATION, CERTIFICATE', '45 DAYS', '2019-05-20', '2019-07-05', '2:00 PM', 5000, 2000, '4551', NULL, 'cash', 0, 'google', 'COUNSELOR', '7', NULL, 'PENDING', '2019-05-16 13:50:02', 6, '2019-05-16 13:50:02', NULL, '2019-05-16 08:20:02', NULL),
(237, 'd8c340b9-ea96-429b-b719-6f67d4e89e52', 1205, 'MOHIT', 'BHARDWAJ', 'male', 'SRESHTA RANI', '1990-04-07', '9478800004', NULL, 'SIRHIND', NULL, 'FATEHGARH SAHIB', 'Punjab', 'India', 'MOHIT78800887@GMAIL.COM', 'CIET', 'Degree', 'ECE', 'PASSOUT', '1', 'REGULAR', '45 DAYS', '2019-05-20', '2019-07-30', '2:00 PM', 9000, 9000, '4451', NULL, 'online', 0, 'google', 'COUNSELOR', '9', NULL, 'JOINED', '2019-05-16 13:54:29', 6, '2019-05-20 18:54:29', 6, '2019-05-16 08:24:29', NULL);
INSERT INTO `app_admission` (`id`, `admission_uid`, `aid`, `first_name`, `last_name`, `gender`, `father_name`, `dob`, `mobile`, `alt_mobile`, `address`, `pg_address`, `city`, `state`, `country`, `email`, `college_name`, `college_deg_type`, `college_stream`, `college_semester`, `course_name`, `other_course`, `course_durartion`, `course_joindate`, `course_enddate`, `course_time`, `course_fee`, `course_advance`, `payment_reciept`, `payment_type`, `payment_mode`, `batch_assigned`, `refer_by`, `refer_name`, `admission_by`, `remarks`, `status`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`) VALUES
(238, '7eb2be38-1049-4db0-b6bd-f5b3a9a790e1', 1206, 'MAYANK', 'CHOTANI', 'male', 'VED CHOTANI', '1999-01-06', '9811443646', NULL, 'GHARUAN', NULL, 'KHARAR', 'Punjab', 'India', 'MAYANKCHUTANI01@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '1', 'MICROSOFT CERTIFICATION, CERTIFICATE', '45 DAYS', '2019-06-03', '2019-07-18', '4:45 PM', 4000, 1000, '4452', NULL, 'cash', 0, 'google', 'COUNSELOR', '14', NULL, 'PENDING', '2019-05-16 16:44:53', 6, '2019-05-16 16:44:53', NULL, '2019-05-16 11:14:53', NULL),
(239, '143a9b24-6eda-49eb-9f30-49477b0a16e3', 1207, 'ABHISHEK', NULL, 'male', 'SANDEEP KUMAR', '1999-02-23', '7889151289', NULL, 'GHARUAN', NULL, 'KHARAR', 'Punjab', 'India', 'ABHI788915@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '1', 'MICROSOFT CERTIFICATION', '45 DAYS', '2019-06-03', '2019-07-18', '4:45 PM', 4000, 2000, '4453', NULL, 'cash', 0, 'google', 'COUNSELOR', '9', NULL, 'PENDING', '2019-05-16 16:47:52', 6, '2019-05-16 16:47:52', NULL, '2019-05-16 11:17:52', NULL),
(240, 'd8dbc814-4d89-48f9-8f45-0ceb6d086b34', 1208, 'GOPAL KUMAR', 'SHAH', 'male', 'LACHMAN SHAH', '2001-01-01', '8306833304', NULL, 'MEWAR UNIVERSITY', NULL, 'RAJASTHAN', 'Rajasthan', 'India', 'GOPALKUMARSHAH@GMAIL.COM', 'MEWAR UNIVERSITY', 'Diploma', 'CSE', '4', '16', 'REGULAR', '45 DAYS', '2019-06-03', '2019-07-18', '6:30 PM', 3500, 1500, '4454', NULL, 'online', 0, 'google', 'COUNSELOR', '7', NULL, 'PENDING', '2019-05-16 18:27:02', 6, '2019-05-16 18:27:02', NULL, '2019-05-16 12:57:02', NULL),
(241, '1f0fdb05-4726-4023-9934-bee1ce94fb01', 1209, 'DEEPAK KUMAR', 'SHAH', 'male', 'RAM PARKASH', '2001-01-01', '8306967036', NULL, 'MEWAR', NULL, 'MEWAR', 'Rajasthan', 'India', 'DEEPAKKUMARSHAH@GMAIL.COM', 'MEWAR UNIVERSITY', 'Diploma', 'CSE', '4', '16', 'REGULAR', '45 DAYS', '2019-06-03', '2019-07-18', '6:30 PM', 3500, 1500, '4455', NULL, 'online', 0, 'google', 'COUNSELOR', '7', NULL, 'PENDING', '2019-05-16 18:29:57', 6, '2019-05-16 18:29:57', NULL, '2019-05-16 12:59:57', NULL),
(242, '6ddc0da1-d73d-434c-b99f-702124061e16', 1210, 'VINAYAK', 'ATTRI', 'male', 'SURESH ATTRI', '2000-03-24', '7988625104', NULL, 'BALDEV NAGAR', NULL, 'AMBALA', 'Haryana', 'India', 'VINAYAKATTRI04@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '1', 'MICROSOFT CERTIFICATION', '45 DAYS', '2019-06-03', '2019-07-18', '12:00 PM', 4000, 1000, '4555', NULL, 'cash', 0, 'google', 'COUNSELOR', '9', NULL, 'PENDING', '2019-05-17 11:52:03', 6, '2019-05-17 11:52:03', NULL, '2019-05-17 06:22:03', NULL),
(243, '5cb0167e-c163-4756-a22f-e2f06d3ba5c9', 1211, 'JAHANGEER', 'RAJAB', 'male', 'RAJAB LONE', '1997-01-03', '7006385526', NULL, 'MEWAR', NULL, 'MEWAR', 'Rajasthan', 'India', 'JAHANGEERRAJAB@GMAIL.COM', 'MEWAR UNIVERSITY', 'Degree', 'BCA', '4', '16', 'REGULAR', '45 DAYS', '2019-06-03', '2019-07-18', '12:00 PM', 3500, 3500, '4458', NULL, 'online', 0, 'google', 'COUNSELOR', '7', NULL, 'PENDING', '2019-05-17 12:00:40', 6, '2019-05-17 12:00:40', NULL, '2019-05-17 06:30:40', NULL),
(244, '75cbab82-2438-4dd7-a68f-42dcd164a613', 1212, 'ABSAR', 'AHMAD', 'male', 'ABRAR AHMAD', '1999-01-01', '8209786636', NULL, 'MEWAR', NULL, 'MEWAR', 'Punjab', 'India', 'ABSARAHMAD@GMAIL.COM', 'MEWAR UNIVERSITY', 'Diploma', 'CSE', '4', '16', 'REGULAR', '45 DAYS', '2019-06-03', '2019-07-18', '6:15 PM', 3500, 1500, '4558', NULL, 'online', 0, 'google', 'COUNSELOR', '7', NULL, 'PENDING', '2019-05-17 18:07:13', 6, '2019-05-17 18:07:13', NULL, '2019-05-17 12:37:13', NULL),
(245, '59031e39-2753-422c-bc98-e83c7c146d93', 1213, 'RUBINA', 'NILGER', 'male', 'MD. ISRAR NILGER', '1998-11-11', '9079532147', NULL, 'MEWAR', NULL, 'MEWAR UNIVERSITY', 'Rajasthan', 'India', 'RUBINANILGER@GMAIL.COM', 'MEWAR UNIVERSITY', 'Master', 'MCA', '4', '16', 'REGULAR', '45 DAYS', '2019-06-03', '2019-07-18', '6:15 PM', 3500, 1500, '4557', NULL, 'online', 0, 'google', 'COUNSELOR', '7', NULL, 'PENDING', '2019-05-17 18:11:42', 6, '2019-05-17 18:11:42', NULL, '2019-05-17 12:41:42', NULL),
(246, '287e33af-10d5-4d83-8a17-24ea46644bd2', 1214, 'SAJAD AHMAD', 'SHEIKH', 'male', 'FAYAZ AHMAD SHEIKH', '1995-01-02', '7006379575', NULL, 'MEWAR', NULL, 'MEWAR', 'Rajasthan', 'India', 'SAJADAHMADSHEIKH@GMAIL.COM', 'MEWAR UNIVERSITY', 'Degree', 'CSE', '4', '1', 'REGULAR', '45 DAYS', '2019-06-03', '2019-07-18', '6:15 PM', 3500, 1500, '4559', NULL, 'online', 0, 'google', 'COUNSELOR', '7', NULL, 'PENDING', '2019-05-17 18:16:45', 6, '2019-05-17 18:16:45', NULL, '2019-05-17 12:46:45', NULL),
(247, 'c25caa4f-6b5b-4665-8309-945384644ec6', 1215, 'NISHANT', NULL, 'male', 'IQBAL SINGH', '1999-10-25', '9882224071', NULL, 'UNA', NULL, 'UNA', 'Himachal Pradesh', 'India', 'NISHANTJASWAL9999@GMAIL.COM', 'G.C. DAULATPUR', 'Degree', 'BBA', '4', '3', 'REGULAR', '45 DAYS', '2019-05-21', '2019-07-05', '6:30 PM', 5000, 2000, '4462', NULL, 'cash', 0, 'google', 'COUNSELOR', '7', NULL, 'PENDING', '2019-05-17 18:21:07', 6, '2019-05-17 18:21:07', NULL, '2019-05-17 12:51:07', NULL),
(248, '567c6f70-3fde-4bcc-aa45-f2a53dd8a0fc', 1216, 'MOKSHAD', 'UPRETI', 'male', 'L.N. UPRETI', '1998-02-12', '9711502018', NULL, 'DELHI', NULL, 'DELHI', 'Delhi', 'India', 'MOKSHAD28316@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '1', 'CERTIFICATE', '45 DAYS', '2019-06-03', '2019-07-18', '11:30 AM', 4000, 2000, '4463', NULL, 'paytm', 0, 'google', 'COUNSELOR', '7', NULL, 'PENDING', '2019-05-18 11:32:59', 6, '2019-05-18 11:32:59', NULL, '2019-05-18 06:02:59', NULL),
(249, 'd5e66fb8-c5d7-4966-9e1e-544fd0990be0', 1217, 'HARJOT', 'SINGH', 'male', 'BHUPINDER SINGH', '1994-10-18', '7087592024', NULL, 'AMLOH', NULL, 'MANDI GOBINDGARH', 'Punjab', 'India', 'GURDIT640@GMAIL.COM', 'DBU', 'Diploma', 'EE', '6', '10', 'SCADA CERTIFICATE', '60 DAYS', '2019-06-03', '2019-07-31', '1:00 PM', 3000, 1000, '4562', NULL, 'cash', 0, 'google', 'COUNSELOR', '9', NULL, 'PENDING', '2019-05-18 12:56:27', 6, '2019-05-18 12:56:27', NULL, '2019-05-18 07:26:27', NULL),
(250, 'ffd66c9a-f927-497c-aaf2-0f60866a4184', 1218, 'MD. ZAIN BIN FAYAZ', 'SAYEED RAFIQI', 'male', 'FAYAZ AHMAD SAYEED RAFIQI', '1999-10-11', '7347391284', NULL, 'JHANJERI HOSTEL', NULL, 'MOHALI', 'Punjab', 'India', '1571ZAIN@GMAIL.COM', 'CGC JHANJERI', 'Degree', 'CSE', '4', '20', 'REGULAR', '45 DAYS', '2019-06-03', '2019-07-18', '1:30 PM', 4000, 2000, '4464', NULL, 'paytm', 0, 'google', 'COUNSELOR', '7', NULL, 'PENDING', '2019-05-18 13:31:48', 6, '2019-05-18 13:31:48', NULL, '2019-05-18 08:01:48', NULL),
(251, 'ec42a0c0-c8ca-4988-b014-e89c6fe67142', 1219, 'PUSHPALATA', 'KUMARI', 'female', 'RAJENDRA PRASAD MANDAL', '1997-08-08', '9056688186', NULL, 'BHAGALPUR', NULL, 'BIHAR', 'Bihar', 'India', 'PUSHPALATA0940@GMAIL.COM', 'ARYANS COLLEGE', 'Degree', 'ECE', '8', '1', 'REGULAR', '45 DAYS', '2019-06-17', '2019-08-05', '4:00 PM', 4000, 3000, '4466', NULL, 'cash', 0, 'google', 'COUNSELOR', '7', NULL, 'PENDING', '2019-05-18 15:57:36', 6, '2019-05-18 15:57:36', NULL, '2019-05-18 10:27:36', NULL),
(252, 'c90a511d-caa8-4663-9d79-32716b11a508', 1220, 'MANAS', 'BHADRA', 'male', 'CHAITANYA BHADRA', '1998-01-13', '8876545578', NULL, 'SONITPUR', NULL, 'ASSAM', 'Assam', 'India', 'MANASBHADRA@GMAIL.COM', 'MEWAR UNIVERSITY', 'Degree', 'CSE', '4', '16', 'REGULAR', '45 DAYS', '2019-06-03', '2019-07-18', '4:00 PM', 3500, 1500, '4467', NULL, 'online', 0, 'google', 'COUNSELOR', '7', NULL, 'PENDING', '2019-05-18 16:02:40', 6, '2019-05-18 16:02:40', NULL, '2019-05-18 10:32:40', NULL),
(253, '78db194d-b2af-43de-9a02-026f5909dfa7', 1221, 'SHIKHA', 'KATOCH', 'female', 'RAJINDER SINGH', '1998-06-23', '9814117808', NULL, 'SECTOR 70', NULL, 'MOHALI', 'Punjab', 'India', 'SHIKHAKATOCH98@GMAIL.COM', 'MODERN GROUP OF COLLEGES', 'Degree', 'B.COM', 'PASSOUT', '3', 'REGULAR', '45 DAYS', '2019-06-03', '2019-07-18', '11:15 AM', 4500, 1500, '4468', NULL, 'cash', 0, 'google', 'WALKIN', '13', NULL, 'PENDING', '2019-05-20 11:11:02', 6, '2019-05-20 11:11:02', NULL, '2019-05-20 05:41:02', NULL),
(254, 'f1b214cc-e912-4759-88a3-0bf8e5c8ce31', 1222, 'SALONI', 'PAREEK', 'female', 'SHIV SHANKAR PAREEK', '1999-03-01', '7742377123', NULL, 'MEWAR', NULL, 'MEWAR', 'Rajasthan', 'India', 'SALONIPAREEK@GMAIL.COM', 'MEWAR UNIVERSITY', 'Master', 'MCA', '4', '16', 'REGULAR', '45 DAYS', '2019-06-03', '2019-07-18', '6:30 PM', 3500, 1500, '4563', NULL, 'online', 0, 'google', 'COUNSELOR', '7', NULL, 'PENDING', '2019-05-20 18:31:17', 6, '2019-05-20 18:31:17', NULL, '2019-05-20 13:01:17', NULL),
(255, '08b4bc35-210b-483b-92aa-b525c4da18d0', 1223, 'SHAGUFTA', 'KHAN', 'male', 'SALIM KHAN', '1998-01-01', '8619162725', NULL, 'MEWAR', NULL, 'MEWAR', 'Rajasthan', 'India', 'SHAGUFTAKHAN@GMAIL.COM', 'MEWAR UNIVERSITY', 'Master', 'MCA', '4', '16', 'REGULAR', '45 DAYS', '2019-06-03', '2019-07-18', '6:45 PM', 3500, 1500, '4564', NULL, 'online', 0, 'google', 'COUNSELOR', '7', NULL, 'PENDING', '2019-05-20 18:40:58', 6, '2019-05-20 18:40:58', NULL, '2019-05-20 13:10:58', NULL),
(256, '966873ed-10f2-434e-a5fb-06743eb958a8', 1224, 'SHALU', 'SONI', 'female', 'ARJUN LAL SONI', '1996-07-14', '9413048185', NULL, 'MEWAR', NULL, 'MEWAR', 'Rajasthan', 'India', 'SHALUSONI2GMAIL.COM', 'MEWAR UNIVERSITY', 'Master', 'MCA', '4', '16', 'REGULAR', '45 DAYS', '2019-06-03', '2019-07-18', '6:45 PM', 3500, 1500, '4565', NULL, 'online', 0, 'google', 'COUNSELOR', '7', NULL, 'PENDING', '2019-05-20 18:43:20', 6, '2019-05-20 18:43:20', NULL, '2019-05-20 13:13:20', NULL),
(257, '8fb18d0a-d7c5-4017-a748-34ca9c057a61', 1225, 'ANOUSHKA', 'TIWARI', 'female', 'SK TIWARI', '1996-03-21', '9988454136', NULL, 'LANDRAN', NULL, 'MOHALI', 'Punjab', 'India', 'ANUSHKATIWARI87@GMAIL.COM', 'CGC LANDRAN', 'Degree', 'ECE', '8', '10', 'CERTIFICATE', '6 MONTHS', '2019-05-20', '2019-06-30', '6:45 PM', 4500, 4500, '4471', NULL, 'online', 0, 'google', 'COUNSELOR', '9', 'CERTIFICATE ONLY', 'JOINED', '2019-05-20 18:46:11', 6, '2019-05-20 18:51:41', 6, '2019-05-20 13:16:11', NULL),
(258, '43e3d32b-a9ff-45c0-90c9-96e096d1b936', 1226, 'BABITA', 'DEVI', 'female', 'UTTAM CHAND', '1999-01-15', '6005706363', NULL, 'JAMMU', NULL, 'JAMMU', 'Jammu and Kashmir', 'India', 'BABITADEVI@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '2', 'REGULAR', '45 DAYS', '2019-05-21', '2019-07-06', '7:00 PM', 6000, 2000, '4476', NULL, 'cash', 0, 'just-dial', 'MS. HARJINDER KAUR', '13', NULL, 'PENDING', '2019-05-20 18:48:43', 6, '2019-05-20 18:48:43', NULL, '2019-05-20 13:18:43', NULL),
(259, '4c3d5ae8-6514-4c52-a91b-a47aed42d3be', 1227, 'AMAN', 'JASWAL', 'female', 'NIKKU RAM', '1999-05-20', '8728982244', NULL, 'GHARUAN', NULL, 'KHARAR', 'Punjab', 'India', 'AMAN42JASWAL@GMAIL.COM', 'CU', 'Degree', 'IT', '4', '2', 'CERTIFICATE', '45 DAYS', '2019-06-03', '2019-07-18', '7:00 PM', 3500, 1000, '4477', NULL, 'paytm', 0, 'google', 'COUNSELOR', '9', NULL, 'PENDING', '2019-05-20 18:51:17', 6, '2019-05-20 18:51:17', NULL, '2019-05-20 13:21:17', NULL),
(260, 'a7802096-5d65-4d07-9c32-ccaa1d3337b7', 1228, 'YOGESH', 'KUMAR', 'male', 'CHATTER SINGH', '1991-03-20', '8950221223', NULL, 'SECTOR 71', NULL, 'MOHALI', 'Punjab', 'India', 'YKSHARMA972@GMAIL.COM', 'WORKING', 'Degree', 'CSE', 'PASSOUT', '1', 'CCNP REGULAR', '120 DAYS', '2019-05-20', '2019-09-30', '7:15 PM', 18000, 5000, '4566', NULL, 'online', 0, 'google', 'COUNSELOR', '9', NULL, 'JOINED', '2019-05-20 19:09:38', 6, '2019-05-20 19:09:53', 6, '2019-05-20 13:39:38', NULL),
(261, '61a41303-873c-4c20-89a5-8a41fa290683', 1229, 'SHAKIB YOUSUF', 'MIR', 'male', 'YOUSUF MIR', '1999-01-01', '9001332539', NULL, 'MEWAR', NULL, 'MEWAR', 'Rajasthan', 'India', 'YOUSUFMIR@GMAIL.COM', 'MEWAR UNIVERSITY', 'Degree', 'cse', '4', '16', 'REGULAR', '45 DAYS', '2019-06-03', '2019-07-18', '12:15 PM', 3500, 1500, '4567', NULL, 'online', 0, 'google', 'COUNSELOR', '7', NULL, 'PENDING', '2019-05-21 12:08:07', 6, '2019-05-21 15:09:47', 6, '2019-05-21 06:38:07', NULL),
(262, '51351ba9-dcfb-4981-853a-5679e75f08b1', 1230, 'manpreet', NULL, 'female', 'sh. hardip singh', '1999-10-19', '7347568352', NULL, 'batala', NULL, 'batala', 'Punjab', 'India', 'anjelmehra68@gmail.com', 'sukhjindra  group of institute', 'Degree', 'b.com', '5', '3', 'regular', '45 days', '2019-06-03', '2019-07-18', '1:30 PM', 5000, 2000, '4481', NULL, 'cash', 0, 'google', 'counselor', '9', NULL, 'PENDING', '2019-05-21 13:28:58', 6, '2019-05-21 13:28:58', NULL, '2019-05-21 07:58:58', NULL),
(263, '10f9fa06-f608-49f7-b4bd-ec0cbe818b66', 1231, 'renu', 'verma', 'female', 'jagpal verma', '1997-12-28', '7497835099', NULL, 'vill. sounta disst , ambala', NULL, 'ambala', 'Haryana', 'India', 'renuverma.1997@gmail.com', 'galaxy  global group of institute dinapur', 'Master', 'm.ba', '3', '3', 'regular', '45 days', '2019-06-03', '2019-07-18', '1:30 PM', 3500, 1000, '4568', NULL, 'cash', 0, 'google', 'counselor', '7', NULL, 'PENDING', '2019-05-21 13:36:49', 6, '2019-05-21 13:36:49', NULL, '2019-05-21 08:06:49', NULL),
(264, '5e950a70-89c6-47ff-baa9-7865e59036a9', 1232, 'harvinder', 'sekhon', 'male', 'salinder singh', '1997-07-06', '8607717034', NULL, 'kurukshetra', NULL, 'KURUKSHETRA', 'Haryana', 'India', 'sekhonharwinder42@gmail.com', 'galaxy group of institution', 'Master', 'm.ba', '2', '3', 'regular', '45 days', '2019-06-03', '2019-07-18', '1:45 PM', 3500, 1000, '4569', NULL, 'cash', 0, 'google', 'counselor', '7', NULL, 'PENDING', '2019-05-21 13:45:13', 6, '2019-05-21 13:45:13', NULL, '2019-05-21 08:15:13', NULL),
(265, 'b68fcdd0-432c-4a9f-aea2-4869d92b512d', 1233, 'NIKITA', 'PATHAK', 'female', 'brij bihari lal', '1994-09-26', '7891099858', NULL, 'mewar', NULL, 'mewar', 'Rajasthan', 'India', 'nikitapathak@gmail.com', 'mewar university', 'Master', 'mca', '4', '16', 'regular', '45 days', '2019-06-03', '2019-07-18', '4:45 PM', 3500, 1500, '4573', NULL, 'online', 0, 'google', 'counselor', '7', NULL, 'PENDING', '2019-05-21 16:57:22', 6, '2019-05-21 16:57:22', NULL, '2019-05-21 11:27:22', NULL),
(266, '02670fc0-8b04-4a1d-b651-bea3c1b8e350', 1234, 'jashan sahib', 'singh', 'male', 'khusdeep kaur', '1998-09-09', '7973742357', NULL, 'mohali', NULL, 'mohali', 'Punjab', 'India', 'jashan1289@gmail.com', 'bbsbec', 'Degree', 'cse', '6', '1', 'regular', '45 days', '2019-06-03', '2019-08-18', '10:45 AM', 6000, 1000, '4576', NULL, 'cash', 0, 'google', 'counselor', '14', NULL, 'PENDING', '2019-05-22 11:01:16', 6, '2019-05-22 11:01:16', NULL, '2019-05-22 05:31:16', NULL),
(267, '137f385c-be26-46f7-b16b-8d21e74bf527', 1235, 'FARUKH', 'KHAN', 'male', 'MASOOM KHAN', '1998-05-10', '8851425343', NULL, 'CHITTORGARH', NULL, 'CHITTORGARH', 'Rajasthan', 'India', 'FARUKH23@GMAIL.COM', 'MEWARUNI', 'Degree', 'CSE', '4', '16', 'REGULAR', '45 DAYS', '2019-06-03', '2019-07-18', '12:30 PM', 3500, 1500, '4577', NULL, 'online', 0, 'google', 'COUNSELOR', '7', NULL, 'PENDING', '2019-05-22 12:35:33', 6, '2019-05-22 12:35:33', NULL, '2019-05-22 07:05:33', NULL),
(268, '826b57e2-a7e2-40ac-98f1-4b0add00a83c', 1236, 'RITIKA', 'PARMAR', 'female', 'BIKRAM SINGH PARMAR', '1998-03-12', '7982584333', NULL, 'GHARUAN', NULL, 'KHARAR', 'Punjab', 'India', 'RITIKATARI98@GMAIL.COM', 'CU', 'Degree', 'CSE', '4', '2', 'MICROSOFT CERTIFICATION', '45 DAYS', '2019-06-03', '2019-07-18', '6:45 PM', 5000, 1000, '4579', NULL, 'paytm', 0, 'google', 'COUNSELOR', '9', NULL, 'PENDING', '2019-05-22 18:37:29', 6, '2019-05-22 18:37:29', NULL, '2019-05-22 13:07:29', NULL),
(269, 'f7157c62-1a8d-416d-aed9-cfb50be934a6', 1237, 'DEBASISH', 'SARKAR', 'male', 'DILIP SARKAR', '1999-03-18', '9366311917', NULL, 'GOBINDGARH', NULL, 'MANDI GOBINDGARH', 'Punjab', 'India', 'SARKARDEBA321@GMAIL.COM', 'DBU', 'Degree', 'CSE', '4', '2', 'REGULAR', '45 DAYS', '2019-06-03', '2019-07-18', '6:45 PM', 5000, 1000, '4580', NULL, 'gpay', 0, 'google', 'COUNSELOR', '9', NULL, 'PENDING', '2019-05-22 18:41:35', 6, '2019-05-22 18:41:35', NULL, '2019-05-22 13:11:35', NULL),
(270, '4f65a30f-8eb3-4682-aef0-cae95a462bc4', 1238, 'SACHIN', 'SHARMA', 'male', 'SANDEEP KUMAR', '1998-12-28', '7018629155', NULL, 'JHANJERI HOSTEL', NULL, 'MOHALI', 'Punjab', 'India', 'SACHINSHARMA98@GMAIL.COM', 'CGCTC', 'Degree', 'CSE', '4', '1', 'REGULAR', '45 DAYS', '2019-06-03', '2019-07-18', '6:45 PM', 5500, 2000, '4489', NULL, 'paytm', 0, 'google', 'COUNSELOR', '7', NULL, 'PENDING', '2019-05-22 18:45:59', 6, '2019-05-22 18:45:59', NULL, '2019-05-22 13:15:59', NULL),
(271, '9d08be08-a04f-42b0-956f-6f4d47aee513', 1239, 'RAJVIR', 'KAUR', 'female', 'jaskirat singh', '1996-01-01', '8360961854', NULL, 'mohali', NULL, 'mohali', 'Punjab', 'India', 'rajvirheer22@gmail.com', 'immanent solutions', 'Master', 'mca', 'passout', '24', 'REGULAR', '75 DAYS', '2019-05-23', '2019-08-10', '10:15 AM', 14000, 5000, '4491', NULL, 'cash', 0, 'advertisement', 'WALKIN', '13', NULL, 'JOINED', '2019-05-23 10:11:24', 6, '2019-05-23 10:12:59', 6, '2019-05-23 04:41:24', NULL),
(272, '8475a538-95a9-4099-96f5-3023fcaee9b7', 1240, 'AMANPREET', 'SINGH', 'male', 'RAGHABIR SINGH', '1998-07-12', '8146842542', NULL, 'DORAHA', NULL, 'DORAHA', 'Punjab', 'India', 'SONIAMANPREET9@GMAIL.COM', 'DBU', 'Degree', 'CSE', '4', '2', 'REGULAR', '45 DAYS', '2019-06-03', '2019-07-18', '10:15 AM', 5000, 1000, '4581', NULL, 'online', 0, 'google', 'COUNSELOR', '9', NULL, 'PENDING', '2019-05-23 10:21:20', 6, '2019-05-23 10:21:20', NULL, '2019-05-23 04:51:20', NULL),
(273, '988d7f85-ad8e-40f4-8f9c-13e5b71a5a1f', 1241, 'RAHUL', 'CHOUDHARY', 'male', 'SHYAMAL  CHOUDHARY', '1998-10-24', '8787883503', NULL, 'TRIPURA', NULL, 'TRIPURA', 'Kerala', 'India', 'RAHULCHOUDHARY716@GMAIL.COM', 'DBU', 'Degree', 'CSE', '4', '2', 'REGULAR', '45 DAYS', '2019-06-03', '2019-07-18', '10:30 AM', 5000, 1000, '4582', NULL, 'online', 0, 'google', 'COUNSELOR', '14', NULL, 'PENDING', '2019-05-23 10:26:58', 6, '2019-05-23 10:26:58', NULL, '2019-05-23 04:56:58', NULL),
(274, 'df47e652-5e74-4172-9256-3d82891c5f4b', 1242, 'SARFARAJ', 'ANSARI', 'male', 'MOHAMAND  JAFAR ANSARI', '1998-09-08', '9815065647', NULL, 'LUDHIANA', NULL, 'LUDHIANA', 'Punjab', 'India', 'SARFARANSARI0988@GMAIL.COM', 'DBU', 'Degree', 'CSE', '4', '2', 'REGULAR', '45 DAYS', '2019-06-03', '2019-07-18', '10:30 AM', 5000, 1000, '4584', NULL, 'online', 0, 'google', 'COUNSELOR', '9', NULL, 'PENDING', '2019-05-23 10:32:32', 6, '2019-05-23 10:32:32', NULL, '2019-05-23 05:02:32', NULL),
(275, '77d176fa-10b8-4d12-80c8-41dc296a6569', 1243, 'KISHOR', 'DAS', 'male', 'JAYANT DAS', '1998-11-07', '9366246879', NULL, 'TRIPURA', NULL, 'TRIPURA', 'Kerala', 'India', 'IAMKISHOR1308@GMAIL.COM', 'DBU', 'Degree', 'CSE', '4', '2', 'REGULAR', '45 DAYS', '2019-06-03', '2019-07-18', '10:45 AM', 5000, 1000, '4583', NULL, 'online', 0, 'google', 'COUNSELOR', '9', NULL, 'PENDING', '2019-05-23 10:37:51', 6, '2019-05-23 10:37:51', NULL, '2019-05-23 05:07:51', NULL),
(276, '72f2b16c-d237-4487-95ab-f30874c74cdd', 1244, 'KOMALPREET', 'SINGH', 'male', 'NAVPREET SINGH', '1993-11-21', '9592100039', NULL, 'MOHALI', NULL, 'MOHALI', 'Punjab', 'India', 'KPS21111993@GMAIL.COM', 'MANAV BHARTI', 'Degree', 'IT', 'PASS OUT', '1', 'REGULAR', '3 MONTHS', '2019-05-23', '2019-08-22', '10:45 AM', 8000, 3500, '4490', NULL, 'cash', 0, 'google', 'MR RAJEEV KUMAR', '13', NULL, 'JOINED', '2019-05-23 10:44:20', 6, '2019-05-23 11:02:44', 6, '2019-05-23 05:14:20', NULL),
(277, 'a3713adc-2773-4b14-b2c7-4e47ae0eef7e', 1245, 'KAPIL', 'SINGH', 'male', 'VIJAY SINGH', '1999-01-01', '7865287146', NULL, 'RAJASTHAN', NULL, 'PILANI', 'Rajasthan', 'India', 'KAPIL123@GMAIL.COM', 'BKBIET PILANI', 'Degree', 'IT', '4', '1', 'REGULAR', '45 DAYS', '2019-06-03', '2019-07-18', '1:00 PM', 4500, 2000, '4585', NULL, 'gpay', 0, 'google', 'COUNSELOR', '7', NULL, 'PENDING', '2019-05-23 13:06:32', 6, '2019-05-23 13:06:32', NULL, '2019-05-23 07:36:32', NULL),
(278, '72e3c02e-0795-490e-8388-05cbbd9bc614', 1246, 'JAIMANDEEP', 'SINGH', 'male', 'PRJITPAL SINGH', '1997-05-02', '9779591461', NULL, 'KHARAR', NULL, 'KHARAR', 'Punjab', 'India', 'JAIMANDEEPSINGH4CO@GMAIL.COM', 'CU', 'Degree', 'CSE', '7', '4', 'REGULAR', '2 MONTH', '2019-05-27', '2019-07-30', '1:30 PM', 9000, 2000, '4586', NULL, 'cash', 0, 'google', 'OLD STUDENT', '13', NULL, 'PENDING', '2019-05-23 13:27:15', 6, '2019-05-23 13:27:15', NULL, '2019-05-23 07:57:15', NULL),
(279, '0b73c61f-33cd-46fd-9028-c6da4955136c', 1247, 'JAIMANDEEP', 'SINGH', 'male', 'PRJITPAL SINGH', '1997-05-02', '9779591476', NULL, 'KHARAR', NULL, 'KHARAR', 'Punjab', 'India', 'JAIMANDEEPSINGH4@GMAIL.COM', 'CU', 'Degree', 'CSE', '7', '4', 'REGULAR', '2 MONTH', '2019-05-27', '2019-07-30', '1:30 PM', 9000, 2000, '4586', NULL, 'cash', 0, 'google', 'OLD STUDENT', '13', NULL, 'PENDING', '2019-05-23 13:29:41', 6, '2019-05-23 13:29:41', NULL, '2019-05-23 07:59:41', NULL),
(280, 'd090ee1f-53dc-430a-8fb0-b4bcbc9f0b89', 1248, 'VARINDER PAUL', 'SINGH', 'male', 'TIRATH SINGH', '1997-06-25', '9915542591', NULL, 'KHARAR', NULL, 'KHARAR', 'Punjab', 'India', 'VARINDERPAUL1@GMAIL.COM', 'CU', 'Degree', 'COMPUTER SCIENCE', '7', '4', 'REGULAR', '2 MONTH', '2019-05-27', '2019-07-30', '1:30 PM', 9000, 2000, '4587', NULL, 'cash', 0, 'google', 'OLD STUDENT', '13', NULL, 'PENDING', '2019-05-23 13:34:46', 6, '2019-05-23 13:34:46', NULL, '2019-05-23 08:04:46', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `app_batch`
--

CREATE TABLE `app_batch` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `admission_uid` varchar(255) NOT NULL,
  `trainer_id` int(11) NOT NULL,
  `batch_date` date NOT NULL,
  `batch_time` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `app_course`
--

CREATE TABLE `app_course` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app_course`
--

INSERT INTO `app_course` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'CCNA', 1, '2019-03-30 07:51:05', '2019-03-30 07:51:05'),
(2, 'PYTHON', 1, '2019-03-30 07:51:43', '2019-03-30 07:51:43'),
(3, 'finance', 1, '2019-03-30 07:51:43', '2019-03-30 07:51:43'),
(4, 'CCNP', 1, '2019-04-01 08:06:54', '2019-04-01 08:06:54'),
(5, 'AUTOCAD-ME', 1, '2019-04-30 16:46:26', '2019-04-30 16:46:26'),
(6, 'AUTOCAD-ME', 2, '2019-04-30 16:47:15', '2019-04-30 16:47:33'),
(7, 'SOLIDWORKS', 1, '2019-04-30 17:38:20', '2019-04-30 17:38:20'),
(8, 'AUTOCAD-CE', 1, '2019-04-30 17:38:36', '2019-04-30 17:38:36'),
(9, 'EMBEDDED SYSTEM', 1, '2019-05-03 16:39:30', '2019-05-03 16:39:30'),
(10, 'AUTOMATION', 1, '2019-05-03 16:39:46', '2019-05-03 16:39:46'),
(11, 'PYTHON WITH IOT', 1, '2019-05-03 16:40:02', '2019-05-03 16:40:02'),
(12, 'PYTHON WITH DATA SCI.', 1, '2019-05-03 16:40:20', '2019-05-03 16:40:20'),
(13, 'MACHINE LEARNING', 1, '2019-05-03 16:40:36', '2019-05-03 16:40:36'),
(14, 'JAVA', 1, '2019-05-04 10:44:18', '2019-05-04 10:44:18'),
(15, 'ANDROID', 1, '2019-05-04 10:44:30', '2019-05-04 10:44:30'),
(16, 'PHP', 1, '2019-05-04 10:44:50', '2019-05-04 10:44:50'),
(17, 'MARKETING', 1, '2019-05-04 11:16:34', '2019-05-04 11:16:34'),
(18, 'ANGULAR JS', 1, '2019-05-06 09:58:56', '2019-05-06 09:58:56'),
(19, 'C', 1, '2019-05-06 13:18:31', '2019-05-06 13:18:31'),
(20, 'C++', 1, '2019-05-06 13:18:43', '2019-05-06 13:18:43'),
(21, 'IOT', 1, '2019-05-08 13:34:16', '2019-05-08 13:34:16'),
(22, 'web designing', 1, '2019-05-09 13:26:22', '2019-05-09 13:26:22'),
(23, 'web development', 1, '2019-05-09 13:26:43', '2019-05-09 13:26:43'),
(24, 'REACT JS', 1, '2019-05-23 10:11:43', '2019-05-23 10:11:43'),
(25, 'NODE JS', 1, '2019-05-23 10:11:54', '2019-05-23 10:11:54');

-- --------------------------------------------------------

--
-- Table structure for table `app_emi`
--

CREATE TABLE `app_emi` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `admission_id` varchar(255) NOT NULL,
  `amount` int(11) NOT NULL,
  `due_date` date NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app_emi`
--

INSERT INTO `app_emi` (`id`, `student_id`, `admission_id`, `amount`, `due_date`, `status`, `created_at`, `updated_at`) VALUES
(3, 8, '63fa03b5-8aef-49b0-b948-ed2ead408e0b', 6000, '2019-06-04', 1, '2019-03-28 08:02:26', '2019-03-28 08:02:26'),
(4, 9, '46cb9099-318a-4ca7-9ccf-7d12135d58fe', 4000, '2019-06-20', 1, '2019-03-28 08:51:01', '2019-03-28 08:51:01'),
(5, 10, 'fe6d9131-6390-474a-ac8a-58474f3acbc5', 4000, '2019-06-20', 1, '2019-03-28 09:01:59', '2019-03-28 09:01:59'),
(6, 11, '78c64a60-df28-4132-85ec-de1d1237d77a', 4000, '2019-06-20', 1, '2019-03-28 09:12:23', '2019-03-28 09:12:23'),
(7, 12, 'dbc6f69c-9dbe-4a80-96bb-d5253faabae6', 4000, '2019-04-04', 1, '2019-03-28 09:49:33', '2019-03-28 09:49:33'),
(8, 13, '82d4e9d7-01bb-4d3f-9184-fe9d7c90ca1b', 4000, '2019-04-02', 1, '2019-03-28 10:02:36', '2019-03-28 10:02:36'),
(9, 14, '86a8f7d4-a89a-41b5-9759-646a23152b91', 4000, '2019-06-07', 1, '2019-03-28 10:09:47', '2019-03-28 10:09:47'),
(10, 15, 'f34a6fa7-5080-437f-8254-0de274920bd6', 3000, '2019-06-06', 1, '2019-03-28 10:23:38', '2019-03-28 10:23:38'),
(11, 16, 'c62f9c02-6fff-4c76-b9ea-8c8ee199c873', 3000, '2019-06-06', 1, '2019-03-28 10:41:07', '2019-03-28 10:41:07'),
(12, 17, '408190e4-6521-4bff-aaa9-f3bc913c9b5f', 3000, '2019-06-06', 1, '2019-03-28 10:46:21', '2019-03-28 10:46:21'),
(13, 18, '55afd108-9a5a-4993-9e60-5624a873b7a8', 3000, '2019-06-06', 1, '2019-03-28 10:52:14', '2019-03-28 10:52:14'),
(14, 19, 'f78a5b47-074f-4f79-b12d-54a966f7f9ff', 3000, '2019-05-20', 1, '2019-03-29 08:12:38', '2019-03-29 08:12:38');

-- --------------------------------------------------------

--
-- Table structure for table `app_fee`
--

CREATE TABLE `app_fee` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `admission_id` varchar(255) NOT NULL,
  `cousellor_id` int(11) NOT NULL,
  `course_amount` int(11) NOT NULL,
  `due_amount` int(11) NOT NULL,
  `due_date` date NOT NULL,
  `last_payment` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app_fee`
--

INSERT INTO `app_fee` (`id`, `student_id`, `admission_id`, `cousellor_id`, `course_amount`, `due_amount`, `due_date`, `last_payment`, `status`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(6, 34, '7e5253eb-4e4d-490c-8c79-e65f037276ca', 7, 8000, 1000, '2019-06-03', 5000, 1, '2019-04-25 16:39:27', 6, '2019-05-21 16:05:49', 6),
(7, 35, '10a5f432-5de5-42e4-a8c5-254a5f954828', 7, 5000, 4000, '2019-06-01', 1000, 1, '2019-04-25 16:48:51', 6, '2019-04-25 16:48:51', NULL),
(8, 36, '37b63498-ad7c-46bd-9f51-18087033fc41', 7, 5000, 4000, '2019-06-03', 1000, 1, '2019-04-25 17:42:55', 6, '2019-04-25 17:42:55', NULL),
(9, 37, '4cb53ae0-46ae-4393-b239-044aeed070c2', 7, 5000, 4000, '2019-06-03', 1000, 1, '2019-04-25 17:45:29', 6, '2019-04-25 17:45:29', NULL),
(10, 38, '94569776-c9ae-4e6d-a946-5348e609013c', 7, 5000, 4000, '2019-06-03', 1000, 1, '2019-04-25 17:53:03', 6, '2019-04-25 17:53:03', NULL),
(11, 39, '8b1ce920-06fc-48b3-b5e4-d9516de0799a', 7, 5000, 4000, '2019-06-03', 1000, 1, '2019-04-25 17:57:31', 6, '2019-04-25 17:57:31', NULL),
(12, 40, '97c8a4c7-0f08-42a0-a0ef-7ee070c46514', 7, 5000, 0, '2019-05-22', 3000, 1, '2019-04-25 18:09:56', 6, '2019-05-22 09:50:03', 6),
(13, 41, '32dd28d2-7d71-48df-9440-356c1503635d', 7, 5000, 0, '2019-05-14', 3000, 1, '2019-04-25 18:15:15', 6, '2019-05-14 11:05:22', 6),
(14, 42, 'bcab8c6d-1f3b-48f2-8836-fc4babfaee15', 7, 5000, 0, '2019-05-14', 3000, 1, '2019-04-29 16:13:45', 6, '2019-05-14 10:13:36', 6),
(15, 43, '48dff572-1541-4e23-ad4b-98a392f761a8', 7, 5000, 0, '2019-05-22', 3000, 1, '2019-04-29 16:17:59', 6, '2019-05-22 09:49:18', 6),
(16, 44, 'd6550116-e524-4d7f-a3a2-f0eb81b1ba8a', 9, 4000, 3000, '2019-05-20', 1000, 1, '2019-04-29 16:25:30', 6, '2019-04-29 16:25:30', NULL),
(17, 45, 'e66163a5-3368-4f9a-ba37-418454879358', 7, 6000, 2000, '2019-06-17', 3000, 1, '2019-04-29 16:30:13', 6, '2019-05-22 14:20:27', 6),
(18, 46, '65ab8576-9097-48a9-8396-6c474c238351', 7, 5500, 5000, '2019-06-03', 500, 1, '2019-04-29 16:39:43', 6, '2019-04-29 16:39:43', NULL),
(19, 47, 'ed8ee1e9-a26b-4263-b7b9-3781a7eb0f49', 7, 5500, 0, '2019-05-21', 5000, 1, '2019-04-29 17:36:31', 6, '2019-05-21 16:07:32', 6),
(20, 48, 'afb89688-652b-4a53-997b-941c857c2b00', 7, 5500, 0, '2019-05-20', 4500, 1, '2019-04-29 17:45:51', 6, '2019-05-20 18:53:34', 6),
(21, 49, '2c64b5da-a520-4560-90b7-d3f35c371c93', 7, 5500, 0, '2019-05-21', 4500, 1, '2019-04-29 17:52:07', 6, '2019-05-21 16:07:00', 6),
(22, 50, '1d0dc134-1250-4015-83e2-6a9cd6a31f82', 7, 5500, 0, '2019-05-20', 5000, 1, '2019-04-29 17:56:49', 6, '2019-05-20 18:52:53', 6),
(23, 51, '1b1d9e9e-6a68-4b53-8566-05e70cdcd2aa', 9, 5000, 3000, '2019-05-20', 2000, 1, '2019-04-29 18:01:13', 6, '2019-04-29 18:01:13', NULL),
(24, 52, '866abc84-91c6-4100-a79a-af0675ce5e12', 7, 5000, 0, '2019-05-14', 3000, 1, '2019-04-29 18:05:54', 6, '2019-05-14 10:43:39', 6),
(25, 53, '60a195ad-264c-4d7d-929c-247ce4ee5f5e', 9, 5000, 3000, '2019-05-15', 2000, 1, '2019-04-30 16:43:10', 6, '2019-05-16 16:24:23', NULL),
(26, 54, '8b5e5db4-1823-47e6-9854-2b139561df42', 9, 4000, 3000, '2019-05-20', 1000, 1, '2019-04-30 16:51:31', 6, '2019-04-30 16:51:31', NULL),
(28, 56, '84ba54e3-efc4-43ec-b2f9-2f8b36875fa4', 9, 5000, 4000, '2019-05-15', 1000, 1, '2019-04-30 17:12:01', 6, '2019-05-16 16:40:23', NULL),
(29, 57, 'b9ba3767-519b-4a93-b1e9-7923577a3a4c', 9, 5000, 4000, '2019-05-15', 1000, 1, '2019-04-30 17:16:07', 6, '2019-05-17 13:34:45', NULL),
(30, 58, '14111cf9-9495-43f4-941c-baed158a3d5e', 9, 5000, 4000, '2019-06-01', 1000, 1, '2019-04-30 17:19:26', 6, '2019-04-30 17:19:26', NULL),
(31, 59, 'c85c1234-778e-4135-a592-375e96aeadc7', 9, 5000, 4000, '2019-06-01', 1000, 1, '2019-04-30 17:55:31', 6, '2019-04-30 17:55:31', NULL),
(32, 60, '5891c114-6e40-4fbc-a567-c0d9cb787aed', 9, 5000, 4000, '2019-06-01', 1000, 1, '2019-04-30 17:59:31', 6, '2019-04-30 17:59:31', NULL),
(33, 61, 'bdd9203c-7b15-4cb9-a573-8d93a3eac6ea', 13, 3000, 0, '2019-04-30', 3000, 1, '2019-04-30 18:07:52', 6, '2019-04-30 18:07:52', NULL),
(34, 62, 'bcc7a276-38fd-4217-b86c-480331a727dc', 9, 4000, 3000, '2019-05-20', 1000, 1, '2019-04-30 18:16:40', 6, '2019-04-30 18:16:40', NULL),
(35, 63, '27059363-46f9-4534-b226-405465efe2de', 9, 5000, 4000, '2019-05-20', 1000, 1, '2019-04-30 18:24:47', 6, '2019-04-30 18:24:47', NULL),
(36, 64, 'a4516a26-b3cb-41d7-ad07-0832cc273f95', 9, 5000, 4000, '2019-05-20', 1000, 1, '2019-04-30 18:29:42', 6, '2019-05-03 17:46:41', NULL),
(37, 65, '7622a286-134d-4fc8-9ee8-0e882ae741fd', 9, 5000, 4000, '2019-05-20', 1000, 1, '2019-04-30 18:36:16', 6, '2019-05-14 15:21:11', NULL),
(38, 66, '6c81ca44-4781-4e97-9646-c0e07868bde1', 9, 5000, 4000, '2019-05-20', 1000, 1, '2019-04-30 18:41:13', 6, '2019-04-30 18:41:13', NULL),
(39, 67, 'ebb005bc-168a-4ba4-bb5e-d096839f06a6', 7, 5000, 0, '2019-05-17', 3000, 1, '2019-04-30 18:46:43', 6, '2019-05-17 17:31:31', 6),
(40, 68, '0e909d28-35c6-45ec-b276-ae21e67f454a', 7, 5000, 0, '2019-05-17', 3000, 1, '2019-05-01 13:57:38', 6, '2019-05-17 17:32:14', 6),
(41, 69, '2ee5ff80-b249-4304-8d4d-2ca037414558', 7, 5500, 4500, '2019-06-01', 1000, 1, '2019-05-01 14:05:35', 6, '2019-05-01 14:05:35', NULL),
(42, 70, '5a5bf019-9d5a-4374-855e-ed630a73dc8d', 9, 5000, 4000, '2019-05-20', 1000, 1, '2019-05-01 14:10:37', 6, '2019-05-02 13:26:40', NULL),
(43, 55, '24d89c1a-8691-44c2-8d19-2e7769ba2aad', 13, 8000, 7000, '2019-05-20', 1000, 1, '2019-05-01 11:16:47', NULL, '2019-05-01 11:16:47', NULL),
(44, 71, 'b321fae4-8e84-41df-b159-aecbbab674de', 7, 5000, 0, '2019-05-14', 3000, 1, '2019-05-02 12:43:31', 6, '2019-05-14 10:46:25', 6),
(45, 72, '5267a7c7-3ef0-45bf-aa0f-6c4b4879bc7e', 9, 5000, 0, '2019-05-02', 5000, 1, '2019-05-02 12:55:24', 6, '2019-05-04 17:27:06', NULL),
(46, 73, '13486acf-c62d-40ec-af1f-feaac0c01be2', 9, 5000, 4000, '2019-05-20', 1000, 1, '2019-05-02 13:02:42', 6, '2019-05-04 17:36:28', NULL),
(47, 74, '2e605f49-9101-4c1d-934c-dea896a1d7d7', 10, 4000, 2000, '2019-05-20', 2000, 1, '2019-05-02 13:18:22', 6, '2019-05-02 13:18:22', NULL),
(48, 75, '55c40eb7-e2b9-4f39-b68e-5c959aa419e5', 10, 4000, 2000, '2019-05-20', 2000, 1, '2019-05-02 13:40:01', 6, '2019-05-02 13:40:01', NULL),
(49, 76, '390b92c9-f346-441d-bbd4-9b9299e304bd', 7, 4500, 2500, '2019-06-01', 2000, 1, '2019-05-02 13:54:36', 6, '2019-05-02 13:54:36', NULL),
(50, 77, '1909249d-436f-4bf6-a265-0cdf0b0d10fd', 10, 4000, 2000, '2019-05-20', 2000, 1, '2019-05-02 14:04:16', 6, '2019-05-02 14:04:16', NULL),
(51, 78, 'b51f4b28-34c3-4a58-84bf-1ca5aa3652cb', 10, 4000, 3000, '2019-06-01', 1000, 1, '2019-05-03 15:44:20', 6, '2019-05-03 15:44:20', NULL),
(52, 79, '9d0d80fe-49c2-4dd7-9e52-238f5fe496ef', 10, 4000, 3000, '2019-05-20', 1000, 1, '2019-05-03 16:22:32', 6, '2019-05-03 16:22:32', NULL),
(53, 80, '3a73cc4b-66a7-48da-a53c-8139942d94cd', 7, 6000, 4000, '2019-05-22', 2000, 1, '2019-05-03 16:31:37', 6, '2019-05-03 16:31:37', NULL),
(54, 81, 'd1e378f2-8e62-4a51-bf92-bb9428be0eef', 7, 6000, 4000, '2019-06-01', 2000, 1, '2019-05-03 16:36:43', 6, '2019-05-03 16:36:43', NULL),
(55, 82, 'dbc720fe-60e0-4124-84b6-1b3fabbe0226', 7, 4000, 2000, '2019-06-01', 2000, 1, '2019-05-03 16:43:54', 6, '2019-05-03 16:43:54', NULL),
(56, 83, 'b522299e-1b88-435a-af04-53fdc4769275', 10, 4000, 3000, '2019-06-01', 1000, 1, '2019-05-03 16:47:42', 6, '2019-05-03 16:47:42', NULL),
(57, 84, '82ee089e-1c7a-44e5-9cf2-7a366524dd6b', 10, 4000, 3000, '2019-06-03', 1000, 1, '2019-05-03 17:33:03', 6, '2019-05-03 17:33:03', NULL),
(58, 85, 'ec572308-3559-4ae7-b10c-a2876e0c5d33', 9, 7000, 3500, '2019-06-05', 3500, 1, '2019-05-03 17:40:01', 6, '2019-05-03 17:40:01', NULL),
(59, 86, '211c1043-45c2-42b1-986c-f4d8cf7295e1', 7, 5000, 4000, '2019-06-01', 1000, 1, '2019-05-04 10:16:31', 6, '2019-05-04 10:16:31', NULL),
(60, 87, 'ddf9d4ee-7dd7-4910-a14f-6fc962f04b56', 14, 5000, 2000, '2019-05-27', 2000, 1, '2019-05-04 10:24:39', 6, '2019-05-17 16:14:30', 6),
(61, 88, 'f9c6c994-51c4-4bc9-8ec5-49fa1756a7bc', 9, 5000, 0, '2019-05-14', 4000, 1, '2019-05-04 10:36:01', 6, '2019-05-14 11:17:12', 6),
(62, 89, '1f1a406f-2412-4ca4-85d3-052cbf6df19a', 10, 5000, 3000, '2019-05-20', 2000, 1, '2019-05-04 10:48:48', 6, '2019-05-04 10:48:48', NULL),
(63, 90, '35ceb37a-e5ce-4dc9-a7a4-ac4370de0ea3', 10, 4000, 2000, '2019-05-20', 2000, 1, '2019-05-04 10:54:27', 6, '2019-05-04 10:54:27', NULL),
(64, 91, '8bb119c2-7400-4138-a597-d3c7cd8bd4f2', 13, 3000, 2000, '2019-06-17', 1000, 1, '2019-05-04 10:59:24', 6, '2019-05-04 10:59:24', NULL),
(65, 92, '7fcac2a5-e1f3-4206-861b-d6d48e905181', 10, 4000, 3000, '2019-06-05', 1000, 1, '2019-05-04 11:04:49', 6, '2019-05-04 11:04:49', NULL),
(66, 93, 'a8c4d4d7-ff73-4bac-bf86-fc5b48dcc9a7', 10, 5000, 4000, '2019-06-05', 1000, 1, '2019-05-04 11:10:12', 6, '2019-05-04 11:10:12', NULL),
(67, 94, '0c089aae-a9b7-4030-a33b-cf0381627389', 7, 5000, 3000, '2019-05-20', 2000, 1, '2019-05-04 11:16:10', 6, '2019-05-04 11:16:10', NULL),
(68, 95, '74138940-2484-4491-bddb-386142c41e09', 9, 5000, 0, '2019-05-23', 4000, 1, '2019-05-04 11:22:02', 6, '2019-05-23 13:15:39', 6),
(69, 96, 'cfb4af32-83e9-4e3e-b114-7d88b7c76798', 9, 5000, 4000, '2019-05-20', 1000, 1, '2019-05-04 11:26:06', 6, '2019-05-04 11:26:06', NULL),
(70, 97, '2bb1265f-6801-4451-9102-0b6129230573', 7, 6000, 3000, '2019-05-20', 2000, 1, '2019-05-04 11:35:19', 6, '2019-05-14 10:15:56', 6),
(71, 98, '73423bd4-470a-454b-a581-3a09709a475a', 7, 6000, 0, '2019-05-14', 4000, 1, '2019-05-04 11:47:25', 6, '2019-05-14 10:18:34', 6),
(72, 99, '143aa09b-f898-486e-a915-136719f2be48', 7, 6000, 0, '2019-05-14', 4000, 1, '2019-05-04 11:54:28', 6, '2019-05-14 10:17:17', 6),
(73, 100, 'bd98b5c6-85b2-462b-ab8c-d6e8b7e8d011', 13, 5500, 3500, '2019-05-20', 2000, 1, '2019-05-04 11:59:53', 6, '2019-05-04 11:59:53', NULL),
(74, 101, 'a873bcba-e6a0-4091-a7e8-2cdf4c6502ee', 9, 2500, 0, '2019-05-04', 2500, 1, '2019-05-04 12:07:49', 6, '2019-05-04 12:07:49', NULL),
(75, 102, '2be244df-99c6-453d-9da2-13afb7959ea5', 9, 2500, 0, '2019-05-04', 2500, 1, '2019-05-04 12:12:31', 6, '2019-05-04 12:12:31', NULL),
(76, 103, 'de000390-b862-45a4-aebe-40f5590fe6d0', 9, 2500, 0, '2019-05-04', 2500, 1, '2019-05-04 12:19:22', 6, '2019-05-04 12:19:22', NULL),
(77, 104, '60f61c7b-2daf-43b2-819e-4db653522800', 7, 8000, 6000, '2019-05-20', 2000, 1, '2019-05-04 12:28:07', 6, '2019-05-04 12:28:07', NULL),
(78, 105, '00a64010-ef1b-4239-b105-1913e5bd47cc', 9, 5000, 3000, '2019-06-05', 1000, 1, '2019-05-04 12:36:59', 6, '2019-05-17 16:12:44', 6),
(79, 106, '440fd186-62d3-4dd0-aa43-371672ba3ece', 9, 2500, 0, '2019-05-04', 2500, 1, '2019-05-04 12:41:23', 6, '2019-05-04 12:41:23', NULL),
(80, 107, '1c4524c3-437e-451f-a4e7-86cd599d39af', 9, 2500, 0, '2019-05-04', 2500, 1, '2019-05-04 12:47:03', 6, '2019-05-04 12:47:03', NULL),
(81, 108, '95562174-826c-46ce-904c-6a6c70089c82', 9, 2500, 0, '2019-05-04', 2500, 1, '2019-05-04 12:56:02', 6, '2019-05-14 15:22:31', NULL),
(82, 109, 'f75660d5-631d-45d6-b796-ec2aff6e2e68', 9, 5000, 2500, '2019-06-10', 1500, 1, '2019-05-04 13:03:32', 6, '2019-05-04 17:00:57', 6),
(83, 110, '1f11a6ac-753b-4d8d-a81c-35f064e1ed86', 9, 2500, 0, '2019-05-04', 2500, 1, '2019-05-04 13:06:56', 6, '2019-05-04 13:06:56', NULL),
(84, 111, '080f74ef-a279-41f4-a9cd-675e7eaf85f1', 7, 3500, 2500, '2019-06-03', 1000, 1, '2019-05-04 13:09:49', 6, '2019-05-04 13:09:49', NULL),
(85, 112, 'cb9a4acd-8fbe-4112-9061-5a7c1b339266', 7, 3500, 2500, '2019-06-05', 1000, 1, '2019-05-04 13:14:08', 6, '2019-05-04 13:14:08', NULL),
(86, 113, 'c9de264e-ecd4-4063-8cbb-041a98c0f96d', 7, 3500, 0, '2019-05-04', 3500, 1, '2019-05-04 13:17:10', 6, '2019-05-04 13:17:10', NULL),
(87, 114, '03efc270-7c1b-49fc-b2b3-6c715232c4da', 9, 2500, 0, '2019-05-04', 2500, 1, '2019-05-04 13:20:39', 6, '2019-05-04 13:20:39', NULL),
(88, 115, '6c01d349-1865-40cb-a582-268f9a26db54', 9, 2500, 0, '2019-05-04', 2500, 1, '2019-05-04 13:23:15', 6, '2019-05-04 13:23:15', NULL),
(89, 116, 'c5dcc609-0248-40c4-997b-16957f673057', 9, 2500, 0, '2019-05-04', 2500, 1, '2019-05-04 13:28:09', 6, '2019-05-04 13:28:09', NULL),
(90, 117, '847b3e35-89bb-4a42-9c10-88cf799826d3', 9, 2500, 0, '2019-05-04', 2500, 1, '2019-05-04 13:31:33', 6, '2019-05-04 13:31:33', NULL),
(91, 118, '39ea8792-38aa-4dee-b757-3ab6c6aa0c73', 9, 2500, 0, '2019-05-04', 2500, 1, '2019-05-04 13:34:17', 6, '2019-05-04 13:34:17', NULL),
(92, 119, '4397135b-ed60-400f-9139-b6d9654728f6', 7, 4000, 2000, '2019-05-20', 2000, 1, '2019-05-04 13:38:36', 6, '2019-05-04 13:38:36', NULL),
(93, 120, '84210973-f0bb-4203-94e2-cb1e83f4bddb', 10, 3000, 2000, '2019-05-20', 1000, 1, '2019-05-04 13:44:29', 6, '2019-05-04 13:44:29', NULL),
(94, 121, '7cd67f19-5435-4b33-8829-24c9cd6eac20', 9, 5000, 4000, '2019-05-20', 1000, 1, '2019-05-04 16:56:01', 6, '2019-05-04 16:56:01', NULL),
(95, 122, '8d6db807-d246-4e5a-800e-1901cc45daea', 7, 4000, 0, '2019-05-15', 2000, 1, '2019-05-04 18:18:11', 6, '2019-05-15 12:43:54', 6),
(96, 123, '49707202-71d7-41fb-b219-9b6b33ee876d', 7, 4000, 2000, '2019-06-10', 1500, 1, '2019-05-04 18:49:36', 6, '2019-05-15 12:47:40', 6),
(97, 124, '7c4d1259-d71d-40a5-942d-10641c141f0e', 14, 5000, 4000, '2019-05-20', 1000, 1, '2019-05-06 10:06:08', 6, '2019-05-06 10:06:08', NULL),
(98, 125, 'bb5b38ff-2d86-4cd1-ae2e-7cf7ed951af8', 14, 5000, 4000, '2019-05-20', 1000, 1, '2019-05-06 10:13:38', 6, '2019-05-06 10:13:38', NULL),
(99, 126, 'f35c05a1-49be-4650-be3b-705368f00e34', 14, 5000, 4000, '2019-07-05', 1000, 1, '2019-05-06 10:19:35', 6, '2019-05-06 10:19:35', NULL),
(100, 127, '26ddf83a-a879-4596-b161-822ad61c2bba', 9, 3000, 0, '2019-05-06', 3000, 1, '2019-05-06 10:49:19', 6, '2019-05-06 10:49:19', NULL),
(101, 128, 'e64916b1-fe7f-471d-a39f-7b7d3697879a', 7, 3000, 0, '2019-05-06', 3000, 1, '2019-05-06 11:03:43', 6, '2019-05-06 11:03:43', NULL),
(102, 129, '9ce7abf6-3388-46d9-8eab-719a0ddf3b85', 9, 2500, 0, '2019-05-06', 2500, 1, '2019-05-06 11:12:29', 6, '2019-05-06 11:12:29', NULL),
(103, 130, 'a6462502-65d2-44c7-8cea-c964abb2331a', 7, 3500, 0, '2019-05-06', 3500, 1, '2019-05-06 11:27:37', 6, '2019-05-06 11:27:37', NULL),
(104, 131, '54b2a19a-c061-4b71-966a-97dd812d0d16', 7, 3000, 2000, '2019-06-01', 1000, 1, '2019-05-06 11:34:41', 6, '2019-05-06 11:34:41', NULL),
(105, 132, '1b86bd8c-824b-412c-942b-50e806467ab3', 7, 3000, 2000, '2019-06-01', 1000, 1, '2019-05-06 11:38:35', 6, '2019-05-06 11:38:35', NULL),
(106, 133, 'a267a9a5-6b20-45b6-837f-a03e92cbf8f0', 7, 4500, 3500, '2019-06-01', 1000, 1, '2019-05-06 11:42:29', 6, '2019-05-06 11:42:29', NULL),
(107, 134, '63cf0374-736c-44a0-b3b7-9d730c316869', 14, 5000, 3000, '2019-06-01', 2000, 1, '2019-05-06 11:46:42', 6, '2019-05-06 11:46:42', NULL),
(108, 135, '7186d53e-2df6-4846-a729-48e2d897d540', 14, 4000, 0, '2019-05-06', 4000, 1, '2019-05-06 11:51:20', 6, '2019-05-06 11:51:20', NULL),
(109, 136, '3614749b-ec22-476b-a8b7-7650cd3e71fd', 9, 4000, 3000, '2019-06-01', 1000, 1, '2019-05-06 11:59:46', 6, '2019-05-06 11:59:46', NULL),
(110, 137, '66fc4fe8-8aa2-47b7-8d20-6a84ea815f66', 9, 2500, 0, '2019-05-06', 2500, 1, '2019-05-06 12:03:38', 6, '2019-05-06 12:03:38', NULL),
(111, 138, 'eba8cabd-975b-48fe-9c24-1e9bda4e4cdb', 14, 4000, 0, '2019-05-14', 2000, 1, '2019-05-06 12:16:58', 6, '2019-05-14 10:37:44', 6),
(112, 139, '7c11ed1a-9a77-4d59-b24f-e77bba43de30', 14, 4000, 0, '2019-05-14', 2000, 1, '2019-05-06 12:26:16', 6, '2019-05-14 10:38:24', 6),
(113, 140, '80aea40d-f150-4707-9388-1acde773cb61', 9, 2500, 0, '2019-05-06', 2500, 1, '2019-05-06 12:32:22', 6, '2019-05-06 12:32:22', NULL),
(114, 141, '028629f8-47a3-4b40-97ea-2132e3d37f42', 9, 4000, 3000, '2019-06-01', 1000, 1, '2019-05-06 12:35:48', 6, '2019-05-06 12:35:48', NULL),
(115, 142, 'b3be6f9f-3ae1-409a-a79e-f258d1d5724e', 10, 4000, 3000, '2019-06-01', 1000, 1, '2019-05-06 12:39:31', 6, '2019-05-06 12:39:31', NULL),
(116, 143, '0acdd244-af77-4106-b569-2476c2718c47', 9, 2500, 0, '2019-05-06', 2500, 1, '2019-05-06 12:43:04', 6, '2019-05-06 12:43:04', NULL),
(117, 144, '16707d80-71df-4d67-9d87-8b09616ffc21', 9, 2500, 0, '2019-05-06', 2500, 1, '2019-05-06 12:58:26', 6, '2019-05-06 12:58:26', NULL),
(118, 145, '39972669-4efd-44e2-9942-78aa8a1a494a', 9, 2500, 0, '2019-05-06', 2500, 1, '2019-05-06 13:01:59', 6, '2019-05-06 13:01:59', NULL),
(119, 146, '6577ded5-e159-4830-809f-3daee96c6fef', 10, 4000, 3000, '2019-06-01', 1000, 1, '2019-05-06 13:05:55', 6, '2019-05-06 13:05:55', NULL),
(120, 147, '047e56f4-8d80-44c5-9a08-09b5c4994e54', 10, 3000, 1500, '2019-06-01', 1500, 1, '2019-05-06 13:09:27', 6, '2019-05-06 13:09:27', NULL),
(121, 148, 'f33823f6-4f6b-4377-9b85-c330d29478d5', 9, 4000, 0, '2019-05-14', 2000, 1, '2019-05-06 13:24:44', 6, '2019-05-14 17:30:28', 6),
(122, 149, 'f2a84306-f0be-42f4-8b3f-493ccbdc5fdf', 10, 4000, 3000, '2019-05-20', 1000, 1, '2019-05-06 13:34:56', 6, '2019-05-06 13:34:56', NULL),
(123, 150, 'cb566a16-f2e1-4af7-b9d8-719e3436464e', 10, 4000, 3000, '2019-06-01', 1000, 1, '2019-05-06 13:40:17', 6, '2019-05-06 13:40:17', NULL),
(124, 151, 'afc25887-281a-457c-87eb-3fbdea748b2f', 9, 2500, 0, '2019-05-06', 2500, 1, '2019-05-06 14:00:45', 6, '2019-05-06 14:00:45', NULL),
(125, 152, '9bae2b63-145d-4019-9bd9-8949cb3a5ff4', 9, 4000, 2000, '2019-06-01', 2000, 1, '2019-05-06 14:16:43', 6, '2019-05-06 14:16:43', NULL),
(126, 153, '7ecf98a2-e191-4348-b645-a1f4740f6aec', 7, 2000, 0, '2019-05-06', 2000, 1, '2019-05-06 15:28:28', 6, '2019-05-06 15:28:28', NULL),
(127, 154, 'b2bc6e74-5919-413e-a87f-30af2ee5b75e', 10, 3000, 0, '2019-05-17', 2000, 1, '2019-05-06 15:49:56', 6, '2019-05-17 11:56:25', 6),
(128, 155, '0819d294-d64c-4c84-b5c1-9a5b0fa99f3b', 7, 4000, 0, '2019-05-15', 3000, 1, '2019-05-08 11:15:34', 6, '2019-05-15 15:45:17', 6),
(129, 156, 'c136cb8a-4bca-47b4-91f9-09a288dce946', 7, 4000, 0, '2019-05-15', 3000, 1, '2019-05-08 11:25:18', 6, '2019-05-15 15:44:31', 6),
(130, 157, '558f004c-2f92-4474-bf38-a231c553e4b3', 7, 4000, 3000, '2019-05-20', 1000, 1, '2019-05-08 11:31:24', 6, '2019-05-08 11:31:24', NULL),
(131, 158, '006e5745-edb6-442d-915f-326c55d9f3f6', 9, 4000, 0, '2019-05-20', 3000, 1, '2019-05-08 11:43:45', 6, '2019-05-20 18:20:40', 6),
(132, 159, '1ac8fd2b-60a7-4af7-9014-281247a5aee4', 7, 3500, 1500, '2019-06-01', 2000, 1, '2019-05-08 11:53:39', 6, '2019-05-10 10:20:14', NULL),
(133, 160, 'e27740f6-da8f-41d5-b918-10a1cfadb92f', 10, 5000, 0, '2019-05-08', 5000, 1, '2019-05-08 12:00:22', 6, '2019-05-08 12:00:22', NULL),
(134, 161, 'd7b2ec58-ab83-4f31-af03-8b83c2963750', 7, 5000, 0, '2019-05-22', 4000, 1, '2019-05-08 12:05:39', 6, '2019-05-22 12:54:57', 6),
(135, 162, '9fba6d50-e975-4ddf-9e96-a39fe363041f', 7, 5000, 4000, '2019-05-20', 1000, 1, '2019-05-08 12:10:37', 6, '2019-05-08 12:10:37', NULL),
(136, 163, '0810c525-6db1-4d92-b473-b12942fd3422', 10, 5000, 0, '2019-05-08', 5000, 1, '2019-05-08 12:17:49', 6, '2019-05-08 12:17:49', NULL),
(137, 164, 'd7033852-8fb0-466e-b33b-5293f4a4fafd', 10, 7000, 6000, '2019-05-20', 1000, 1, '2019-05-08 12:27:46', 6, '2019-05-08 12:51:19', NULL),
(138, 165, 'a467c7a9-652f-48d0-934b-6ddec6e0c7e3', 10, 7000, 0, '2019-05-16', 6000, 1, '2019-05-08 12:43:58', 6, '2019-05-16 13:43:06', 6),
(139, 166, '0d28bc33-7bb6-4bf6-8afe-ce67bd9b0e0e', 10, 7000, 6000, '2019-05-20', 1000, 1, '2019-05-08 12:49:53', 6, '2019-05-08 12:49:53', NULL),
(140, 167, 'ff24b346-73c3-4d9a-8d2c-7d68df16ee97', 10, 5000, 0, '2019-05-17', 4000, 1, '2019-05-08 13:02:08', 6, '2019-05-17 11:55:52', 6),
(141, 168, '6f630443-3d1b-4eb6-a22b-a469c528e20f', 10, 4000, 0, '2019-05-15', 3000, 1, '2019-05-08 13:12:21', 6, '2019-05-15 15:46:33', 6),
(142, 169, 'c427596e-93c1-46e8-ad42-198732745076', 10, 4000, 3000, '2019-05-20', 1000, 1, '2019-05-08 13:27:21', 6, '2019-05-08 13:27:21', NULL),
(143, 170, '6a990eb8-29e4-4486-bfd3-470d28d0d0da', 14, 6000, 5000, '2019-06-01', 1000, 1, '2019-05-08 13:41:28', 6, '2019-05-08 13:41:28', NULL),
(144, 171, '058c17c1-6e10-4c01-b433-b844c6ec5aee', 10, 3000, 2000, '2019-05-20', 1000, 1, '2019-05-08 13:47:40', 6, '2019-05-08 13:47:40', NULL),
(145, 172, 'c7bdbc57-8ab0-4b61-b63e-42920482fbaa', 9, 3000, 2000, '2019-06-01', 1000, 1, '2019-05-09 10:56:06', 6, '2019-05-09 10:56:06', NULL),
(146, 173, 'bf1f4250-6ff4-49eb-b51b-ca740a0dbf08', 7, 6500, 5500, '2019-05-20', 1000, 1, '2019-05-09 11:03:27', 6, '2019-05-21 12:08:47', NULL),
(147, 174, 'c4a1749b-1360-4cb4-a9f0-9db699667d44', 7, 6500, 5500, '2019-06-01', 1000, 1, '2019-05-09 11:11:45', 6, '2019-05-09 11:11:45', NULL),
(148, 175, '29aeabbe-af19-4acd-8152-54aed1a5a6b5', 7, 6500, 5500, '2019-06-01', 1000, 1, '2019-05-09 11:18:23', 6, '2019-05-09 11:18:23', NULL),
(149, 176, '150e512f-20ac-41aa-82a8-61cc7c1ca11c', 7, 6500, 6000, '2019-06-01', 500, 1, '2019-05-09 11:25:03', 6, '2019-05-09 11:25:03', NULL),
(150, 177, '465ec25f-7405-473b-9711-adf30b4827e0', 9, 5000, 4000, '2019-05-20', 1000, 1, '2019-05-09 11:32:23', 6, '2019-05-09 11:32:23', NULL),
(151, 178, '6182469f-eda6-4088-8611-395efc4e780a', 10, 4000, 3000, '2019-06-01', 1000, 1, '2019-05-09 11:40:26', 6, '2019-05-09 11:40:26', NULL),
(152, 179, '8dca276c-81cf-462f-a1b1-39545d84c457', 10, 4000, 3000, '2019-06-01', 1000, 1, '2019-05-09 11:47:10', 6, '2019-05-09 11:47:10', NULL),
(153, 180, 'f9666d28-6c52-4bb4-a2b9-dafcd0526e52', 10, 4000, 3000, '2019-06-01', 1000, 1, '2019-05-09 12:15:53', 6, '2019-05-09 12:15:53', NULL),
(154, 181, 'e1d97f93-f81a-4892-b383-b44cbf1eff53', 10, 7000, 6000, '2019-06-01', 1000, 1, '2019-05-09 12:24:19', 6, '2019-05-09 12:24:19', NULL),
(155, 182, 'e59d4b59-dba0-434d-95f8-6705686776ed', 9, 5000, 4000, '2019-06-01', 1000, 1, '2019-05-09 12:31:17', 6, '2019-05-09 12:31:17', NULL),
(156, 183, 'fe2a1c8f-4cc7-428d-8fb9-4abe8380944c', 9, 5000, 4000, '2019-06-01', 1000, 1, '2019-05-09 12:38:48', 6, '2019-05-09 12:38:48', NULL),
(157, 184, '1d49dccb-fe0e-4109-834d-699953de8cba', 10, 3000, 2000, '2019-05-20', 1000, 1, '2019-05-09 12:46:56', 6, '2019-05-09 12:46:56', NULL),
(158, 185, '4e03fed8-7a34-4e95-824b-4823b267cc29', 14, 5000, 0, '2019-05-21', 2500, 1, '2019-05-09 12:56:50', 6, '2019-05-21 15:07:05', 6),
(159, 186, '41865773-6845-4b69-98e4-2fff988fe799', 10, 7000, 3000, '2019-06-03', 3000, 1, '2019-05-09 13:04:01', 6, '2019-05-22 10:32:45', 6),
(160, 187, '7ca2d055-6526-4719-a576-8c34375a7ed5', 10, 7000, 3000, '2019-06-03', 3000, 1, '2019-05-09 13:10:33', 6, '2019-05-22 10:32:14', 6),
(161, 188, '0c2f0e4f-5294-4ea1-8e62-f63fe7215b5f', 7, 5000, 0, '2019-05-23', 3000, 1, '2019-05-09 13:20:28', 6, '2019-05-23 11:02:00', 6),
(162, 189, 'dadeedcf-0989-43bc-b3f8-7bf78c5c1dd5', 7, 4000, 0, '2019-05-20', 4000, 1, '2019-05-09 13:32:06', 6, '2019-05-09 13:32:06', NULL),
(163, 190, 'c0d66886-90b1-4e01-8df6-f853fe801ae5', 7, 4000, 0, '2019-05-09', 4000, 1, '2019-05-09 13:43:08', 6, '2019-05-09 13:43:08', NULL),
(164, 191, 'd5631f1a-5fc7-4395-a0cd-5a5a50e2e921', 7, 4000, 2000, '2019-05-20', 2000, 1, '2019-05-09 13:48:53', 6, '2019-05-09 13:48:53', NULL),
(165, 192, '5ae577bb-92fc-4509-96da-712edddd0976', 9, 4200, 1000, '2019-06-01', 3200, 1, '2019-05-09 14:00:57', 6, '2019-05-09 14:00:57', NULL),
(166, 193, 'd6466d19-a9bf-472d-8b8f-2b2e22ae7c98', 9, 4200, 3200, '2019-06-01', 1000, 1, '2019-05-09 14:07:46', 6, '2019-05-09 14:07:46', NULL),
(167, 194, '2c4e2bcb-9875-4d76-8aad-2a2060cac163', 9, 4200, 3200, '2019-06-01', 1000, 1, '2019-05-10 10:39:40', 6, '2019-05-10 10:57:04', NULL),
(168, 195, 'a6f2be60-7bb9-4207-ab05-05cda99ad527', 9, 4200, 3200, '2019-06-01', 1000, 1, '2019-05-10 10:56:21', 6, '2019-05-10 10:56:21', NULL),
(169, 196, 'dd20c929-63b6-45fa-97a8-ca3fd87d9b0d', 9, 2500, 0, '2019-05-10', 2500, 1, '2019-05-10 11:06:17', 6, '2019-05-10 11:06:17', NULL),
(170, 197, 'b438e00c-425a-4b2b-bd0b-a39a1d201fe3', 9, 2500, 0, '2019-05-10', 2500, 1, '2019-05-10 11:17:47', 6, '2019-05-10 11:17:47', NULL),
(171, 198, 'd0e157ce-cfcd-43c4-aec6-09df3c7dbdae', 10, 3000, 0, '2019-05-10', 3000, 1, '2019-05-10 11:24:47', 6, '2019-05-10 11:24:47', NULL),
(172, 199, '4344a3e4-e8bb-4b1f-99af-6ff356aac570', 7, 4000, 2000, '2019-06-01', 2000, 1, '2019-05-10 11:49:53', 6, '2019-05-10 11:49:53', NULL),
(173, 200, '08fbc98d-f6f6-4439-91e3-d411ca11815c', 7, 4000, 2000, '2019-06-01', 2000, 1, '2019-05-10 11:56:14', 6, '2019-05-10 11:56:14', NULL),
(174, 201, '72e01610-7ffc-457f-8c1c-742d630aa158', 7, 4000, 2000, '2019-06-01', 2000, 1, '2019-05-10 12:04:02', 6, '2019-05-10 12:04:02', NULL),
(175, 202, '1feaddb4-1d93-47ad-a24e-de36294fd4d3', 7, 4000, 2000, '2019-06-01', 2000, 1, '2019-05-10 12:08:46', 6, '2019-05-10 12:08:46', NULL),
(176, 203, 'e24c37ab-5d80-4de4-9041-97a5016ca469', 7, 4000, 2000, '2019-06-01', 2000, 1, '2019-05-10 12:14:58', 6, '2019-05-10 12:14:58', NULL),
(177, 204, '0ad09b4e-4da8-4b41-b9aa-97f5d9d0b89c', 7, 4000, 2000, '2019-06-01', 2000, 1, '2019-05-10 12:21:33', 6, '2019-05-10 12:21:33', NULL),
(178, 205, 'c3a8eb17-dc95-4c73-a583-81035a92a090', 7, 4000, 2000, '2019-06-01', 2000, 1, '2019-05-10 12:28:45', 6, '2019-05-10 12:28:45', NULL),
(179, 206, '5f986de6-902a-4139-925c-913d2b4d82b3', 13, 5000, 4000, '2019-06-01', 1000, 1, '2019-05-10 12:37:41', 6, '2019-05-10 12:37:41', NULL),
(180, 207, '2f83a33c-4577-4964-8c0e-c8886c2ce35d', 13, 5000, 4000, '2019-06-01', 1000, 1, '2019-05-10 12:45:09', 6, '2019-05-10 12:45:09', NULL),
(181, 208, 'f0b6c34b-b8b4-440e-aa5f-d0a34b9823e3', 13, 5000, 4000, '2019-06-01', 1000, 1, '2019-05-10 12:51:24', 6, '2019-05-10 12:51:24', NULL),
(182, 209, '41680e3a-6c84-4538-82cc-f58a0824129b', 14, 5000, 0, '2019-05-14', 2500, 1, '2019-05-10 12:58:45', 6, '2019-05-14 10:20:12', 6),
(183, 210, 'ffdf941a-1c6a-48bb-8d25-e18cb12a9abe', 14, 5000, 0, '2019-05-14', 2500, 1, '2019-05-10 13:03:46', 6, '2019-05-15 11:18:25', 6),
(184, 211, 'ae93f45e-53f8-456e-89b0-e68cb62772d6', 7, 4500, 2500, '2019-06-01', 2000, 1, '2019-05-10 13:10:42', 6, '2019-05-10 13:10:42', NULL),
(185, 212, '73f101b3-9e6e-46c1-aa2e-c0a202b1b1ef', 14, 4500, 3500, '2019-06-01', 1000, 1, '2019-05-10 13:18:01', 6, '2019-05-10 13:18:01', NULL),
(186, 213, '877a4b0d-90ee-4867-8126-7fa133aff399', 14, 4500, 0, '2019-05-10', 4500, 1, '2019-05-10 13:23:37', 6, '2019-05-10 13:23:37', NULL),
(187, 214, 'b1e42017-4327-4956-8f3e-635b6856c7cc', 7, 4500, 2500, '2019-06-03', 2000, 1, '2019-05-13 10:37:42', 6, '2019-05-13 10:37:42', NULL),
(188, 215, '8ce66694-e818-4c3f-b82a-7c2ceb7c7178', 7, 5000, 0, '2019-05-15', 4000, 1, '2019-05-13 10:45:30', 6, '2019-05-15 15:43:38', 6),
(189, 216, 'bfc689ef-222d-4b26-8bf9-9f27d63c54a2', 9, 7000, 3000, '2019-06-03', 3000, 1, '2019-05-13 10:49:09', 6, '2019-05-17 12:30:32', 6),
(190, 217, '752e8ced-5a95-4506-9430-a3fd7668855f', 9, 4500, 2500, '2019-06-10', 2000, 1, '2019-05-13 10:52:33', 6, '2019-05-13 10:52:33', NULL),
(191, 218, '150d87bd-2d9d-4b99-b82e-cb7a89cf7cd2', 9, 4000, 2000, '2019-06-01', 2000, 1, '2019-05-13 10:55:32', 6, '2019-05-13 10:55:32', NULL),
(192, 219, '3e65b3c6-509c-4281-8b40-83bd84fbf519', 13, 6000, 4000, '2019-05-20', 2000, 1, '2019-05-13 10:59:52', 6, '2019-05-13 10:59:52', NULL),
(193, 220, '647a12ec-f3f2-4724-84e0-fec8a319be2b', 14, 3500, 0, '2019-05-13', 3500, 1, '2019-05-13 11:03:17', 6, '2019-05-13 11:03:17', NULL),
(194, 221, '751cb3d4-c2d2-4f8e-8279-42a3760700ca', 14, 3500, 0, '2019-05-15', 2500, 1, '2019-05-13 11:07:33', 6, '2019-05-15 15:45:55', 6),
(195, 222, '56cb56ee-12b8-4dbc-9315-39a56bf0d73e', 13, 6000, 4000, '2019-05-20', 2000, 1, '2019-05-13 11:10:18', 6, '2019-05-13 11:10:18', NULL),
(196, 223, '09d1d97c-fa82-49fc-a0e1-dd9afde1ca77', 13, 6000, 4000, '2019-05-20', 2000, 1, '2019-05-13 11:15:23', 6, '2019-05-13 11:15:23', NULL),
(197, 224, 'af1344fc-3ec3-4f4b-bfbf-e955e6645405', 13, 4000, 2500, '2019-05-20', 1500, 1, '2019-05-13 11:19:40', 6, '2019-05-13 18:21:34', NULL),
(198, 225, 'c57473a8-ce23-476a-a81d-7b5fa5455a11', 7, 4000, 3000, '2019-05-22', 1000, 1, '2019-05-13 12:34:43', 6, '2019-05-13 12:34:43', NULL),
(199, 226, 'e794b62e-4de4-4eea-baf2-6c050d099c47', 7, 3000, 1500, '2019-06-07', 1500, 1, '2019-05-13 15:00:31', 6, '2019-05-13 15:00:31', NULL),
(200, 227, 'a3c2f8c7-9601-421c-949f-c60f78b7e82b', 9, 4000, 2000, '2019-06-10', 2000, 1, '2019-05-14 11:47:02', 6, '2019-05-14 11:47:02', NULL),
(201, 228, '0c100f74-b578-463a-9e82-53fcd3f2c66f', 9, 4000, 2000, '2019-06-10', 2000, 1, '2019-05-14 11:51:41', 6, '2019-05-14 11:51:41', NULL),
(202, 229, 'd257ac9f-5954-42ba-b356-5835d04ea0d9', 14, 3500, 2000, '2019-06-10', 1500, 1, '2019-05-14 11:57:43', 6, '2019-05-14 11:57:43', NULL),
(203, 230, '375d7efd-b7ab-496c-815f-470096559878', 9, 6000, 2500, '2019-06-10', 2500, 1, '2019-05-14 17:44:54', 6, '2019-05-21 17:45:49', 6),
(204, 231, 'e1375589-b232-488f-83ed-7937afe03234', 9, 4000, 2000, '2019-06-01', 2000, 1, '2019-05-15 11:14:12', 6, '2019-05-16 14:03:26', NULL),
(205, 232, '855d92a5-7e93-42cd-b818-0d19b87730ab', 9, 4000, 0, '2019-05-20', 3000, 1, '2019-05-15 18:32:10', 6, '2019-05-20 18:21:28', 6),
(206, 233, 'e7193187-a3dc-4ac7-a237-a7a0826ba21a', 9, 5000, 0, '2019-05-20', 4000, 1, '2019-05-15 18:35:31', 6, '2019-05-20 18:23:25', 6),
(207, 234, 'faee42a6-0f0b-4a71-b77c-c1f234cda67e', 9, 5000, 0, '2019-05-20', 4000, 1, '2019-05-15 18:39:21', 6, '2019-05-20 18:22:31', 6),
(208, 235, 'df55f3ff-5089-48cd-baff-16913118b671', 7, 5000, 3000, '2019-05-24', 2000, 1, '2019-05-16 13:46:09', 6, '2019-05-16 13:46:09', NULL),
(209, 236, '5d0ba6dc-b3b0-4d8c-9528-d600e0ed598f', 7, 5000, 3000, '2019-05-27', 2000, 1, '2019-05-16 13:50:02', 6, '2019-05-16 13:50:02', NULL),
(210, 237, 'd8c340b9-ea96-429b-b719-6f67d4e89e52', 9, 9000, 0, '2019-05-16', 9000, 1, '2019-05-16 13:54:29', 6, '2019-05-16 13:54:29', NULL),
(211, 238, '7eb2be38-1049-4db0-b6bd-f5b3a9a790e1', 14, 4000, 3000, '2019-06-10', 1000, 1, '2019-05-16 16:44:53', 6, '2019-05-16 16:44:53', NULL),
(212, 239, '143a9b24-6eda-49eb-9f30-49477b0a16e3', 9, 4000, 2000, '2019-06-10', 2000, 1, '2019-05-16 16:47:52', 6, '2019-05-16 16:47:52', NULL),
(213, 240, 'd8dbc814-4d89-48f9-8f45-0ceb6d086b34', 7, 3500, 2000, '2019-06-07', 1500, 1, '2019-05-16 18:27:02', 6, '2019-05-16 18:27:02', NULL),
(214, 241, '1f0fdb05-4726-4023-9934-bee1ce94fb01', 7, 3500, 2000, '2019-06-07', 1500, 1, '2019-05-16 18:29:57', 6, '2019-05-16 18:29:57', NULL),
(215, 242, '6ddc0da1-d73d-434c-b99f-702124061e16', 9, 4000, 3000, '2019-06-15', 1000, 1, '2019-05-17 11:52:03', 6, '2019-05-17 11:52:03', NULL),
(216, 243, '5cb0167e-c163-4756-a22f-e2f06d3ba5c9', 7, 3500, 0, '2019-05-17', 3500, 1, '2019-05-17 12:00:40', 6, '2019-05-17 12:00:40', NULL),
(217, 244, '75cbab82-2438-4dd7-a68f-42dcd164a613', 7, 3500, 2000, '2019-06-07', 1500, 1, '2019-05-17 18:07:13', 6, '2019-05-17 18:07:13', NULL),
(218, 245, '59031e39-2753-422c-bc98-e83c7c146d93', 7, 3500, 2000, '2019-06-07', 1500, 1, '2019-05-17 18:11:42', 6, '2019-05-17 18:11:42', NULL),
(219, 246, '287e33af-10d5-4d83-8a17-24ea46644bd2', 7, 3500, 2000, '2019-06-07', 1500, 1, '2019-05-17 18:16:45', 6, '2019-05-17 18:16:45', NULL),
(220, 247, 'c25caa4f-6b5b-4665-8309-945384644ec6', 7, 5000, 3000, '2019-05-24', 2000, 1, '2019-05-17 18:21:07', 6, '2019-05-17 18:21:07', NULL),
(221, 248, '567c6f70-3fde-4bcc-aa45-f2a53dd8a0fc', 7, 4000, 2000, '2019-06-17', 2000, 1, '2019-05-18 11:32:59', 6, '2019-05-18 11:32:59', NULL),
(222, 249, 'd5e66fb8-c5d7-4966-9e1e-544fd0990be0', 9, 3000, 0, '2019-05-21', 2000, 1, '2019-05-18 12:56:27', 6, '2019-05-21 18:53:01', 6),
(223, 250, 'ffd66c9a-f927-497c-aaf2-0f60866a4184', 7, 4000, 2000, '2019-06-10', 2000, 1, '2019-05-18 13:31:48', 6, '2019-05-18 13:31:48', NULL),
(224, 251, 'ec42a0c0-c8ca-4988-b014-e89c6fe67142', 7, 4000, 1000, '2019-07-01', 3000, 1, '2019-05-18 15:57:36', 6, '2019-05-18 15:57:36', NULL),
(225, 252, 'c90a511d-caa8-4663-9d79-32716b11a508', 7, 3500, 2000, '2019-06-06', 1500, 1, '2019-05-18 16:02:40', 6, '2019-05-18 16:02:40', NULL),
(226, 253, '78db194d-b2af-43de-9a02-026f5909dfa7', 13, 4500, 3000, '2019-06-06', 1500, 1, '2019-05-20 11:11:02', 6, '2019-05-20 11:11:02', NULL),
(227, 254, 'f1b214cc-e912-4759-88a3-0bf8e5c8ce31', 7, 3500, 2000, '2019-06-06', 1500, 1, '2019-05-20 18:31:17', 6, '2019-05-20 18:31:17', NULL),
(228, 255, '08b4bc35-210b-483b-92aa-b525c4da18d0', 7, 3500, 2000, '2019-06-07', 1500, 1, '2019-05-20 18:40:58', 6, '2019-05-20 18:40:58', NULL),
(229, 256, '966873ed-10f2-434e-a5fb-06743eb958a8', 7, 3500, 2000, '2019-06-07', 1500, 1, '2019-05-20 18:43:20', 6, '2019-05-20 18:43:20', NULL),
(230, 257, '8fb18d0a-d7c5-4017-a748-34ca9c057a61', 9, 4500, 0, '2019-05-20', 4500, 1, '2019-05-20 18:46:11', 6, '2019-05-20 18:46:11', NULL),
(231, 258, '43e3d32b-a9ff-45c0-90c9-96e096d1b936', 13, 6000, 4000, '2019-06-03', 2000, 1, '2019-05-20 18:48:43', 6, '2019-05-20 18:48:43', NULL),
(232, 259, '4c3d5ae8-6514-4c52-a91b-a47aed42d3be', 9, 3500, 2500, '2019-06-03', 1000, 1, '2019-05-20 18:51:17', 6, '2019-05-20 18:51:17', NULL),
(233, 260, 'a7802096-5d65-4d07-9c32-ccaa1d3337b7', 9, 18000, 13000, '2019-06-14', 5000, 1, '2019-05-20 19:09:38', 6, '2019-05-20 19:09:38', NULL),
(234, 261, '61a41303-873c-4c20-89a5-8a41fa290683', 7, 3500, 2000, '2019-06-10', 1500, 1, '2019-05-21 12:08:07', 6, '2019-05-21 15:09:47', NULL),
(235, 262, '51351ba9-dcfb-4981-853a-5679e75f08b1', 9, 5000, 3000, '2019-06-03', 2000, 1, '2019-05-21 13:28:58', 6, '2019-05-21 13:28:58', NULL),
(236, 263, '10f9fa06-f608-49f7-b4bd-ec0cbe818b66', 7, 3500, 2500, '2019-06-03', 1000, 1, '2019-05-21 13:36:49', 6, '2019-05-21 13:36:49', NULL),
(237, 264, '5e950a70-89c6-47ff-baa9-7865e59036a9', 7, 3500, 2500, '2019-06-03', 1000, 1, '2019-05-21 13:45:13', 6, '2019-05-21 13:45:13', NULL),
(238, 265, 'b68fcdd0-432c-4a9f-aea2-4869d92b512d', 7, 3500, 2000, '2019-06-10', 1500, 1, '2019-05-21 16:57:22', 6, '2019-05-21 16:57:22', NULL),
(239, 266, '02670fc0-8b04-4a1d-b651-bea3c1b8e350', 14, 6000, 5000, '2019-06-03', 1000, 1, '2019-05-22 11:01:16', 6, '2019-05-22 11:01:16', NULL),
(240, 267, '137f385c-be26-46f7-b16b-8d21e74bf527', 7, 3500, 2000, '2019-06-10', 1500, 1, '2019-05-22 12:35:33', 6, '2019-05-22 12:35:33', NULL),
(241, 268, '826b57e2-a7e2-40ac-98f1-4b0add00a83c', 9, 5000, 4000, '2019-06-20', 1000, 1, '2019-05-22 18:37:29', 6, '2019-05-22 18:37:29', NULL),
(242, 269, 'f7157c62-1a8d-416d-aed9-cfb50be934a6', 9, 5000, 4000, '2019-06-10', 1000, 1, '2019-05-22 18:41:35', 6, '2019-05-22 18:41:35', NULL),
(243, 270, '4f65a30f-8eb3-4682-aef0-cae95a462bc4', 7, 5500, 3500, '2019-06-17', 2000, 1, '2019-05-22 18:45:59', 6, '2019-05-22 18:45:59', NULL),
(244, 271, '9d08be08-a04f-42b0-956f-6f4d47aee513', 13, 14000, 9000, '2019-06-17', 5000, 1, '2019-05-23 10:11:24', 6, '2019-05-23 10:12:44', NULL),
(245, 272, '8475a538-95a9-4099-96f5-3023fcaee9b7', 9, 5000, 4000, '2019-06-03', 1000, 1, '2019-05-23 10:21:20', 6, '2019-05-23 10:21:20', NULL),
(246, 273, '988d7f85-ad8e-40f4-8f9c-13e5b71a5a1f', 14, 5000, 4000, '2019-06-03', 1000, 1, '2019-05-23 10:26:58', 6, '2019-05-23 10:26:58', NULL),
(247, 274, 'df47e652-5e74-4172-9256-3d82891c5f4b', 9, 5000, 4000, '2019-06-03', 1000, 1, '2019-05-23 10:32:32', 6, '2019-05-23 10:32:32', NULL),
(248, 275, '77d176fa-10b8-4d12-80c8-41dc296a6569', 9, 5000, 4000, '2019-06-03', 1000, 1, '2019-05-23 10:37:51', 6, '2019-05-23 10:37:51', NULL),
(249, 276, '72f2b16c-d237-4487-95ab-f30874c74cdd', 13, 8000, 4500, '2019-06-17', 3500, 1, '2019-05-23 10:44:20', 6, '2019-05-23 10:44:20', NULL),
(250, 277, 'a3713adc-2773-4b14-b2c7-4e47ae0eef7e', 7, 4500, 2500, '2019-06-10', 2000, 1, '2019-05-23 13:06:32', 6, '2019-05-23 13:06:32', NULL),
(251, 278, '72e3c02e-0795-490e-8388-05cbbd9bc614', 13, 9000, 7000, '2019-06-03', 2000, 1, '2019-05-23 13:27:15', 6, '2019-05-23 13:27:15', NULL),
(252, 279, '0b73c61f-33cd-46fd-9028-c6da4955136c', 13, 9000, 7000, '2019-06-10', 2000, 1, '2019-05-23 13:29:41', 6, '2019-05-23 13:29:41', NULL),
(253, 280, 'd090ee1f-53dc-430a-8fb0-b4bcbc9f0b89', 13, 9000, 7000, '2019-06-10', 2000, 1, '2019-05-23 13:34:46', 6, '2019-05-23 13:34:46', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `app_inquiry`
--

CREATE TABLE `app_inquiry` (
  `id` int(11) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `gender` varchar(100) NOT NULL,
  `dob` date NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile` varchar(100) NOT NULL,
  `alt_mobile` varchar(100) DEFAULT NULL,
  `address` varchar(255) NOT NULL,
  `pg_address` varchar(255) DEFAULT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `country` varchar(100) NOT NULL,
  `course_name` varchar(255) NOT NULL,
  `course_join_date` date NOT NULL,
  `course_join_time` varchar(100) NOT NULL,
  `refer_by` varchar(100) NOT NULL,
  `refer_name` varchar(100) NOT NULL,
  `enquiry_by` int(11) NOT NULL,
  `status` enum('OPEN','CLOSE','CONVERT','FAILED') NOT NULL DEFAULT 'OPEN',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app_inquiry`
--

INSERT INTO `app_inquiry` (`id`, `first_name`, `last_name`, `gender`, `dob`, `email`, `mobile`, `alt_mobile`, `address`, `pg_address`, `city`, `state`, `country`, `course_name`, `course_join_date`, `course_join_time`, `refer_by`, `refer_name`, `enquiry_by`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Manpreet', 'singh', 'male', '2019-03-22', 'macgadger@outlook.com', '7696200218', NULL, 'SCO 701, Seventh Floor', NULL, 'goraya', 'Punjab', 'india', 'php 6 months', '2019-03-31', '10:00 AM', 'friend', 'sam', 1, 'OPEN', '2019-03-23 04:37:55', '2019-03-23 04:37:55');

-- --------------------------------------------------------

--
-- Table structure for table `app_migrations`
--

CREATE TABLE `app_migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `app_migrations`
--

INSERT INTO `app_migrations` (`id`, `migration`, `batch`) VALUES
(1, '2019_02_15_071445_create_users_table', 1),
(2, '2019_02_15_073504_create_profile_table', 1),
(3, '2019_02_15_073637_create_customers_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `app_payment`
--

CREATE TABLE `app_payment` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `admission_id` varchar(255) NOT NULL,
  `cousellor_id` int(11) NOT NULL,
  `reciept_no` varchar(100) NOT NULL,
  `amount` int(11) NOT NULL,
  `last_due` int(11) NOT NULL,
  `payment_mode` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL,
  `remarks` text,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app_payment`
--

INSERT INTO `app_payment` (`id`, `student_id`, `admission_id`, `cousellor_id`, `reciept_no`, `amount`, `last_due`, `payment_mode`, `type`, `remarks`, `status`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(12, 34, '7e5253eb-4e4d-490c-8c79-e65f037276ca', 7, '4053', 2000, 6000, 'paytm', 'ADVANCE', 'he will pay in two installments.', 1, '2019-04-25 16:39:27', 6, '2019-04-25 16:39:27', NULL),
(13, 35, '10a5f432-5de5-42e4-a8c5-254a5f954828', 7, '4110', 1000, 4000, 'paytm', 'ADVANCE', 'HE WILL PAY IN TWO INSTALMENTS', 1, '2019-04-25 16:48:51', 6, '2019-04-25 16:48:51', NULL),
(14, 36, '37b63498-ad7c-46bd-9f51-18087033fc41', 7, '4111', 1000, 4000, 'paytm', 'ADVANCE', '', 1, '2019-04-25 17:42:55', 6, '2019-04-25 17:42:55', NULL),
(15, 37, '4cb53ae0-46ae-4393-b239-044aeed070c2', 7, '4112', 1000, 4000, 'paytm', 'ADVANCE', '', 1, '2019-04-25 17:45:29', 6, '2019-04-25 17:45:29', NULL),
(16, 38, '94569776-c9ae-4e6d-a946-5348e609013c', 7, '4113', 1000, 4000, 'paytm', 'ADVANCE', '', 1, '2019-04-25 17:53:03', 6, '2019-04-25 17:53:03', NULL),
(17, 39, '8b1ce920-06fc-48b3-b5e4-d9516de0799a', 7, '4115', 1000, 4000, 'paytm', 'ADVANCE', '', 1, '2019-04-25 17:57:31', 6, '2019-04-25 17:57:31', NULL),
(18, 40, '97c8a4c7-0f08-42a0-a0ef-7ee070c46514', 7, '4118', 2000, 3000, 'cash', 'ADVANCE', '', 1, '2019-04-25 18:09:56', 6, '2019-04-25 18:09:56', NULL),
(19, 41, '32dd28d2-7d71-48df-9440-356c1503635d', 7, '4119', 2000, 3000, 'cash', 'ADVANCE', '', 1, '2019-04-25 18:15:15', 6, '2019-04-25 18:15:15', NULL),
(20, 42, 'bcab8c6d-1f3b-48f2-8836-fc4babfaee15', 7, '4120', 2000, 3000, 'cash', 'ADVANCE', '', 1, '2019-04-29 16:13:45', 6, '2019-04-29 16:13:45', NULL),
(21, 43, '48dff572-1541-4e23-ad4b-98a392f761a8', 7, '4121', 2000, 3000, 'cash', 'ADVANCE', '', 1, '2019-04-29 16:17:59', 6, '2019-04-29 16:17:59', NULL),
(22, 44, 'd6550116-e524-4d7f-a3a2-f0eb81b1ba8a', 9, '4135', 1000, 3000, 'paytm', 'ADVANCE', '', 1, '2019-04-29 16:25:30', 6, '2019-04-29 16:25:30', NULL),
(23, 45, 'e66163a5-3368-4f9a-ba37-418454879358', 7, '4092', 1000, 5000, 'online', 'ADVANCE', '', 1, '2019-04-29 16:30:13', 6, '2019-04-29 16:30:13', NULL),
(24, 46, '65ab8576-9097-48a9-8396-6c474c238351', 7, '4139', 500, 5000, 'cash', 'ADVANCE', '', 1, '2019-04-29 16:39:43', 6, '2019-04-29 16:39:43', NULL),
(25, 47, 'ed8ee1e9-a26b-4263-b7b9-3781a7eb0f49', 7, '4140', 500, 5000, 'cash', 'ADVANCE', '', 1, '2019-04-29 17:36:31', 6, '2019-04-29 17:36:31', NULL),
(26, 48, 'afb89688-652b-4a53-997b-941c857c2b00', 7, '4141', 1000, 4500, 'gpay', 'ADVANCE', '', 1, '2019-04-29 17:45:51', 6, '2019-04-29 17:45:51', NULL),
(27, 49, '2c64b5da-a520-4560-90b7-d3f35c371c93', 7, '4142', 1000, 4500, 'cash', 'ADVANCE', '', 1, '2019-04-29 17:52:07', 6, '2019-04-29 17:52:07', NULL),
(28, 50, '1d0dc134-1250-4015-83e2-6a9cd6a31f82', 7, '4138', 500, 5000, 'cash', 'ADVANCE', '', 1, '2019-04-29 17:56:49', 6, '2019-04-29 17:56:49', NULL),
(29, 51, '1b1d9e9e-6a68-4b53-8566-05e70cdcd2aa', 9, '4096', 2000, 3000, 'online', 'ADVANCE', '', 1, '2019-04-29 18:01:13', 6, '2019-04-29 18:01:13', NULL),
(30, 52, '866abc84-91c6-4100-a79a-af0675ce5e12', 7, '4144', 500, 7500, 'cash', 'ADVANCE', 'he will pay in two installments', 1, '2019-04-29 18:05:54', 6, '2019-04-29 18:05:54', NULL),
(31, 53, '60a195ad-264c-4d7d-929c-247ce4ee5f5e', 9, '4095', 2000, 3000, 'online', 'ADVANCE', '', 1, '2019-04-30 16:43:10', 6, '2019-04-30 16:43:10', NULL),
(32, 54, '8b5e5db4-1823-47e6-9854-2b139561df42', 9, '4166', 1000, 3000, 'paytm', 'ADVANCE', '', 1, '2019-04-30 16:51:31', 6, '2019-04-30 16:51:31', NULL),
(33, 55, '24d89c1a-8691-44c2-8d19-2e7769ba2aad', 12, '4167', 1000, 7000, 'paytm', 'ADVANCE', '', 1, '2019-04-30 17:05:53', 6, '2019-04-30 17:05:53', NULL),
(34, 56, '84ba54e3-efc4-43ec-b2f9-2f8b36875fa4', 9, '4169', 1000, 4000, 'paytm', 'ADVANCE', '', 1, '2019-04-30 17:12:01', 6, '2019-04-30 17:12:01', NULL),
(35, 57, 'b9ba3767-519b-4a93-b1e9-7923577a3a4c', 9, '4170', 1000, 4000, 'paytm', 'ADVANCE', '', 1, '2019-04-30 17:16:07', 6, '2019-04-30 17:16:07', NULL),
(36, 58, '14111cf9-9495-43f4-941c-baed158a3d5e', 9, '4173', 1000, 4000, 'cash', 'ADVANCE', '', 1, '2019-04-30 17:19:26', 6, '2019-04-30 17:19:26', NULL),
(37, 59, 'c85c1234-778e-4135-a592-375e96aeadc7', 9, '4174', 1000, 4000, 'cash', 'ADVANCE', '', 1, '2019-04-30 17:55:31', 6, '2019-04-30 17:55:31', NULL),
(38, 60, '5891c114-6e40-4fbc-a567-c0d9cb787aed', 9, '4172', 1000, 4000, 'cash', 'ADVANCE', '', 1, '2019-04-30 17:59:31', 6, '2019-04-30 17:59:31', NULL),
(39, 61, 'bdd9203c-7b15-4cb9-a573-8d93a3eac6ea', 13, '4206', 3000, 0, 'cash', 'ADVANCE', '', 1, '2019-04-30 18:07:52', 6, '2019-04-30 18:07:52', NULL),
(40, 62, 'bcc7a276-38fd-4217-b86c-480331a727dc', 9, '4207', 1000, 3000, 'paytm', 'ADVANCE', '', 1, '2019-04-30 18:16:40', 6, '2019-04-30 18:16:40', NULL),
(41, 63, '27059363-46f9-4534-b226-405465efe2de', 9, '4209', 1000, 4000, 'paytm', 'ADVANCE', '', 1, '2019-04-30 18:24:47', 6, '2019-04-30 18:24:47', NULL),
(42, 64, 'a4516a26-b3cb-41d7-ad07-0832cc273f95', 9, '4210', 1000, 4000, 'cash', 'ADVANCE', '', 1, '2019-04-30 18:29:42', 6, '2019-04-30 18:29:42', NULL),
(43, 65, '7622a286-134d-4fc8-9ee8-0e882ae741fd', 9, '4211', 1000, 4000, 'gpay', 'ADVANCE', '', 1, '2019-04-30 18:36:16', 6, '2019-04-30 18:36:16', NULL),
(44, 66, '6c81ca44-4781-4e97-9646-c0e07868bde1', 9, '4213', 1000, 4000, 'paytm', 'ADVANCE', '', 1, '2019-04-30 18:41:13', 6, '2019-04-30 18:41:13', NULL),
(45, 67, 'ebb005bc-168a-4ba4-bb5e-d096839f06a6', 7, '4195', 2000, 3000, 'paytm', 'ADVANCE', '', 1, '2019-04-30 18:46:43', 6, '2019-04-30 18:46:43', NULL),
(46, 68, '0e909d28-35c6-45ec-b276-ae21e67f454a', 7, '4196', 2000, 3000, 'paytm', 'ADVANCE', '', 1, '2019-05-01 13:57:38', 6, '2019-05-01 13:57:38', NULL),
(47, 69, '2ee5ff80-b249-4304-8d4d-2ca037414558', 7, '4199', 1000, 4500, 'paytm', 'ADVANCE', '', 1, '2019-05-01 14:05:35', 6, '2019-05-01 14:05:35', NULL),
(48, 70, '5a5bf019-9d5a-4374-855e-ed630a73dc8d', 9, '4218', 1000, 4000, 'paytm', 'ADVANCE', '', 1, '2019-05-01 14:10:37', 6, '2019-05-01 14:10:37', NULL),
(49, 71, 'b321fae4-8e84-41df-b159-aecbbab674de', 7, '4217', 2000, 3000, 'paytm', 'ADVANCE', '', 1, '2019-05-02 12:43:31', 6, '2019-05-02 12:43:31', NULL),
(50, 72, '5267a7c7-3ef0-45bf-aa0f-6c4b4879bc7e', 9, '4219', 5000, 0, 'paytm', 'ADVANCE', '', 1, '2019-05-02 12:55:24', 6, '2019-05-02 12:55:24', NULL),
(51, 73, '13486acf-c62d-40ec-af1f-feaac0c01be2', 9, '4220', 1000, 4000, 'paytm', 'ADVANCE', '', 1, '2019-05-02 13:02:42', 6, '2019-05-02 13:02:42', NULL),
(52, 74, '2e605f49-9101-4c1d-934c-dea896a1d7d7', 10, '4301', 2000, 2000, 'cash', 'ADVANCE', '', 1, '2019-05-02 13:18:22', 6, '2019-05-02 13:18:22', NULL),
(53, 75, '55c40eb7-e2b9-4f39-b68e-5c959aa419e5', 10, '4302', 2000, 2000, 'cash', 'ADVANCE', '', 1, '2019-05-02 13:40:01', 6, '2019-05-02 13:40:01', NULL),
(54, 76, '390b92c9-f346-441d-bbd4-9b9299e304bd', 7, '4303', 2000, 2500, 'cash', 'ADVANCE', '', 1, '2019-05-02 13:54:36', 6, '2019-05-02 13:54:36', NULL),
(55, 77, '1909249d-436f-4bf6-a265-0cdf0b0d10fd', 10, '4305', 2000, 2000, 'cash', 'ADVANCE', '', 1, '2019-05-02 14:04:16', 6, '2019-05-02 14:04:16', NULL),
(56, 78, 'b51f4b28-34c3-4a58-84bf-1ca5aa3652cb', 10, '4306', 1000, 3000, 'online', 'ADVANCE', '', 1, '2019-05-03 15:44:20', 6, '2019-05-03 15:44:20', NULL),
(57, 79, '9d0d80fe-49c2-4dd7-9e52-238f5fe496ef', 10, '4307', 1000, 3000, 'cash', 'ADVANCE', '', 1, '2019-05-03 16:22:32', 6, '2019-05-03 16:22:32', NULL),
(58, 80, '3a73cc4b-66a7-48da-a53c-8139942d94cd', 7, '4308', 2000, 4000, 'paytm', 'ADVANCE', '', 1, '2019-05-03 16:31:37', 6, '2019-05-03 16:31:37', NULL),
(59, 81, 'd1e378f2-8e62-4a51-bf92-bb9428be0eef', 7, '4309', 2000, 4000, 'online', 'ADVANCE', '', 1, '2019-05-03 16:36:43', 6, '2019-05-03 16:36:43', NULL),
(60, 82, 'dbc720fe-60e0-4124-84b6-1b3fabbe0226', 7, '4312', 2000, 2000, 'online', 'ADVANCE', '', 1, '2019-05-03 16:43:54', 6, '2019-05-03 16:43:54', NULL),
(61, 83, 'b522299e-1b88-435a-af04-53fdc4769275', 10, '4316', 1000, 3000, 'online', 'ADVANCE', '', 1, '2019-05-03 16:47:42', 6, '2019-05-03 16:47:42', NULL),
(62, 84, '82ee089e-1c7a-44e5-9cf2-7a366524dd6b', 10, '4317', 1000, 3000, 'cash', 'ADVANCE', '', 1, '2019-05-03 17:33:03', 6, '2019-05-03 17:33:03', NULL),
(63, 85, 'ec572308-3559-4ae7-b10c-a2876e0c5d33', 9, '4323', 3500, 3500, 'paytm', 'ADVANCE', '', 1, '2019-05-03 17:40:01', 6, '2019-05-03 17:40:01', NULL),
(64, 86, '211c1043-45c2-42b1-986c-f4d8cf7295e1', 7, '4114', 1000, 4000, 'paytm', 'ADVANCE', '', 1, '2019-05-04 10:16:31', 6, '2019-05-04 10:16:31', NULL),
(65, 87, 'ddf9d4ee-7dd7-4910-a14f-6fc962f04b56', 14, '4218', 1000, 4000, 'paytm', 'ADVANCE', '', 1, '2019-05-04 10:24:39', 6, '2019-05-04 10:24:39', NULL),
(66, 88, 'f9c6c994-51c4-4bc9-8ec5-49fa1756a7bc', 9, '4324', 1000, 4000, 'paytm', 'ADVANCE', '', 1, '2019-05-04 10:36:01', 6, '2019-05-04 10:36:01', NULL),
(67, 89, '1f1a406f-2412-4ca4-85d3-052cbf6df19a', 10, '4223', 2000, 3000, 'online', 'ADVANCE', '', 1, '2019-05-04 10:48:48', 6, '2019-05-04 10:48:48', NULL),
(68, 90, '35ceb37a-e5ce-4dc9-a7a4-ac4370de0ea3', 10, '4224', 2000, 2000, 'cash', 'ADVANCE', '', 1, '2019-05-04 10:54:27', 6, '2019-05-04 10:54:27', NULL),
(69, 91, '8bb119c2-7400-4138-a597-d3c7cd8bd4f2', 13, '4225', 1000, 2000, 'cash', 'ADVANCE', '', 1, '2019-05-04 10:59:24', 6, '2019-05-04 10:59:24', NULL),
(70, 92, '7fcac2a5-e1f3-4206-861b-d6d48e905181', 10, '4325', 1000, 3000, 'online', 'ADVANCE', '', 1, '2019-05-04 11:04:49', 6, '2019-05-04 11:04:49', NULL),
(71, 93, 'a8c4d4d7-ff73-4bac-bf86-fc5b48dcc9a7', 10, '4326', 1000, 4000, 'online', 'ADVANCE', '', 1, '2019-05-04 11:10:12', 6, '2019-05-04 11:10:12', NULL),
(72, 94, '0c089aae-a9b7-4030-a33b-cf0381627389', 7, '4228', 2000, 3000, 'paytm', 'ADVANCE', '', 1, '2019-05-04 11:16:10', 6, '2019-05-04 11:16:10', NULL),
(73, 95, '74138940-2484-4491-bddb-386142c41e09', 9, '4230', 1000, 4000, 'gpay', 'ADVANCE', '', 1, '2019-05-04 11:22:02', 6, '2019-05-04 11:22:02', NULL),
(74, 96, 'cfb4af32-83e9-4e3e-b114-7d88b7c76798', 9, '4231', 1000, 4000, 'gpay', 'ADVANCE', '', 1, '2019-05-04 11:26:06', 6, '2019-05-04 11:26:06', NULL),
(75, 97, '2bb1265f-6801-4451-9102-0b6129230573', 7, '4330', 1000, 5000, 'cash', 'ADVANCE', '', 1, '2019-05-04 11:35:19', 6, '2019-05-04 11:35:19', NULL),
(76, 98, '73423bd4-470a-454b-a581-3a09709a475a', 7, '4331', 2000, 4000, 'online', 'ADVANCE', '', 1, '2019-05-04 11:47:25', 6, '2019-05-04 11:47:25', NULL),
(77, 99, '143aa09b-f898-486e-a915-136719f2be48', 7, '4332', 2000, 4000, 'online', 'ADVANCE', '', 1, '2019-05-04 11:54:28', 6, '2019-05-04 11:54:28', NULL),
(78, 100, 'bd98b5c6-85b2-462b-ab8c-d6e8b7e8d011', 13, '4333', 2000, 3500, 'gpay', 'ADVANCE', '', 1, '2019-05-04 11:59:53', 6, '2019-05-04 11:59:53', NULL),
(79, 101, 'a873bcba-e6a0-4091-a7e8-2cdf4c6502ee', 9, '4232', 2500, 0, 'paytm', 'ADVANCE', '', 1, '2019-05-04 12:07:49', 6, '2019-05-04 12:07:49', NULL),
(80, 102, '2be244df-99c6-453d-9da2-13afb7959ea5', 9, '4233', 2500, 0, 'paytm', 'ADVANCE', '', 1, '2019-05-04 12:12:31', 6, '2019-05-04 12:12:31', NULL),
(81, 103, 'de000390-b862-45a4-aebe-40f5590fe6d0', 9, '4234', 2500, 0, 'paytm', 'ADVANCE', '', 1, '2019-05-04 12:19:22', 6, '2019-05-04 12:19:22', NULL),
(82, 104, '60f61c7b-2daf-43b2-819e-4db653522800', 7, '4334', 2000, 6000, 'gpay', 'ADVANCE', '', 1, '2019-05-04 12:28:07', 6, '2019-05-04 12:28:07', NULL),
(83, 105, '00a64010-ef1b-4239-b105-1913e5bd47cc', 9, '4240', 1000, 4000, 'online', 'ADVANCE', '', 1, '2019-05-04 12:36:59', 6, '2019-05-04 12:36:59', NULL),
(84, 106, '440fd186-62d3-4dd0-aa43-371672ba3ece', 9, '4241', 2500, 0, 'gpay', 'ADVANCE', '', 1, '2019-05-04 12:41:23', 6, '2019-05-04 12:41:23', NULL),
(85, 107, '1c4524c3-437e-451f-a4e7-86cd599d39af', 9, '4242', 2500, 0, 'gpay', 'ADVANCE', '', 1, '2019-05-04 12:47:03', 6, '2019-05-04 12:47:03', NULL),
(86, 108, '95562174-826c-46ce-904c-6a6c70089c82', 9, '4243', 2500, 0, 'gpay', 'ADVANCE', '', 1, '2019-05-04 12:56:02', 6, '2019-05-04 12:56:02', NULL),
(87, 109, 'f75660d5-631d-45d6-b796-ec2aff6e2e68', 9, '4244', 1000, 4000, 'cash', 'ADVANCE', '', 1, '2019-05-04 13:03:32', 6, '2019-05-04 13:03:32', NULL),
(88, 110, '1f11a6ac-753b-4d8d-a81c-35f064e1ed86', 9, '4245', 2500, 0, 'gpay', 'ADVANCE', '', 1, '2019-05-04 13:06:56', 6, '2019-05-04 13:06:56', NULL),
(89, 111, '080f74ef-a279-41f4-a9cd-675e7eaf85f1', 7, '4339', 1000, 2500, 'paytm', 'ADVANCE', '', 1, '2019-05-04 13:09:49', 6, '2019-05-04 13:09:49', NULL),
(90, 112, 'cb9a4acd-8fbe-4112-9061-5a7c1b339266', 7, '4340', 1000, 2500, 'paytm', 'ADVANCE', '', 1, '2019-05-04 13:14:08', 6, '2019-05-04 13:14:08', NULL),
(91, 113, 'c9de264e-ecd4-4063-8cbb-041a98c0f96d', 7, '4341', 3500, 0, 'paytm', 'ADVANCE', '', 1, '2019-05-04 13:17:10', 6, '2019-05-04 13:17:10', NULL),
(92, 114, '03efc270-7c1b-49fc-b2b3-6c715232c4da', 9, '4246', 2500, 0, 'gpay', 'ADVANCE', '', 1, '2019-05-04 13:20:39', 6, '2019-05-04 13:20:39', NULL),
(93, 115, '6c01d349-1865-40cb-a582-268f9a26db54', 9, '4247', 2500, 0, 'gpay', 'ADVANCE', '', 1, '2019-05-04 13:23:15', 6, '2019-05-04 13:23:15', NULL),
(94, 116, 'c5dcc609-0248-40c4-997b-16957f673057', 9, '4248', 2500, 0, 'gpay', 'ADVANCE', '', 1, '2019-05-04 13:28:09', 6, '2019-05-04 13:28:09', NULL),
(95, 117, '847b3e35-89bb-4a42-9c10-88cf799826d3', 9, '4249', 2500, 0, 'gpay', 'ADVANCE', '', 1, '2019-05-04 13:31:33', 6, '2019-05-04 13:31:33', NULL),
(96, 118, '39ea8792-38aa-4dee-b757-3ab6c6aa0c73', 9, '4250', 2500, 0, 'gpay', 'ADVANCE', '', 1, '2019-05-04 13:34:17', 6, '2019-05-04 13:34:17', NULL),
(97, 119, '4397135b-ed60-400f-9139-b6d9654728f6', 7, '4342', 2000, 2000, 'paytm', 'ADVANCE', '', 1, '2019-05-04 13:38:36', 6, '2019-05-04 13:38:36', NULL),
(98, 120, '84210973-f0bb-4203-94e2-cb1e83f4bddb', 10, '4343', 1000, 2000, 'cash', 'ADVANCE', '', 1, '2019-05-04 13:44:29', 6, '2019-05-04 13:44:29', NULL),
(99, 121, '7cd67f19-5435-4b33-8829-24c9cd6eac20', 9, '4344', 1000, 4000, 'paytm', 'ADVANCE', '', 1, '2019-05-04 16:56:01', 6, '2019-05-04 16:56:01', NULL),
(100, 109, 'f75660d5-631d-45d6-b796-ec2aff6e2e68', 9, '4408', 1500, 2500, 'paytm', 'FEE', '', 1, '2019-05-04 16:59:05', 6, '2019-05-04 16:59:05', NULL),
(101, 122, '8d6db807-d246-4e5a-800e-1901cc45daea', 7, '4345', 500, 3500, 'online', 'ADVANCE', '', 1, '2019-05-04 18:18:11', 6, '2019-05-04 18:18:11', NULL),
(102, 123, '49707202-71d7-41fb-b219-9b6b33ee876d', 7, '4346', 500, 3500, 'online', 'ADVANCE', '', 1, '2019-05-04 18:49:36', 6, '2019-05-04 18:49:36', NULL),
(103, 124, '7c4d1259-d71d-40a5-942d-10641c141f0e', 14, '4255', 1000, 4000, 'gpay', 'ADVANCE', '', 1, '2019-05-06 10:06:08', 6, '2019-05-06 10:06:08', NULL),
(104, 125, 'bb5b38ff-2d86-4cd1-ae2e-7cf7ed951af8', 14, '4256', 1000, 4000, 'gpay', 'ADVANCE', '', 1, '2019-05-06 10:13:38', 6, '2019-05-06 10:13:38', NULL),
(105, 126, 'f35c05a1-49be-4650-be3b-705368f00e34', 14, '4257', 1000, 4000, 'gpay', 'ADVANCE', '', 1, '2019-05-06 10:19:35', 6, '2019-05-06 10:19:35', NULL),
(106, 127, '26ddf83a-a879-4596-b161-822ad61c2bba', 9, '4347', 3000, 0, 'online', 'ADVANCE', '', 1, '2019-05-06 10:49:19', 6, '2019-05-06 10:49:19', NULL),
(107, 128, 'e64916b1-fe7f-471d-a39f-7b7d3697879a', 7, '4348', 3000, 0, 'online', 'ADVANCE', '', 1, '2019-05-06 11:03:43', 6, '2019-05-06 11:03:43', NULL),
(108, 129, '9ce7abf6-3388-46d9-8eab-719a0ddf3b85', 9, '4258', 2500, 0, 'gpay', 'ADVANCE', '', 1, '2019-05-06 11:12:29', 6, '2019-05-06 11:12:29', NULL),
(109, 130, 'a6462502-65d2-44c7-8cea-c964abb2331a', 7, '4349', 3500, 0, 'cash', 'ADVANCE', '', 1, '2019-05-06 11:27:37', 6, '2019-05-06 11:27:37', NULL),
(110, 131, '54b2a19a-c061-4b71-966a-97dd812d0d16', 7, '4351', 1000, 2000, 'cash', 'ADVANCE', '', 1, '2019-05-06 11:34:41', 6, '2019-05-06 11:34:41', NULL),
(111, 132, '1b86bd8c-824b-412c-942b-50e806467ab3', 7, '4352', 1000, 2000, 'cash', 'ADVANCE', '', 1, '2019-05-06 11:38:35', 6, '2019-05-06 11:38:35', NULL),
(112, 133, 'a267a9a5-6b20-45b6-837f-a03e92cbf8f0', 7, '4264', 1000, 3500, 'cash', 'ADVANCE', '', 1, '2019-05-06 11:42:29', 6, '2019-05-06 11:42:29', NULL),
(113, 134, '63cf0374-736c-44a0-b3b7-9d730c316869', 14, '4355', 2000, 3000, 'paytm', 'ADVANCE', '', 1, '2019-05-06 11:46:42', 6, '2019-05-06 11:46:42', NULL),
(114, 135, '7186d53e-2df6-4846-a729-48e2d897d540', 14, '4356', 4000, 0, 'cash', 'ADVANCE', '', 1, '2019-05-06 11:51:20', 6, '2019-05-06 11:51:20', NULL),
(115, 136, '3614749b-ec22-476b-a8b7-7650cd3e71fd', 9, '4357', 1000, 3000, 'paytm', 'ADVANCE', '', 1, '2019-05-06 11:59:46', 6, '2019-05-06 11:59:46', NULL),
(116, 137, '66fc4fe8-8aa2-47b7-8d20-6a84ea815f66', 9, '4358', 2500, 0, 'gpay', 'ADVANCE', '', 1, '2019-05-06 12:03:38', 6, '2019-05-06 12:03:38', NULL),
(117, 138, 'eba8cabd-975b-48fe-9c24-1e9bda4e4cdb', 14, '4359', 2000, 2000, 'paytm', 'ADVANCE', '', 1, '2019-05-06 12:16:58', 6, '2019-05-06 12:16:58', NULL),
(118, 139, '7c11ed1a-9a77-4d59-b24f-e77bba43de30', 14, '4360', 2000, 2000, 'cash', 'ADVANCE', '', 1, '2019-05-06 12:26:16', 6, '2019-05-06 12:26:16', NULL),
(119, 140, '80aea40d-f150-4707-9388-1acde773cb61', 9, '4268', 2500, 0, 'gpay', 'ADVANCE', '', 1, '2019-05-06 12:32:22', 6, '2019-05-06 12:32:22', NULL),
(120, 141, '028629f8-47a3-4b40-97ea-2132e3d37f42', 9, '4361', 1000, 3000, 'paytm', 'ADVANCE', '', 1, '2019-05-06 12:35:48', 6, '2019-05-06 12:35:48', NULL),
(121, 142, 'b3be6f9f-3ae1-409a-a79e-f258d1d5724e', 10, '4365', 1000, 3000, 'cash', 'ADVANCE', '', 1, '2019-05-06 12:39:31', 6, '2019-05-06 12:39:31', NULL),
(122, 143, '0acdd244-af77-4106-b569-2476c2718c47', 9, '4271', 2500, 0, 'gpay', 'ADVANCE', '', 1, '2019-05-06 12:43:04', 6, '2019-05-06 12:43:04', NULL),
(123, 144, '16707d80-71df-4d67-9d87-8b09616ffc21', 9, '4272', 2500, 0, 'gpay', 'ADVANCE', '', 1, '2019-05-06 12:58:26', 6, '2019-05-06 12:58:26', NULL),
(124, 145, '39972669-4efd-44e2-9942-78aa8a1a494a', 9, '4273', 2500, 0, 'gpay', 'ADVANCE', '', 1, '2019-05-06 13:01:59', 6, '2019-05-06 13:01:59', NULL),
(125, 146, '6577ded5-e159-4830-809f-3daee96c6fef', 10, '4367', 1000, 3000, 'cash', 'ADVANCE', '', 1, '2019-05-06 13:05:55', 6, '2019-05-06 13:05:55', NULL),
(126, 147, '047e56f4-8d80-44c5-9a08-09b5c4994e54', 10, '4369', 1500, 1500, 'paytm', 'ADVANCE', '', 1, '2019-05-06 13:09:27', 6, '2019-05-06 13:09:27', NULL),
(127, 148, 'f33823f6-4f6b-4377-9b85-c330d29478d5', 9, '4274', 2000, 2000, 'cash', 'ADVANCE', '', 1, '2019-05-06 13:24:44', 6, '2019-05-06 13:24:44', NULL),
(128, 149, 'f2a84306-f0be-42f4-8b3f-493ccbdc5fdf', 10, '4275', 1000, 3000, 'cash', 'ADVANCE', '', 1, '2019-05-06 13:34:56', 6, '2019-05-06 13:34:56', NULL),
(129, 150, 'cb566a16-f2e1-4af7-b9d8-719e3436464e', 10, '4276', 1000, 3000, 'paytm', 'ADVANCE', '', 1, '2019-05-06 13:40:17', 6, '2019-05-06 13:40:17', NULL),
(130, 151, 'afc25887-281a-457c-87eb-3fbdea748b2f', 9, '4371', 2500, 0, 'gpay', 'ADVANCE', '', 1, '2019-05-06 14:00:45', 6, '2019-05-06 14:00:45', NULL),
(131, 152, '9bae2b63-145d-4019-9bd9-8949cb3a5ff4', 9, '4373', 2000, 2000, 'paytm', 'ADVANCE', '', 1, '2019-05-06 14:16:43', 6, '2019-05-06 14:16:43', NULL),
(132, 153, '7ecf98a2-e191-4348-b645-a1f4740f6aec', 7, '4277', 2000, 0, 'online', 'ADVANCE', '', 1, '2019-05-06 15:28:28', 6, '2019-05-06 15:28:28', NULL),
(133, 154, 'b2bc6e74-5919-413e-a87f-30af2ee5b75e', 10, '4278', 1000, 3000, 'paytm', 'ADVANCE', '', 1, '2019-05-06 15:49:56', 6, '2019-05-06 15:49:56', NULL),
(134, 155, '0819d294-d64c-4c84-b5c1-9a5b0fa99f3b', 7, '4279', 1000, 3000, 'paytm', 'ADVANCE', '', 1, '2019-05-08 11:15:34', 6, '2019-05-08 11:15:34', NULL),
(135, 156, 'c136cb8a-4bca-47b4-91f9-09a288dce946', 7, '4280', 1000, 3000, 'paytm', 'ADVANCE', '', 1, '2019-05-08 11:25:18', 6, '2019-05-08 11:25:18', NULL),
(136, 157, '558f004c-2f92-4474-bf38-a231c553e4b3', 7, '4281', 1000, 3000, 'paytm', 'ADVANCE', '', 1, '2019-05-08 11:31:24', 6, '2019-05-08 11:31:24', NULL),
(137, 158, '006e5745-edb6-442d-915f-326c55d9f3f6', 9, '4284', 1000, 3000, 'paytm', 'ADVANCE', '', 1, '2019-05-08 11:43:45', 6, '2019-05-08 11:43:45', NULL),
(138, 159, '1ac8fd2b-60a7-4af7-9014-281247a5aee4', 7, '4374', 2000, 1500, 'gpay', 'ADVANCE', '', 1, '2019-05-08 11:53:39', 6, '2019-05-08 11:53:39', NULL),
(139, 160, 'e27740f6-da8f-41d5-b918-10a1cfadb92f', 10, '4375', 5000, 0, 'cash', 'ADVANCE', '', 1, '2019-05-08 12:00:22', 6, '2019-05-08 12:00:22', NULL),
(140, 161, 'd7b2ec58-ab83-4f31-af03-8b83c2963750', 7, '4376', 1000, 4000, 'cash', 'ADVANCE', '', 1, '2019-05-08 12:05:39', 6, '2019-05-08 12:05:39', NULL),
(141, 162, '9fba6d50-e975-4ddf-9e96-a39fe363041f', 7, '4377', 1000, 4000, 'cash', 'ADVANCE', '', 1, '2019-05-08 12:10:37', 6, '2019-05-08 12:10:37', NULL),
(142, 163, '0810c525-6db1-4d92-b473-b12942fd3422', 10, '4378', 5000, 0, 'cash', 'ADVANCE', '', 1, '2019-05-08 12:17:49', 6, '2019-05-08 12:17:49', NULL),
(143, 164, 'd7033852-8fb0-466e-b33b-5293f4a4fafd', 10, '4379', 1000, 5000, 'cash', 'ADVANCE', '', 1, '2019-05-08 12:27:46', 6, '2019-05-08 12:27:46', NULL),
(144, 165, 'a467c7a9-652f-48d0-934b-6ddec6e0c7e3', 10, '4380', 1000, 5000, 'cash', 'ADVANCE', '', 1, '2019-05-08 12:43:58', 6, '2019-05-08 12:43:58', NULL),
(145, 166, '0d28bc33-7bb6-4bf6-8afe-ce67bd9b0e0e', 10, '4381', 1000, 6000, 'cash', 'ADVANCE', '', 1, '2019-05-08 12:49:53', 6, '2019-05-08 12:49:53', NULL),
(146, 167, 'ff24b346-73c3-4d9a-8d2c-7d68df16ee97', 10, '4382', 1000, 5000, 'cash', 'ADVANCE', '', 1, '2019-05-08 13:02:08', 6, '2019-05-08 13:02:08', NULL),
(147, 168, '6f630443-3d1b-4eb6-a22b-a469c528e20f', 10, '4383', 1000, 3000, 'cash', 'ADVANCE', '', 1, '2019-05-08 13:12:21', 6, '2019-05-08 13:12:21', NULL),
(148, 169, 'c427596e-93c1-46e8-ad42-198732745076', 10, '4384', 1000, 3000, 'cash', 'ADVANCE', '', 1, '2019-05-08 13:27:21', 6, '2019-05-08 13:27:21', NULL),
(149, 170, '6a990eb8-29e4-4486-bfd3-470d28d0d0da', 14, '4385', 1000, 5000, 'online', 'ADVANCE', '', 1, '2019-05-08 13:41:28', 6, '2019-05-08 13:41:28', NULL),
(150, 171, '058c17c1-6e10-4c01-b433-b844c6ec5aee', 10, '4386', 1000, 2000, 'paytm', 'ADVANCE', '', 1, '2019-05-08 13:47:40', 6, '2019-05-08 13:47:40', NULL),
(151, 172, 'c7bdbc57-8ab0-4b61-b63e-42920482fbaa', 9, '4387', 1000, 2000, 'paytm', 'ADVANCE', '', 1, '2019-05-09 10:56:06', 6, '2019-05-09 10:56:06', NULL),
(152, 173, 'bf1f4250-6ff4-49eb-b51b-ca740a0dbf08', 7, '4292', 1000, 5500, 'cash', 'ADVANCE', '', 1, '2019-05-09 11:03:27', 6, '2019-05-09 11:03:27', NULL),
(153, 174, 'c4a1749b-1360-4cb4-a9f0-9db699667d44', 7, '4293', 1000, 5500, 'cash', 'ADVANCE', '', 1, '2019-05-09 11:11:45', 6, '2019-05-09 11:11:45', NULL),
(154, 175, '29aeabbe-af19-4acd-8152-54aed1a5a6b5', 7, '4294', 1000, 5500, 'cash', 'ADVANCE', '', 1, '2019-05-09 11:18:23', 6, '2019-05-09 11:18:23', NULL),
(155, 176, '150e512f-20ac-41aa-82a8-61cc7c1ca11c', 7, '4295', 500, 6000, 'cash', 'ADVANCE', '', 1, '2019-05-09 11:25:03', 6, '2019-05-09 11:25:03', NULL),
(156, 177, '465ec25f-7405-473b-9711-adf30b4827e0', 9, '4389', 1000, 4000, 'paytm', 'ADVANCE', '', 1, '2019-05-09 11:32:23', 6, '2019-05-09 11:32:23', NULL),
(157, 178, '6182469f-eda6-4088-8611-395efc4e780a', 10, '4296', 1000, 3000, 'gpay', 'ADVANCE', '', 1, '2019-05-09 11:40:26', 6, '2019-05-09 11:40:26', NULL),
(158, 179, '8dca276c-81cf-462f-a1b1-39545d84c457', 10, '4297', 1000, 3000, 'gpay', 'ADVANCE', '', 1, '2019-05-09 11:47:10', 6, '2019-05-09 11:47:10', NULL),
(159, 180, 'f9666d28-6c52-4bb4-a2b9-dafcd0526e52', 10, '4298', 1000, 3000, 'gpay', 'ADVANCE', '', 1, '2019-05-09 12:15:53', 6, '2019-05-09 12:15:53', NULL),
(160, 181, 'e1d97f93-f81a-4892-b383-b44cbf1eff53', 10, '4299', 1000, 6000, 'paytm', 'ADVANCE', '', 1, '2019-05-09 12:24:19', 6, '2019-05-09 12:24:19', NULL),
(161, 182, 'e59d4b59-dba0-434d-95f8-6705686776ed', 9, '4390', 1000, 4000, 'paytm', 'ADVANCE', '', 1, '2019-05-09 12:31:17', 6, '2019-05-09 12:31:17', NULL),
(162, 183, 'fe2a1c8f-4cc7-428d-8fb9-4abe8380944c', 9, '4391', 1000, 4000, 'paytm', 'ADVANCE', '', 1, '2019-05-09 12:38:48', 6, '2019-05-09 12:38:48', NULL),
(163, 184, '1d49dccb-fe0e-4109-834d-699953de8cba', 10, '4400', 1000, 2000, 'cash', 'ADVANCE', '', 1, '2019-05-09 12:46:56', 6, '2019-05-09 12:46:56', NULL),
(164, 185, '4e03fed8-7a34-4e95-824b-4823b267cc29', 14, '4407', 2500, 2500, 'paytm', 'ADVANCE', '', 1, '2019-05-09 12:56:50', 6, '2019-05-09 12:56:50', NULL),
(165, 186, '41865773-6845-4b69-98e4-2fff988fe799', 10, '4409', 1000, 6000, 'cash', 'ADVANCE', '', 1, '2019-05-09 13:04:01', 6, '2019-05-09 13:04:01', NULL),
(166, 187, '7ca2d055-6526-4719-a576-8c34375a7ed5', 10, '4410', 1000, 6000, 'cash', 'ADVANCE', '', 1, '2019-05-09 13:10:33', 6, '2019-05-09 13:10:33', NULL),
(167, 188, '0c2f0e4f-5294-4ea1-8e62-f63fe7215b5f', 7, '4413', 2000, 3000, 'online', 'ADVANCE', '', 1, '2019-05-09 13:20:28', 6, '2019-05-09 13:20:28', NULL),
(168, 189, 'dadeedcf-0989-43bc-b3f8-7bf78c5c1dd5', 7, '4415', 4000, 0, 'paytm', 'ADVANCE', '', 1, '2019-05-09 13:32:06', 6, '2019-05-09 13:32:06', NULL),
(169, 190, 'c0d66886-90b1-4e01-8df6-f853fe801ae5', 7, '4414', 4000, 0, 'paytm', 'ADVANCE', '', 1, '2019-05-09 13:43:08', 6, '2019-05-09 13:43:08', NULL),
(170, 191, 'd5631f1a-5fc7-4395-a0cd-5a5a50e2e921', 7, '4416', 2000, 2000, 'paytm', 'ADVANCE', '', 1, '2019-05-09 13:48:53', 6, '2019-05-09 13:48:53', NULL),
(171, 192, '5ae577bb-92fc-4509-96da-712edddd0976', 9, '4420', 3200, 1000, 'online', 'ADVANCE', '', 1, '2019-05-09 14:00:57', 6, '2019-05-09 14:00:57', NULL),
(172, 193, 'd6466d19-a9bf-472d-8b8f-2b2e22ae7c98', 9, '4419', 1000, 3200, 'online', 'ADVANCE', '', 1, '2019-05-09 14:07:46', 6, '2019-05-09 14:07:46', NULL),
(173, 194, '2c4e2bcb-9875-4d76-8aad-2a2060cac163', 9, '4418', 1000, 3200, 'online', 'ADVANCE', '', 1, '2019-05-10 10:39:40', 6, '2019-05-10 10:39:40', NULL),
(174, 195, 'a6f2be60-7bb9-4207-ab05-05cda99ad527', 9, '4421', 1000, 3200, 'online', 'ADVANCE', '', 1, '2019-05-10 10:56:21', 6, '2019-05-10 10:56:21', NULL),
(175, 196, 'dd20c929-63b6-45fa-97a8-ca3fd87d9b0d', 9, '4422', 2500, 0, 'gpay', 'ADVANCE', '', 1, '2019-05-10 11:06:17', 6, '2019-05-10 11:06:17', NULL),
(176, 197, 'b438e00c-425a-4b2b-bd0b-a39a1d201fe3', 9, '4423', 2500, 0, 'gpay', 'ADVANCE', '', 1, '2019-05-10 11:17:47', 6, '2019-05-10 11:17:47', NULL),
(177, 198, 'd0e157ce-cfcd-43c4-aec6-09df3c7dbdae', 10, '4425', 3000, 0, 'gpay', 'ADVANCE', '', 1, '2019-05-10 11:24:47', 6, '2019-05-10 11:24:47', NULL),
(178, 199, '4344a3e4-e8bb-4b1f-99af-6ff356aac570', 7, '4426', 2000, 2000, 'cash', 'ADVANCE', '', 1, '2019-05-10 11:49:53', 6, '2019-05-10 11:49:53', NULL),
(179, 200, '08fbc98d-f6f6-4439-91e3-d411ca11815c', 7, '4427', 2000, 2000, 'cash', 'ADVANCE', '', 1, '2019-05-10 11:56:14', 6, '2019-05-10 11:56:14', NULL),
(180, 201, '72e01610-7ffc-457f-8c1c-742d630aa158', 7, '4427', 2000, 2000, 'cash', 'ADVANCE', '', 1, '2019-05-10 12:04:02', 6, '2019-05-10 12:04:02', NULL),
(181, 202, '1feaddb4-1d93-47ad-a24e-de36294fd4d3', 7, '4428', 2000, 2000, 'online', 'ADVANCE', '', 1, '2019-05-10 12:08:46', 6, '2019-05-10 12:08:46', NULL),
(182, 203, 'e24c37ab-5d80-4de4-9041-97a5016ca469', 7, '4429', 2000, 2000, 'cash', 'ADVANCE', '', 1, '2019-05-10 12:14:58', 6, '2019-05-10 12:14:58', NULL),
(183, 204, '0ad09b4e-4da8-4b41-b9aa-97f5d9d0b89c', 7, '4430', 2000, 2000, 'cash', 'ADVANCE', '', 1, '2019-05-10 12:21:33', 6, '2019-05-10 12:21:33', NULL),
(184, 205, 'c3a8eb17-dc95-4c73-a583-81035a92a090', 7, '4431', 2000, 2000, 'cash', 'ADVANCE', '', 1, '2019-05-10 12:28:45', 6, '2019-05-10 12:28:45', NULL),
(185, 206, '5f986de6-902a-4139-925c-913d2b4d82b3', 13, '4501', 1000, 4000, 'cash', 'ADVANCE', '', 1, '2019-05-10 12:37:41', 6, '2019-05-10 12:37:41', NULL),
(186, 207, '2f83a33c-4577-4964-8c0e-c8886c2ce35d', 13, '4502', 1000, 4000, 'cash', 'ADVANCE', '', 1, '2019-05-10 12:45:09', 6, '2019-05-10 12:45:09', NULL),
(187, 208, 'f0b6c34b-b8b4-440e-aa5f-d0a34b9823e3', 13, '4503', 1000, 4000, 'cash', 'ADVANCE', '', 1, '2019-05-10 12:51:24', 6, '2019-05-10 12:51:24', NULL),
(188, 209, '41680e3a-6c84-4538-82cc-f58a0824129b', 14, '4432', 2500, 2500, 'gpay', 'ADVANCE', '', 1, '2019-05-10 12:58:45', 6, '2019-05-10 12:58:45', NULL),
(189, 210, 'ffdf941a-1c6a-48bb-8d25-e18cb12a9abe', 14, '4433', 2500, 2500, 'gpay', 'ADVANCE', '', 1, '2019-05-10 13:03:46', 6, '2019-05-10 13:03:46', NULL),
(190, 211, 'ae93f45e-53f8-456e-89b0-e68cb62772d6', 7, '4505', 2000, 2500, 'gpay', 'ADVANCE', '', 1, '2019-05-10 13:10:42', 6, '2019-05-10 13:10:42', NULL),
(191, 212, '73f101b3-9e6e-46c1-aa2e-c0a202b1b1ef', 14, '4510', 1000, 3500, 'paytm', 'ADVANCE', '', 1, '2019-05-10 13:18:01', 6, '2019-05-10 13:18:01', NULL),
(192, 213, '877a4b0d-90ee-4867-8126-7fa133aff399', 14, '4439', 4500, 0, 'paytm', 'ADVANCE', '', 1, '2019-05-10 13:23:37', 6, '2019-05-10 13:23:37', NULL),
(193, 214, 'b1e42017-4327-4956-8f3e-635b6856c7cc', 7, '4442', 2000, 2500, 'gpay', 'ADVANCE', '', 1, '2019-05-13 10:37:42', 6, '2019-05-13 10:37:42', NULL),
(194, 215, '8ce66694-e818-4c3f-b82a-7c2ceb7c7178', 7, '4443', 1000, 4000, 'gpay', 'ADVANCE', '', 1, '2019-05-13 10:45:30', 6, '2019-05-13 10:45:30', NULL),
(195, 216, 'bfc689ef-222d-4b26-8bf9-9f27d63c54a2', 9, '4515', 1000, 6000, 'gpay', 'ADVANCE', '', 1, '2019-05-13 10:49:09', 6, '2019-05-13 10:49:09', NULL),
(196, 217, '752e8ced-5a95-4506-9430-a3fd7668855f', 9, '4518', 2000, 2500, 'cash', 'ADVANCE', '', 1, '2019-05-13 10:52:33', 6, '2019-05-13 10:52:33', NULL),
(197, 218, '150d87bd-2d9d-4b99-b82e-cb7a89cf7cd2', 9, '4520', 2000, 2000, 'paytm', 'ADVANCE', '', 1, '2019-05-13 10:55:32', 6, '2019-05-13 10:55:32', NULL),
(198, 219, '3e65b3c6-509c-4281-8b40-83bd84fbf519', 13, '4521', 2000, 4000, 'paytm', 'ADVANCE', '', 1, '2019-05-13 10:59:52', 6, '2019-05-13 10:59:52', NULL),
(199, 220, '647a12ec-f3f2-4724-84e0-fec8a319be2b', 14, '4522', 3500, 0, 'cash', 'ADVANCE', '', 1, '2019-05-13 11:03:17', 6, '2019-05-13 11:03:17', NULL),
(200, 221, '751cb3d4-c2d2-4f8e-8279-42a3760700ca', 14, '4523', 1000, 2500, 'gpay', 'ADVANCE', '', 1, '2019-05-13 11:07:33', 6, '2019-05-13 11:07:33', NULL),
(201, 222, '56cb56ee-12b8-4dbc-9315-39a56bf0d73e', 13, '4524', 2000, 4000, 'paytm', 'ADVANCE', '', 1, '2019-05-13 11:10:18', 6, '2019-05-13 11:10:18', NULL),
(202, 223, '09d1d97c-fa82-49fc-a0e1-dd9afde1ca77', 13, '4525', 2000, 4000, 'paytm', 'ADVANCE', '', 1, '2019-05-13 11:15:23', 6, '2019-05-13 11:15:23', NULL),
(203, 224, 'af1344fc-3ec3-4f4b-bfbf-e955e6645405', 13, '4526', 1500, 2500, 'paytm', 'ADVANCE', '', 1, '2019-05-13 11:19:40', 6, '2019-05-13 11:19:40', NULL),
(204, 225, 'c57473a8-ce23-476a-a81d-7b5fa5455a11', 7, '4388', 1000, 3000, 'paytm', 'ADVANCE', '', 1, '2019-05-13 12:34:43', 6, '2019-05-13 12:34:43', NULL),
(205, 226, 'e794b62e-4de4-4eea-baf2-6c050d099c47', 7, '4533', 1500, 1500, 'paytm', 'ADVANCE', '', 1, '2019-05-13 15:00:31', 6, '2019-05-13 15:00:31', NULL),
(206, 41, '32dd28d2-7d71-48df-9440-356c1503635d', 7, '4527', 3000, 0, 'cash', 'FEE', '', 1, '2019-05-14 10:12:54', 6, '2019-05-14 10:12:54', NULL),
(207, 42, 'bcab8c6d-1f3b-48f2-8836-fc4babfaee15', 7, '4528', 3000, 0, 'cash', 'FEE', '', 1, '2019-05-14 10:13:36', 6, '2019-05-14 10:13:36', NULL),
(208, 97, '2bb1265f-6801-4451-9102-0b6129230573', 7, '4530', 2000, 3000, 'online', 'FEE', '', 1, '2019-05-14 10:15:56', 6, '2019-05-14 10:15:56', NULL),
(209, 99, '143aa09b-f898-486e-a915-136719f2be48', 7, '4531', 4000, 0, 'online', 'FEE', '', 1, '2019-05-14 10:17:17', 6, '2019-05-14 10:17:17', NULL),
(210, 98, '73423bd4-470a-454b-a581-3a09709a475a', 7, '4529', 4000, 0, 'online', 'FEE', '', 1, '2019-05-14 10:18:34', 6, '2019-05-14 10:18:34', NULL),
(211, 209, '41680e3a-6c84-4538-82cc-f58a0824129b', 14, '4516', 2500, 0, 'gpay', 'FEE', '', 1, '2019-05-14 10:20:12', 6, '2019-05-14 10:20:12', NULL),
(212, 210, 'ffdf941a-1c6a-48bb-8d25-e18cb12a9abe', 14, '4517', 2500, 0, 'online', 'FEE', '', 1, '2019-05-14 10:24:37', 6, '2019-05-14 10:24:37', NULL),
(213, 88, 'f9c6c994-51c4-4bc9-8ec5-49fa1756a7bc', 9, '4436', 4000, 0, 'cash', 'FEE', '', 1, '2019-05-14 10:36:49', 6, '2019-05-14 10:36:49', NULL),
(214, 138, 'eba8cabd-975b-48fe-9c24-1e9bda4e4cdb', 14, '4437', 2000, 0, 'gpay', 'FEE', '', 1, '2019-05-14 10:37:44', 6, '2019-05-14 10:37:44', NULL),
(215, 139, '7c11ed1a-9a77-4d59-b24f-e77bba43de30', 14, '4438', 2000, 0, 'gpay', 'FEE', '', 1, '2019-05-14 10:38:24', 6, '2019-05-14 10:38:24', NULL),
(216, 52, '866abc84-91c6-4100-a79a-af0675ce5e12', 7, '4148', 1500, 3000, 'cash', 'FEE', '', 1, '2019-05-14 10:43:12', 6, '2019-05-14 10:43:12', NULL),
(217, 52, '866abc84-91c6-4100-a79a-af0675ce5e12', 7, '4444', 3000, 0, 'online', 'FEE', '', 1, '2019-05-14 10:43:39', 6, '2019-05-14 10:43:39', NULL),
(218, 71, 'b321fae4-8e84-41df-b159-aecbbab674de', 7, '4445', 3000, 0, 'cash', 'FEE', '', 1, '2019-05-14 10:46:25', 6, '2019-05-14 10:46:25', NULL),
(219, 227, 'a3c2f8c7-9601-421c-949f-c60f78b7e82b', 9, '4446', 2000, 2000, 'cash', 'ADVANCE', '', 1, '2019-05-14 11:47:02', 6, '2019-05-14 11:47:02', NULL),
(220, 228, '0c100f74-b578-463a-9e82-53fcd3f2c66f', 9, '4447', 2000, 2000, 'cash', 'ADVANCE', '', 1, '2019-05-14 11:51:41', 6, '2019-05-14 11:51:41', NULL),
(221, 229, 'd257ac9f-5954-42ba-b356-5835d04ea0d9', 14, '4534', 1500, 2000, 'paytm', 'ADVANCE', '', 1, '2019-05-14 11:57:43', 6, '2019-05-14 11:57:43', NULL),
(222, 148, 'f33823f6-4f6b-4377-9b85-c330d29478d5', 9, '4538', 2000, 0, 'online', 'FEE', '', 1, '2019-05-14 17:30:28', 6, '2019-05-14 17:30:28', NULL),
(223, 230, '375d7efd-b7ab-496c-815f-470096559878', 9, '4448', 1000, 5000, 'cash', 'ADVANCE', '', 1, '2019-05-14 17:44:54', 6, '2019-05-14 17:44:54', NULL),
(224, 231, 'e1375589-b232-488f-83ed-7937afe03234', 9, '4449', 2000, 2000, 'cash', 'ADVANCE', '', 1, '2019-05-15 11:14:12', 6, '2019-05-15 11:14:12', NULL),
(225, 122, '8d6db807-d246-4e5a-800e-1901cc45daea', 7, '4265', 1500, 2000, 'cash', 'FEE', '', 1, '2019-05-15 12:43:11', 6, '2019-05-15 12:43:11', NULL),
(226, 122, '8d6db807-d246-4e5a-800e-1901cc45daea', 7, '4539', 2000, 0, 'cash', 'FEE', '', 1, '2019-05-15 12:43:54', 6, '2019-05-15 12:43:54', NULL),
(227, 123, '49707202-71d7-41fb-b219-9b6b33ee876d', 7, '4266', 1500, 2000, 'cash', 'FEE', '', 1, '2019-05-15 12:47:40', 6, '2019-05-15 12:47:40', NULL),
(228, 215, '8ce66694-e818-4c3f-b82a-7c2ceb7c7178', 7, '4540', 4000, 0, 'gpay', 'FEE', '', 1, '2019-05-15 15:43:38', 6, '2019-05-15 15:43:38', NULL),
(229, 156, 'c136cb8a-4bca-47b4-91f9-09a288dce946', 7, '4541', 3000, 0, 'cash', 'FEE', '', 1, '2019-05-15 15:44:31', 6, '2019-05-15 15:44:31', NULL),
(230, 155, '0819d294-d64c-4c84-b5c1-9a5b0fa99f3b', 7, '4542', 3000, 0, 'paytm', 'FEE', '', 1, '2019-05-15 15:45:17', 6, '2019-05-15 15:45:17', NULL),
(231, 221, '751cb3d4-c2d2-4f8e-8279-42a3760700ca', 14, '4543', 2500, 0, 'cash', 'FEE', '', 1, '2019-05-15 15:45:55', 6, '2019-05-15 15:45:55', NULL),
(232, 168, '6f630443-3d1b-4eb6-a22b-a469c528e20f', 10, '4544', 3000, 0, 'online', 'FEE', '', 1, '2019-05-15 15:46:33', 6, '2019-05-15 15:46:33', NULL),
(233, 232, '855d92a5-7e93-42cd-b818-0d19b87730ab', 9, '4546', 1000, 3000, 'paytm', 'ADVANCE', '', 1, '2019-05-15 18:32:10', 6, '2019-05-15 18:32:10', NULL),
(234, 233, 'e7193187-a3dc-4ac7-a237-a7a0826ba21a', 9, '4547', 1000, 4000, 'gpay', 'ADVANCE', '', 1, '2019-05-15 18:35:31', 6, '2019-05-15 18:35:31', NULL),
(235, 234, 'faee42a6-0f0b-4a71-b77c-c1f234cda67e', 9, '4548', 1000, 4000, 'gpay', 'ADVANCE', '', 1, '2019-05-15 18:39:21', 6, '2019-05-15 18:39:21', NULL),
(236, 165, 'a467c7a9-652f-48d0-934b-6ddec6e0c7e3', 10, '4553', 6000, 0, 'cash', 'FEE', '', 1, '2019-05-16 13:43:06', 6, '2019-05-16 13:43:06', NULL),
(237, 235, 'df55f3ff-5089-48cd-baff-16913118b671', 7, '4550', 2000, 3000, 'cash', 'ADVANCE', '', 1, '2019-05-16 13:46:09', 6, '2019-05-16 13:46:09', NULL),
(238, 236, '5d0ba6dc-b3b0-4d8c-9528-d600e0ed598f', 7, '4551', 2000, 3000, 'cash', 'ADVANCE', '', 1, '2019-05-16 13:50:02', 6, '2019-05-16 13:50:02', NULL),
(239, 237, 'd8c340b9-ea96-429b-b719-6f67d4e89e52', 9, '4451', 9000, 0, 'online', 'ADVANCE', '', 1, '2019-05-16 13:54:29', 6, '2019-05-16 13:54:29', NULL),
(240, 238, '7eb2be38-1049-4db0-b6bd-f5b3a9a790e1', 14, '4452', 1000, 3000, 'cash', 'ADVANCE', '', 1, '2019-05-16 16:44:53', 6, '2019-05-16 16:44:53', NULL),
(241, 239, '143a9b24-6eda-49eb-9f30-49477b0a16e3', 9, '4453', 2000, 2000, 'cash', 'ADVANCE', '', 1, '2019-05-16 16:47:52', 6, '2019-05-16 16:47:52', NULL),
(242, 240, 'd8dbc814-4d89-48f9-8f45-0ceb6d086b34', 7, '4454', 1500, 2000, 'online', 'ADVANCE', '', 1, '2019-05-16 18:27:02', 6, '2019-05-16 18:27:02', NULL),
(243, 241, '1f0fdb05-4726-4023-9934-bee1ce94fb01', 7, '4455', 1500, 2000, 'online', 'ADVANCE', '', 1, '2019-05-16 18:29:57', 6, '2019-05-16 18:29:57', NULL),
(244, 242, '6ddc0da1-d73d-434c-b99f-702124061e16', 9, '4555', 1000, 3000, 'cash', 'ADVANCE', '', 1, '2019-05-17 11:52:03', 6, '2019-05-17 11:52:03', NULL),
(245, 167, 'ff24b346-73c3-4d9a-8d2c-7d68df16ee97', 10, '4456', 4000, 0, 'cash', 'FEE', '', 1, '2019-05-17 11:55:52', 6, '2019-05-17 11:55:52', NULL),
(246, 154, 'b2bc6e74-5919-413e-a87f-30af2ee5b75e', 10, '4457', 2000, 0, 'gpay', 'FEE', '', 1, '2019-05-17 11:56:25', 6, '2019-05-17 11:56:25', NULL),
(247, 243, '5cb0167e-c163-4756-a22f-e2f06d3ba5c9', 7, '4458', 3500, 0, 'online', 'ADVANCE', '', 1, '2019-05-17 12:00:40', 6, '2019-05-17 12:00:40', NULL),
(248, 216, 'bfc689ef-222d-4b26-8bf9-9f27d63c54a2', 9, '4459', 3000, 3000, 'cash', 'FEE', '', 1, '2019-05-17 12:30:32', 6, '2019-05-17 12:30:32', NULL),
(249, 105, '00a64010-ef1b-4239-b105-1913e5bd47cc', 9, '4560', 1000, 3000, 'online', 'FEE', '', 1, '2019-05-17 16:12:44', 6, '2019-05-17 16:12:44', NULL),
(250, 87, 'ddf9d4ee-7dd7-4910-a14f-6fc962f04b56', 14, '4561', 2000, 2000, 'online', 'FEE', '', 1, '2019-05-17 16:14:30', 6, '2019-05-17 16:14:30', NULL),
(251, 67, 'ebb005bc-168a-4ba4-bb5e-d096839f06a6', 7, '4460', 3000, 0, 'cash', 'FEE', '', 1, '2019-05-17 17:31:31', 6, '2019-05-17 17:31:31', NULL),
(252, 68, '0e909d28-35c6-45ec-b276-ae21e67f454a', 7, '4461', 3000, 0, 'cash', 'FEE', '', 1, '2019-05-17 17:32:14', 6, '2019-05-17 17:32:14', NULL),
(253, 244, '75cbab82-2438-4dd7-a68f-42dcd164a613', 7, '4558', 1500, 2000, 'online', 'ADVANCE', '', 1, '2019-05-17 18:07:13', 6, '2019-05-17 18:07:13', NULL),
(254, 245, '59031e39-2753-422c-bc98-e83c7c146d93', 7, '4557', 1500, 2000, 'online', 'ADVANCE', '', 1, '2019-05-17 18:11:42', 6, '2019-05-17 18:11:42', NULL),
(255, 246, '287e33af-10d5-4d83-8a17-24ea46644bd2', 7, '4559', 1500, 2000, 'online', 'ADVANCE', '', 1, '2019-05-17 18:16:45', 6, '2019-05-17 18:16:45', NULL),
(256, 247, 'c25caa4f-6b5b-4665-8309-945384644ec6', 7, '4462', 2000, 3000, 'cash', 'ADVANCE', '', 1, '2019-05-17 18:21:07', 6, '2019-05-17 18:21:07', NULL),
(257, 248, '567c6f70-3fde-4bcc-aa45-f2a53dd8a0fc', 7, '4463', 2000, 2000, 'paytm', 'ADVANCE', '', 1, '2019-05-18 11:32:59', 6, '2019-05-18 11:32:59', NULL),
(258, 249, 'd5e66fb8-c5d7-4966-9e1e-544fd0990be0', 9, '4562', 1000, 2000, 'cash', 'ADVANCE', '', 1, '2019-05-18 12:56:27', 6, '2019-05-18 12:56:27', NULL),
(259, 250, 'ffd66c9a-f927-497c-aaf2-0f60866a4184', 7, '4464', 2000, 2000, 'paytm', 'ADVANCE', '', 1, '2019-05-18 13:31:48', 6, '2019-05-18 13:31:48', NULL),
(260, 251, 'ec42a0c0-c8ca-4988-b014-e89c6fe67142', 7, '4466', 3000, 1000, 'cash', 'ADVANCE', '', 1, '2019-05-18 15:57:36', 6, '2019-05-18 15:57:36', NULL),
(261, 252, 'c90a511d-caa8-4663-9d79-32716b11a508', 7, '4467', 1500, 2000, 'online', 'ADVANCE', '', 1, '2019-05-18 16:02:40', 6, '2019-05-18 16:02:40', NULL),
(262, 253, '78db194d-b2af-43de-9a02-026f5909dfa7', 13, '4468', 1500, 3000, 'cash', 'ADVANCE', '', 1, '2019-05-20 11:11:02', 6, '2019-05-20 11:11:02', NULL),
(263, 158, '006e5745-edb6-442d-915f-326c55d9f3f6', 9, '4472', 3000, 0, 'online', 'FEE', '', 1, '2019-05-20 18:20:40', 6, '2019-05-20 18:20:40', NULL),
(264, 232, '855d92a5-7e93-42cd-b818-0d19b87730ab', 9, '4473', 3000, 0, 'online', 'FEE', '', 1, '2019-05-20 18:21:28', 6, '2019-05-20 18:21:28', NULL),
(265, 234, 'faee42a6-0f0b-4a71-b77c-c1f234cda67e', 9, '4474', 4000, 0, 'online', 'FEE', '', 1, '2019-05-20 18:22:31', 6, '2019-05-20 18:22:31', NULL),
(266, 233, 'e7193187-a3dc-4ac7-a237-a7a0826ba21a', 9, '4475', 4000, 0, 'online', 'FEE', '', 1, '2019-05-20 18:23:25', 6, '2019-05-20 18:23:25', NULL),
(267, 254, 'f1b214cc-e912-4759-88a3-0bf8e5c8ce31', 7, '4563', 1500, 2000, 'online', 'ADVANCE', '', 1, '2019-05-20 18:31:17', 6, '2019-05-20 18:31:17', NULL),
(268, 255, '08b4bc35-210b-483b-92aa-b525c4da18d0', 7, '4564', 1500, 2000, 'online', 'ADVANCE', '', 1, '2019-05-20 18:40:58', 6, '2019-05-20 18:40:58', NULL),
(269, 256, '966873ed-10f2-434e-a5fb-06743eb958a8', 7, '4565', 1500, 2000, 'online', 'ADVANCE', '', 1, '2019-05-20 18:43:20', 6, '2019-05-20 18:43:20', NULL),
(270, 257, '8fb18d0a-d7c5-4017-a748-34ca9c057a61', 9, '4471', 4500, 0, 'online', 'ADVANCE', '', 1, '2019-05-20 18:46:11', 6, '2019-05-20 18:46:11', NULL),
(271, 258, '43e3d32b-a9ff-45c0-90c9-96e096d1b936', 13, '4476', 2000, 4000, 'cash', 'ADVANCE', '', 1, '2019-05-20 18:48:43', 6, '2019-05-20 18:48:43', NULL),
(272, 259, '4c3d5ae8-6514-4c52-a91b-a47aed42d3be', 9, '4477', 1000, 2500, 'paytm', 'ADVANCE', '', 1, '2019-05-20 18:51:17', 6, '2019-05-20 18:51:17', NULL),
(273, 50, '1d0dc134-1250-4015-83e2-6a9cd6a31f82', 7, '4478', 5000, 0, 'online', 'FEE', '', 1, '2019-05-20 18:52:53', 6, '2019-05-20 18:52:53', NULL),
(274, 48, 'afb89688-652b-4a53-997b-941c857c2b00', 7, '4479', 4500, 0, 'online', 'FEE', '', 1, '2019-05-20 18:53:34', 6, '2019-05-20 18:53:34', NULL),
(275, 260, 'a7802096-5d65-4d07-9c32-ccaa1d3337b7', 9, '4566', 5000, 13000, 'online', 'ADVANCE', '', 1, '2019-05-20 19:09:38', 6, '2019-05-20 19:09:38', NULL),
(276, 261, '61a41303-873c-4c20-89a5-8a41fa290683', 7, '4567', 1500, 2000, 'online', 'ADVANCE', '', 1, '2019-05-21 12:08:07', 6, '2019-05-21 12:08:07', NULL),
(277, 262, '51351ba9-dcfb-4981-853a-5679e75f08b1', 9, '4481', 2000, 3000, 'cash', 'ADVANCE', '', 1, '2019-05-21 13:28:58', 6, '2019-05-21 13:28:58', NULL),
(278, 263, '10f9fa06-f608-49f7-b4bd-ec0cbe818b66', 7, '4568', 1000, 2500, 'cash', 'ADVANCE', '', 1, '2019-05-21 13:36:49', 6, '2019-05-21 13:36:49', NULL),
(279, 264, '5e950a70-89c6-47ff-baa9-7865e59036a9', 7, '4569', 1000, 2500, 'cash', 'ADVANCE', '', 1, '2019-05-21 13:45:13', 6, '2019-05-21 13:45:13', NULL),
(280, 185, '4e03fed8-7a34-4e95-824b-4823b267cc29', 14, '4482', 2500, 0, 'paytm', 'FEE', '', 1, '2019-05-21 15:07:05', 6, '2019-05-21 15:07:05', NULL),
(281, 34, '7e5253eb-4e4d-490c-8c79-e65f037276ca', 7, '4572', 5000, 1000, 'online', 'FEE', '', 1, '2019-05-21 16:05:49', 6, '2019-05-21 16:05:49', NULL),
(282, 49, '2c64b5da-a520-4560-90b7-d3f35c371c93', 7, '4571', 4500, 0, 'online', 'FEE', '', 1, '2019-05-21 16:07:00', 6, '2019-05-21 16:07:00', NULL),
(283, 47, 'ed8ee1e9-a26b-4263-b7b9-3781a7eb0f49', 7, '4570', 5000, 0, 'online', 'FEE', '', 1, '2019-05-21 16:07:32', 6, '2019-05-21 16:07:32', NULL),
(284, 265, 'b68fcdd0-432c-4a9f-aea2-4869d92b512d', 7, '4573', 1500, 2000, 'online', 'ADVANCE', '', 1, '2019-05-21 16:57:22', 6, '2019-05-21 16:57:22', NULL),
(285, 230, '375d7efd-b7ab-496c-815f-470096559878', 9, '4483', 2500, 2500, 'online', 'FEE', '', 1, '2019-05-21 17:45:49', 6, '2019-05-21 17:45:49', NULL),
(286, 249, 'd5e66fb8-c5d7-4966-9e1e-544fd0990be0', 9, '4484', 2000, 0, 'online', 'FEE', '', 1, '2019-05-21 18:53:01', 6, '2019-05-21 18:53:01', NULL),
(287, 43, '48dff572-1541-4e23-ad4b-98a392f761a8', 7, '4575', 3000, 0, 'cash', 'FEE', '', 1, '2019-05-22 09:49:18', 6, '2019-05-22 09:49:18', NULL),
(288, 40, '97c8a4c7-0f08-42a0-a0ef-7ee070c46514', 7, '4574', 3000, 0, 'cash', 'FEE', '', 1, '2019-05-22 09:50:03', 6, '2019-05-22 09:50:03', NULL),
(289, 187, '7ca2d055-6526-4719-a576-8c34375a7ed5', 10, '4485', 3000, 3000, 'cash', 'FEE', '', 1, '2019-05-22 10:32:14', 6, '2019-05-22 10:32:14', NULL),
(290, 186, '41865773-6845-4b69-98e4-2fff988fe799', 10, '4486', 3000, 3000, 'cash', 'FEE', '', 1, '2019-05-22 10:32:45', 6, '2019-05-22 10:32:45', NULL),
(291, 266, '02670fc0-8b04-4a1d-b651-bea3c1b8e350', 14, '4576', 1000, 5000, 'cash', 'ADVANCE', '', 1, '2019-05-22 11:01:16', 6, '2019-05-22 11:01:16', NULL),
(292, 267, '137f385c-be26-46f7-b16b-8d21e74bf527', 7, '4577', 1500, 2000, 'online', 'ADVANCE', '', 1, '2019-05-22 12:35:33', 6, '2019-05-22 12:35:33', NULL),
(293, 161, 'd7b2ec58-ab83-4f31-af03-8b83c2963750', 7, '4487', 4000, 0, 'cash', 'FEE', '', 1, '2019-05-22 12:54:57', 6, '2019-05-22 12:54:57', NULL),
(294, 45, 'e66163a5-3368-4f9a-ba37-418454879358', 7, '4488', 3000, 2000, 'cash', 'FEE', '', 1, '2019-05-22 14:20:27', 6, '2019-05-22 14:20:27', NULL),
(295, 268, '826b57e2-a7e2-40ac-98f1-4b0add00a83c', 9, '4579', 1000, 4000, 'paytm', 'ADVANCE', '', 1, '2019-05-22 18:37:29', 6, '2019-05-22 18:37:29', NULL),
(296, 269, 'f7157c62-1a8d-416d-aed9-cfb50be934a6', 9, '4580', 1000, 4000, 'gpay', 'ADVANCE', '', 1, '2019-05-22 18:41:35', 6, '2019-05-22 18:41:35', NULL),
(297, 270, '4f65a30f-8eb3-4682-aef0-cae95a462bc4', 7, '4489', 2000, 3500, 'paytm', 'ADVANCE', '', 1, '2019-05-22 18:45:59', 6, '2019-05-22 18:45:59', NULL),
(298, 271, '9d08be08-a04f-42b0-956f-6f4d47aee513', 13, '4491', 5000, 9000, 'cash', 'ADVANCE', '', 1, '2019-05-23 10:11:24', 6, '2019-05-23 10:11:24', NULL),
(299, 272, '8475a538-95a9-4099-96f5-3023fcaee9b7', 9, '4581', 1000, 4000, 'online', 'ADVANCE', '', 1, '2019-05-23 10:21:20', 6, '2019-05-23 10:21:20', NULL),
(300, 273, '988d7f85-ad8e-40f4-8f9c-13e5b71a5a1f', 14, '4582', 1000, 4000, 'online', 'ADVANCE', '', 1, '2019-05-23 10:26:58', 6, '2019-05-23 10:26:58', NULL),
(301, 274, 'df47e652-5e74-4172-9256-3d82891c5f4b', 9, '4584', 1000, 4000, 'online', 'ADVANCE', '', 1, '2019-05-23 10:32:32', 6, '2019-05-23 10:32:32', NULL),
(302, 275, '77d176fa-10b8-4d12-80c8-41dc296a6569', 9, '4583', 1000, 4000, 'online', 'ADVANCE', '', 1, '2019-05-23 10:37:51', 6, '2019-05-23 10:37:51', NULL),
(303, 276, '72f2b16c-d237-4487-95ab-f30874c74cdd', 13, '4490', 3500, 4500, 'cash', 'ADVANCE', '', 1, '2019-05-23 10:44:20', 6, '2019-05-23 10:44:20', NULL),
(304, 188, '0c2f0e4f-5294-4ea1-8e62-f63fe7215b5f', 7, '4578', 3000, 0, 'cash', 'FEE', '', 1, '2019-05-23 11:02:00', 6, '2019-05-23 11:02:00', NULL),
(305, 277, 'a3713adc-2773-4b14-b2c7-4e47ae0eef7e', 7, '4585', 2000, 2500, 'gpay', 'ADVANCE', '', 1, '2019-05-23 13:06:32', 6, '2019-05-23 13:06:32', NULL),
(306, 95, '74138940-2484-4491-bddb-386142c41e09', 9, '4492', 4000, 0, 'gpay', 'FEE', '', 1, '2019-05-23 13:15:39', 6, '2019-05-23 13:15:39', NULL),
(307, 278, '72e3c02e-0795-490e-8388-05cbbd9bc614', 13, '4586', 2000, 7000, 'cash', 'ADVANCE', '', 1, '2019-05-23 13:27:15', 6, '2019-05-23 13:27:15', NULL),
(308, 279, '0b73c61f-33cd-46fd-9028-c6da4955136c', 13, '4586', 2000, 7000, 'cash', 'ADVANCE', '', 1, '2019-05-23 13:29:41', 6, '2019-05-23 13:29:41', NULL),
(309, 280, 'd090ee1f-53dc-430a-8fb0-b4bcbc9f0b89', 13, '4587', 2000, 7000, 'cash', 'ADVANCE', '', 1, '2019-05-23 13:34:46', 6, '2019-05-23 13:34:46', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `app_profile`
--

CREATE TABLE `app_profile` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('active','suspend','trash') COLLATE utf8mb4_unicode_ci NOT NULL,
  `join_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `app_profile`
--

INSERT INTO `app_profile` (`id`, `user_id`, `first_name`, `last_name`, `mobile`, `address`, `city`, `state`, `country`, `status`, `join_date`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`) VALUES
(1, 1, 'balvir', 'chahal', '9872551010', 'C-124, Industrial Area, Phase 8', 'Mohali', 'punjab', 'india', 'active', '2019-03-05 18:56:16', '2019-03-04 23:30:00', 1, '2019-04-25 06:44:13', NULL, '2019-04-25 07:06:49', NULL),
(5, 6, 'Pawan', 'Kaur', '7415151523', 'C-124, INDUSTRIAL AREA, PHASE 8\r\n(NEAR SASIIT COLLEGE)', 'MOHALI', 'Punjab', 'India', 'active', '2019-03-28 00:00:00', '2019-03-28 07:26:10', 1, '2019-04-30 16:25:23', 1, '2019-04-25 07:06:49', NULL),
(6, 7, 'Mandeep', 'Kaur', '7814141400', 'Mohali', 'Mohali', 'Punjab', 'India', 'active', '2019-04-02 06:31:58', '2019-04-02 06:31:58', 1, '2019-04-25 06:44:18', NULL, '2019-04-25 07:06:49', NULL),
(8, 9, 'Sandeep', 'Kaur', '9041887744', 'C-124, LTOP, Industrial Area, Phase 8 Mohali', 'Mohali', 'Punjab', 'India', 'active', '2019-04-16 11:42:34', '2019-04-16 11:42:34', 1, '2019-04-25 06:44:20', NULL, '2019-04-25 07:06:49', NULL),
(9, 10, 'Nisha', 'Thakur', '7814141400', 'C-124, LTOP, Industrial Area, Phase 8 Mohali', 'Mohali', 'Punjab', 'India', 'active', '2019-04-16 11:46:19', '2019-04-16 11:46:19', 1, '2019-04-25 06:44:23', NULL, '2019-04-25 07:06:49', NULL),
(10, 12, 'manpreet', 'singh', '7696200218', 'vip road', 'zirakpue', 'punjab', 'india', 'suspend', '2019-04-25 00:00:00', '2019-04-25 12:14:34', 1, '2019-04-26 16:44:17', 1, '2019-04-26 16:44:01', 1),
(11, 13, 'Direct', 'Admission', '9872551010', 'C-124, LTOP, Industrial Area, Phase 8 Mohali', 'Mohali', 'Punjab', 'India', 'active', '2019-04-30 00:00:00', '2019-04-30 17:32:56', 1, '2019-04-30 17:32:56', NULL, '2019-04-30 12:02:56', NULL),
(12, 14, 'nisha/sandeep', 'kaur', '9041887744', 'a2it.mohali@gmail.com', 'mohali', 'punjab', 'india', 'active', '2019-05-02 00:00:00', '2019-05-02 13:23:59', 6, '2019-05-02 13:23:59', NULL, '2019-05-02 07:53:59', NULL),
(13, 15, 'Rajeev', 'Kumar', '9814256189', 'Roper', 'Roper', 'Punjab', 'India', 'active', '2019-05-16 00:00:00', '2019-05-16 17:54:33', 1, '2019-05-16 17:54:33', NULL, '2019-05-16 12:24:33', NULL),
(14, 16, 'Deepak', 'Kumar', '9872551010', 'C-124, LTOP, Industrial Area, Phase 8 Mohali', 'Mohali', 'Punjab', 'India', 'active', '2019-05-16 00:00:00', '2019-05-16 17:55:50', 1, '2019-05-16 17:55:50', NULL, '2019-05-16 12:25:50', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `app_roles`
--

CREATE TABLE `app_roles` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `can_view` int(11) NOT NULL DEFAULT '0',
  `can_update` int(11) NOT NULL DEFAULT '0',
  `can_remove` int(11) NOT NULL DEFAULT '0',
  `can_account` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app_roles`
--

INSERT INTO `app_roles` (`id`, `name`, `can_view`, `can_update`, `can_remove`, `can_account`, `status`, `created_at`, `updated_at`) VALUES
(1, 'ADMIN', 1, 1, 1, 1, 1, '2019-03-27 06:33:50', '2019-03-27 08:56:23'),
(2, 'MANAGER', 1, 1, 0, 0, 1, '2019-03-27 06:34:18', '2019-03-27 06:57:03'),
(3, 'COUNSELOR', 1, 0, 0, 0, 1, '2019-03-27 06:56:06', '2019-03-27 08:46:50'),
(4, 'ACCOUNTS', 1, 1, 0, 1, 1, '2019-03-27 07:50:32', '2019-03-27 07:50:32'),
(5, 'Trainer', 1, 0, 0, 0, 1, '2019-05-16 17:52:42', '2019-05-16 17:52:42');

-- --------------------------------------------------------

--
-- Table structure for table `app_users`
--

CREATE TABLE `app_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('active','suspend') COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` int(11) NOT NULL DEFAULT '3',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `app_users`
--

INSERT INTO `app_users` (`id`, `email`, `password`, `status`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin@admin.com', '$2y$10$GtFN3a3gLnwdc2c0/wm/Su1s/C4Nsqgr60zAnEYzT2Ve/DzXlWNoe', 'active', 1, NULL, '2019-03-05 06:30:00', '2019-03-28 06:27:12'),
(6, 'Pawan@gmail.com', '$2y$10$TvQa1l1EU4w641AYVLDa7uAJbrzNkdTXqEl1AOnU2lHOTUX8Tc9mi', 'active', 1, NULL, '2019-03-28 07:26:10', '2019-04-30 16:25:23'),
(7, 'info@zccindia.com', '$2y$10$qAzmvOnDUHaYrhdx1rm22ull2rBg8vgii/mIA49c/01UDqewp77aG', 'active', 3, NULL, '2019-04-02 06:31:58', '2019-04-30 16:25:57'),
(9, 'sandeep@gmail.com', '$2y$10$E0b1hazt7SGyPA2oQpcOm.jSk7PJ/Ak31.m9svTa5rg.ZnLTEg6O2', 'active', 3, NULL, '2019-04-16 11:42:34', '2019-04-16 11:42:34'),
(10, 'nisha@gmail.com', '$2y$10$iJfeGLjtW.aCNpFDB6BgDOARDC8W99sQiYMUsemz7dFs4g4Xl/LAW', 'active', 3, NULL, '2019-04-16 11:46:19', '2019-04-16 11:46:19'),
(12, 'macgadger@gmail.com', '$2y$10$1ZfNpq.GzodpGLiKSgUUxORe6Rq6gVyKHLlYuOiIWv3dgBZ7krkP.', 'suspend', 3, NULL, '2019-04-25 12:14:34', '2019-04-27 15:11:12'),
(13, 'da@a2itsoft.com', '$2y$10$IzGk3N51YJYlekt9/9BV9.nSqVD1tMCnHOrvjY8aLVHBPCRgERCua', 'active', 3, NULL, '2019-04-30 17:32:56', '2019-05-04 16:43:52'),
(14, 'misha@gmail.com', '$2y$10$xODq2thUvRtztgJrThgIuuMe4hIb5aYtOsmxZV28PXTen37r9PF6W', 'active', 3, NULL, '2019-05-02 13:23:59', '2019-05-04 10:03:35'),
(15, 'rajeev@a2itsoft.com', '$2y$10$0UHM2XjGGwwbzuIZMd0OUOsmFydxRPEiKtgWB22ZwjI/ip8Q6TbHS', 'active', 5, NULL, '2019-05-16 17:54:33', '2019-05-16 17:54:33'),
(16, 'deepak@a2itsoft.com', '$2y$10$QDl6sMLVJlfINDi0NyZBcuWutyVeJdGsWOHTEL6O0SlSgmegSrzd2', 'active', 5, NULL, '2019-05-16 17:55:50', '2019-05-16 17:55:50');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `app_admission`
--
ALTER TABLE `app_admission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_batch`
--
ALTER TABLE `app_batch`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `student_id` (`student_id`),
  ADD KEY `student_id_2` (`student_id`);

--
-- Indexes for table `app_course`
--
ALTER TABLE `app_course`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_emi`
--
ALTER TABLE `app_emi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_fee`
--
ALTER TABLE `app_fee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_inquiry`
--
ALTER TABLE `app_inquiry`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_migrations`
--
ALTER TABLE `app_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_payment`
--
ALTER TABLE `app_payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_profile`
--
ALTER TABLE `app_profile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_roles`
--
ALTER TABLE `app_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_users`
--
ALTER TABLE `app_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `app_admission`
--
ALTER TABLE `app_admission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=281;
--
-- AUTO_INCREMENT for table `app_batch`
--
ALTER TABLE `app_batch`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `app_course`
--
ALTER TABLE `app_course`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `app_emi`
--
ALTER TABLE `app_emi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `app_fee`
--
ALTER TABLE `app_fee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=254;
--
-- AUTO_INCREMENT for table `app_inquiry`
--
ALTER TABLE `app_inquiry`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `app_migrations`
--
ALTER TABLE `app_migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `app_payment`
--
ALTER TABLE `app_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=310;
--
-- AUTO_INCREMENT for table `app_profile`
--
ALTER TABLE `app_profile`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `app_roles`
--
ALTER TABLE `app_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `app_users`
--
ALTER TABLE `app_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
