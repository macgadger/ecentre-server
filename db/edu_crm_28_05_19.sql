-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 28, 2019 at 02:02 PM
-- Server version: 5.7.26-0ubuntu0.18.04.1
-- PHP Version: 7.2.17-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `edu_crm`
--

-- --------------------------------------------------------

--
-- Table structure for table `app_admission`
--

CREATE TABLE `app_admission` (
  `id` int(11) NOT NULL,
  `admission_uid` varchar(255) NOT NULL,
  `aid` int(11) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `gender` varchar(10) NOT NULL,
  `father_name` varchar(100) NOT NULL,
  `dob` date NOT NULL,
  `mobile` varchar(10) NOT NULL,
  `alt_mobile` varchar(10) DEFAULT NULL,
  `address` varchar(255) NOT NULL,
  `pg_address` varchar(255) DEFAULT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `country` varchar(100) NOT NULL,
  `email` varchar(255) NOT NULL,
  `college_name` varchar(255) NOT NULL,
  `college_deg_type` varchar(100) NOT NULL,
  `college_stream` varchar(100) NOT NULL,
  `college_semester` varchar(100) NOT NULL,
  `course_name` varchar(255) NOT NULL,
  `other_course` varchar(255) DEFAULT NULL,
  `course_durartion` varchar(100) NOT NULL,
  `course_joindate` date NOT NULL,
  `course_enddate` date NOT NULL,
  `course_time` varchar(100) NOT NULL,
  `course_fee` int(11) NOT NULL,
  `course_advance` int(11) NOT NULL,
  `payment_reciept` varchar(100) NOT NULL,
  `payment_type` varchar(100) DEFAULT NULL,
  `payment_mode` varchar(100) NOT NULL,
  `batch_assigned` int(11) NOT NULL DEFAULT '0',
  `refer_by` varchar(100) NOT NULL,
  `refer_name` varchar(100) NOT NULL,
  `admission_by` varchar(100) NOT NULL,
  `remarks` text,
  `status` enum('PENDING','JOINED','LEAVE','TRASH') NOT NULL DEFAULT 'PENDING',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app_admission`
--

INSERT INTO `app_admission` (`id`, `admission_uid`, `aid`, `first_name`, `last_name`, `gender`, `father_name`, `dob`, `mobile`, `alt_mobile`, `address`, `pg_address`, `city`, `state`, `country`, `email`, `college_name`, `college_deg_type`, `college_stream`, `college_semester`, `course_name`, `other_course`, `course_durartion`, `course_joindate`, `course_enddate`, `course_time`, `course_fee`, `course_advance`, `payment_reciept`, `payment_type`, `payment_mode`, `batch_assigned`, `refer_by`, `refer_name`, `admission_by`, `remarks`, `status`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`) VALUES
(35, '54609060-a2c5-4875-b595-31424bfe8568', 1001, 'Manpreet', NULL, 'male', 'Harjinder singh', '2019-05-01', '7696200218', NULL, 'SCO 701, Seventh Floor', 'Tricity Trade Towen', 'zirak', 'Punjab', 'IN', 'macgadger@outlook.com', 'ctc', 'Degree', 'sci', 'final', '2', NULL, '6 months', '2019-05-10', '2019-05-30', '4:30 PM', 15000, 2000, 'crc897493', NULL, 'cash', 0, 'google', 'sam', '9', 'pay in easy emi', 'PENDING', '2019-05-06 10:54:08', 1, '2019-05-05 12:00:34', 1, '2019-05-06 10:54:08', NULL),
(36, 'd1e20727-96e8-4155-a6ab-0e62fe2301ed', 1002, 'sameer', NULL, 'male', 'sam', '2019-05-01', '7696200214', NULL, 'vip', NULL, 'zira', 'Punjab', 'India', 'mac@mac.com', 'ldc', 'Degree', 'sci', 'final', '3', NULL, '3 months', '2019-05-08', '2019-05-31', '4:30 PM', 6000, 1500, 'cdc34234', NULL, 'cash', 0, 'google', 'sam', '4', NULL, 'PENDING', '2019-05-06 11:01:53', 1, '2019-05-06 13:36:07', NULL, '2019-05-06 11:01:53', NULL),
(37, '37f5750a-42ee-49d7-bd5b-cd0b87ed2496', 1003, 'rakeeb', NULL, 'male', 'sufiq', '2019-05-01', '7696200555', NULL, 'vip', 'road', 'chdq', 'Punjab', 'India', 'sufi@sufi.com', 'ksc', 'Degree', 'final', 'ilksjd', '2', NULL, '2 months', '2019-05-10', '2019-05-30', '7:30 PM', 10000, 2000, 'crd8798', NULL, 'cash', 0, 'google', 'sas', '4', NULL, 'PENDING', '2019-05-06 13:55:07', 1, '2019-05-06 13:55:07', NULL, '2019-05-06 13:55:07', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `app_batch`
--

CREATE TABLE `app_batch` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `admission_uid` varchar(255) NOT NULL,
  `trainer_id` int(11) NOT NULL,
  `batch_date` date NOT NULL,
  `batch_time` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `app_certificate`
--

CREATE TABLE `app_certificate` (
  `id` int(11) NOT NULL,
  `admission_uid` varchar(255) NOT NULL,
  `aid` int(11) NOT NULL,
  `issued_on` date NOT NULL,
  `issued_by` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app_certificate`
--

INSERT INTO `app_certificate` (`id`, `admission_uid`, `aid`, `issued_on`, `issued_by`, `status`, `created_at`, `updated_at`) VALUES
(1, '37f5750a-42ee-49d7-bd5b-cd0b87ed2496', 1003, '2019-05-08', 1, 1, '2019-05-16 12:30:16', '2019-05-23 07:10:22');

-- --------------------------------------------------------

--
-- Table structure for table `app_certi_request`
--

CREATE TABLE `app_certi_request` (
  `id` int(11) NOT NULL,
  `aid` int(11) NOT NULL,
  `trainer_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `course_name` varchar(255) NOT NULL,
  `course_from` date NOT NULL,
  `course_to` date NOT NULL,
  `ratings` json NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app_certi_request`
--

INSERT INTO `app_certi_request` (`id`, `aid`, `trainer_id`, `name`, `phone`, `email`, `course_name`, `course_from`, `course_to`, `ratings`, `status`, `created_at`, `updated_at`) VALUES
(4, 1001, 10, 'Manpreet Singh', '7696200218', 'macgadger@outlook.com', 'php 6 months', '2019-05-01', '2019-05-31', '{\"ovr_sat\": \"yes\", \"was_all\": \"yes\", \"was_kno\": \"yes\", \"pro_name\": \"sampler\", \"a2it_rating\": \"3\", \"tr_enc_rate\": \"3\", \"tr_sub_rate\": \"3\", \"cor_lab_rate\": \"3\", \"pro_sat_rate\": \"3\", \"tr_pres_rate\": \"3\", \"tr_time_rate\": \"3\", \"cor_staf_rate\": \"4\"}', 1, '2019-05-22 12:32:05', '2019-05-28 08:01:44'),
(10, 1001, 11, 'davinder', '7696200220', 'davin@in.com', 'php advance', '2019-05-01', '2019-05-30', '{\"ovr_sat\": \"yes\", \"was_all\": \"yes\", \"was_kno\": \"yes\", \"pro_name\": \"sampler\", \"a2it_rating\": \"4\", \"tr_enc_rate\": \"4\", \"tr_sub_rate\": \"4\", \"cor_lab_rate\": \"3\", \"pro_sat_rate\": \"2\", \"tr_pres_rate\": \"4\", \"tr_time_rate\": \"3\", \"cor_staf_rate\": \"3\"}', 1, '2019-05-23 09:14:13', '2019-05-28 08:01:50');

-- --------------------------------------------------------

--
-- Table structure for table `app_course`
--

CREATE TABLE `app_course` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app_course`
--

INSERT INTO `app_course` (`id`, `name`, `status`, `created_at`, `updated_at`, `deleted_at`, `deleted_by`) VALUES
(1, 'CCNA', 2, '2019-03-30 07:13:56', '2019-04-18 12:02:13', '2019-04-18 11:58:39', NULL),
(2, 'advance php', 1, '2019-03-30 01:50:06', '2019-03-30 01:50:06', '2019-04-18 11:58:39', NULL),
(3, '.NEt', 1, '2019-04-09 06:35:44', '2019-04-18 11:57:53', '2019-04-18 11:58:39', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `app_emi`
--

CREATE TABLE `app_emi` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `admission_id` varchar(255) NOT NULL,
  `amount` int(11) NOT NULL,
  `due_date` date NOT NULL,
  `status` enum('PAID','UNPAID') NOT NULL DEFAULT 'UNPAID',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `app_fee`
--

CREATE TABLE `app_fee` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `admission_id` varchar(255) NOT NULL,
  `cousellor_id` int(11) NOT NULL,
  `course_amount` int(11) NOT NULL,
  `due_amount` int(11) NOT NULL,
  `due_date` date NOT NULL,
  `last_payment` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app_fee`
--

INSERT INTO `app_fee` (`id`, `student_id`, `admission_id`, `cousellor_id`, `course_amount`, `due_amount`, `due_date`, `last_payment`, `status`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(7, 35, '54609060-a2c5-4875-b595-31424bfe8568', 9, 15000, 11500, '2019-05-20', 1500, 1, '2019-05-06 10:54:08', 1, '2019-05-06 12:36:20', 1),
(8, 36, 'd1e20727-96e8-4155-a6ab-0e62fe2301ed', 4, 6000, 3000, '2019-05-31', 1500, 1, '2019-05-06 11:01:53', 1, '2019-05-06 13:51:48', 1),
(9, 37, '37f5750a-42ee-49d7-bd5b-cd0b87ed2496', 4, 10000, 8000, '2019-05-20', 2000, 1, '2019-05-06 13:55:08', 1, '2019-05-06 13:55:08', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `app_inquiry`
--

CREATE TABLE `app_inquiry` (
  `id` int(11) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `gender` varchar(100) NOT NULL,
  `dob` date NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile` varchar(100) NOT NULL,
  `alt_mobile` varchar(100) DEFAULT NULL,
  `address` varchar(255) NOT NULL,
  `pg_address` varchar(255) DEFAULT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `country` varchar(100) NOT NULL,
  `course_name` varchar(255) NOT NULL,
  `course_join_date` date NOT NULL,
  `course_join_time` varchar(100) NOT NULL,
  `refer_by` varchar(100) NOT NULL,
  `refer_name` varchar(100) NOT NULL,
  `enquiry_by` int(11) NOT NULL,
  `status` enum('OPEN','CLOSE','CONVERT','FAILED') NOT NULL DEFAULT 'OPEN',
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app_inquiry`
--

INSERT INTO `app_inquiry` (`id`, `first_name`, `last_name`, `gender`, `dob`, `email`, `mobile`, `alt_mobile`, `address`, `pg_address`, `city`, `state`, `country`, `course_name`, `course_join_date`, `course_join_time`, `refer_by`, `refer_name`, `enquiry_by`, `status`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(1, 'Manpreet', 'singh', 'male', '2019-03-22', 'macgadger@outlook.com', '7696200218', NULL, 'SCO 701, Seventh Floor', NULL, 'goraya', 'Punjab', 'india', 'php 6 months', '2019-03-31', '10:00 AM', 'friend', 'sam', 1, 'OPEN', NULL, '2019-03-23 04:37:55', NULL, '2019-03-23 04:37:55');

-- --------------------------------------------------------

--
-- Table structure for table `app_migrations`
--

CREATE TABLE `app_migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `app_migrations`
--

INSERT INTO `app_migrations` (`id`, `migration`, `batch`) VALUES
(1, '2019_02_15_071445_create_users_table', 1),
(2, '2019_02_15_073504_create_profile_table', 1),
(3, '2019_02_15_073637_create_customers_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `app_payment`
--

CREATE TABLE `app_payment` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `admission_id` varchar(255) NOT NULL,
  `cousellor_id` int(11) NOT NULL,
  `reciept_no` varchar(100) NOT NULL,
  `amount` int(11) NOT NULL,
  `last_due` int(11) NOT NULL,
  `payment_mode` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL,
  `remarks` text,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app_payment`
--

INSERT INTO `app_payment` (`id`, `student_id`, `admission_id`, `cousellor_id`, `reciept_no`, `amount`, `last_due`, `payment_mode`, `type`, `remarks`, `status`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(15, 35, '54609060-a2c5-4875-b595-31424bfe8568', 9, 'crc897493', 2000, 13000, 'cash', 'ADVANCE', 'pay in easy emi', 1, '2019-05-06 10:54:08', 1, '2019-05-06 12:01:20', NULL),
(16, 36, 'd1e20727-96e8-4155-a6ab-0e62fe2301ed', 4, 'cdc34234', 1500, 4500, 'cash', 'ADVANCE', '', 1, '2019-05-06 11:01:53', 1, '2019-05-06 11:01:53', NULL),
(17, 35, '54609060-a2c5-4875-b595-31424bfe8568', 9, 'crc90890', 1500, 11500, 'cash', 'FEE', 'fee', 1, '2019-05-06 12:36:19', 1, '2019-05-06 12:36:19', NULL),
(18, 36, 'd1e20727-96e8-4155-a6ab-0e62fe2301ed', 4, 'crc3423', 1500, 3000, 'cash', 'FEE', 'none', 1, '2019-05-06 13:51:48', 1, '2019-05-06 13:51:48', NULL),
(19, 37, '37f5750a-42ee-49d7-bd5b-cd0b87ed2496', 4, 'crd8798', 2000, 8000, 'cash', 'ADVANCE', '', 1, '2019-05-06 13:55:08', 1, '2019-05-06 13:55:08', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `app_profile`
--

CREATE TABLE `app_profile` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('active','suspend','trash') COLLATE utf8mb4_unicode_ci NOT NULL,
  `join_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `app_profile`
--

INSERT INTO `app_profile` (`id`, `user_id`, `first_name`, `last_name`, `mobile`, `address`, `city`, `state`, `country`, `status`, `join_date`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`) VALUES
(1, 1, 'mac', 'singh', '7696200218', 'vip road', 'zirakpur', 'punjab', 'india', 'active', '2019-03-05', '2019-03-04 23:30:00', 1, '2019-04-18 11:38:35', NULL, '2019-04-18 11:29:50', NULL),
(3, 4, 'Manpreet', 'Singh', '7696200218', 'SCO 701, Seventh Floor\r\nTricity Trade Towen', 'Zirakpur', 'Punjab', 'India', 'active', '2019-03-01', '2019-03-06 06:03:32', 1, '2019-04-23 08:05:26', 1, '2019-04-18 11:36:47', 1),
(4, 9, 'Rajni', 'Sharma', '7696550225', 'VIP ROAD', 'Zirakpur', 'Punjab', 'India', 'active', '2019-04-23', '2019-04-23 08:10:07', 1, '2019-04-23 08:10:07', NULL, '2019-04-23 08:10:07', NULL),
(5, 10, 'amandeep', 'sigh', '7696255898', 'vip', 'goraya', 'punjab', 'india', 'active', '2019-05-16', '2019-05-16 12:42:18', 1, '2019-05-16 12:42:18', NULL, '2019-05-16 12:42:18', NULL),
(6, 11, 'davinder', 'singh', '7696899544', 'mohali', 'mohali', 'punjab', 'india', 'active', '2019-05-16', '2019-05-16 12:43:07', 1, '2019-05-16 12:43:07', NULL, '2019-05-16 12:43:07', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `app_roles`
--

CREATE TABLE `app_roles` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `can_view` int(11) NOT NULL DEFAULT '0',
  `can_update` int(11) NOT NULL DEFAULT '0',
  `can_remove` int(11) NOT NULL DEFAULT '0',
  `can_account` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app_roles`
--

INSERT INTO `app_roles` (`id`, `name`, `can_view`, `can_update`, `can_remove`, `can_account`, `status`, `created_at`, `updated_at`) VALUES
(1, 'ADMIN', 1, 1, 1, 1, 1, '2019-03-27 06:33:50', '2019-03-27 08:56:23'),
(2, 'MANAGER', 1, 1, 0, 0, 1, '2019-03-27 06:34:18', '2019-03-27 06:57:03'),
(3, 'COUNSELOR', 1, 0, 0, 0, 1, '2019-03-27 06:56:06', '2019-03-27 08:46:50'),
(4, 'ACCOUNTS', 1, 1, 0, 1, 1, '2019-03-27 07:50:32', '2019-03-27 07:50:32'),
(5, 'TRAINER', 1, 0, 0, 0, 1, '2019-05-16 12:38:01', '2019-05-16 12:38:25');

-- --------------------------------------------------------

--
-- Table structure for table `app_users`
--

CREATE TABLE `app_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('active','suspend') COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` int(11) NOT NULL DEFAULT '3',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `app_users`
--

INSERT INTO `app_users` (`id`, `email`, `password`, `status`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin@admin.com', '$2y$10$GtFN3a3gLnwdc2c0/wm/Su1s/C4Nsqgr60zAnEYzT2Ve/DzXlWNoe', 'active', 1, NULL, '2019-03-05 06:30:00', '2019-03-30 07:01:18'),
(4, 'macgadger@outlook.com', '$2y$10$TjStCeIIZdAWr2JWbFC6s.yhTDVGRzeROiQGIIQ29MhaE7Ro.dA.O', 'active', 3, NULL, '2019-03-06 06:03:32', '2019-04-18 13:18:41'),
(9, 'rajni@outlook.com', '$2y$10$RrHc8ci9mASXwYzIrQSCn.BTd.Y2VeRwuGmmH7RRuOZhKVHsTH9vK', 'active', 3, NULL, '2019-04-23 08:10:07', '2019-04-27 09:21:17'),
(10, 'ama@aman.com', '$2y$10$PGTRC.SXsI6SYjQZvIaWQ.T1iMcuQg8kq9zorEqRUkgO0iLZ4ytXW', 'active', 5, NULL, '2019-05-16 12:42:18', '2019-05-16 12:42:18'),
(11, 'david@gmail.com', '$2y$10$t6EcsjBF9Tz0tvaHiMnofuC6mhWwy/HtizO6917nvvI2.Q8efWZoC', 'active', 5, NULL, '2019-05-16 12:43:07', '2019-05-16 12:43:07');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `app_admission`
--
ALTER TABLE `app_admission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_batch`
--
ALTER TABLE `app_batch`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `student_id` (`student_id`),
  ADD KEY `student_id_2` (`student_id`);

--
-- Indexes for table `app_certificate`
--
ALTER TABLE `app_certificate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_certi_request`
--
ALTER TABLE `app_certi_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_course`
--
ALTER TABLE `app_course`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_emi`
--
ALTER TABLE `app_emi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_fee`
--
ALTER TABLE `app_fee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_inquiry`
--
ALTER TABLE `app_inquiry`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_migrations`
--
ALTER TABLE `app_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_payment`
--
ALTER TABLE `app_payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_profile`
--
ALTER TABLE `app_profile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_roles`
--
ALTER TABLE `app_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_users`
--
ALTER TABLE `app_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `app_admission`
--
ALTER TABLE `app_admission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `app_batch`
--
ALTER TABLE `app_batch`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `app_certificate`
--
ALTER TABLE `app_certificate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `app_certi_request`
--
ALTER TABLE `app_certi_request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `app_course`
--
ALTER TABLE `app_course`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `app_emi`
--
ALTER TABLE `app_emi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `app_fee`
--
ALTER TABLE `app_fee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `app_inquiry`
--
ALTER TABLE `app_inquiry`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `app_migrations`
--
ALTER TABLE `app_migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `app_payment`
--
ALTER TABLE `app_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `app_profile`
--
ALTER TABLE `app_profile`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `app_roles`
--
ALTER TABLE `app_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `app_users`
--
ALTER TABLE `app_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
