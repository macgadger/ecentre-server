<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Attendance as AttendanceModel;

class Attendance extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'attendance';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Attendance at Midnight';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $attn = new AttendanceModel;
            $attn->trainer_id = 1;
            $attn->batch_id   = 1;
            $attn->log        = "created by cron";
            $attn->remark     = "sample cron data";
        $attn->save();
    }
}
