<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Emi extends Model
{
    //
    protected $table      = 'emi';
    protected $primaryKey = 'student_id';
    protected $fillable = ['student_id', 'amount','due_date'];
    protected $guarded  = [];
    protected $hidden   = ['created_at'];
}
