<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BatchCategory extends Model
{
    //
    protected $table      = 'batch_category';
    //protected $primaryKey = 'id';
    protected $fillable = [];
    protected $guarded  = [];
    protected $hidden   = [];
}
