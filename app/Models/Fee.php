<?php

namespace App\Models;
use Auth;
use Illuminate\Database\Eloquent\Model;

class Fee extends Model
{
    //
    protected $table      = 'fee';
    protected $primaryKey = 'id';
    protected $fillable = [];
    protected $guarded  = [];
    protected $hidden   = [];
	
	 /**
     *  Setup model event hooks
     */
    public static function boot()
    {
        parent::boot();
        // Event to happen On Creating
		self::creating(function ($model) {
            $model->created_by = Auth::user()->id;
        });
		
		// Event to happen On Creating
		self::updating(function ($model) {
            $model->updated_by = Auth::user()->id;
        });
		
		// Event to happen On Deleting
		self::deleting(function ($model) {
            //$model->deleted_by = Auth::user()->id;
        });
    }

    public function admission(){
        return $this->belongsTo('App\Models\Admissions','admission_id','admission_uid');
    }

    public function profile(){
        return $this->hasOne('App\Models\Profile','user_id','cousellor_id');
    }

    public function payments(){
        return $this->hasMany('App\Models\Payments','cousellor_id','cousellor_id');
    }

}
