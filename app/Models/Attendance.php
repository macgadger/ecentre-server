<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    //
    protected $table      = 'attendance';
    //protected $primaryKey = 'id';
    protected $fillable = [];
    protected $guarded  = [];
    protected $hidden   = [];

    public function batch()
    {
        return $this->hasOne('App\Models\Batch','id','batch_id');
    }

    public function trainer()
    {
        return $this->hasOne('App\Models\Profile','id','trainer_id');
    }
}
