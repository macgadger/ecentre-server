<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Batch extends Model
{
    //
    protected $table      = 'batch';
    //protected $primaryKey = 'id';
    protected $fillable = [];
    protected $guarded  = [];
    protected $hidden   = [];

    public function category()
    {
        return $this->hasOne('App\Models\BatchCategory','id','category_id');
    }

    public function trainer(){
        return $this->hasOne('App\Models\Profile','id','trainer_id');
    }

    public function admissions(){
        return $this->hasMany('App\Models\Admissions','batch_id','id');
    }

    public function attendance(){
        return $this->hasMany('App\Models\Attendance','trainer_id','trainer_id');
    }

}
