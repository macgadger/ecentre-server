<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Users extends Authenticatable
{
    //        
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password', 'status', 'role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
         'password', 'remember_token', 'created_at' , 'status'
    ];

    public function profile(){
        return $this->belongsTo('App\Models\Profile','id','user_id');
    }

    public function admission(){
        return $this->hasMany('App\Models\Admissions','admission_by','id');
    }

    public function admreports(){
        return $this->hasMany('App\Models\Admissions','admission_by','id');
    }

    public function getRole(){
        return $this->hasOne(Roles::class,'id','role');
    }

    public function getProfile(){
        return $this->hasOne('App\Models\Profile','user_id','id');                        
    }
}