<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Profile extends Model
{
    //
    protected $table      = 'profile';
    protected $primaryKey = 'user_id';
    protected $fillable = [];
    protected $guarded  = [];
    protected $hidden   = ['created_at'];

    /**
     *  Setup model event hooks
    */
    public static function boot()
    {
        parent::boot();
        // Event to happen On Create
        self::creating(function ($model) {
            $model->created_by    = Auth::user()->id;
        });
        
        // Event to happen On Update
		self::updating(function ($model) {
            $model->updated_by = Auth::user()->id;
        });
    }
    
    public function getFullNameAttribute()
    {
        return ucwords("{$this->first_name} {$this->last_name}");
    }

    public function user()
    {
        return $this->belongsTo('App\Models\Users','user_id','id');
    }

    public function getUserinfo(){
        return $this->hasOne('App\Models\Users','id','user_id');                        
    }
}
