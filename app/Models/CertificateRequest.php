<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CertificateRequest extends Model
{
    //
    protected $table      = 'certi_request';
    protected $primaryKey = 'id';
    protected $fillable = [];
    protected $guarded  = [];
    protected $hidden   = ['created_at'];

    public function profile()
    {
        return $this->hasOne('App\Models\Profile','user_id','trainer_id');
    }

    public function admission()
    {
        return $this->hasOne('App\Models\Admissions','aid','aid');
    }

}
