<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reviews extends Model
{
    //
    protected $table      = 'reviews';
    //protected $primaryKey = 'id';
    protected $fillable = [];
    protected $guarded  = [];
    protected $hidden   = [];

    public function admissions()
    {
        return $this->hasOne('App\Models\Admissions','aid', 'student_id');
    }
}
