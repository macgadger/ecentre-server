<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Uuid;

class Students extends Model
{
    //
    protected $table      = 'student';
    protected $primaryKey = 'id';
    protected $fillable = [];
    protected $guarded  = [];
    protected $hidden   = ['created_at'];

    /**
     *  Setup model event hooks
     */
    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->admission_uid = (string) Uuid::generate(4);
        });
    }

    public function getBatch(){
        return $this->hasOne('App\Models\Batch','student_id','id');                        
    }
	
	public function getCourse(){
        return $this->hasOne('App\Models\Course','id','course_name');
    }
}
