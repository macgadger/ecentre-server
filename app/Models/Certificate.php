<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Certificate extends Model
{
    //
    protected $table      = 'certificate';
    protected $primaryKey = 'id';
    protected $fillable = [];
    protected $guarded  = [];
    protected $hidden   = ['created_at'];

    public function admissions()
    {
        return $this->belongsTo('App\Models\Admissions','admission_uid','admission_uid');
    }

    public function profile()
    {
        return $this->hasOne('App\Models\Profile','user_id','issued_by');
    }

}
