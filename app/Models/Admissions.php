<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;
use Uuid, Auth;

class Admissions extends Model
{
    //use SoftDeletes;
    //
    protected $table      = 'admission';
    protected $primaryKey = 'id';
    protected $fillable = [];
    protected $guarded  = [];
    protected $hidden   = ['created_at'];

    /**
     *  Setup model event hooks
     */
    public static function boot()
    {
        parent::boot();
        // Event to happen On Create
        self::creating(function ($model) {
            $model->admission_uid = (string) Uuid::generate(4);
            $model->created_by    = Auth::user()->id;
        });

        // Event to happen On Update
		self::updating(function ($model) {
            $model->updated_by = Auth::user()->id;
        });
    }

    public function profile()
    {
        return $this->belongsTo('App\Models\Profile','admission_by','user_id');
    }

    public function certificate()
    {
        return $this->belongsTo('App\Models\Certificate','admission_uid','admission_uid');
    }

    public function getFullNameAttribute()
    {
        return ucwords("{$this->first_name} {$this->last_name}");
    }

    public function getFormatCreatedAtAttribute()
    {
        return date('d M,Y',strtotime($this->created_at));
    }

    public function scopeAdmissions($query, $id)
    {
        if(Auth::user()->role == 3){
            return $query->whereIn('status',['PENDING','JOINED','TRASH','DROPOUT','COMPLETED','ATTNBLOCK'])->where('admission_by',$id);
        }else{
            return $query->whereIn('status',['PENDING','JOINED','TRASH','DROPOUT','COMPLETED','ATTNBLOCK']);
        }
    }

    public function payment()
    {
        return $this->hasMany('App\Models\Payments','admission_id','admission_uid');
    }

    public function fees(){
        return $this->belongsTo('App\Models\Fee','admission_uid','admission_id');
    }

    public function getBatch(){
        return $this->hasOne('App\Models\Batch','student_id','id');
    }

    public function getCourse(){
        return $this->hasOne('App\Models\Course','id','course_name');
    }

    public function getDues(){
        return $this->hasOne('App\Models\Fee','admission_id','admission_uid')->where('status','DUE');
    }

    public function batch(){
        return $this->hasOne('App\Models\Batch','id','batch_id');
    }
}
