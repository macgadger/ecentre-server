<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Route;
use Closure, Auth;
use App\Models\Users;


class RoleAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }

}
