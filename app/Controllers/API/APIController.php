<?php
namespace App\Controllers\API;

use Illuminate\Http\Request;
use App\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

use App\Models\Users;
use App\Models\Profile;
use App\Models\Admissions;
use App\Models\Certificate;
use App\Models\CertificateRequest;
use DB;

class APIController extends Controller
{
    //
    public function getTainers(){
        $trainers = Users::with('profile')->where('role',5)->get();
        return $trainers;
    }

    //
    public function validateID(Request $request)
    {
        $input = $request->all();
        $acode = $input['id'];
        if(strtoupper($input['code']) == 'A2ITMH'){
            $adm = Admissions::where('aid',$acode)->first();
            return ($adm!=NULL) ? array('status' => true, 'meesage' => 'validated') : array('status' => false, 'meesage' => 'invalid');
        }
        else{
            return array('status' => false, 'meesage' => 'invalid');
        }
    }

    //
    public function verifyCertificate(Request $request)
    {
        $input = $request->all();
        $acode = $input['id'];

        if(strtoupper($input['code']) == 'A2ITMH')
        {
            $adm = Certificate::with('admissions')->where('admission_uid','LIKE','%'.$acode.'%')->first();
            if($adm){
                return array(
                    'status' => true , 'message' => 'Certificate Issued', 
                    'data' => array(
                        'name'        => ucwords($adm->admissions->full_name),
                        'issued_by'   => 'A2IT',
                        'issued_on'   => date('d/m/Y',strtotime($adm->created_at)),
                        'course_name' => strtoupper($adm->admissions->getCourse->name),
                ));
            }else{
                return array('status' => false , 'message' => 'Certificate not found or issued yet');
            }
        }
        else{
           return array('status' => false , 'message' => 'Inavlid GCIC Code');
        }
    }

    //
    public function applyCertificate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'cid'          => 'required',
            'name'         => 'required',
            'mobile'       => 'required',
            'email'        => 'required',
            'course_name'  => 'required',
            'course_from'  => 'required',
            'course_to'    => 'required',
            'trainer_id'   => 'required',
        ]);
        if($validator->fails()) {    
            return array('status' => false, 'message' => 'Validation error');
        }
        $input = $request->all();
    
        $ratings = array(
            "a2it_rating"   => $input['a2it_rating'],
            "tr_sub_rate"   => $input['tr_sub_rate'],
            "tr_pres_rate"  => $input['tr_pres_rate'],
            "tr_time_rate"  => $input['tr_time_rate'],
            "tr_enc_rate"   => $input['tr_enc_rate'],
            "pro_name"      => $input['pro_name'],
            "pro_sat_rate"  => $input['pro_sat_rate'],
            "was_kno"       => $input['was_kno'],
            "was_all"       => $input['was_all'],
            "cor_staf_rate" => $input['cor_staf_rate'],
            "cor_lab_rate"  => $input['cor_lab_rate'],
            "ovr_sat"       => $input['ovr_sat']
        );
        //
        $cRequest = new CertificateRequest();

        $cRequest->aid          = $input['cid'];
        $cRequest->trainer_id   = $input['trainer_id'];
        $cRequest->name         = $input['name'];
        $cRequest->phone        = $input['mobile'];
        $cRequest->email        = $input['email'];
        $cRequest->course_name  = $input['course_name'];
        $cRequest->course_from  = date('Y-m-d',strtotime($input['course_from']));
        $cRequest->course_to    = date('Y-m-d',strtotime($input['course_to']));
        $cRequest->ratings      = json_encode($ratings);
        $cRequest->save();
        if($cRequest)
            return array('status' => true);
        else
            return array('status' => false);
    }

}
