<?php

namespace App\Controllers\App;

use Illuminate\Http\Request;
use App\Controllers\Controller;
use App\Models\Admissions;
use Auth,Hash;
use App\Models\Batch;
use App\Models\BatchCategory;
use App\Models\Course;
use App\Models\Users;

class BatchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $batch = Batch::all()->sortByDesc("id");
        return view('app.pages.batch.index',[
            'batch' => $batch,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('app.pages.batch.new',[
            'category' => BatchCategory::where('status',1 )->get(),
            'trainers' => Users::where('role',5)->get(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $batch  = new Batch;
        $batch->category_id = $request->category;
        $batch->title       = $request->name;
        $batch->start_time  = date('H:i:s',strtotime($request->start));
        $batch->end_time    = date('H:i:s',strtotime($request->end));
        $batch->trainer_id  = $request->trainer;
        $batch->save();
        //
        return redirect()->route('batch.index')->with(['status' => 'Batch created Successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $crse = Course::find($id);
        return view('app.pages.course.view',[
            'course' => $crse
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $crse = Batch::find($id);
        return view('app.pages.batch.edit',[
            'batch' => $crse
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $input = $request->all();
        $crse = Course::find($id)->update($input);
        return redirect()->route('course.index')->with(['status' => 'Course Updated Successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $crse  = Course::find($id);
        $crse->update([
            'status'     => 2,
            'deleted_at' => date('Y-m-d H:i:s', time()),
            'deleted_by' => Auth::user()->id
        ]);
        return "Trashed Successfully";
    }

    public function assignBatch(Request $request)
    {
        if($request->isMethod('POST'))
        {
            $admision = Admissions::whereIn('id', $request->students)
                                  ->update([
                                      'batch_id'       => $request->batch,
                                      'batch_assigned' => 1
                                    ]);
            return redirect()->route('batch.assigned',['filter' => 'all'])->with(['status' => 'Batch assigned Successfully']);
        }
        else
        {
            $batches  = Batch::where('status',1)->get();
            $admision = Admissions::where('batch_assigned',0)->get();
            return view('app.pages.batch.assign.index',[
                'batches'   => $batches,
                'admission' => $admision,
            ]);
        }
    }

    public function editBatch(Request $request, $id)
    {
        if($request->isMethod('POST'))
        {
            $batch = Admissions::find($id);
            $batch->update(['batch_id' => $request->batch ]);
            //
            return redirect()->route('batch.assigned',['filter' => 'all'])->with(['status' => 'Batch Updated Successfully']);
        }
        else
        {
            $batches  = Batch::where('status',1)->get();
            $admision = Admissions::find($id);
            return view('app.pages.batch.assign.edit',[
                'batches'   => $batches,
                'admission' => $admision,
            ]);
        }
    }

    public function unassignBatch($id)
    {
        $admision = Admissions::find($id);
        $admision->update(['batch_assigned' => 0, 'batch_id' => NULL]);

    }

    public function assignedBatches($filter)
    {
        if($filter == 'all'){
            $batch  = Admissions::where('batch_assigned', 1)->get();
        }
        else{
            $batch  = Admissions::where('batch_assigned',1)
                            ->where('batch_id', $filter)
                            ->get();
        }
        $batches = Batch::where('status',1)->get();
        //
        return view('app.pages.batch.assigned',[
            'batch'   => $batch,
            'batches' => $batches,
        ]);
    }

    public function trainerBatch($filter='all')
    {
        if($filter == 'all'){
            $batch  = null;
        }
        else{
            $batch  = Admissions::where('batch_assigned',1)
                            ->where('batch_id', $filter)
                            ->get();
        }
        $batches = Batch::where('trainer_id',Auth::user()->profile->id)->where('status',1)->get();
        //
        return view('app.pages.batch.mybatch',[
            'batch'   => $batch,
            'batches' => $batches,
        ]);
    }

}
