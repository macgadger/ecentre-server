<?php

namespace App\Controllers\App;

use Illuminate\Http\Request;
use App\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Arr;
use Auth, DB, Hash;
use App\Models\Admissions;
use App\Models\Users;
use App\Models\Certificate;
use App\Models\CertificateRequest;
use App\Models\Profile;

class CertificateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cert = Certificate::with('admissions')->get()->sortByDesc("id");
        return view('app.pages.certificate.index',[
            'cert' => $cert,
        ]);
    }

    public function issueCertificate($id)
    {
        $cert = Certificate::find($id);
         $cert->update([
             'issued_on' => date('Y-m-d'),
             'status'    => 0,
         ]);
         return redirect()->back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('app.pages.certificate.new');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function issue($id)
    {
        $student = Admissions::where('id',$id)->first();
		$uid     = substr($student->admission_uid, -4);

        $img = Image::canvas(1122,800);
        //$img = Image::make(public_path('uploads/overlay.png'))->;
		//$img = Image::make(public_path('uploads/certi-crop.jpg'))->opacity(100);

        /*$img->text('This certificate is awarded to', 70, 330, function($font) {
            $font->file(public_path('uploads/font/GreatVibes-Regular.ttf'));
            $font->size(32);
            $font->color('#0c0c0c');
            $font->align('left');
            $font->valign('top');
            $font->angle(0);
        });*/

        $img->text(ucwords($student->full_name), 0, 377, function($font) {
            $font->file(public_path('uploads/font/Roboto-Bold.ttf'));
            $font->size(36);
            $font->color('#0c0c0c');
            $font->align('left');
            $font->valign('top');
            $font->angle(0);
        });

        $img->text('has successfully completed the requirements to be recognized as a certified Professional of:', 0, 440, function($font) {
            $font->file(public_path('uploads/font/Roboto-Light.ttf'));
            $font->size(20);
            $font->color('#0c0c0c');
            $font->align('left');
            $font->valign('top');
            $font->angle(0);
        });

        $img->text(strtoupper($student->getCourse->name), 0, 490, function($font) {
            $font->file(public_path('uploads/font/Roboto-Bold.ttf'));
            $font->size(36);
            $font->color('#0c0c0c');
            $font->align('left');
            $font->valign('top');
            $font->angle(0);
        });

        $img->text('From : ', 0, 550, function($font) {
            $font->file(public_path('uploads/font/Roboto-Light.ttf'));
            $font->size(20);
            $font->color('#0c0c0c');
            $font->align('left');
            $font->valign('top');
            $font->angle(0);
        });

        $img->text(date('d M, Y',strtotime($student->course_joindate)), 65, 550, function($font) {
            $font->file(public_path('uploads/font/Roboto-Light.ttf'));
            $font->size(20);
            $font->color('#0c0c0c');
            $font->align('left');
            $font->valign('top');
            $font->angle(0);
        });

        $img->text('to', 190, 550, function($font) {
            $font->file(public_path('uploads/font/Roboto-Light.ttf'));
            $font->size(20);
            $font->color('#0c0c0c');
            $font->align('left');
            $font->valign('top');
            $font->angle(0);
        });

        $img->text(date('d M, Y',strtotime($student->course_enddate)), 220, 550, function($font) {
            $font->file(public_path('uploads/font/Roboto-Light.ttf'));
            $font->size(20);
            $font->color('#0c0c0c');
            $font->align('left');
            $font->valign('top');
            $font->angle(0);
        });

        $img->text('| Registration Number : ', 340, 550, function($font) {
            $font->file(public_path('uploads/font/Roboto-Light.ttf'));
            $font->size(20);
            $font->color('#0c0c0c');
            $font->align('left');
            $font->valign('top');
            $font->angle(0);
        });

        $img->text('A2IT-'.$student->aid.'-'.strtoupper($uid), 550, 550, function($font) {
            $font->file(public_path('uploads/font/Roboto-Black.ttf'));
            $font->size(20);
            $font->color('#0c0c0c');
            $font->align('left');
            $font->valign('top');
            $font->angle(0);
        });

        /*$img->text('At', 70, 555, function($font) {
            $font->file(public_path('uploads/font/GreatVibes-Regular.ttf'));
            $font->size(22);
            $font->color('#0c0c0c');
            $font->align('left');
            $font->valign('top');
            $font->angle(0);
        });

        $img->text('Mohali', 100, 560, function($font) {
            $font->file(public_path('uploads/font/Roboto-Black.ttf'));
            $font->size(18);
            $font->color('#0c0c0c');
            $font->align('left');
            $font->valign('top');
            $font->angle(0);
        });*/

    	$path = public_path('uploads/certificate/preview.png');
        $img->save($path);
        return view('app.pages.certificate.issue');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function requests()
    {
        $cert = CertificateRequest::with(['admission','admission.batch.trainer','admission.getCourse'])->where('status',1)->get()->sortByDesc("id");
        return view('app.pages.certificate.requests',[
            'cert' => $cert
        ]);
    }

    public function authorise($id)
    {
        $cert = CertificateRequest::find($id);
        $admisn = Admissions::where('aid', $cert->aid)->first();

        $cert->update([
            'status' => 0,
        ]);

        $certificate = Certificate::create([
            'aid'           => $cert->aid,
            'admission_uid' => $admisn->admission_uid,
            'issued_by'     => Auth::user()->id,
            'status'        => 1,
        ]);
        return redirect()->route('certificate.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'      => 'required|min:6|max:50|unique:users',
            'password'   => 'required|min:6|max:30|confirmed',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $input = $request->all();
        // Start Database transaction
        DB::beginTransaction();
        try
        {
            DB::commit();
        }catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        return redirect('/account/app/certificate')->with(['status' => 'Certificate Created Successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $emp = Profile::with('user')->where('id',$id)->first();
        // return view('app.pages.employee.view',[
        //     'employee' => $emp
        // ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $emp = Profile::with('user')->where('user_id',$id)->first();
        return view('app.pages.employee.edit',[
            'employee' => $emp
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $input = $request->all();

        // Start Database transaction
        DB::beginTransaction();
        try
        {
            DB::commit();
        }catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        return redirect()->route('employee.index')->with(['status' => 'Employee Profile Updated Successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $emps  = Profile::where('id',$id)->first();
		$emps->update([
            'status' => 'TRASH',
            'deleted_at' => date('Y-m-d H:i:s', time()),
            'deleted_by' => Auth::user()->id
        ]);
		$usr  = Users::find($emps->user_id)->update([
			'status' => 'suspend'
		]);
        return "Trashed Successfully";
    }

    /**
     * Search a resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
            $input     = $request->all();
            $admission = Admissions::with('certificate')->where('first_name','LIKE','%'.$input['query'].'%')
                                ->orWhere('last_name','LIKE','%'.$input['query'].'%')
                                ->orWhere('aid',$input['query'])
                                ->get();

            $data = '';
            foreach($admission as $key => $item)
            {
                $data.= "<tr>";
                $data.= "<td>".$item->aid."</td>";
                $data.= "<td>".strtoupper($item->first_name.' '.$item->last_name)."</td>";
                $data.= "<td>".strtoupper($item->mobile)."</td>";
                $data.= "<td>".strtoupper($item->getCourse->name)."</td>";
                $data.= "<td>";
                if(isset($item->certificate)){
                    $data.= "<span class='label label-success'>Issued</span>";
                }else{
                    $data.= "<span class='label label-danger'>Not Issued</span>";
                }
                $data.="</td>";
                if(isset($item->certificate)){
                    $data.= "<td>".date('d M,Y',strtotime($item->certificate->issued_on))."</td>";
                }else{
                    $data.="<td><span class='label label-danger'>Not Issued</span></td>";
                }
                $data.= "<td><a class='btn btn-success btn-flat big' href='".route('certificate.issue',['id' => $item->id])."'>Issue Certificate</a></td>";
                $data.="</tr>";
            }
            return $data;
    }

}
