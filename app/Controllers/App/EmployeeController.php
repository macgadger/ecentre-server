<?php

namespace App\Controllers\App;

use Illuminate\Http\Request;
use App\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Arr;
use Auth, DB, Hash;
use App\Models\Users;
use App\Models\Profile;
use App\Models\Roles;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->getRole->name == 'ADMIN'){
            $emps = Profile::whereIn('status',['active','suspend','trash'])->get()->sortByDesc("id");
        }else{
            $emps = Profile::whereIn('status',['active','suspend'])->get()->sortByDesc("id");
        }
        return view('app.pages.employee.index',[
            'employee' => $emps,
        ]);  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $role = Roles::where('status','1')->get();
        return view('app.pages.employee.new',[
            'roles' => $role
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'      => 'required|min:6|max:50|unique:users',
            'password'   => 'required|min:6|max:30|confirmed',
        ]);
        if ($validator->fails()) {    
            return redirect()->back()->withErrors($validator)->withInput();
        }
        
        $input = $request->all();
        // Start Database transaction
        DB::beginTransaction();
        try 
        {
            $user = Users::create([
                'email'     => strtolower($input['email']),
                'password'  => Hash::make($input['password']),
                'status'    => $input['status'],
                'role'      => $input['role']
            ]);

            $profile = Profile::create([
                'user_id'   => $user->id,
                'first_name'=> strtolower($input['first_name']),
                'last_name' => strtolower($input['last_name']),
                'mobile'    => $input['mobile'],
                'address'   => $input['address'],
                'city'      => $input['city'],
                'state'     => $input['state'],
                'country'   => $input['country'],
                'status'    => $input['status'],
                'join_date' => date('Y-m-d',strtotime($input['join_date'])),
            ]);
            DB::commit();
        }catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        return redirect('/account/app/employee')->with(['status' => 'Employee Added Successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $emp = Profile::with('user')->where('id',$id)->first();
        return view('app.pages.employee.view',[
            'employee' => $emp
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $emp = Profile::with('user')->where('user_id',$id)->first();
        return view('app.pages.employee.edit',[
            'employee' => $emp
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $input = $request->all();
        $role   = $input['role'];
		$status = $input['status'];
        Arr::forget($input, ['_token','_method','user_name','role']);
        $input['join_date'] = date('Y-m-d',strtotime($input['join_date']));
        // Start Database transaction
        DB::beginTransaction();
        try 
        {
            $employee = Profile::where('id',$id)->first();
            $employee->update($input);
            $role     = Users::find($employee->user_id)->update([
						'role'   => $role,
				        'status' => ($status =='active') ? 'active' : 'suspend',
						]);
            DB::commit();
        }catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        return redirect()->route('employee.index')->with(['status' => 'Employee Profile Updated Successfully']);
    }

    public function updatePassword(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'password'   => 'required|min:6|max:30|confirmed',
        ]);
        if ($validator->fails()) {    
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $input    = $request->all();
        $pass     = Hash::make($input['password']);
        $employee = Profile::where('id',$id)->first();
        $password = Users::find($employee->user_id)->update(['password' => $pass]);
        return redirect()->route('employee.index')->with(['status' => 'Password Updated Successfully']); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $emps  = Profile::where('id',$id)->first();
		$emps->update([
            'status' => 'TRASH',
            'deleted_at' => date('Y-m-d H:i:s', time()),
            'deleted_by' => Auth::user()->id
        ]);
		$usr  = Users::find($emps->user_id)->update([
			'status' => 'suspend'
		]);
        return "Trashed Successfully";
    }

    private function processRecord($model , $type = 'NEW')
    {
        // Start Database transaction
        DB::beginTransaction();
        try 
        {
            if($type == "NEW"){

            }elseif($type == "UPDATE"){

            }
        }
        catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }
}
