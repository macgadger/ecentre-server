<?php
namespace App\Controllers\App;

use Illuminate\Http\Request;
use App\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

use Request as urlRequest;
use Session,Cache;
use App\Models\Admissions;
use App\Models\Reviews;
use Carbon;

class ReviewsController extends Controller
{
    //
    public function index()
    {
        $reviews = Reviews::with(['admissions','admissions.getcourse','admissions.batch','admissions.batch.trainer'])->where('status', 1)->get();
        //return $reviews;
        return view('app.pages.review.index',[
            'reviews' => $reviews
        ]);
    }

    public function show($id)
    {
        $review = Reviews::with(['admissions','admissions.getcourse','admissions.batch','admissions.batch.trainer'])->find($id);
        //return $review->diary;
        $diary   = json_decode($review->diary);
        return view('app.pages.review.view',[
            'review' => $review,
            'diary'  => $diary,
        ]);
    }

}
