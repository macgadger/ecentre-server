<?php

namespace App\Controllers\App;

use Illuminate\Http\Request;
use App\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Arr;
use Auth,Hash;
use App\Models\BatchCategory;

class BatchCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $batch = BatchCategory::all()->sortByDesc("id");
        return view('app.pages.batch.category.index',[
            'batch' => $batch,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('app.pages.batch.category.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input  = $request->all();
        $course = BatchCategory::create($input);
        return redirect()->route('batch-category.index')->with(['status' => 'Batch Category created Successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $crse = Course::find($id);
        return view('app.pages.course.view',[
            'course' => $crse
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $crse = Course::find($id);
        return view('app.pages.course.edit',[
            'course' => $crse
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $input = $request->all();
        $crse = Course::find($id)->update($input);
        return redirect()->route('course.index')->with(['status' => 'Course Updated Successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $crse  = Course::find($id);
        $crse->update([
            'status'     => 2,
            'deleted_at' => date('Y-m-d H:i:s', time()),
            'deleted_by' => Auth::user()->id
        ]);
        return "Trashed Successfully";
    }

}
