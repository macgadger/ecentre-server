<?php

namespace App\Controllers\App;

use Illuminate\Http\Request;
use App\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Auth, DB, Hash;
use Carbon;
use App\Models\Users;
use App\Models\Admissions;
use App\Models\Attendance;
use App\Models\Batch;
use DateTime;
use Illuminate\Support\Arr;

class AttendanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $attn = Attendance::where('trainer_id',Auth::user()->profile->id)->where('status',1)->get();
        return view('app.pages.attendance.index',[
            'attendance' => $attn
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function indexToday($filter = 'today')
    {
        $batch = Batch::where('trainer_id',Auth::user()->profile->id)->get();
        if($filter == 'today'){
            //$adms  = Admissions::whereIn('batch_id',Arr::pluck($batch,'id'))->where('batch_assigned',1)->get();
            $adms = null;
        }else{
            $adms  = Admissions::where('batch_id',$filter)->where('batch_assigned',1)->get();
        }
        return view('app.pages.attendance.today',[
            'batch'    => $batch,
            'students' => $adms,
            'filter'   => $filter,
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function indexAdmin($filter = 'all')
    {
        $batch = Batch::where('status',1)->get();
        if($filter == 'all'){
            $adms  = Attendance::where('status',1)->get();
        }
        else{
            $adms  = Attendance::where('batch_id',$filter)->get();
        }
        //
        return view('app.pages.attendance.admin',[
            'batch'    => $batch,
            'students' => $adms,
            'filter'   => $filter,
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function indexMake($filter = null)
    {
        $batch   = Batch::where('status',1)->get();
        $trainer = Users::where('role',5)->where('status',1)->get();

        if($filter == null){
            $adms = null;
        }
        else{
            $adms = Admissions::where('batch_id',$filter)->where('batch_assigned',1)->get();
        }
        return view('app.pages.attendance.make',[
            'trainer'  => $trainer,
            'batch'    => $batch,
            'students' => $adms,
            'filter'   => $filter,
        ]);
    }

        /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeAdmin(Request $request)
    {
        $verify = Attendance::where('batch_id'  , $request->batch_id)
                            ->where('trainer_id', $request->trainer)
                            ->whereDate('created_at', date('Y-m-d',strtotime($request->attendance_date)))
                            ->get();

        if(count($verify) > 0){
            return redirect()->back()->with(['error' => 'Error: You or Trainer(s) have already marked Attendance on this Batch.'])->withInput();
        }

        $verify = Batch::where('id',$request->batch_id)->where('trainer_id', $request->trainer)->first();
        if(!$verify){
            return redirect()->back()->with(['error' => 'Error: Selected Trainer is not associated with current Selected Batch'])->withInput();
        }
        //
        $attn_date = DateTime::createFromFormat('Y-m-d H:i:s', date('Y-m-d',strtotime($request->attendance_date)).date('H:i:s'));
        $attn = new Attendance;
        $attn->trainer_id = $request->trainer;
        $attn->batch_id   = $request->batch_id;
        $attn->log        = json_encode($request->attendance_ids);
        $attn->remark     = $request->remarks;
        $attn->created_at = $attn_date->format('Y-m-d H:i:s');
        $attn->save();
        //
        return redirect()->route('attendance.admin',['filter' => 'all'])->with(['status' => 'Attendace created Successfully']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('app.pages.certificate.new');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $verify = Attendance::where('batch_id'  , $request->batch_id)
                            ->where('trainer_id', Auth::user()->profile->id)
                            ->whereDate('created_at', Carbon\Carbon::today())
                            ->get();
        //
        if(count($verify) > 0){
            return redirect()->back()->with(['error' => 'Error: You have already marked Attendance on this Batch.']);
        }
        $attn = new Attendance;
        $attn->trainer_id = Auth::user()->profile->id;
        $attn->batch_id   = $request->batch_id;
        $attn->log        = json_encode($request->attendance_ids);
        $attn->remark     = $request->remarks;
        $attn->save();
        //
        return redirect()->route('attendance.index')->with(['status' => 'Attendace created Successfully'])->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $attn = Attendance::find($id);
        foreach (json_decode($attn->log) as $key => $active)
        {
            $adm    = Admissions::where('id',$key)->first();
            $data[] = array(
                'present'=> $active,
                'aid'    => $adm->aid,
                'name'   => $adm->full_name,
                'course' => $adm->getCourse->name,
                'date'   => date('d M y @ H:i A',strtotime($attn->created_at))
            );
        }

        return view('app.pages.attendance.view',[
            'attn'   => $data,
            'remark' => $attn->remark,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $emp = Profile::with('user')->where('user_id',$id)->first();
        return view('app.pages.employee.edit',[
            'employee' => $emp
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $input = $request->all();

        // Start Database transaction
        DB::beginTransaction();
        try
        {
            DB::commit();
        }catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        return redirect()->route('employee.index')->with(['status' => 'Employee Profile Updated Successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $emps  = Profile::where('id',$id)->first();
		$emps->update([
            'status' => 'TRASH',
            'deleted_at' => date('Y-m-d H:i:s', time()),
            'deleted_by' => Auth::user()->id
        ]);
		$usr  = Users::find($emps->user_id)->update([
			'status' => 'suspend'
		]);
        return "Trashed Successfully";
    }

    public function getAttendance($id)
    {
        $admn = Admissions::find($id);
        $attn = Attendance::where('batch_id',$admn->batch_id)->get();
        //
        $present = 0;
        $absent  = 0;
        $holiday = 0;
        $leave   = 0;
        $days    = 0;
        $abCont  = 0;
        $attendance = array();

        foreach($attn as $index => $data)
        {
            foreach(json_decode($data->log) as $key => $item)
            {
                if($key == $id)
                {
                    $present = $present + (($item=="present") ? 1 : 0);
                    $absent  = $absent  + (($item=="absent")  ? 1 : 0);
                    $holiday = $holiday + (($item=="holiday") ? 1 : 0);
                    $leave   = $leave   + (($item=="leave")   ? 1 : 0);
                    $days    = $days+1;

                    $past    = Carbon\Carbon::now()->subDays(2)->format('Y-m-d');
                    $today   = Carbon\Carbon::today()->format('Y-m-d');
                    $anDate  = date('Y-m-d',strtotime($data->created_at));

                    if($item == "absent"){
                        $abCont = $abCont + (($anDate >= $past && $anDate < $today) ? 1 : 0);
                    }
                    //
                    $attendance['dairy'][] = array(
                        'student_id' => $key,
                        'date'       => date('Y-m-d H:i:s',strtotime($data->created_at)),
                        'attendance' => $item,
                        'subject'    => $data->remark,
                    );
                    //
                    $attendance['stats'] = array(
                        'present' => $present,
                        'absent'  => $absent,
                        'holiday' => $holiday,
                        'leave'   => $leave,
                        'days'    => $days,
                        'cont_absent'  => $abCont,
                    );
                }
            }
        }
        return $attendance;
    }

}
