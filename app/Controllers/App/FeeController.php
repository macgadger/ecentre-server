<?php

namespace App\Controllers\App;

use Illuminate\Http\Request;
use App\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Arr;
use Auth, DB, Hash;
use Endroid\QrCode\QrCode;
use Endroid\QrCode\ErrorCorrectionLevel;
use Carbon;
use App\Models\Users;
use App\Models\Profile;
use App\Models\Admissions;
use App\Models\Batch;
use App\Models\Fee;
use App\Models\Emi;
use App\Models\Course;
use App\Models\Payments;

class FeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admission = Admissions::with(['fees','payment'])->whereIn('status',['PENDING','JOINED'])->get()->sortByDesc("id");
        return view('app.pages.fee.index',[
            'admission' => $admission,
        ]);
    }

    // public function counsellorfeesIndex()
    // {
    //     $today  = Carbon\Carbon::now()->format('Y-m-d');
    //     $admission =  Admissions::with(['payment','fees' => function ($query) use($today) {
    //                                 $query->whereDate('due_date','<', $today)->where('due_amount','!=',0);
    //                             }])->admissions(Auth::user()->id)->get()->sortByDesc("id");
    //     return view('app.pages.fee.counsellorfees_index',[
    //         'admission' => $admission,
    //         'today'     => $today,
    //     ]);
    // }

    public function counsellorFeesIndex()
    {
        $today  = Carbon\Carbon::now()->format('Y-m-d');
        $admission =  Admissions::with(['payment','fees' => function ($query) use($today) {
                                    $query->whereDate('due_date','<', $today)->where('due_amount','!=',0);
                                }])->admissions(Auth::user()->id)->get()->sortByDesc("id");
        return view('app.pages.fee.counsellorfees_index',[
            'admission' => $admission,
            'today'     => $today,
        ]);
    }

	 /**
     * Display a listing of the resource using fiter.
     *
     * @return \Illuminate\Http\Response
     */
	public function filter($type)
    {
        $today  = Carbon\Carbon::now()->format('Y-m-d');
        if($type == 'today') {
            $adms   = Admissions::with(['payment','fees' => function ($query) use($today) {
                $query->where([
                    ['due_date','=', Carbon\Carbon::now()->subDays(0)->toDateString()],
                    ['due_amount', '!=', 0],
                ]);
            }])->get()->sortByDesc("id");
        }
        else if($type == 'due') {
            $adms   = Admissions::with(['payment','fees' => function ($query) use($today) {
                $query->whereDate('due_date','>', $today)->where('due_amount','!=',0);
            }])->get()->sortByDesc("id");
        }
        else if($type == 'overdue') {
            $adms   = Admissions::with(['payment','fees' => function ($query) use($today) {
                $query->whereDate('due_date','<', $today)->where('due_amount','!=',0);
            }])->get()->sortByDesc("id");
        }
        else if($type == 'collection') {
            $adms  = Payments::with('admission')->whereDate('created_at',$today)->get()->sortByDesc("id");
            $total = Payments::whereDate('created_at',$today)->sum('amount');
            return view('app.pages.fee.collection',[
                'admission' => $adms,
                'total'     => $total,
            ]);
        }
        //
        return view('app.pages.fee.index',[
            'admission' => $adms,
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function due()
    {
        $fee   = Fee::with('admission')->where('status',1)->get()->sortByDesc("id");
		$today = Carbon\Carbon::now()->format('Y-m-d');
        return view('app.pages.fee.due',[
            'fees' => $fee,
			'today' => $today
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('app.pages.fee.new');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function pay($id)
    {
        $this->generateQR();
        $admission = Admissions::with(['fees','payment'])->find($id);
        if($admission->payment->sum('amount') == $admission->fees->course_amount){
            return redirect()->back()->with(['status' => 'Fees Already Paid full']);
        }else{
            return view('app.pages.fee.new',[
                'admission' => $admission,
            ]);
        }
    }

    /**
     * Searc a resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        if ($request->isMethod('POST'))
        {
            $input     = $request->all();
            $admission = Admissions::where('first_name','LIKE','%'.$input['query'].'%')
                                ->orWhere('last_name','LIKE','%'.$input['query'].'%')
                                ->orWhere('aid',$input['query'])
                                ->get();
            $data = '';
            foreach($admission as $key => $item)
            {
                $data.= "<tr>";
                $data.= "<td>".$item->aid."</td>";
                $data.= "<td>".strtoupper($item->first_name.' '.$item->last_name)."</td>";
                $data.= "<td>".strtoupper($item->mobile)."</td>";
                $data.= "<td>".strtoupper($item->getCourse->name)."</td>";
                $data.= "<td>".date('d M,Y',strtotime($item->course_joindate))."</td>";
                $data.= "<td>".date('d M,Y',strtotime($item->course_enddate))."</td>";
                $data.= "<td><a class='btn btn-danger btn-flat big' href='../fee/pay/".$item->id."'><i class='fa fa-rupee'></i> Pay Fee</a></td>";
                $data.="</tr>";
            }
            return $data;
        }else{
            return view('app.pages.fee.search');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'amount'          => 'required',
            'date_due'        => 'required',
            'payment_reciept' => 'required',
            'payment_mode'    => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        //
        $input      = $request->all();
        $amount     = $input['amount'];
        $total      = $input['total'];
        $amount_due = $input['due']-$input['amount'];

        if($amount > $input['due']){
            return redirect()->back()->withErrors([
                'Amount should not be greater than Due Amount'
            ])->withInput();
        }

        DB::beginTransaction();
        try{
            $payment   = Payments::create([
                'student_id'    => $input['sid'],
                'admission_id'  => $input['aid'],
                'cousellor_id'  => $input['cousellor_id'],
                'reciept_no'    => $input['payment_reciept'],
                'amount'        => $input['amount'],
                'last_due'      => $amount_due,
                'payment_mode'  => $input['payment_mode'],
                'type'          => 'FEE',
                'remarks'       => (isset($input['remarks'])) ? $input['remarks'] : '',
                'status'        => 1
            ]);

            $fee  = Fee::where('admission_id',$input['aid'])->first();
            $fee->update([
                'due_amount'    => $amount_due,
                'due_date'      => date('Y-m-d',strtotime($input['date_due'])),
                'last_payment'  => $input['amount'],
            ]);
            // Commit the queries
            DB::commit();
        }catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        //
        return redirect('/account/app/fee')->with(['status' => 'Fee Paid Successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $fee = Fee::where('id',$id)->first();
        return view('app.pages.fee.view',[
            'fees' => $fee
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $fee = Fee::find($id);
        return view('app.pages.fee.edit',[
            'fees' => $fee
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function generateQR()
    {
        $merchant['pn']  = 'Manpreet Singh';
        $merchant['pa']  = '7696200218@paytm';
        $merchant['am']  = '';
        $merchant['tid'] = '';
        $merchant['tr']  = '';
        $merchant['tn']  = '';
        $merchant['url'] = '';//'http://3.18.222.53/account/app/feepay';

        $upi_qr = 'upi://pay?'.http_build_query($merchant);
        $qrpath = public_path('uploads/qrcode/qr.png');
        //
        $qrCode = new QrCode();
        $qrCode->setText($upi_qr);
        $qrCode->setSize(300);
        $qrCode->setMargin(10);
        $qrCode->setErrorCorrectionLevel(new ErrorCorrectionLevel(ErrorCorrectionLevel::HIGH));
        $qrCode->writeFile($qrpath);
        return true;
    }

}
