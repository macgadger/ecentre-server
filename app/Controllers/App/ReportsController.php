<?php

namespace App\Controllers\App;

use Illuminate\Http\Request;
use App\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Auth, DB, Hash;
use App\Models\Users;
use App\Models\Admissions;


class ReportsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $staff = Users::with('profile')->where('role','3')->where('status','active')->get();
        return view('app.pages.reports.index',[
            'staff' => $staff
        ]);
    }

    /**
     * Display a listing of the resource using fiter.
     *
     * @return \Illuminate\Http\Response
     */
	public function filter(Request $request)
    {
        //
        if($request->isMethod('GET'))
        {
            $staff = Users::with('profile')->where('role','3')->where('status','active')->get();
            return view('app.pages.reports.index',[
                'staff' => $staff
            ]);
        }
        else{
            $from = date('Y-m-d',strtotime($request->date_from));
            $to   = date('Y-m-d',strtotime($request->date_to));
            
            if($request->search_type == 'admission' && $request->couselor != 'all')
            {
                $data = Admissions::whereBetween(DB::raw('date(created_at)'), [$from, $to])
                        ->where('admission_by',$request->couselor)
                        ->get()->sortBy("id");
            }elseif($request->search_type == 'fee' && $request->couselor != 'all'){
                $data = Admissions::with(['fees','payment'])
                        ->whereBetween(DB::raw('date(created_at)'), [$from, $to])
                        ->where('admission_by',$request->couselor)
                        ->get()->sortBy("id");
            }else
            {
                if($request->search_type == 'admission'){
                    $data = Admissions::whereBetween(DB::raw('date(created_at)'), [$from, $to])->get()->sortBy("id");
                }else{
                    $data = Admissions::with(['fees','payment'])->whereBetween(DB::raw('date(created_at)'), [$from, $to])->get()->sortBy("id");
                }
            }

            return redirect()->back()->with([
                'type'  => $request->search_type,
                'data'  => $data,
            ])->withInput();
    
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('app.pages.certificate.new');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'      => 'required|min:6|max:50|unique:users',
            'password'   => 'required|min:6|max:30|confirmed',
        ]);
        if ($validator->fails()) {    
            return redirect()->back()->withErrors($validator)->withInput();
        }
        
        $input = $request->all();
        // Start Database transaction
        DB::beginTransaction();
        try 
        {
            DB::commit();
        }catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        return redirect('/account/app/certificate')->with(['status' => 'Certificate Created Successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $emp = Profile::with('user')->where('id',$id)->first();
        // return view('app.pages.employee.view',[
        //     'employee' => $emp
        // ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $emp = Profile::with('user')->where('user_id',$id)->first();
        return view('app.pages.employee.edit',[
            'employee' => $emp
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $input = $request->all();
        
        // Start Database transaction
        DB::beginTransaction();
        try 
        {
            DB::commit();
        }catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        return redirect()->route('employee.index')->with(['status' => 'Employee Profile Updated Successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $emps  = Profile::where('id',$id)->first();
		$emps->update([
            'status' => 'TRASH',
            'deleted_at' => date('Y-m-d H:i:s', time()),
            'deleted_by' => Auth::user()->id
        ]);
		$usr  = Users::find($emps->user_id)->update([
			'status' => 'suspend'
		]);
        return "Trashed Successfully";
    }

}