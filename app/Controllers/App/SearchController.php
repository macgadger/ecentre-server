<?php

namespace App\Controllers\App;

use App\Http\Requests;
use App\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Students;

class SearchController extends Controller
{
    
    public function filter(Request $request)
    {
        // 
        return $request->all();
        if($request->search_type == 'admissions')
        {
            if ($request->has('query')) {
                return Students::where('first_name' , $request->input('query'))
                               ->orWhere('last_name', $request->input('query'))
                               ->orWhere('mobile', $request->input('query'))
                               ->orWhere('email', $request->input('query'))
                               ->orWhere('course_name', $request->input('query'))
                               ->get();
            }
        }
    }


}