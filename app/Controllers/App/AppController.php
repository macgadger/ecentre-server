<?php

namespace App\Controllers\App;

use App\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Fee;
use App\Models\Payments;
use App\Models\Users;
use App\Models\Batch;
use App\Models\Attendance;
use App\Models\Admissions;
use Auth;
use Carbon;
use DB, Debugbar;
use Illuminate\Support\Arr;
use App\Models\Reviews;

class AppController extends Controller
{
    public function index($filter = 'today')
    {
        //
        $today = Carbon\Carbon::now();

        if (Auth::user()->role == 1) {
            return $this->adminReport($filter);
        }
        elseif(Auth::user()->role == 3) {
            return $this->counselorReport($filter);
        }
        elseif(Auth::user()->role == 5){
            return $this->trainerReport($filter);
        }
    }

    private function trainerReport($filter)
    {
        // $batch    = Batch::with('admissions')->where('trainer_id',Auth::user()->profile->id)->first();
        // $attn     = Attendance::with(['trainer','batch'])->where('trainer_id',Auth::user()->profile->id)->get();
        // $review   = Reviews::select('student_id',
        //                     DB::raw('count(trainer_id ='.Auth::user()->profile->id.') as total_review'),
        //                     DB::raw('sum(overall_rating)  as total_rating')
        //                 )->first();

        // $overall_rating = ($review->total_review != 0) ? $review->total_rating/$review->total_review : 0;

        // $batch = Batch::with('admissions')->where('trainer_id',Auth::user()->profile->id)->first();
        // $attn  = Attendance::where('trainer_id',Auth::user()->profile->id)->get();
        // $stAtn = Arr::pluck($batch['admissions'],'id');
        // foreach($stAtn as $sid){
        //     $atn[] = $this->getAttendance($sid);
        // }

        // return collect($atn);
        //
        // $batch    = Batch::with('admissions')->where('trainer_id',Auth::user()->profile->id)->first();
        // $attn     = Attendance::with(['trainer','batch'])->where('trainer_id',Auth::user()->profile->id)->get();
        // $review   = Reviews::select('student_id',
        //                     DB::raw('count(trainer_id ='.Auth::user()->profile->id.') as total_review'),
        //                     DB::raw('sum(overall_rating)  as total_rating')
        //                 )->first();
        // $overall_rating = $review->total_rating/$review->total_review;

        // return view('app.pages.dashboard', [
        //     'batch'          => $batch,
        //     'batches'        => Batch::where('trainer_id',Auth::user()->profile->id)->count(),
        //     'attendance'     => $attn,
        //     'overall_rating' => $overall_rating,
        // ]);

        $batch = Batch::with('admissions')->where('trainer_id',Auth::user()->profile->id)->first();
        $attn  = Attendance::with(['trainer','batch'])->where('trainer_id',Auth::user()->profile->id)->get();

        return view('app.pages.dashboard', [
            'batch'      => $batch,
            'batches'    => Batch::where('trainer_id',Auth::user()->profile->id)->count(),
            'attendance' => $attn,
        ]);
    }

    private function getAttendance($id)
    {
        $admn = Admissions::find($id);
        $attn = Attendance::where('batch_id',$admn->batch_id)->get();
        //
        $present = 0;
        $absent  = 0;
        $holiday = 0;
        $leave   = 0;
        $days    = 0;
        $abCont  = 0;
        $attendance = array();

        foreach($attn as $data)
        {
            foreach(json_decode($data->log) as $key => $item)
            {
                if($key == $id)
                {
                    $present = $present + (($item=="present") ? 1 : 0);
                    $absent  = $absent  + (($item=="absent")  ? 1 : 0);
                    $holiday = $holiday + (($item=="holiday") ? 1 : 0);
                    $leave   = $leave   + (($item=="leave")   ? 1 : 0);
                    $days    = $days+1;

                    $past    = Carbon\Carbon::now()->subDays(2)->format('Y-m-d');
                    $today   = Carbon\Carbon::today()->format('Y-m-d');
                    $anDate  = date('Y-m-d',strtotime($data->created_at));

                    if($item == "absent"){
                        $abCont = $abCont + (($anDate >= $past && $anDate < $today) ? 1 : 0);
                    }
                    //
                    $attendance['dairy'][] = array(
                        'student_id' => $key,
                        'date'       => date('Y-m-d H:i:s',strtotime($data->created_at)),
                        'attendance' => $item,
                        'subject'    => $data->remark,
                    );
                    //
                    $attendance['stats'] = array(
                        'present' => $present,
                        'absent'  => $absent,
                        'holiday' => $holiday,
                        'leave'   => $leave,
                        'days'    => $days,
                        'cont_absent'  => $abCont,
                    );
                }
            }
        }
        return $attendance;
    }

    private function adminReport($filter)
    {
        $today = Carbon\Carbon::now()->format('Y-m-d');

        if(empty($filter) || $filter == 'today'){
            $filterDays = 0;
        }elseif($filter == 'weekly'){
            $filterDays = 7;
        }elseif($filter == 'monthly'){
            $filterDays = 30;
        }else{
            $filterDays = 90;
        }

		//Debugbar::info(Carbon\Carbon::now()->subDays($filterDays)->toDateString());
        //
        $users = Users::select('id')->where('role', 3)->get()->toArray();
        $usr   = Arr::flatten($users);

        //Cousellor Metrics by Filter
        $counselor_adm = Admissions::with('profile')->whereIn('admission_by', $usr)
                        ->where('created_at','>=', Carbon\Carbon::now()->subDays($filterDays)->toDateString())
                        ->select('admission_by',
                            DB::raw('(count(status = "JOINED") & count(status="PENDING")) as adm_total'),
                            DB::raw('sum(status = "JOINED")  as adm_joined'),
                            DB::raw('sum(status = "PENDING") as adm_pending'),
                            DB::raw('sum(status = "LEAVE") as adm_leave')
                        )->groupBy('admission_by')->get();


         $counselor_fee = Fee::whereIn('cousellor_id',$usr)
			 			->with(['profile','payments' => function($query) use($filterDays){
			 				$query->where('created_at','>=', Carbon\Carbon::now()->subDays($filterDays)->toDateString());
		 				}])
                        ->where('created_at','>=', Carbon\Carbon::now()->subDays($filterDays)->toDateString())
			 			->select('cousellor_id',
                           DB::raw('sum(course_amount) AS fee_total')
                        )->groupBy('cousellor_id')->get();

        $adm_pending = Admissions::whereDate('course_joindate', '>=', $today)->where([
            ['status', '=', 'PENDING'],
            //['batch_assigned', '=', '0'],
        ])->count();

        $adm_expired = Admissions::whereDate('course_joindate', '<', $today)->where([
            ['status', '=', 'PENDING'],
            ['status', '!=', 'DROPOUT'],
            ['batch_assigned', '=', '0'],
        ])->count();

        $students    = Admissions::where('status', 'JOINED')->count();
        $fee_collect = Payments::whereDate('created_at', $today)->sum('amount');

        $fee_dues    = Fee::where([
            ['due_date','=', Carbon\Carbon::now()->subDays(0)->toDateString()],
            ['due_amount', '!=', 0],
        ])->count();

        $fee_due_sum = Fee::where([
            ['due_date','=', Carbon\Carbon::now()->subDays(0)->toDateString()],
            ['due_amount', '!=', 0],
        ])->sum('due_amount');

        $fee_expired     = Fee::whereDate('due_date', '<', $today)->where('due_amount','!=',0)->count();
        $admisn = Admissions::where('status','DROPOUT')->get();
        $fee_expired_sum = Fee::whereNotIn('student_id',Arr::pluck($admisn,'id'))->whereDate('due_date', '<', $today)->sum('due_amount');
        return view('app.pages.dashboard', [
            'today' => $today,
            'adm_pending' => $adm_pending,
            'adm_expired' => $adm_expired,
            'students' => $students,
            'fee_collect' => $fee_collect,
            'fee_dues' => $fee_dues,
            'fee_due_sum' => $fee_due_sum,
            'fee_expired' => $fee_expired,
            'fee_expired_sum' => $fee_expired_sum,
            'counselor_adm' => $counselor_adm,
            'counselor_fee' => $counselor_fee,
        ]);
    }

    private function counselorReport()
    {
        $today = Carbon\Carbon::now()->format('Y-m-d');
        $month = Carbon\Carbon::now()->format('m');

        $counselor_adm = Admissions::where('admission_by', Auth::user()->id)->whereMonth('created_at',$month)->select('admission_by',
            DB::raw('(count(status = "JOINED") & count(status="PENDING")) as adm_total'),
            DB::raw('sum(status = "JOINED")  as adm_joined'),
            DB::raw('sum(status = "PENDING") as adm_pending'),
            DB::raw('sum(status = "LEAVE") as adm_leave')
        )->groupBy('admission_by')->get();

        $admission     = Admissions::admissions(Auth::user()->id)->where('status','!=', 'DROPOUT')->get();
        $counselor_fee = Fee::whereIn('admission_id',Arr::pluck($admission,'admission_uid'))
                              ->whereDate('due_date','<',$today)
                              ->sum('due_amount');

        $adm_expired = Admissions::where('admission_by', Auth::user()->id)
                                ->whereDate('course_joindate', '<', $today)
                                ->where([
                                        ['status', '=', 'PENDING'],
                                        //['batch_assigned', '=', '0'],
                                ])->count();

        $adm_pending = Admissions::where('admission_by', Auth::user()->id)
                                ->whereDate('course_joindate', '>', $today)
                                ->where([
                                        ['status', '=', 'PENDING'],
                                        //['batch_assigned', '=', '0'],
                                ])->count();

        $adm_drop    = Admissions::where('admission_by', Auth::user()->id)
                                ->whereDate('course_joindate', '<', $today)
                                ->where([
                                        ['status', '=', 'DROPOUT'],
                                        //['batch_assigned', '=', '0'],
                                ])->count();

        $adm_joined    = Admissions::where('admission_by', Auth::user()->id)
                                ->whereDate('course_joindate', '<', $today)
                                ->where([
                                        ['status', '=', 'JOINED'],
                                        //['batch_assigned', '=', '0'],
                                ])->count();

        $adm_pending = Admissions::where('admission_by', Auth::user()->id)
                                ->whereDate('course_joindate', '>', $today)
                                ->where([
                                        ['status', '=', 'PENDING'],
                                        ['batch_assigned', '=', '0'],
                                ])->count();

        $adm_drop    = Admissions::where('admission_by', Auth::user()->id)
                                ->whereDate('course_joindate', '<', $today)
                                ->where([
                                        ['status', '=', 'DROPOUT'],
                                        ['batch_assigned', '=', '0'],
                                ])->count();

        $adm_joined    = Admissions::where('admission_by', Auth::user()->id)
                                ->whereDate('course_joindate', '<', $today)
                                ->where([
                                        ['status', '=', 'JOINED'],
                                        ['batch_assigned', '=', '0'],
                                ])->count();
        //
        return view('app.pages.dashboard', [
            'counselor_adm' => $counselor_adm,
            'counselor_fee' => $counselor_fee,
            'adm_expired'   => $adm_expired,
            'adm_drop'      => $adm_drop,
            'adm_joined'    => $adm_joined,
            'adm_pending'   => $adm_pending
        ]);
    }

}
