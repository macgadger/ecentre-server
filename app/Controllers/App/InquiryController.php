<?php

namespace App\Controllers\App;

use Illuminate\Http\Request;
use App\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Arr;
use Auth,Hash;
use App\Models\Inquiry;
use App\Models\Students;

class InquiryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $inq = Inquiry::all()->sortByDesc("id");
        return view('app.pages.inquiry.index',[
            'inquiry' => $inq,
        ]);  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('app.pages.inquiry.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input   =  $request->all();
        //
        $input['enquiry_by']       = Auth::user()->id;
        $input['dob']              = date('Y-m-d',strtotime($input['dob']));
        $input['course_join_date'] = date('Y-m-d',strtotime($input['course_join_date']));
        $inquiry = Inquiry::create($input);
        //
        return redirect('/account/app/inquiry')->with(['status' => 'Inquiry Added Successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $inq = Inquiry::where('id',$id)->first();
        return view('app.pages.inq.view',[
            'inqs' => $inq
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $inq = Inquiry::find($id);
        return view('app.pages.inquiry.edit',[
            'inqs' => $inq
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}