<?php

namespace App\Controllers\App;

use Illuminate\Http\Request;
use App\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Arr;
use Auth, DB, Hash;
use Carbon;
use App\Models\Users;
use App\Models\Profile;
use App\Models\Admissions;
use App\Models\Batch;
use App\Models\Fee;
use App\Models\Payments;
use App\Models\Emi;
use App\Models\Course;

class AdmissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $adms   = Admissions::with(['fees','payment'])->admissions(Auth::user()->id)->get()->sortByDesc("id");
        $today  = Carbon\Carbon::now()->format('Y-m-d');
        //
        return view('app.pages.admission.index',[
            'admissions' => $adms,
            'today'      => $today,
        ]);
    }

	 /**
     * Display a listing of the resource using fiter.
     *
     * @return \Illuminate\Http\Response
     */
	public function filter($type)
    {
        $today  = Carbon\Carbon::now()->format('Y-m-d');
        $month  = Carbon\Carbon::now()->format('m');

		if($type == 'today') {
            $adms = Admissions::admissions(Auth::user()->id)->whereDate('course_joindate','=',$today)->get();
        }
        else if($type == 'pending') {
            $adms = Admissions::admissions(Auth::user()->id)->whereDate('course_joindate','>=',$today)->where([
                ['status','=','PENDING'],
                ['batch_assigned','=','0'],
            ])->get();
        }
        else if($type == 'expired') {
            $adms = Admissions::admissions(Auth::user()->id)->whereDate('course_joindate','<',$today)->where([
                ['status','=','PENDING'],
                ['batch_assigned','=','0'],
            ])->get();
        }
        else if($type == 'dropout') {
            $adms = Admissions::admissions(Auth::user()->id)->where('status','DROPOUT')->get();
        }
        else if($type == 'completed') {
            $adms = Admissions::admissions(Auth::user()->id)->where('status','COMPLETED')->get();
        }
        else if($type == 'attnblock') {
            $adms = Admissions::admissions(Auth::user()->id)->where('status','ATTNBLOCK')->get();
        }
        else if($type == 'joined') {
            $adms = Admissions::admissions(Auth::user()->id)->where('status','JOINED')->get();
        }
        else if($type == 'month' && Auth::user()->role == 3)
        {
            $adms = Admissions::admissions(Auth::user()->id)->whereMonth('created_at',$month)->get();
        }
        return view('app.pages.admission.index',[
            'admissions' => $adms,
            'today'      => $today,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $crse  = Course::where('status','1')->get();
        $aid   = Admissions::latest('id')->select('aid')->first();
        $staff = Users::where('role','3')->where('status','active')->get();

        return view('app.pages.admission.new',[
            'courses'  => $crse,
            'aid'      => ($aid) ? $aid->aid+1 : 1001,
			'staff'    => $staff
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'mobile'     => 'required|min:10|max:10|unique:admission',
            'email'      => 'required|min:6|max:30|unique:admission',
            'course_fee' => 'required',
            'course_advance' => 'required',
            'fee_due'        => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $input = $request->all();
        $this->writeData($input);
        //
        return redirect('/account/app/admission')->with(['status' => 'Admission Created Successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $adms = Admissions::find($id);
        return view('app.pages.admission.view',[
            'admission' => $adms
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $adms  = Admissions::with('fees')->find($id);
        $crse  = Course::where('status','1')->get();
        $staff = Users::where('role','3')->get();

        return view('app.pages.admission.edit',[
            'admission' => $adms,
            'courses'  => $crse,
			'staff'    => $staff
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $input = $request->all();
        $input['dob']             = date('Y-m-d',strtotime($input['dob']));
        $input['course_joindate'] = date('Y-m-d',strtotime($input['course_joindate']));
        $input['course_enddate']  = date('Y-m-d',strtotime($input['course_enddate']));

        if(isset($input['other_course'])){
            $input['other_course'] = implode(',',$input['other_course']);
        }

        $admission = Admissions::find($id);
        $admission->update($input);
        $fee_vouch = Fee::where('admission_id',$admission->admission_uid);
        $total     = Payments::where('admission_id',$admission->admission_uid)->sum('amount');
        $due       = $input['course_fee']-$total;
        $fee_vouch->update([
            'course_amount' => $input['course_fee'],
            'due_amount'    => $due
        ]);
        return redirect()->route('admission.index')->with(['status' => 'Admission Updated Successfully']);
    }

    /**
     * Update the status resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function status(Request $request, $id)
    {
        if ($request->isMethod('POST'))
        {
            $input     = $request->all();
            $admission = Admissions::find($id);
            $today     = Carbon\Carbon::now()->format('Y-m-d');
            $join_date = date('Y-m-d',strtotime($admission->join_date));
            $admission->update([
                'course_joindate' => ($input['status'] == 'JOINED') ?  $today : $join_date,
                'status'    => $input['status'],
                'remarks'   => $input['remarks'],
            ]);
            return redirect()->route('admission.index')->with(['status' => 'Status Updated Successfully']);
        }else{
            $status = Admissions::find($id);
            return view('app.pages.admission.status',['data' => $status]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $adms  = Admissions::where('id',$id)->update([
            'status' => 'TRASH',
            'deleted_at' => date('Y-m-d H:i:s', time()),
            'deleted_by' => Auth::user()->id
        ]);
        return "Trashed Successfully";
    }

    private function writeData($input)
    {
           // Start Database transaction
           DB::beginTransaction();
           try
           {
            $studentData = $input;
            Arr::forget($studentData, ['emi_duedate','emi_amount','fee_due','nextfee_date']);
            $studentData['dob']             = date('Y-m-d',strtotime($input['dob']));
            $studentData['course_joindate'] = date('Y-m-d',strtotime($input['course_joindate']));
            $studentData['course_enddate']  = date('Y-m-d',strtotime($input['course_enddate']));
            /*if(isset($studentData['other_course'])){
                $studentData['other_course'] = implode(',',$studentData['other_course']);
            }*/

            $admission = Admissions::create($studentData);
            $payment   = Payments::create([
                'student_id'    => $admission->id,
                'admission_id'  => $admission->admission_uid,
                'cousellor_id'  => $input['admission_by'],
                'reciept_no'    => $input['payment_reciept'],
                'amount'        => $input['course_advance'],
                'last_due'      => $input['fee_due'],
                'payment_mode'  => $input['payment_mode'],
                'type'          => 'ADVANCE',
                'remarks'       => (isset($input['remarks'])) ? $input['remarks'] : '',
                'status'        => 1
            ]);

            $fee  = Fee::create([
                'student_id'    => $admission->id,
                'admission_id'  => $admission->admission_uid,
                'cousellor_id'  => $input['admission_by'],
                'course_amount' => $input['course_fee'],
                'due_amount'    => $input['fee_due'],
                'last_payment'  => $input['course_advance'],
                'due_date'      => date('Y-m-d',strtotime($input['nextfee_date'])),
                'status'        => 1,
            ]);

            // Commit the queries
            DB::commit();
         }
         catch (\Exception $e) {
            DB::rollback();
            throw $e;
         }
    }
}
