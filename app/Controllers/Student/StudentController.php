<?php
namespace App\Controllers\Student;

use Illuminate\Http\Request;
use App\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

use Request as urlRequest;
use Session,Cache;
use App\Models\Admissions;
use App\Models\Reviews;
use Carbon;
use App\Models\Attendance;
use App\Models\CertificateRequest;

class StudentController extends Controller
{
    //
    public function index(){
        return redirect()->intended('student/signin');        
    } 

    /**
    * Authenticate user and Logged in
    */
    public function authenticate(Request $request)
    {
         $admission = Admissions::with(['batch', 'batch.trainer'])->where('aid', $request->aid)->first();
         session()->put('profile', $admission);
         return redirect()->intended('student/dashboard');  
    }


    /**
     * Open User signin form
     */
    public function signin(){
       
        return view('app.pages.student.login');          
    }

    /**
    * Signout current signed in user
    */
    public function signout(){
        //Auth::logout();
        Session::flush();
        //Cache::flush();           
        return redirect(urlRequest::root().'/student/signin');        
    }    

    public function dashboard()
    {
        $admission = session()->get('profile');
        $certificate  = CertificateRequest::where('aid', $admission->aid)->first();
        $attendence = $this->getAttendance($admission->id);
        //return $attendence;
        return view('app.pages.dashboard',[
            'admission'   => $admission,
            'attendence'  => $attendence,
            'certificate' => $certificate,
        ]);
    }

    public function reviewTrainer(Request $request)
    {
        if($request->isMethod('post'))
        {
            $student = session::get('profile');
            $diary   = json_encode($request->all());
            $trainer = $student->batch->trainer->id;
            $overall_rating = ($request->a2it_rating + $request->tr_sub_rate + $request->tr_pres_rate + $request->tr_time_rate + $request->tr_enc_rate + $request->pro_sat_rate + $request->cor_staf_rate+ $request->cor_lab_rate)/8;
            $review = Reviews::create([
                'student_id'     => $student->aid,
                'trainer_id'     => $trainer,
                'diary'          => $diary,
                'overall_rating' => $overall_rating
            ]);
            return redirect('/student/dashboard')->with(['status' => 'Thanks For Your Review' ]);
            
        }
        $admission = session::get('profile');
        $today = Carbon\Carbon::now()->subDays(30)->format('Y-m-d');
        $review  = Reviews::where('student_id', $admission->aid)->whereDate('created_at','>',$today)->latest()->first();
        return view('app.pages.student.review',[
            'admission' => $admission,
            'review'    => $review
        ]);
    }

    private function getAttendance($id)
    {
        $admn = Admissions::find($id);
        $attn = Attendance::where('batch_id',$admn->batch_id)->get();
        //
        $present = 0;
        $absent  = 0;
        $holiday = 0;
        $leave   = 0;
        $days    = 0;
        $abCont  = 0;
        $attendance = array();

        foreach($attn as $index => $data)
        {
            foreach(json_decode($data->log) as $key => $item)
            {
                if($key == $id)
                {
                    $present = $present + (($item=="present") ? 1 : 0);
                    $absent  = $absent  + (($item=="absent")  ? 1 : 0);
                    $holiday = $holiday + (($item=="holiday") ? 1 : 0);
                    $leave   = $leave   + (($item=="leave")   ? 1 : 0);
                    $days    = $days+1;

                    $past    = Carbon\Carbon::now()->subDays(2)->format('Y-m-d');
                    $today   = Carbon\Carbon::today()->format('Y-m-d');
                    $anDate  = date('Y-m-d',strtotime($data->created_at));

                    if($item == "absent"){
                        $abCont = $abCont + (($anDate >= $past && $anDate < $today) ? 1 : 0);
                    }
                    //
                    $attendance['dairy'][] = array(
                        'student_id' => $key,
                        'date'       => date('Y-m-d H:i:s',strtotime($data->created_at)),
                        'attendance' => $item,
                        'subject'    => $data->remark,
                    );
                    //
                    $attendance['stats'] = array(
                        'present' => $present,
                        'absent'  => $absent,
                        'holiday' => $holiday,
                        'leave'   => $leave,
                        'days'    => $days,
                        'cont_absent'  => $abCont,
                    );
                }
            }
        }
        return $attendance;
    }

    public function applyCertificate()
    {
        $admission = session()->get('profile');
        $certificate = CertificateRequest::create([
            'aid' => $admission->aid,
        ]);
        return response()->json(['status' => TRUE]);
    }

}
