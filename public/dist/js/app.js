function init(title) {
    //
    $('.sidebar-menu').tree();
    //
    $("#msg-alert").fadeTo(2000, 500).slideUp(500, function () {
        $("#msg-alert").slideUp(1000);
    });
    //
    $(document).on('click','.openBtn', function () {
        $('#loader').show();
        var dataURL = $(this).attr('data-href');
		//console.log(dataURL);
        $('.modal-body').load(dataURL, function () {
            $('#mod-title').html(title);
            $('#openModal').modal({
				backdrop: 'static',
    			keyboard: false, 
                show: true
            });
            $('#loader').hide();
        });
    });
}

function admission_init() 
{
    //Initialize Select2 Elements
    $('.select2').select2();

    //Date picker
    $('.datepicker').datepicker({
        autoclose: true,
        format: "dd-mm-yyyy",
        todayHighlight: true,
        todayBtn: true,
        clearBtn: true,
    });

    //Timepicker
    $('.timepicker').timepicker({
        showInputs: true
    });

    $("#addrow").on("click", function () {
        var newRow = $("<tr>");
        var cols = "";
        cols += '<td><div class="form-group has-feedback"><input type="text" class="form-control datepicker_start" name="emi_duedate[]" autocomplete="emi_duedate[]"  placeholder="Enter EMI Pay Date" required /><div class="help-block with-errors"></div></div></td>';
        cols += '<td><div class="form-group has-feedback"><input type="text" class="form-control" name="emi_amount[]" autocomplete="emi_amount[]"  placeholder="Enter EMI Amount" required /><div class="help-block with-errors"></div></div></td>';
        cols += '<td><input type="button" class="ibtnDel btn btn-md btn-danger" value="x"></td>';
        newRow.append(cols);
        $("table.order-list").append(newRow);
    });

    $('body').on('focus', ".datepicker_start", function () {
        $(this).datepicker({
            autoclose: true,
            format: "dd-mm-yyyy",
            todayHighlight: true
        });
    });

    $("table.order-list").on("click", ".ibtnDel", function (event) {
        $(this).closest("tr").remove();
        //counter -= 1
    });

}

function calc_total(){
    var fees  = parseInt($("input[name=course_fee]").val());
    var adv   = parseInt($("input[name=course_advance]").val());    
    if(fees!='NaN'){
        $('#cfee').html(fees+'/-');
        $('#ctotal').html(fees+'/-');
    }
    if(fees!='NaN' && adv!='NaN'){
        $('#cadv').html(adv+'/-');
        
        var due = fees-adv;
		$('#fee_due').val(due);
        $('#cdue').html(due+'/-');
    }
}


function mode_select(val) {
    if (val === 'emi') {
        $('#emi_due_init').prop('required', true);
        $('#emi_amount_init').prop('required', true);
        $('#emi_section').show(500);
    } else {
        $('#emi_section').hide();
    }
}

function inquiry_init(){
        //Initialize Select2 Elements
        $('.select2').select2();

        //Date picker
        $('.datepicker').datepicker({
            autoclose: true,
            format: "dd-mm-yyyy",
            todayHighlight: true,
            todayBtn: true,
            clearBtn: true,
        });

        //Timepicker
        $('.timepicker').timepicker({
            showInputs: true
        });
}

function deleteRecord(uri){
    //var result = confirm("Want to delete?");
    Swal.fire({
            title: 'Are you sure you want to Delete?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Delete',
            }).then((result) => {
            if (result.value) 
            {
                $.ajax({
                    url:   uri,
                    type: 'DELETE',
                    success: function(result) {
                        location.reload();
                    }
                });    
            }
        });
}

function applyCertificate(uri){
    Swal.fire({
            title: 'Are you sure you want to apply certificate?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Apply',
            }).then((result) => {
            if (result.value) 
            {
                $.ajax({
                    url:   uri,
                    type: 'GET',
                    success: function(result) {
                        location.reload();
                    }
                });    
            }
        });
}